﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Irms.Tests.Unit")]
[assembly: InternalsVisibleTo("Irms.Tests.Integration")]
namespace Irms.Infrastructure
{
    public interface IMarker
    {
    }
}
