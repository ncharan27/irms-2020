﻿using Autofac;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Infrastructure.Services;
using Irms.Infrastructure.Services.EmailReachability;
using Irms.Infrastructure.Services.Sendgrid;
using Irms.Infrastructure.Services.Twilio;
using System.Net;

namespace Irms.Infrastructure
{
    public class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var cfg = AppConfig.Get(new string[0]);

            bool.TryParse(cfg["Email:TestMode"], out var testModeEmail);
            if (testModeEmail)
            {
                builder.RegisterType<TestMessageSender>().As<IEmailSender>();
                builder.RegisterType<TestMessageBGSender>().As<IEmailBGSender>();
            }
            else
            {
                builder.RegisterType<MessageSender>().As<IEmailSender>();
                builder.RegisterType<MessageBGSender>().As<IEmailBGSender>();
                builder.RegisterType<SendgridEmailSender>().As<ISendgridEmailSender>();
                builder.RegisterType<SendgridEmailBGSender>().As<ISendgridEmailBGSender>();
            }

            bool.TryParse(cfg["Sms:TestMode"], out var testModeSms);
            if (testModeSms)
            {
                builder.RegisterType<TestMessageSender>().As<ISmsSender>();
                builder.RegisterType<TestMessageBGSender>().As<ISmsBGSender>();
            }
            else
            {
                builder.RegisterType<MessageSender>().As<ISmsSender>();
                builder.RegisterType<MessageBGSender>().As<ISmsBGSender>();

                //malath
                builder.RegisterType<MalathSmsSender>().As<IMalathSmsSender>();
                builder.RegisterType<MalathSmsBGSender>().As<IMalathSmsBGSender>();

                ////unifonic
                //builder.RegisterType<UnifonicSmsSender>().As<IUnifonicSmsSender>();
                //builder.RegisterType<UnifonicSmsBGSender>().As<IUnifonicSmsBGSender>();

                //twilio
                builder.RegisterType<TwilioSmsSender>().As<ITwilioSmsSender>();
                builder.RegisterType<TwilioSmsBGSender>().As<ITwilioSmsBGSender>();
                builder.RegisterType<TwillioWhatsappBGSender>().As<IWhatsappBGSender>();
            }

            builder.RegisterType<TwillioWhatsappSender>().As<IWhatsappSender>();

            builder.RegisterType<DomainParser>().As<IDomainParser>();
            builder.RegisterType<DNSRecordsManager>().As<IDNSRecordsManager>();
            builder.RegisterType<FileUploadService>().As<IFileUploadService>();
            builder.RegisterType<FileTypeValidator>().As<IFileTypeValidator>();
            builder.RegisterType<ImageResizer>().As<IImageResizer>();
            builder.RegisterType<PhoneNumberValidator>().AsImplementedInterfaces();
            builder.RegisterType<EmailReachabilityManager>().AsImplementedInterfaces();
            builder.RegisterType<WhatsappReachabilityManager>().AsImplementedInterfaces();
            builder.RegisterType<ReCaptchaValidator>().AsImplementedInterfaces();
            builder.RegisterType<WebRequest>().AsImplementedInterfaces();
            builder.RegisterType<ExcelGenerator>().As<IExcelGenerator>();
            builder.RegisterType<UniqueUrlGenerator>().As<IUniqueUrlGenerator>();
        }
    }
}
