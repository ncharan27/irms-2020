﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services.Sendgrid
{
    public interface ISendgridBGConfigurationProvider
    {
        Task<SendgridConfiguration> GetConfiguration(Guid tenantId, CancellationToken token);
    }
}
