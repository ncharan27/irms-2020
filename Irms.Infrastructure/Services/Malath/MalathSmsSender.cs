﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Events.Commands;
using Irms.Domain.Entities;
using Irms.Application;
using Newtonsoft.Json;
using MediatR;
using Irms.Domain;

namespace Irms.Infrastructure.Services
{
    /// <summary>
    /// Implementation of SMS sender by Malath API
    /// </summary>
    public class MalathSmsSender : IMalathSmsSender
    {
        private readonly Regex _notNumber = new Regex("[^0-9]+", RegexOptions.Compiled);
        private readonly Regex _isUnicode = new Regex("[^\x00-\x7F]+", RegexOptions.Compiled);
        private readonly Regex _responseCode = new Regex("^[0-9]{1,3}", RegexOptions.Compiled);

        private readonly IMalathConfigurationProvider _configurationProvider;
        private readonly ILogger<MalathSmsSender> _logger;
        private readonly HttpClient _client;
        private readonly TenantBasicInfo _tenant;
        private readonly IMediator _mediator;

        public MalathSmsSender(
            IMalathConfigurationProvider configurationProvider,
            ILogger<MalathSmsSender> logger,
            IHttpClientFactory httpFactory,
            TenantBasicInfo tenant,
            IMediator mediator)
        {
            _configurationProvider = configurationProvider;
            _logger = logger;
            _client = httpFactory.CreateClient();
            _client.BaseAddress = new Uri("http://sms.malath.net.sa");
            _tenant = tenant;
            _mediator = mediator;
        }

        public async Task<bool> SendSms(SmsMessage message, CancellationToken token)
        {
            var cfg = await _configurationProvider.GetConfiguration(token);
            if (!cfg.IsValid)
            {
                throw new IncorrectRequestException("Malath SMS is not configured");
            }

            var recipients = message.Recipients.Select(x => _notNumber.Replace(x.Phone, string.Empty));
            var text = FillPlaceholders(message);
            var isUnicode = _isUnicode.IsMatch(text);

            var values = new Dictionary<string, string>
            {
                { "username", cfg.Username },
                { "password", cfg.Password },
                { "mobile", string.Join(",", recipients) },
                { "unicode", isUnicode ? "U" : "E" },
                { "message", isUnicode ? Encode(text) : text },
                { "sender", cfg.SenderName }
            };

            var url = "httpSmsProvider.aspx?" + string.Join("&", values.Select(v => $"{v.Key}={v.Value}"));

            var result = await _client.GetAsync(url, token);
            var response = _responseCode.Match(await result.Content.ReadAsStringAsync()).Value;

            if (result.IsSuccessStatusCode && response.Equals("0"))
            {
                foreach (var recipient in message.Recipients)
                {
                    _logger.LogInformation("SMS to number {0} has been sent", recipient.Phone, message);
                }

                var cmd = new ProviderLogsCmd
                {
                    ProviderType = ProviderType.Malath,
                    TenantId = _tenant.Id,
                    Request = JsonConvert.SerializeObject(message),
                    RequestDate = DateTime.UtcNow,
                    Response = JsonConvert.SerializeObject(result),
                    ResponseDate = DateTime.UtcNow,
                    CampaignInvitationId = message.CampaignInvitationId
                };

                await _mediator.Send(cmd, token);
                return true;
            }
            else
            {
                _logger.LogError("SMS sending failed, Malath error: " + InterpretErrorCode(response));
                return false;
            }
        }

        private static string FillPlaceholders(SmsMessage message)
        {
            var messageText = message.MessageTemplate;
            foreach (var variable in message.Recipients.First().TemplateVariables)
            {
                messageText = messageText.Replace(variable.Name, variable.Value);
            }

            return messageText;
        }

        private static string Encode(string input)
        {
            //Encodes unicode into the hexadecimal with reverse bytes order in pairs
            var bytes = Encoding.Unicode.GetBytes(input);
            var builder = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i += 2)
            {
                builder.AppendFormat("{0:X2}", bytes[i + 1]);
                builder.AppendFormat("{0:X2}", bytes[i]);
            }

            return builder.ToString();
        }

        private static string InterpretErrorCode(string code)
        {
            switch (code)
            {
                case "101": return "Parameter is missing";
                case "104": return "Either user name or password are missing or your Account is on hold";
                case "105": return "Credit are not available";
                case "106": return "Wrong Unicode";
                case "107": return "Blocked sender name";
                case "108": return "Missing sender name";
                default: return "Unknown error";
            }
        }

        public Task<bool> SendWebhookSms(SmsMessage sms, CancellationToken token)
        {
            throw new NotImplementedException();
        }
    }
}
