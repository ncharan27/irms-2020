﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public interface IMalathBGConfigurationProvider
    {
        Task<MalathConfiguration> GetConfiguration(Guid tenantId, CancellationToken token);
    }
}