﻿using Irms.Application.Abstract.Services;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public class ImageResizer : IImageResizer
    {
        public Task ResizeImage(Stream inputStream, Stream outputStream, int size, int quality)
        {
            using (var image = new Bitmap(inputStream))
            {
                //int width, height;
                //if (image.Width > image.Height)
                //{
                //    width = size;
                //    height = Convert.ToInt32(image.Height * size / (double)image.Width);
                //}
                //else
                //{
                //    width = Convert.ToInt32(image.Width * size / (double)image.Height);
                //    height = size;
                //}

                //var resized = new Bitmap(width, height);
                //using (var graphics = Graphics.FromImage(resized))
                //{
                //    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                //    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //    graphics.CompositingMode = CompositingMode.SourceCopy;
                //    graphics.DrawImage(image, 0, 0, width, height);

                    var qualityParamId = Encoder.Quality;
                    var encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                    var codec = ImageCodecInfo.GetImageDecoders()
                        .FirstOrDefault(c => c.FormatID == ImageFormat.Jpeg.Guid);
                    image.Save(outputStream, codec, encoderParameters);
                //}
            }

            return Task.CompletedTask;
        }
    }
}
