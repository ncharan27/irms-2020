﻿using Irms.Application.Abstract.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Irms.Infrastructure.Services
{
    public class UniqueUrlGenerator : IUniqueUrlGenerator
    {
        private const string Alphabet = "23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_";
        private static readonly int Base = Alphabet.Length;
        private static readonly DateTime UnixOffsetBegin = new DateTime(1970, 1, 1);

        public string Generate()
        {
            var ticks = (DateTimeOffset.UtcNow - UnixOffsetBegin).Ticks;
            var sb = new StringBuilder();
            while (ticks > 0)
            {
                sb.Insert(0, Alphabet.ElementAt((int)(ticks % Base)));
                ticks = ticks / Base;
            }
            return sb.ToString();
        }
    }
}
