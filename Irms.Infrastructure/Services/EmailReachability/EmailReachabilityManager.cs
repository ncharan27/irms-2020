﻿using Irms.Application.Abstract;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;

namespace Irms.Infrastructure.Services.EmailReachability
{
    public class EmailReachabilityManager : IEmailReachabilityManager
    {
        private readonly IConfiguration _config;
        public EmailReachabilityManager(IConfiguration config)
        {
            _config = config;
        }

        public bool CheckEmailReachability(string email)
        {
            //Thread.Sleep(TimeSpan.FromSeconds(4));
            //return new Random().Next(100) <= 50;
            var zeroBounceAPI = new ZeroBounce();

            //set input parameters
            zeroBounceAPI.ApiKey = _config["ZeroBounce:ApiKey"]; //Required
            zeroBounceAPI.RequestTimeOut = 150000; // "Any integer value in milliseconds

            //validate email and assign results to an object
            var apiProperties = zeroBounceAPI.ValidateEmail(email, string.Empty);

            //var apiCredits = zeroBounceAPI.GetCredits();

            switch (apiProperties.Status)
            {
                case "Invalid":
                    Console.WriteLine("invalid");
                    return false;
                case "Valid":
                    Console.WriteLine("valid");
                    return true;
                default:
                    Console.WriteLine(apiProperties.Status);
                    return false;
            }
        }
    }


}
