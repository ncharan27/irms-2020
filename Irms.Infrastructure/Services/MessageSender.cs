﻿using Irms.Application.Abstract.Services.Notifications;
using Irms.Infrastructure.Services.Sendgrid;
using Irms.Infrastructure.Services.Twilio;
using PhoneNumbers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Infrastructure.Services
{
    public class MessageSender : ISmsSender, IEmailSender
    {
        private static readonly string SectionTitleSeparator = new string('-', 20) + Environment.NewLine;
        private static readonly int[] GccCountryCodes = new int[] { 966, 971, 965, 973, 974, 968 };
        private readonly ISendgridEmailSender _sendgrid;
        private readonly IMalathSmsSender _malath;
        private readonly ITwilioSmsSender _twilio;

        public MessageSender(ISendgridEmailSender sendgrid,
            IMalathSmsSender malath,
            ITwilioSmsSender twilio)
        {
            _sendgrid = sendgrid;
            _malath = malath;
            _twilio = twilio;
        }

        #region email
        public async Task<bool> SendEmail(EmailMessage message, CancellationToken token)
        {
            var result = await _sendgrid.SendEmail(message, token);
            return result;
        }
        #endregion

        #region sms
        public Task<bool> SendSms(SmsMessage message, CancellationToken token)
        {
            var isInternationalPhoneNumber = message.Recipients.Any(x => IsInternationalPhone(x.Phone));
            if (isInternationalPhoneNumber)
            {
                var result = _twilio.SendSms(message, token);
                return result;
            }
            else
            {
                var result = _malath.SendSms(message, token);
                return result;
            }
        }

        public Task<bool> SendWebhookSms(SmsMessage message, CancellationToken token)
        {
            var isInternationalPhoneNumber = message.Recipients.Any(x => IsInternationalPhone(x.Phone));
            if (isInternationalPhoneNumber)
            {
                var result = _twilio.SendSms(message, token);
                return result;
            }
            else
            {
                var result = _malath.SendSms(message, token);
                return result;
            }
        }

        private bool IsInternationalPhone(string unvalidatedPhone)
        {
            var util = PhoneNumberUtil.GetInstance();
            try
            {

                var phone = util.Parse(unvalidatedPhone, null);
                bool isValid = util.IsValidNumber(phone);

                //check if code is on Gcc Country Codes array (not international)
                if (isValid && GccCountryCodes.Contains(phone.CountryCode))
                {
                    return false;
                }

                return true;
            }
            catch (NumberParseException)
            {
                return false;
            }
        }
        #endregion
    }
}
