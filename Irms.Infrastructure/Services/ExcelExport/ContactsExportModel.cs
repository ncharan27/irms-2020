﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Infrastructure.Services.ExcelExport
{
    public class ContactsExportModel
    {
        public IEnumerable<IExportModel> Contacts { get; set; }
        public IEnumerable<ContactFieldModel> CustomFields { get; set; }
        public long CustomFieldsCount { get; set; }
        public IEnumerable<ContactCustomFieldModel> ContactCustomFields { get; set; }


        public class ContactFieldModel
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public class ContactCustomFieldModel
        {
            public Guid ContactId { get; set; }
            public Guid CustomFieldId { get; set; }
            public string Value { get; set; }
        }
    }
}
