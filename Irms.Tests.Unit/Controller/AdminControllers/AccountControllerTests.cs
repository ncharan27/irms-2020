﻿using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Data.IdentityClasses;
using Irms.Domain;
using Irms.Infrastructure.Services;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.WebApi.Controllers;
using Irms.WebApi.Helpers.UserManagement;
using Irms.WebApi.ViewModels.Account;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class AccountControllerTests
    {
        private readonly IMediator _mediator = new Mock<IMediator>().Object;
        private readonly IReCaptchaValidator _captchaValidator = new Mock<IReCaptchaValidator>().Object;

        [TestMethod]
        public async Task CanRecoverPasswordBySms_PhoneInvalid()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            var result = true;

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.IsPhoneNumberConfirmedAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(result));

            userManager
                .Setup(x => x.GenerateChangePhoneNumberTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string n) => Task.FromResult(token));

            notifier
                .Setup(x => x.NotifyBySms(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None))
                .Returns((string n, string c, CancellationToken t) => MediatR.Unit.Task);

            otpNotifier
                .Setup(x => x.NotifyBySms(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None))
                .Returns((string e, string m, CancellationToken t) => Task.FromResult(result));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var recoverPasswordByEmailViewModel = new RecoverPasswordBySmsViewModel()
            { PhoneNumber = "+380972197945" };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.RecoverPasswordBySms(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "Phone number format is invalid");
        }

        [TestMethod]
        public async Task CanGetPasswordResetTokenByCode()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.ChangePhoneNumberAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string p, string c) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(token));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = GetAccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object);

            var recoverPasswordByEmailViewModel = new GetTokenByCodeViewModel()
            {
                PhoneNumber = "14523678",
                Code = "124563"
            };

            //Act
            var apiResult = await sut.GetPasswordResetTokenByCode(
                recoverPasswordByEmailViewModel,
                CancellationToken.None);

            //Assert
            Assert.AreEqual(apiResult.Token, "123456");
            Assert.AreEqual(apiResult.UserId, user.Id.ToString());
        }

        [TestMethod]
        public async Task CanGetPasswordResetTokenByCode_PhoneInvalid()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.ChangePhoneNumberAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string p, string c) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(token));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var recoverPasswordByEmailViewModel = new GetTokenByCodeViewModel()
            {
                PhoneNumber = "+380972197945",
                Code = "124563"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.GetPasswordResetTokenByCode(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "Phone number format is invalid");
        }

        [TestMethod]
        public async Task CanSetNewPassword()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            userManager
                .Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string t, string p) => Task.FromResult(succeeded));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var setNewPasswordViewModel = new SetNewPasswordViewModel()
            {
                UserId = user.Id.ToString(),
                Token = token,
                NewPassword = "123asdQ!",
                ConfirmPassword = "123asdQ!"
            };

            //Act
            await sut.SetNewPassword(
                setNewPasswordViewModel,
                CancellationToken.None);
        }

        [TestMethod]
        public async Task CanCheckEmailUniqueness()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            IUser user = null;

            userManager
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns((string i) => Task.FromResult(user));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var checkEmailUniquenessViewModel = new CheckEmailUniquenessViewModel("j.doe@example.com");

            //Act
            var apiResult = await sut.CheckEmailUniqueness(
                checkEmailUniquenessViewModel,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
        }

        [TestMethod]
        public async Task CanCheckPhoneUniqueness()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        Request =
                        {
                            Host = new HostString("Referer")
                        }
                    }
                }
            };

            var checkPhoneUniquenessViewModel = new CheckPhoneUniquenessViewModel("+966501234567");

            //Act
            var apiResult = await sut.CheckPhoneUniqueness(
                checkPhoneUniquenessViewModel,
                CancellationToken.None);

            //Assert
            Assert.AreEqual(apiResult.FormattedPhoneNo, "+966501234567");
            Assert.AreEqual(apiResult.IsValid, true);
            Assert.AreEqual(apiResult.IsUnique, false);
        }

        [TestMethod]
        public async Task CanCheckPhoneUniqueness_PhoneInvalid()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var checkPhoneUniquenessViewModel = new CheckPhoneUniquenessViewModel("+380972197945");

            //Act
            var apiResult = await sut.CheckPhoneUniqueness(
                checkPhoneUniquenessViewModel,
                CancellationToken.None);

            //Assert
            Assert.AreEqual(apiResult.FormattedPhoneNo, null);
            Assert.AreEqual(apiResult.IsValid, false);
            Assert.AreEqual(apiResult.IsUnique, false);
        }

        [TestMethod]
        public void CanFormatPhoneNumber()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var checkPhoneUniquenessViewModel = new CheckPhoneUniquenessViewModel("+966501234567");

            //Act
            var apiResult = sut.FormatPhoneNumber(
                checkPhoneUniquenessViewModel,
                CancellationToken.None);

            //Assert
            Assert.AreEqual(apiResult.FormattedPhoneNo, "+966501234567");
            Assert.AreEqual(apiResult.IsValid, true);
        }

        [TestMethod]
        public async Task CanChangePassword()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            userManager
                .Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string t, string p) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser u, string p, string q) => Task.FromResult(succeeded));

            var userI = new Mock<ClaimsPrincipal>();

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));


            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = userI.Object
                    }
                }
            };

            var changePasswordViewModel = new ChangePasswordViewModel
            {
                CurrentPassword = "123qweQ!",
                NewPassword = "123asdQ!",
                NewPasswordConfirmation = "123asdQ!"
            };

            //Act
            var apiResult = await sut.ChangePassword(
                changePasswordViewModel,
                CancellationToken.None);

            //Assert
            Assert.IsTrue(apiResult);
        }

        [TestMethod]
        public void CanNotRecoverPasswordByEmail_UserNotFound()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            IUser user = null;
            var token = "CfDJ8M7beJ/ylpZIm/sT3fwgqCrufKCmNKCg0QN4LPor/TBLQz418WTYp0Jr+vmzADO7kHLUT8Ahbt6HbmVjCjrClc55MdLz/BZB7KUWyE2ccl86ZHFEIMNYRqBS0RcFFwtm2NkzGYLUj4C2rN+fX8/oMFNxAv0qXOzACLkWhIWiSDU2A2UVWCFoNz0YUC/fOVNIK9tslpAAG50jEcGLlkn8kEUfjf7fxlHi9CQTkFuqd7bDL8AWR6pjplC3zZm8i1d8Fw==";
            var result = true;

            userManager
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.IsEmailConfirmedAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(result));

            userManager
                .Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(token));

            notifier
                .Setup(x => x.NotifyByEmail(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None))
                .Returns((string e, string m, CancellationToken t) => MediatR.Unit.Task);

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        Request =
                        {
                            Host = new HostString("default.takamul.com")
                        }
                    }
                }
            };

            var recoverPasswordByEmailViewModel = new RecoverPasswordByEmailViewModel()
            { Email = "pak@email.com" };


            //Act
            //var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.RecoverPasswordByEmail(
            //    recoverPasswordByEmailViewModel,
            //    CancellationToken.None));

            //Assert
            //Assert.AreEqual(ex.Message, "We couldn't find your account registered with this email address");
        }

        [TestMethod]
        public async Task CanNotRecoverPasswordByEmail_EmailNotConfirmed()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var notifier = new Mock<IForgotPasswordNotifier>();


            IUser user = new ApplicationUser();
            var token = "CfDJ8M7beJ/ylpZIm/sT3fwgqCrufKCmNKCg0QN4LPor/TBLQz418WTYp0Jr+vmzADO7kHLUT8Ahbt6HbmVjCjrClc55MdLz/BZB7KUWyE2ccl86ZHFEIMNYRqBS0RcFFwtm2NkzGYLUj4C2rN+fX8/oMFNxAv0qXOzACLkWhIWiSDU2A2UVWCFoNz0YUC/fOVNIK9tslpAAG50jEcGLlkn8kEUfjf7fxlHi9CQTkFuqd7bDL8AWR6pjplC3zZm8i1d8Fw==";
            var result = false;

            userManager
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.IsEmailConfirmedAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(result));

            userManager
                .Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(token));

            notifier
                .Setup(x => x.NotifyByEmail(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None))
                .Returns((string e, string m, CancellationToken t) => MediatR.Unit.Task);

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        Request =
                        {
                            Host = new HostString("default.takamul.com")
                        }
                    }
                }
            };

            var recoverPasswordByEmailViewModel = new RecoverPasswordByEmailViewModel()
            { Email = "test@tenant.net" };


            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.RecoverPasswordByEmail(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "The specified email is not confirmed");
        }

        [TestMethod]
        public async Task CanNotRecoverPasswordBySms_UserNotFound()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            IUser user = null;
            var token = "123456";
            var result = true;

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.IsPhoneNumberConfirmedAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(result));

            userManager
                .Setup(x => x.GenerateChangePhoneNumberTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string n) => Task.FromResult(token));

            notifier
                .Setup(x => x.NotifyBySms(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None))
                .Returns((string n, string c, CancellationToken t) => MediatR.Unit.Task);

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var recoverPasswordByEmailViewModel = new RecoverPasswordBySmsViewModel()
            { PhoneNumber = "+380972197945" };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.RecoverPasswordBySms(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "We couldn't find your account registered with this mobile number");
        }

        [TestMethod]
        public async Task CanNotRecoverPasswordBySms_PhoneNotConfirmed()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            var result = false;

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.IsPhoneNumberConfirmedAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(result));

            userManager
                .Setup(x => x.GenerateChangePhoneNumberTokenAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string n) => Task.FromResult(token));

            notifier
                .Setup(x => x.NotifyBySms(It.IsAny<string>(), It.IsAny<string>(), CancellationToken.None))
                .Returns((string n, string c, CancellationToken t) => MediatR.Unit.Task);

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var recoverPasswordByEmailViewModel = new RecoverPasswordBySmsViewModel()
            { PhoneNumber = "+380972197945" };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.RecoverPasswordBySms(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "The specified phone number is not confirmed");
        }

        [TestMethod]
        public async Task CanNotSetNewPassword_UserIdIncorrect()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            IUser user = null;
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            userManager
                .Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string t, string p) => Task.FromResult(succeeded));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var setNewPasswordViewModel = new SetNewPasswordViewModel()
            {
                UserId = Guid.NewGuid().ToString(),
                Token = token,
                NewPassword = "123asdQ!",
                ConfirmPassword = "123asdQ!"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.SetNewPassword(
                setNewPasswordViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "User Id is incorrect");
        }

        [TestMethod]
        public async Task CanNotSetNewPassword_TokenIsInvalid()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Failed());

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            userManager
                .Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string t, string p) => Task.FromResult(succeeded));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var setNewPasswordViewModel = new SetNewPasswordViewModel()
            {
                UserId = user.Id.ToString(),
                Token = token,
                NewPassword = "123asdQ!",
                ConfirmPassword = "123asdQ!"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.SetNewPassword(
                setNewPasswordViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "Token is invalid");
        }

        [TestMethod]
        public async Task CanNotChangePassword_UserNotFound()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            IUser user = null;
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            userManager
                .Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string t, string p) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser u, string p, string q) => Task.FromResult(succeeded));

            var userI = new Mock<ClaimsPrincipal>();

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = userI.Object
                    }
                }
            };

            var changePasswordViewModel = new ChangePasswordViewModel
            {
                CurrentPassword = "123qweQ!",
                NewPassword = "123asdQ!",
                NewPasswordConfirmation = "123asdQ!"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.ChangePassword(
                changePasswordViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "Something went wrong. User can not be found by the claim");
        }

        [TestMethod]
        public async Task CanNotChangePassword_PasswordInvalid()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            IIdentityResult succeeded = new IdResult(IdentityResult.Failed(new IdentityError { Code = "42", Description = "Foo" }));

            userManager
                .Setup(x => x.FindByIdAsync(It.IsAny<Guid>()))
                .Returns((Guid i) => Task.FromResult(user));

            userManager
                .Setup(x => x.ResetPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string t, string p) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser u, string p, string q) => Task.FromResult(succeeded));

            var userI = new Mock<ClaimsPrincipal>();

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        User = userI.Object
                    }
                }
            };

            var changePasswordViewModel = new ChangePasswordViewModel
            {
                CurrentPassword = "123qweQ!",
                NewPassword = "123asdQ!",
                NewPasswordConfirmation = "123asdQ!"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.ChangePassword(
                changePasswordViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "42: Foo");
        }

        [TestMethod]
        public async Task CanNotGetPasswordResetTokenByCode_PhoneNumberIncorrect()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();
            var notifier = new Mock<IForgotPasswordNotifier>();

            IUser user = null;
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Success);

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.ChangePhoneNumberAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string p, string c) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(token));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var recoverPasswordByEmailViewModel = new GetTokenByCodeViewModel()
            {
                PhoneNumber = "+380972197945",
                Code = "124563"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.GetPasswordResetTokenByCode(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "Phone number is incorrect");
        }

        [TestMethod]
        public async Task CannotGetPasswordResetTokenByCode_CodeIncorrect()
        {
            //Arrange
            var userManager = new Mock<IUserManager>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var notifier = new Mock<IForgotPasswordNotifier>();
            var otpNotifier = new Mock<IOTPCodeNotifier>();

            var tenantBasicInfo = TenantBasicInfoBuilder.BuildDefault();

            IUser user = new ApplicationUser
            {
                Id = Guid.NewGuid(),
                UserName = "j.doe@example.com",
                Email = "j.doe@example.com",
                PhoneNumber = "+380972197945",
                TenantId = tenantBasicInfo.Id,
                EmailConfirmed = true
            };
            var token = "123456";
            IIdentityResult succeeded = new IdResult(IdentityResult.Failed());

            userManager
                .Setup(x => x.FindByPhoneNumberAsync(It.IsAny<string>()))
                .Returns((string e) => Task.FromResult(user));

            userManager
                .Setup(x => x.ChangePhoneNumberAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((ApplicationUser q, string p, string c) => Task.FromResult(succeeded));

            userManager
                .Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<ApplicationUser>()))
                .Returns((ApplicationUser q) => Task.FromResult(token));

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>())).Returns((string n, string c) => (true, new ValidPhoneNumber(n)));
            var iv = true;
            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out iv, It.IsAny<string>())).Returns(new ValidPhoneNumber("+966501234567"));

            var sut = new AccountController(userManager.Object, phoneValidator.Object, notifier.Object, otpNotifier.Object, _mediator, _captchaValidator);

            var recoverPasswordByEmailViewModel = new GetTokenByCodeViewModel()
            {
                PhoneNumber = "+380972197945",
                Code = "124563"
            };

            //Act
            var ex = await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.GetPasswordResetTokenByCode(
                recoverPasswordByEmailViewModel,
                CancellationToken.None));

            //Assert
            Assert.AreEqual(ex.Message, "The code is incorrect");
        }


        private AccountController GetAccountController(IUserManager userManager, IPhoneNumberValidator phoneValidator, IForgotPasswordNotifier notifier, IOTPCodeNotifier otpNotifier, bool isAdmin = false, string hostString = "default.takamul.com")
        {
            var controller = new AccountController(userManager, phoneValidator, notifier, otpNotifier, _mediator, _captchaValidator);
            if (controller.ControllerContext == null || controller.HttpContext == null)
            {
                controller.ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext
                    {
                        Request =
                        {
                            Host = new HostString(hostString) //Request.Headers?[]
                        }
                    }
                };
            };
            if(isAdmin)
                controller.HttpContext.Request.Headers.Add("Referer", "admin");
            return controller;
        }
    }
}
