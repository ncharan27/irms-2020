﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Application.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Data.Read.Templates.Queries;
using Irms.Domain.Entities.Templates;
using Irms.Data.Read.Templates;
using Irms.Application.Templates.Commands;
using Irms.WebApi.LocalizationTransform.Template.Models;
using Irms.WebApi.AdminControllers.Settings;
using Irms.WebApi.LocalizationTransform.Template;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class TemplateControllerTests
    {
        private static readonly Guid En = new Guid("2C99A21A-F1D4-4EA8-8BA9-C8BA90C64E2A");
        private static readonly Guid Ar = new Guid("C454253B-4197-4EEB-BC3E-96F2915B04AD");

        private ILanguageIds _ids;

        private ITemplateTranslator Translator => new TemplateTranslator(_ids);

        [TestInitialize]
        public void Init()
        {
            var idMock = new Mock<ILanguageIds>();
            idMock.Setup(x => x.ArCulture).Returns("ar-SA");
            idMock.Setup(x => x.EnCulture).Returns("en-US");
            idMock.Setup(x => x.Ar()).ReturnsAsync(Ar);
            idMock.Setup(x => x.En()).ReturnsAsync(En);

            _ids = idMock.Object;
        }

        [TestMethod]
        public async Task CanGetTemplateSelectionList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new TemplateSelectionListItem
                {
                    Id = id,
                    IsEnabled = true,
                    HasEmailTemplate = true,
                    HasSmsTemplate = true,
                    TemplateType = TemplateType.EventAssignment
                },
                new TemplateSelectionListItem
                {
                    Id = Guid.NewGuid(),
                    IsEnabled = true,
                    HasEmailTemplate = true,
                    HasSmsTemplate = false,
                    TemplateType = TemplateType.ForgotPassword
                }
            };
            IPageResult<TemplateSelectionListItem> templateListItems = new PageResult<TemplateSelectionListItem>(items, 10);

            mediator
                .Setup(x => x.Send(It.IsAny<GetTemplateSelectionList>(), CancellationToken.None))
                .Returns((GetTemplateSelectionList q, CancellationToken t) => Task.FromResult(templateListItems));

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            var result = await sut.GetTemplateSelectionList(
                new GetTemplateSelectionList(1, 10, string.Empty),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var template = result.Items.First();

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(template.Id, id);
            Assert.AreEqual(template.Title, "Event Assignment");
            Assert.AreEqual(template.HasEmailTemplate, true);
            Assert.AreEqual(template.HasSmsTemplate, true);
            Assert.AreEqual(template.IsEnabled, true);
            Assert.AreEqual(result.Items.Skip(1).First().Title, "Forgot Password");
        }

        [TestMethod]
        public async Task CanGetTemplateSelectionDetails()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();
            var emailId = Guid.NewGuid();
            var smsId = Guid.NewGuid();

            var templateDetails = new TemplateSelectionDetails
            {
                Id = id,
                IsEnabled = true,
                TemplateType = TemplateType.EventEnrollment,
                Email = new TemplateSelectionLookup
                {
                    Id = emailId,
                    Name = "Email Template"
                },
                Sms = new TemplateSelectionLookup
                {
                    Id = smsId,
                    Name = "Sms Template"
                }
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetTemplateSelectionDetails>(), CancellationToken.None))
                .Returns((GetTemplateSelectionDetails q, CancellationToken t) => Task.FromResult(templateDetails));

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            var result = await sut.GetTemplateSelectionDetails(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Title, "Event Enrollment");
            Assert.AreEqual(result.IsEnabled, true);
            Assert.AreEqual(result.Email.Name, "Email Template");
            Assert.AreEqual(result.Sms.Name, "Sms Template");
            Assert.AreEqual(result.Email.Id, emailId);
            Assert.AreEqual(result.Sms.Id, smsId);
        }

        [TestMethod]
        public async Task CanUpdateTemplateSelection()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();
            var emailId = Guid.NewGuid();
            var smsId = Guid.NewGuid();

            var templateDetails = new TemplateSelectionDetails
            {
                Id = id,
                IsEnabled = true,
                Email = new TemplateSelectionLookup
                {
                    Id = emailId,
                    Name = "Email Template"
                },
                Sms = new TemplateSelectionLookup
                {
                    Id = smsId,
                    Name = "Sms Template"
                }
            };

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateTemplateSelectionCmd>(), CancellationToken.None))
                .Returns((UpdateTemplateSelectionCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            await sut.UpdateTemplateSelection(
                new UpdateTemplateSelectionCmd(Guid.NewGuid(), true, Guid.NewGuid(), Guid.NewGuid()),
                CancellationToken.None);
        }

        [TestMethod]
        public async Task CanGetTemplateList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();
            var items = new[]
            {
                new TemplateListItem
                {
                    Id = id,
                    Name = "Template",
                    EmailCompatible = true,
                    SmsCompatible = true
                },
                new TemplateListItem
                {
                    Id = Guid.NewGuid(),
                    Name = "Template 2",
                    EmailCompatible = true,
                    SmsCompatible = false
                }
            };
            IPageResult<TemplateListItem> templateListItems = new PageResult<TemplateListItem>(items, 10);

            mediator
                .Setup(x => x.Send(It.IsAny<GetTemplateList>(), CancellationToken.None))
                .Returns((GetTemplateList q, CancellationToken t) => Task.FromResult(templateListItems));

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            var result = await sut.GetTemplateList(
                new GetTemplateList(TemplateType.EventAssignment, TemplateCompatibility.Email, 1, 10, string.Empty),
                CancellationToken.None);

            //Assert

            Assert.IsNotNull(result);

            var template = result.Items.First();
            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(template.Id, id);
            Assert.AreEqual(template.Name, "Template");
            Assert.AreEqual(template.EmailCompatible, true);
            Assert.AreEqual(template.SmsCompatible, true);
            Assert.AreEqual(result.Items.Skip(1).First().Name, "Template 2");
        }

        [TestMethod]
        public async Task CanGetTemplateDetails()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();
            var date = DateTime.UtcNow.AddDays(-1);

            var templateDetails = new TemplateDetails
            {
                Id = id,
                Name = "Template",
                Type = TemplateType.ActivationMessage,
                EmailCompatible = true,
                SmsCompatible = true,
                EmailSubject = "email subject",
                EmailBody = "email body",
                SmsText = "sms text",
                ModifiedBy = "Admin",
                ModifiedOn = date,
                Translations = new List<TemplateTranslation>
                {
                    new TemplateTranslation(new Guid("2C99A21A-F1D4-4EA8-8BA9-C8BA90C64E2A"), "email subject", "email body", "sms text"),
                    new TemplateTranslation(new Guid("C454253B-4197-4EEB-BC3E-96F2915B04AD"), "AEmail Sub", "AEmail Body", "Asms Text")
                }
            };

            mediator
                .Setup(x => x.Send(It.IsAny<GetTemplateDetails>(), CancellationToken.None))
                .Returns((GetTemplateDetails q, CancellationToken t) => Task.FromResult(templateDetails));

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            var result = await sut.GetTemplateDetails(
                Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "Template");
            Assert.AreEqual(result.Type, TemplateType.ActivationMessage);
            Assert.AreEqual(result.SmsCompatible, true);
            Assert.AreEqual(result.EmailCompatible, true);
            Assert.AreEqual(result.EnglishEmailSubject, "email subject");
            Assert.AreEqual(result.EnglishEmailBody, "email body");
            Assert.AreEqual(result.EnglishSmsText, "sms text");
            Assert.AreEqual(result.ModifiedBy, "Admin");
            Assert.AreEqual(result.ModifiedOn, date);
        }

        [TestMethod]
        public async Task CanCreateTemplate()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateTemplateCmd>(), CancellationToken.None))
                .Returns((CreateTemplateCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            var apiResult = await sut.CreateTemplate(
                new TemplateDetailsCreate
                {
                    Name = "Template",
                    Type = TemplateType.EventAssignment,
                    EmailCompatible = true,
                    SmsCompatible = true,
                    ArabicEnabled = true,
                    EnglishEmailSubject = "email subject",
                    EnglishEmailBody = "email body",
                    EnglishSmsText = "sms text",
                    ArabicEmailSubject = "A email subject",
                    ArabicEmailBody = "A email body",
                    ArabicSmsText = "A sms text",
                },
                CancellationToken.None);

            //Assert
            Assert.AreEqual(apiResult, id);
        }

        [TestMethod]
        public async Task CanUpdateTemplate()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<UpdateTemplateCmd>(), CancellationToken.None))
                .Returns((UpdateTemplateCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            await sut.UpdateTemplate(
                new TemplateDetailsUpdate
                {
                    Name = "Template",
                    Id = Guid.NewGuid(),
                    EmailCompatible = true,
                    SmsCompatible = true,
                    ArabicEnabled = true,
                    EnglishEmailSubject = "email subject",
                    EnglishEmailBody = "email body",
                    EnglishSmsText = "sms text",
                    ArabicEmailSubject = "A email subject",
                    ArabicEmailBody = "A email body",
                    ArabicSmsText = "A sms text",
                },
                CancellationToken.None);
        }

        [TestMethod]
        public async Task CanDeleteTemplates()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var not = new Mock<INotifierProvider>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<DeleteTemplatesCmd>(), CancellationToken.None))
                .Returns((DeleteTemplatesCmd q, CancellationToken t) => MediatR.Unit.Task);

            var sut = new TemplateController(mediator.Object, not.Object, Translator);

            //Act
            await sut.DeleteTemplates(
                new DeleteTemplatesCmd(
                new[]
                {
                    id,
                    Guid.NewGuid()
                }),
                CancellationToken.None);
        }
    }
}
