﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.WebApi.AdminControllers.Settings;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Subscription.ReadModels;
using Irms.Data.Read.Subscription.Queries;
using Irms.Data.Read.Abstract;
using Irms.WebApi.AdminControllers;
using Irms.Data.Read;
using Irms.Application.Subscriptions.Commands;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class SubscriptionControllerTests
    {
        /// <summary>
        /// get subscription list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetSubscriptionList()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            var items = new[]
            {
                new SubscriptionListItem()
                {
                    Id = id,
                    IsActive = true,
                    Product = "rsvp",
                    Tenant = "ithra"
                },
                new SubscriptionListItem()
                {
                    Id = Guid.NewGuid(),
                    IsActive = false,
                    Product = "rsvp two",
                    Tenant = "ithra two"
                },
            };

            IPageResult<SubscriptionListItem> subscriptionListItems = new PageResult<SubscriptionListItem>(items, 2);

            mediator
                .Setup(x => x.Send(It.IsAny<GetSubscriptionListQuery>(), CancellationToken.None))
                    .Returns((GetSubscriptionListQuery q, CancellationToken t) => Task.FromResult(subscriptionListItems));

            var sut = new SubscriptionController(mediator.Object);

            //Act
            var result = await sut.GetSubscriptionList(
                new GetSubscriptionListQuery(1, 10, TimeFilterRange.AllTime, string.Empty),
                CancellationToken.None);

            //Assert
            //Assert
            Assert.IsNotNull(result);

            var subscription = result.Items.First();

            Assert.AreEqual(result.TotalCount, 2);
            Assert.AreEqual(subscription.Id, id);
            Assert.AreEqual(subscription.IsActive, true);
            Assert.AreEqual(subscription.Product, "rsvp");
            Assert.AreEqual(subscription.Tenant, "ithra");
            Assert.AreEqual(result.Items.Skip(1).First().IsActive, false);

        }

        /// <summary>
        /// create subscription
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateSubscription()
        {
            //Arrange
            var mediator = new Mock<IMediator>();
            var id = Guid.NewGuid();

            mediator
                .Setup(x => x.Send(It.IsAny<CreateSubscriptionCmd>(), CancellationToken.None))
                .Returns((CreateSubscriptionCmd q, CancellationToken t) => Task.FromResult(id));

            var sut = new SubscriptionController(mediator.Object);

            //Act
            var apiResult = await sut.Create(
                new CreateSubscriptionCmd
                {
                    ProductId = Guid.NewGuid(),
                    TenantId = Guid.NewGuid()
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
        }
    }
}
