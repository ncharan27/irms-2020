﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Application.Contact.Commands;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using Irms.WebApi.TenantControllers;
using Irms.Data.Read.Abstract;
using System.Linq;
using Irms.Application.Abstract.Services;
using Irms.Infrastructure.Services.ExcelExport;
using Irms.Domain;

namespace Irms.Tests.Unit.Controller.TenantControllers
{
    [TestClass]
    public class ContactControllerTests
    {
        private Mock<IMediator> _mediator;
        private Mock<IPhoneNumberValidator> _phoneValidator;
        private Mock<IExcelGenerator> _excelGenerator;

        [TestInitialize]
        public void Init()
        {
            _mediator = new Mock<IMediator>();
            _phoneValidator = new Mock<IPhoneNumberValidator>();
            _excelGenerator = new Mock<IExcelGenerator>();

            var value = true;

            _phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out value, null))
                .Returns((IValidPhoneNumber)null);
        }

        /// <summary>
        /// can get global contacts count, check result is not null and equal "count"
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetGlobalContactsCount()
        {
            //Arrange
            const long count = 2;

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactsCount>(), CancellationToken.None))
                .Returns((GetContactsCount q, CancellationToken t) => Task.FromResult(count));

            //Act
            var apiResult = await Create().GetGuestsCount(CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, count);
        }

        /// <summary>
        /// can get contacts by contact list Id, check result is not null and result list count equal guidList count
        /// </summary>
        /// <param name="contactListId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetContactsByContactListId()
        {
            //Arrange
            Guid tenantId = Guid.NewGuid();
            Guid ContactListId1 = Guid.NewGuid();
            Guid ContactListId2 = Guid.NewGuid();
            Guid ContactListId3 = Guid.NewGuid();

            var guidList = new List<Guid>() { ContactListId1, ContactListId2, ContactListId3 };

            var result = new List<ContactItemAndListId>
            {
                new ContactItemAndListId {ContactListId = ContactListId1, Item = new  ContactItem(){ Id = Guid.NewGuid(), FullName="TestItemName1", TenantId = tenantId, Title ="Test Title1"}},
                new ContactItemAndListId {ContactListId = ContactListId2, Item = new  ContactItem(){ Id = Guid.NewGuid(), FullName="TestItemName2", TenantId = tenantId, Title ="Test Title2"}},
                new ContactItemAndListId {ContactListId = ContactListId3, Item = new  ContactItem(){ Id = Guid.NewGuid(), FullName="TestItemName3", TenantId = tenantId, Title ="Test Title3"}}
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactsByListId>(), CancellationToken.None))
                .Returns((GetContactsByListId q, CancellationToken t) => Task.FromResult((IReadOnlyCollection<ContactItemAndListId>)result));

            //Act
            var apiResult = await Create().GetContactsByContactListId(guidList, CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.Count(), guidList.Count());
        }

        /// <summary>
        /// can get global guest list, check apiResult is not null and apiResult count equal contactLists count
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetGlobalGuestList()
        {
            //Arrange

            var result = new List<ContactListItem>
            {
                new ContactListItem {Id = Guid.NewGuid(), Name = "Test1"},
                new ContactListItem {Id = Guid.NewGuid(), Name = "Test2"}
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactList>(), CancellationToken.None))
                .Returns((GetContactList q, CancellationToken t) => Task.FromResult((IReadOnlyCollection<ContactListItem>)result));

            //Act
            var apiResult = await Create().GetGlobalGuestList(CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.Count, 2);
        }

        /// <summary>
        /// can get local contacts count, check apiResult is not null and apiResult count equal contactLists count
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetLocalContactsCount()
        {
            //Arrange

            long count = 2;

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactsCount>(), CancellationToken.None))
                .Returns((GetContactsCount q, CancellationToken t) => Task.FromResult(count));

            //Act
            var apiResult = await Create().GetContactsCount(CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, count);
        }


        [TestMethod]
        public async Task CanGetGblobalGuestListsFiltered()
        {
            //Arrange

            var id = Guid.NewGuid();
            var items = new List<ContactListItemFiltered>
            {
                new ContactListItemFiltered {Id = id, Name = "Test1"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test2"}
            };

            IPageResult<ContactListItemFiltered> templateListItems = new PageResult<ContactListItemFiltered>(items, 10);

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactListFiltered>(), CancellationToken.None))
                .Returns((GetContactListFiltered q, CancellationToken t) => Task.FromResult(templateListItems));

            //Act
            var result = await Create().GetGlobalGuestListsFiltered(
                new GetContactListFiltered(1, 10, string.Empty),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var contactList = result.Items.First(t => t.Id == id);

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(contactList.Id, id);
            Assert.AreEqual(contactList.Name, items[0].Name);
        }

        /// <summary>
        /// can get correct Guest Contact List Filtered
        /// </summary>
        /// <returns></returns>
        [TestMethod]

        public async Task CanGetEventGuestListsFiltered()
        {
            //Arrange

            var id = Guid.NewGuid();
            var items = new List<ContactListItemFiltered>
            {
                new ContactListItemFiltered {Id = id, Name = "Test1"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test2"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test3"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test4"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test5"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test6"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test7"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test8"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test9"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test10"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test11"}
            };

            IPageResult<ContactListItemFiltered> templateListItems = new PageResult<ContactListItemFiltered>(items, items.Count());

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactListFiltered>(), CancellationToken.None))
                .Returns((GetContactListFiltered q, CancellationToken t) => Task.FromResult(templateListItems));

            //Act
            var result = await Create().GetEventGuestListsFiltered(
                new GetContactListFiltered(1, 10, string.Empty),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var contactList = result.Items.First(t => t.Id == id);

            Assert.AreEqual(result.TotalCount, items.Count());
            Assert.AreEqual(contactList.Id, id);
            Assert.AreEqual(contactList.Name, "Test1");
        }

        /// <summary>
        /// can get local guest contact by list id
        /// </summary>
        /// <returns></returns>
        [TestMethod]

        public async Task CanGetLocalGuestContactByListId()
        {
            // Arrange
            var id = Guid.NewGuid();
            var contactListId = Guid.NewGuid();

            var items = new List<GuestContactItemFiltered>
            {
                new GuestContactItemFiltered
                {
                    Id = id,
                    ContactListId = contactListId,
                    FullName = "Test123",
                    Email = "test@test.com",
                    MobileNumber = "0111234567"
                }
            };

            IPageResult<GuestContactItemFiltered> templateListItems = new PageResult<GuestContactItemFiltered>(items, items.Count);

            _mediator
                .Setup(x => x.Send(It.IsAny<GetGuestContactByListFiltered>(), CancellationToken.None))
                .Returns((GetGuestContactByListFiltered q, CancellationToken t) =>
                {
                    return Task.FromResult(templateListItems);
                });

            // Act
            var result = await Create().GetContactByListIdFiltered(
                new GetGuestContactByListFiltered(1, 10, "Test123"),
                CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);

            var guestContactList = result.Items.First(t => t.FullName.Equals("Test123"));

            Assert.AreEqual(result.TotalCount, items.Count(item => item.FullName == "Test123"));
            Assert.AreEqual(result.Items.Count(), 1);
            Assert.AreEqual(guestContactList.Id, id);
            Assert.AreEqual(guestContactList.ContactListId, contactListId);
            Assert.AreEqual(guestContactList.FullName, "Test123");
            Assert.AreEqual(guestContactList.Email, "test@test.com");
            Assert.AreEqual(guestContactList.MobileNumber, "0111234567");
        }

        /// <summary>
        /// can filter search GetGuestContactListFiltered
        /// </summary>
        /// <returns></returns>
        [TestMethod]

        public async Task CanFilterSearchGetContactListsFiltered()
        {
            //Arrange

            var id = Guid.NewGuid();
            var items = new List<ContactListItemFiltered>
            {
                new ContactListItemFiltered {Id = id, Name = "Test12"}
            };

            IPageResult<ContactListItemFiltered> templateListItems = new PageResult<ContactListItemFiltered>(items, items.Count);

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactListFiltered>(), CancellationToken.None))
                .Returns((GetContactListFiltered q, CancellationToken t) =>
                {
                    return Task.FromResult(templateListItems);
                });

            //Act
            var result = await Create().GetContactListsFiltered(
                new GetContactListFiltered(1, 10, "Test12"),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var contactList = result.Items.First(t => t.Name.Equals("Test12"));

            Assert.AreEqual(result.TotalCount, items.Count(item => item.Name == "Test12"));
            Assert.AreEqual(result.Items.Count(), 1);
            Assert.AreEqual(contactList.Id, id);
            Assert.AreEqual(contactList.Name, "Test12");
        }

        /// <summary>
        /// can pagination GetGuestContactListFiltered
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanPaginationGetContactListsFiltered()
        {
            //Arrange

            var id = Guid.NewGuid();
            var items = new List<ContactListItemFiltered>
            {
                new ContactListItemFiltered {Id = id , Name = "Test0"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test1"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test2"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test3"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test4"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test5"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test6"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test7"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test8"},
                new ContactListItemFiltered {Id = Guid.NewGuid(), Name = "Test9"}
            };

            IPageResult<ContactListItemFiltered> templateListItems = new PageResult<ContactListItemFiltered>(items, items.Count());

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactListFiltered>(), CancellationToken.None))
                .Returns((GetContactListFiltered q, CancellationToken t) => Task.FromResult(templateListItems));

            //Act
            var result = await Create().GetContactListsFiltered(
                new GetContactListFiltered(1, 10, string.Empty),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var contactList = result.Items.First(t => t.Id == id);

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(result.Items.Count(), 10);
            Assert.AreEqual(contactList.Id, id);
            Assert.AreEqual(contactList.Name, "Test0");
        }

        [TestMethod]
        public async Task CanCreateGlobalContactList()
        {
            //Arrange

            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<CreateGlobalContactListCmd>(), CancellationToken.None))
                .Returns((CreateGlobalContactListCmd q, CancellationToken t) => Task.FromResult(new Guid?(id)));


            //Act
            var apiResult = await Create().CreateContactList(
                new CreateGlobalContactListCmd
                {
                    Name = "Contact list one"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// add guests in guest list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanAddGuestsInGuestList()
        {
            // Arrange

            var listId = Guid.NewGuid();
            var guestIds = new List<Guid> { Guid.NewGuid() };

            _mediator
                .Setup(x => x.Send(It.IsAny<AddGuestsInGuestListCmd>(), CancellationToken.None))
                .Returns((AddGuestsInGuestListCmd q, CancellationToken t) => Task.FromResult(listId));

            // Act

            var apiResult = await Create().AddGuestsInGuestList(
                new AddGuestsInGuestListCmd
                {
                    ListId = listId,
                    GuestIds = guestIds
                },
                CancellationToken.None);

            // Assert

            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, listId);
        }


        /// <summary>
        /// can create event guest list, check if result are equal and is not null, 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateEventGuestList()
        {
            //Arrange

            var id = Guid.NewGuid();
            var eventId = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<CreateGuestListCmd>(), CancellationToken.None))
                .Returns((CreateGuestListCmd q, CancellationToken t) => Task.FromResult(new Guid?(id)));

            //Act
            var apiResult = await Create().CreateEventGuestList(
                new CreateGuestListCmd("Contact list one", eventId)
                {
                },
                CancellationToken.None); ;

            //Assert

            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }


        [TestMethod]
        public async Task CanCreateGlobalContact()
        {
            //Arrange

            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<CreateContactCmd>(), CancellationToken.None))
                .Returns((CreateContactCmd q, CancellationToken t) => Task.FromResult(new Guid?(id)));


            //Act
            var apiResult = await Create().CreateContact(
                new CreateContactCmd
                {
                    Email = "test@one.com",
                    FullName = "Test name",
                    MobileNumber = "234234"
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// Can delete contact list by Id, check is result not null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteContactList()
        {
            //Arrange

            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<DeleteContactListCmd>(), CancellationToken.None))
                .Returns((DeleteContactListCmd q, CancellationToken t) => Task.FromResult(new Guid?(id)));


            //Act
            var apiResult = await Create().DeleteContactList(
                new DeleteContactListCmd
                {
                    Id = id
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// can delete contacts by ids
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteContacts()
        {
            //Arrange
            var ids = Guid.NewGuid().Enumerate();

            _mediator
                .Setup(x => x.Send(It.IsAny<DeleteContactCmd>(), CancellationToken.None))
                .Returns((DeleteContactCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            //Act
            var apiResult = await Create().DeleteContact(
                new DeleteContactCmd
                {
                    Ids = ids.ToList()
                },
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, MediatR.Unit.Value);
        }

        [TestMethod]
        public async Task CanCheckEmailUniqueness()
        {
            //Arrange

            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<IsEmailUnique>(), CancellationToken.None))
                .Returns((IsEmailUnique q, CancellationToken t) => Task.FromResult(true));


            //Act
            var apiResult = await Create().CheckEmailUniqueness(
                new IsEmailUnique("test@test.com"),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, true);
        }

        [TestMethod]
        public async Task CheckPhoneUniqueness()
        {
            //Arrange

            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<IsPhoneUnique>(), CancellationToken.None))
                .Returns((IsPhoneUnique q, CancellationToken t) => Task.FromResult(true));


            //Act
            var apiResult = await Create().CheckPhoneUniqueness(
                new IsPhoneUnique("+966514578962"),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, true);
        }

        /// <summary>
        /// check RenameContactList method, check result is not null and equal to request Id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanRenameContactList()
        {
            // Arrange
            var id = Guid.NewGuid();

            _mediator
                .Setup(x => x.Send(It.IsAny<RenameGuestListCmd>(), CancellationToken.None))
                .Returns((RenameGuestListCmd q, CancellationToken t) => Task.FromResult(id));

            // Act
            var apiResult = await Create().RenameContactList(
                new RenameGuestListCmd
                {
                    Id = id,
                    Name = "Name"
                },
                CancellationToken.None);

            // Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult, id);
        }

        /// <summary>
        /// loads list name, checks result is not null and equal to provided name
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetListName()
        {
            // Arrange
            var id = Guid.NewGuid();

            var listName = new ContactListName
            {
                IsLive = true,
                ListName = "123"
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactListName>(), CancellationToken.None))
                .Returns((GetContactListName q, CancellationToken t) => Task.FromResult(listName));

            // Act
            var apiResult = await Create().GetListName(
                new GetContactListName(Guid.NewGuid()),
                CancellationToken.None);

            // Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.ListName, listName.ListName);
            Assert.AreEqual(apiResult.IsLive, listName.IsLive);
        }

        /// <summary>
        /// Exports data, checks result is not null and is an excel file
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanExportGuestList()
        {
            // Arrange
            var id = Guid.NewGuid();

            var list = new List<IExportModel>();
            list.Add(new GuestExportModel
            {
                FullName = "123"
            });

            var resp = new ContactsExportModel
            {
                Contacts = new List<GuestExportModel>(),
                ContactCustomFields = new List<ContactsExportModel.ContactCustomFieldModel>(),
                CustomFields = new List<ContactsExportModel.ContactFieldModel>()
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactsForExport>(), CancellationToken.None))
                .Returns((GetContactsForExport q, CancellationToken t) => Task.FromResult(resp));

            // Act
            var apiResult = await Create().ExportContacts(
                new GetContactsForExport
                {
                    Filename = "123",
                    Columns = new string[] { "FullName" }
                },
                CancellationToken.None);

            // Assert
            Assert.IsNotNull(apiResult);
            Assert.AreEqual(apiResult.ContentType, "application/excel");
            Assert.AreEqual(apiResult.FileDownloadName, "123.xlsx");
        }

        /// <summary>
        /// unit test to get all contacts
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetAllContacts()
        {
            //Arrange
            var id = Guid.NewGuid();
            var items = new List<AllContactItem>
            {
                new AllContactItem
                {
                    Id = id,
                    FullName = "Test1",
                    Email = "email",
                    Phone = "phone",
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow
                },
                new AllContactItem
                {
                    Id = Guid.NewGuid(),
                    FullName = "test2",
                    Email = "email2",
                    Phone = "phone2",
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow
                },
            };

            IPageResult<AllContactItem> listItems = new PageResult<AllContactItem>(items, 10);

            _mediator
                .Setup(x => x.Send(It.IsAny<GetAllContacts>(), CancellationToken.None))
                .Returns((GetAllContacts q, CancellationToken t) => Task.FromResult(listItems));

            //Act
            var result = await Create().GetAllContacts(
                new GetAllContacts(1, 10, string.Empty),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var contact = result.Items.First(t => t.Id == id);

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(contact.Id, id);
            Assert.AreEqual(contact.FullName, items[0].FullName);
            Assert.AreEqual(contact.Email, items[0].Email);
            Assert.AreEqual(contact.Phone, items[0].Phone);
        }

        [TestMethod]
        public async Task CanGetContactListItemByListId()
        {
            //Arrange
            var id = Guid.NewGuid();
            var items = new List<ContactListItemByListId>
            {
                new ContactListItemByListId
                {
                    Id = id,
                    FullName = "Test1",
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow
                },
                new ContactListItemByListId
                {
                    Id = id,
                    FullName = "Test2",
                    CreatedOn = DateTime.UtcNow,
                    ModifiedOn = DateTime.UtcNow
                },
            };

            IPageResult<ContactListItemByListId> templateListItems = new PageResult<ContactListItemByListId>(items, 10);

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactListByListId>(), CancellationToken.None))
                .Returns((GetContactListByListId q, CancellationToken t) => Task.FromResult(templateListItems));

            //Act
            var result = await Create().GetContactListItemByListId(
                new GetContactListByListId(1, 10, string.Empty, id),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);

            var contactList = result.Items.First(t => t.Id == id);

            Assert.AreEqual(result.TotalCount, 10);
            Assert.AreEqual(contactList.Id, id);
            Assert.AreEqual(contactList.FullName, items[0].FullName);
        }

        /// <summary>
        /// unit test to remove contacts from list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanRemoveContactFromList()
        {
            //Arrange
            var id = Guid.NewGuid();
            var cmd = new RemoveContactsFromContactListCmd
            {
                ContactListId = id,
                ContactIds = Guid.NewGuid().Enumerate()
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<RemoveContactsFromContactListCmd>(), CancellationToken.None))
                .Returns((RemoveContactsFromContactListCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            //Act
            var result = await Create().RemoveContactFromList(cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, MediatR.Unit.Value);
        }

        /// <summary>
        /// unit test to fetch unlocked guest lists
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanFetchUnlockedLists()
        {
            //Arrange
            IEnumerable<EventGuestReadModel> response = new List<EventGuestReadModel>
            {
                new EventGuestReadModel
                {
                    Id = Guid.NewGuid(),
                    Value = "123"
                }
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<GetEventGuestListsQuery>(), CancellationToken.None))
                .Returns((GetEventGuestListsQuery q, CancellationToken t) => Task.FromResult(response));

            //Act
            var result = await Create().LoadUnlockedLists(Guid.NewGuid(),
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(response.Count(), result.Count());
            Assert.AreEqual(response.First().Id, result.First().Id);
            Assert.AreEqual(response.First().Value, result.First().Value);
        }

        /// <summary>
        /// unit test to fetch contacts with custom fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanFetchContactsWithCustomFields()
        {
            //Arrange
            Guid id = Guid.NewGuid();
            IEnumerable<ContactWithCustomFields> response = new List<ContactWithCustomFields>
            {
                new ContactWithCustomFields
                {
                    Id = id,
                    FullName = "Test1",
                    Email = "saif.rehman@takamul.net.sa",
                    Gender = Domain.Entities.Gender.Male
                },
                new ContactWithCustomFields
                {
                    Id = Guid.NewGuid(),
                    FullName = "Test2",
                    Email = "faisal.basheer@takamul.net.sa",
                    Gender = Domain.Entities.Gender.Male
                },
            };

            IPageResult<ContactWithCustomFields> list = new PageResult<ContactWithCustomFields>(response, 10);

            _mediator
                .Setup(x => x.Send(It.IsAny<GetContactsWithCustomFieldsQuery>(), CancellationToken.None))
                .Returns((GetContactsWithCustomFieldsQuery q, CancellationToken t) => Task.FromResult(list));

            //Act
            var result = await Create().FetchContactsWithCustomFields(
                new GetContactsWithCustomFieldsQuery(1, 1, string.Empty, Guid.NewGuid()),
                    CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(response.Count(), result.Items.Count());
            Assert.AreEqual(response.First().Id, result.Items.First().Id);
            Assert.AreEqual(response.First().FullName, result.Items.First().FullName);
            Assert.AreEqual(response.First().Email, result.Items.First().Email);
            Assert.AreEqual(response.First().Gender, result.Items.First().Gender);
        }

        /// <summary>
        /// unit test to append contacts to the list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanAppendContactsToList()
        {
            //Arrange
            _mediator
                .Setup(x => x.Send(It.IsAny<AppendContactsToListCmd>(), CancellationToken.None))
                .Returns((AppendContactsToListCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            //Act
            try
            {
                await Create().AppendContactsToList(new AppendContactsToListCmd(),
                    CancellationToken.None);
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail("Expected no exception, got: ", ex.Message);
            }
        }

        /// <summary>
        /// append one list to another list
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanAppendListToList()
        {
            //Arrange
            _mediator
                .Setup(x => x.Send(It.IsAny<AppendListToListCmd>(), CancellationToken.None))
                .Returns((AppendListToListCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            //Act
            try
            {
                await Create().AppendListToList(new AppendListToListCmd
                {
                    ToListId = Guid.NewGuid(),
                    FromListId = Guid.NewGuid()
                },
                    CancellationToken.None);
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail("Expected no exception, got: ", ex.Message);
            }
        }

        /// <summary>
        /// unit test to update contacts bulk
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateContacts()
        {
            //Arrange
            _mediator
                .Setup(x => x.Send(It.IsAny<UpdateContactCmd>(), CancellationToken.None))
                .Returns((UpdateContactCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            //Act
            try
            {
                var result = await Create().UpdateContacts(new UpdateContactCmd
                {

                    Id = Guid.NewGuid(),
                    FullName = "saif ur rehman"
                },
                CancellationToken.None);

                Assert.AreEqual(result, MediatR.Unit.Value);
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail("Expected no exception, got: ", ex.Message);
            }
        }
        private ContactController Create()
        {
            return new ContactController(_mediator.Object, _phoneValidator.Object, _excelGenerator.Object);
        }
    }
}
