﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.WebApi.TenantControllers;
using System;
using Irms.Application.RfiForms.Commands;
using Irms.Data.Read.RfiForm.Queries;
using Irms.Application.Contact.Commands;

namespace Irms.Tests.Unit.Controller.AdminControllers
{
    [TestClass]
    public class WebhookControllerTests
    {

        private Mock<IMediator> _mediator;

        [TestInitialize]
        public void Init()
        {
            _mediator = new Mock<IMediator>();
        }

        /// <summary>
        /// unit test for create/update campaign RFI form
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanPushTwilioEvent()
        {
            //Arrange
            _mediator
                .Setup(x => x.Send(It.IsAny<TwilioWebhookSmsCmd>(), CancellationToken.None))
                    .Returns((TwilioWebhookSmsCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            var sut = new WebhookController(_mediator.Object);

            //Act
            var cmd = new TwilioWebhookSmsCmd
            {
                ApiVersion = "2010-04-01",
                AccountSid = "asdoighas;dgkasdhg;alsdkghas;ldgkhasdgk;lhgkas;dglhaslkdgasd;g",
                Body = null,
                From = "+1902395712035972",
                To = "+91285309851023598",
                MessageSid = "a;sdlfjkal;sdkfjaskl;dfkjsadf;lasjdfk;lsadfjas;dfjsdakf",
                MessageStatus = "delivered",
                SmsSid = "asdfasdkl;fja;sdlfkjasd;fjasdlkfjasd;flkasdjg;gasasdfgj",
                SmsStatus = "delivered"
            };

            var result = await sut.TwilioEvent(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, MediatR.Unit.Value);
        }


        /// <summary>
        /// unit test for get whatsapp bot response
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetTwilioWhatsappBotResponse()
        {
            //Arrange
            _mediator
                .Setup(x => x.Send(It.IsAny<TwilioWhatsappBotResponseCmd>(), CancellationToken.None))
                    .Returns((TwilioWhatsappBotResponseCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            var sut = new WebhookController(_mediator.Object);

            //Act
            var cmd = new TwilioWhatsappBotResponseCmd
            {
                ApiVersion = "2010-04-01",
                AccountSid = "asdoighas;dgkasdhg;alsdkghas;ldgkhasdgk;lhgkas;dglhaslkdgasd;g",
                Body = null,
                From = "+1902395712035972",
                To = "+91285309851023598",
                MessageSid = "a;sdlfjkal;sdkfjaskl;dfkjsadf;lasjdfk;lsadfjas;dfjsdakf",
            };

            var result = await sut.TwilioWhatsappBotResponse(cmd, CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, MediatR.Unit.Value);
        }

        /// <summary>
        /// unit test to push sendgrid webhook event
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanPushSendgridEvent()
        {
            //Arrange
            var id = Guid.NewGuid();

            var model = new[]
            {
                new WebhookSendgridModel
                {
                email = "saif.rehman@takamul.net.sa",
                @event = "testeve",
                ip = "192.168.1.1",
                response = "testress",
                sg_event_id = "vis3284932k",
                sg_message_id = "eEKDAYcOT5mjJ5LLdIOZsA",
                smtpid = "testst",
                timestamp = 1524042,
                tls = 0
                }
            };

            _mediator
                .Setup(x => x.Send(It.IsAny<SendgridWebhookEmailCmd>(), CancellationToken.None))
                    .Returns((SendgridWebhookEmailCmd q, CancellationToken t) => Task.FromResult(MediatR.Unit.Value));

            var sut = new WebhookController(_mediator.Object);

            //Act
            var result = await sut.SendgridEvent(
                model,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result, MediatR.Unit.Value);
        }
    }
}