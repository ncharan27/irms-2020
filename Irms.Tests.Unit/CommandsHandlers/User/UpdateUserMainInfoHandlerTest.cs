﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.UsersInfo.CommandHandlers;
using Irms.Application.UsersInfo.Commands;
using Irms.Application.UsersInfo.Events;
using Irms.Domain.Entities;
using Irms.Tests.Unit.ValidData;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.User
{
    [TestClass]
    public class UpdateUserMainInfoHandlerTest
    {
        [TestMethod]
        public async Task CanHandle()
        {
            var mediator = new Mock<IMediator>();
            var currentUser = new Mock<ICurrentUser>();
            var userRepository = new Mock<IUserInfoRepository<UserInfo, Guid>>();
            var mapper = new Mock<IMapper>();

            mediator
                .Setup(x => x.Publish(It.IsAny<UserInfoUpdated>(), CancellationToken.None))
                .Returns((UserInfoUpdated q, CancellationToken t) => MediatR.Unit.Task);

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            userRepository
                .Setup(x => x.Create(It.IsAny<UserInfo>(), CancellationToken.None))
                .Returns((UserInfo e, CancellationToken t) => Task.FromResult(Guid.NewGuid()));

            var validData = new EntitiesValidData();
            var expectedResult = validData.UserInfoValidData();

            mapper
                .Setup(x => x.Map<UserInfo>(It.IsAny<UpdateUserMainInfoCmd>()))
                .Returns(expectedResult);

            var userHandler = new UpdateUserMainInfoHandler(userRepository.Object, mediator.Object, mapper.Object, currentUser.Object);
            
            var cmd = validData.UpdateUserMainInfoRequestValidData();

            //Act
            var id = await userHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
        }
    }
}
