﻿using Irms.Application.Product.CommandHandlers;
using Irms.Application.Abstract.Repositories;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Product.Commands;
using System.Threading;
using Irms.Application.Product.Events;

namespace Irms.Tests.Unit.CommandsHandlers.Product
{
    [TestClass]
    public class DeleteProductsByIdsHandlerTest
    {
        [TestMethod]
        public async Task CanHandle()
        {
            var mediator = new Mock<IMediator>();
            var prodactRepository = new Mock<IProductRepository<Domain.Entities.Product, Guid>>();
            var transactionManager = new Mock<ITransactionManager>();
            var transaction = new Mock<ITransaction>();
            
            Guid[] ids = new Guid[] { Guid.NewGuid(), Guid.NewGuid() };
            IEnumerable<Guid> idsList = new List<Guid>(ids);

            mediator
                .Setup(x => x.Publish(It.IsAny<ProductCreated>(), CancellationToken.None))
                .Returns((ProductCreated q, CancellationToken t) => MediatR.Unit.Task);

            prodactRepository
                .Setup(x => x.Create(It.IsAny<Domain.Entities.Product>(), CancellationToken.None))
                .Returns((Domain.Entities.Product e, CancellationToken t) => Task.FromResult(Guid.NewGuid()));
            
            transactionManager
               .Setup(x => x.BeginTransactionAsync(CancellationToken.None))
               .Returns((CancellationToken t) => Task.FromResult(transaction.Object));

            var prodHandler = new DeleteProductsByIdsHandler(mediator.Object, prodactRepository.Object, transactionManager.Object);
            var cmd = new DeleteProductsByIdsCmd(idsList);


            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
        }

    }
}
