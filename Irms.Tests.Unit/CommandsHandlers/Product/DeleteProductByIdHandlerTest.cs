﻿using Irms.Application.Product.CommandHandlers;
using Irms.Application.Abstract.Repositories;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Product.Commands;
using System.Threading;
using Irms.Application.Product.Events;

namespace Irms.Tests.Unit.CommandsHandlers.Product
{
    [TestClass]
    public class DeleteProductByIdHandlerTest
    {
        [TestMethod]
        public async Task CanHandle()
        {
            var mediator = new Mock<IMediator>();
            var prodactRepository = new Mock<IProductRepository<Domain.Entities.Product, Guid>>();

            mediator
                .Setup(x => x.Publish(It.IsAny<ProductCreated>(), CancellationToken.None))
                .Returns((ProductCreated q, CancellationToken t) => MediatR.Unit.Task);

            var id = Guid.NewGuid();
            prodactRepository
                .Setup(x => x.Create(It.IsAny<Domain.Entities.Product>(), CancellationToken.None))
                .Returns((Domain.Entities.Product e, CancellationToken t) => Task.FromResult(id));



            var prodHandler = new DeleteProductByIdHandler(mediator.Object, prodactRepository.Object);
            var cmd = new DeleteProductByIdCmd(id);


            //Act
            var response = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(response);
        }

    }
}
