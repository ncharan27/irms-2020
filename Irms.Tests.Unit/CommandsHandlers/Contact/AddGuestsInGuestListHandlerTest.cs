﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.CommandHandlers;
using Irms.Application.Contact.Commands;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class AddGuestsInGuestListHandlerTest
    {
        [TestMethod]
        public async Task CanAddGuestsInGuestListHandler()
        {
            // Arrange

            var listId = Guid.NewGuid();

            var mediator = new Mock<IMediator>();
            var contactListToContactsRepository = new Mock<IContactListToContactsRepository<ContactListToContact, Guid>>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            contactListToContactsRepository
                .Setup(x => x.CreateRange(It.IsAny<List<ContactListToContact>>(), CancellationToken.None))
                .Returns(Task.CompletedTask);

            mediator
                .Setup(x => x.Publish(It.IsAny<AddGuestsInGuestListHandler>(), CancellationToken.None))
                .Returns(Task.CompletedTask);

            var procHandler = new AddGuestsInGuestListHandler(contactListToContactsRepository.Object, tenant, mediator.Object);
            var cmd = new AddGuestsInGuestListCmd
            {
                ListId = listId,
                GuestIds = new List<Guid>()
            };

            // Act

            var id = await procHandler.Handle(
                cmd,
                CancellationToken.None);

            // Assert

            Assert.IsNotNull(id);
            Assert.AreEqual(id, listId);
        }
    }
}
