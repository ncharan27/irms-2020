﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.CommandHandlers;
using Irms.Application.Contact.Commands;
using Irms.Application.Events.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class CreateGlobalContactHandlerTest
    {
        [TestMethod]
        public async Task CanCreateContactHandler()
        {
            // Arrange

            var mediator = new Mock<IMediator>();
            var contactListRepository = new Mock<IContactListRepository<Domain.Entities.ContactList, Guid>>();
            var contactRepository = new Mock<IContactRepository<Domain.Entities.Contact, Guid>>();
            var contactListToContactsRepository = new Mock<IContactListToContactsRepository<Domain.Entities.ContactListToContact, Guid>>();
            var mapper = new Mock<IMapper>();
            var manager = new Mock<ITransactionManager>();
            var transaction = new Mock<ITransaction>();

            mediator
                .Setup(x => x.Publish(It.IsAny<GlobalContactCreated>(), CancellationToken.None))
                .Returns((GlobalContactCreated q, CancellationToken t) => MediatR.Unit.Task);

            var expectedResult = new ContactBuilder().Build();

            mapper
                .Setup(x => x.Map<Domain.Entities.Contact>(It.IsAny<CreateContactCmd>()))
                .Returns(expectedResult);

            manager
                .Setup(x => x.BeginTransactionAsync(CancellationToken.None))
                .Returns((CancellationToken t) => Task.FromResult(transaction.Object));

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var prodHandler = new CreateContactHandler(tenant, contactListRepository.Object, contactRepository.Object, contactListToContactsRepository.Object, mediator.Object, mapper.Object, manager.Object);
            var cmd = new CreateContactCmd();


            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
        }
    }
}
