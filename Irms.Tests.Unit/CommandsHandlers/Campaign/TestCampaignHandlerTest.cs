﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.CampaignInvitations;
using Irms.Application.CampaignInvitations.Events;
using Irms.Application.Campaigns.CommandHandlers;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.Campaign
{
    [TestClass]
    public class TestCampaignHandlerTest
    {
        /// <summary>
        /// Tests command handler to set test campaign
        /// </summary>
        [TestMethod]
        public async Task CampaignTestHandlerTest()
        {
            // Arrange
            var mediator = new Mock<IMediator>();
            var notifier = new Mock<ICampaignInvitationTestNotifier>();
            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();

            notifier
                .Setup(x => x.NotifyByEmail(It.IsAny<InvitationMessage>(), CancellationToken.None))
                .Returns((InvitationMessage msg, CancellationToken t) => Task.FromResult((true, (EmailMessage)null)));

            notifier
                .Setup(x => x.NotifyBySms(It.IsAny<InvitationMessage>(), CancellationToken.None))
                .Returns((InvitationMessage msg, CancellationToken t) => Task.FromResult((true, (SmsMessage)null)));

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignSmsSent>(), CancellationToken.None))
                .Returns((CampaignSmsSent msg, CancellationToken t) => MediatR.Unit.Task);

            mediator
                .Setup(x => x.Publish(It.IsAny<TestEmailSent>(), CancellationToken.None))
                .Returns((TestEmailSent msg, CancellationToken t) => MediatR.Unit.Task);

            var emailTemplate = new CampaignEmailTemplate()
            {
                Body = "body",
                SenderEmail = "email@adsg.sdaf",
                Subject = "subject"
            };

            var smsTemplate = new CampaignSmsTemplate()
            {
                Body = "body",
                SenderName = "sender"
            };

            repo
                .Setup(x => x.GetCampaignEmailTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid msg, CancellationToken t) => Task.FromResult(emailTemplate));

            repo
                .Setup(x => x.GetCampaignSmsTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid msg, CancellationToken t) => Task.FromResult(smsTemplate));

            var cmd = new TestCampaignCmd
            {
                Id = Guid.NewGuid(),
                Emails = new List<string>(),
                PhoneNumbers = new List<string>(),
            };

            var procHandler = new TestCampaignHandler(mediator.Object, notifier.Object, repo.Object);
            // Act
            var result = await procHandler.Handle(cmd, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
