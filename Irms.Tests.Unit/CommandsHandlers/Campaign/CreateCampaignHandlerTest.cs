﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.CommandHandlers;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class CreateCampaignHandlerTest
    {
        [TestMethod]
        public async Task CanCreateCampaignHandler()
        {
            var mediator = new Mock<IMediator>();
            var repo = new Mock<ICampaignRepository<Domain.Entities.Campaign, Guid>>();
            var mapper = new Mock<IMapper>();

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignCreated>(), CancellationToken.None))
                .Returns((CampaignCreated q, CancellationToken t) => MediatR.Unit.Task);

            repo
                .Setup(x => x.CheckBOTCodeUniqueness(It.IsAny<string>(), CancellationToken.None))
                .Returns((string q, CancellationToken t) => Task.FromResult(true));

            var expectedResult = new CampaignBuilder().Build();

            mapper
                .Setup(x => x.Map<Domain.Entities.Campaign>(It.IsAny<CreateCampaignCmd>()))
                .Returns(expectedResult);

            var prodHandler = new CreateCampaignHandler(repo.Object, mediator.Object, mapper.Object);
            var cmd = new CreateCampaignCmd() { EventId = Guid.NewGuid() };

            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
            Assert.AreEqual(id, expectedResult.Id);
        }
    }
}
