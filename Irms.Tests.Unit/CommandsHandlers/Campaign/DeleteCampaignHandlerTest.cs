﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.CommandHandlers;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class DeleteCampaignHandlerTest
    {
        [TestMethod]
        public async Task CanDeleteCampaignHandler()
        {
            var mediator = new Mock<IMediator>();
            var repo = new Mock<ICampaignRepository<Domain.Entities.Campaign, Guid>>();

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignDeleted>(), CancellationToken.None))
                .Returns((CampaignDeleted q, CancellationToken t) => MediatR.Unit.Task);

            var expectedResult = new CampaignBuilder().Build();

            var prodHandler = new DeleteCampaignHandler(repo.Object, mediator.Object);
            var cmd = new DeleteCampaignCmd() { Id = Guid.NewGuid() };

            //Act
            var response = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(response);
        }
    }
}
