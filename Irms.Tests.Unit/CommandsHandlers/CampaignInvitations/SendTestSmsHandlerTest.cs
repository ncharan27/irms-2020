﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using Irms.Infrastructure.Services;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitations
{
    [TestClass]
    public class SendTestSmsHandlerTest
    {
        /// <summary>
        /// tests if it can send sms, checks if result is the same as input amount of numbers
        /// </summary>
        [TestMethod]
        public async Task CanSendTestSmsHandlerTest()
        {
            //Arrange
            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();
            var smsSender = new Mock<ISmsSender>();
            var phoneValidator = new Mock<IPhoneNumberValidator>();
            var mediator = new Mock<IMediator>();

            var validPhoneNumber = new Mock<IValidPhoneNumber>();

            var template = new CampaignSmsTemplate
            {
                Body = "123"
            };

            repo
                .Setup(x => x.GetCampaignSmsTemplateById(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(template));

            //     bool valid = true;
            //phoneValidator
            //    .Setup(x => x.Validate(It.IsAny<string>(), out valid, null))
            //    .Returns((bool isValid, IValidPhoneNumber number) => (true, validPhoneNumber.Object));

            var value = true;

            phoneValidator.Setup(x => x.Validate(It.IsAny<string>(), out value, null))
                .Returns(new ValidPhoneNumber("+923459891810"));

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignSmsSent>(), CancellationToken.None))
                .Returns((CampaignSmsSent q, CancellationToken t) => MediatR.Unit.Task);

            smsSender
                .Setup(x => x.SendSms(It.IsAny<SmsMessage>(), CancellationToken.None))
                .Returns((SmsMessage q, CancellationToken t) => Task.FromResult(true));

            var procHandler = new SendTestSmsHandler(repo.Object, mediator.Object, smsSender.Object, phoneValidator.Object);
            var cmd = new SendTestSmsCmd
            {
                InvitationId = Guid.NewGuid(),
                SmsList = new string[] { "+966556565656" }
            };

            //Act
            var result = await procHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 1);
        }
    }
}
