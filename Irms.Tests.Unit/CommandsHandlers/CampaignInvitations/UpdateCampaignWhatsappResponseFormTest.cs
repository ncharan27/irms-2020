﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.CommandsHandlers.CampaignInvitations
{
    [TestClass]
    public class UpdateCampaignWhatsappResponseFormTest
    {
        [TestMethod]
        public async Task CanUpdateCampaignWhatsappResponseFormTest()
        {
            // Arrange
            var repo = new Mock<ICampaignInvitationRepository<CampaignInvitation, Guid>>();

            var mediator = new Mock<IMediator>();

            var mapper = new Mock<IMapper>();

            var repoResult = new CampaignWhatsappTemplate
            {
                AcceptButtonText = "accept",
                AcceptHtml = "accepthtml",
                BackgroundImagePath = "path",
                Body = "body",
                CampaignInvitationId = Guid.NewGuid(),
                ProceedButtonText = "proceed",
                RejectButtonText = "reject",
                RejectHtml = "reject",
                Rsvphtml = "rsvp",
                SenderName = "sender_name",
                TemplateId = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                ThemeJson = "{}",
                WelcomeHtml = "welcome"
            };

            var config = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();

            config
                .SetupGet(x => x[It.IsAny<string>()])
                .Returns("123");

            repo
                .Setup(x => x.GetCampaignWhatsappTemplateByInvitationId(It.IsAny<Guid>(), CancellationToken.None))
                .Returns((Guid q, CancellationToken t) => Task.FromResult(repoResult));

            repo
                .Setup(x => x.CreateCampaignWhatsappTemplate(It.IsAny<CampaignWhatsappTemplate>(), CancellationToken.None));

            repo
                .Setup(x => x.UpdateCampaignWhatsappTemplate(It.IsAny<CampaignWhatsappTemplate>(), CancellationToken.None));

            mapper
                .Setup(x => x.Map<CampaignWhatsappTemplate>(It.IsAny<UpdateCampaignWhatsappResponseFormCmd>()))
                .Returns((UpdateCampaignWhatsappResponseFormCmd q) => repoResult);

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignInvitationCreatedOrUpdated>(), CancellationToken.None));

            // Act

            var result = await new UpdateCampaignWhatsappResponseFormHandler(repo.Object, mediator.Object, mapper.Object, config.Object)
                .Handle(new UpdateCampaignWhatsappResponseFormCmd
                {
                    AcceptButtonText = "accept",
                    AcceptHtml = "accepthtml",
                    BackgroundImagePath = "path",
                    CampaignInvitationId = Guid.NewGuid(),
                    ProceedButtonText = "proceed",
                    RejectButtonText = "reject",
                    RejectHtml = "reject",
                    RsvpHtml = "rsvp",
                    ThemeJson = "{}",
                    WelcomeHtml = "welcome"
                }, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
