﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.CommandHandlers;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Tests.Unit.DataBuilder.Tenant;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.CommandsHandlers.Contact
{
    [TestClass]
    public class CreateOrUpdateCampaignInvitationHandlerTest
    {
        /// <summary>
        /// unit test to create/update campaign invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateOrUpdateCampaignInvitationHandler()
        {
            var mediator = new Mock<IMediator>();
            var repo = new Mock<ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid>>();
            var mapper = new Mock<IMapper>();

            mediator
                .Setup(x => x.Publish(It.IsAny<CampaignInvitationCreatedOrUpdated>(), CancellationToken.None))
                .Returns((CampaignInvitationCreatedOrUpdated q, CancellationToken t) => MediatR.Unit.Task);

            var expectedResult = new CampaignInvitationBuilder().Build();

            mapper
                .Setup(x => x.Map<Domain.Entities.CampaignInvitation>(It.IsAny<CreateOrUpdateCampaignInvitationCmd>()))
                .Returns(expectedResult);

            var prodHandler = new CreateOrUpdateCampaignInvitationHandler(repo.Object, mediator.Object, mapper.Object);
            var cmd = new CreateOrUpdateCampaignInvitationCmd()
            {
                Id = Guid.NewGuid(),
                IsInstant = false,
                CampaignId = Guid.NewGuid(),
            };

            //Act
            var id = await prodHandler.Handle(
                cmd,
                CancellationToken.None);

            //Assert
            Assert.IsNotNull(id);
            Assert.AreEqual(id, expectedResult.Id);
        }
    }
}
