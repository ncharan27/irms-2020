﻿using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class ContactBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }

        public string Title { get; set; }
        public string FullName { get; set; }
        public string PreferredName { get; set; }
        public int GenderId { get; set; }

        public string Email { get; set; }
        public string AlternativeEmail { get; set; }

        public string MobileNumber { get; set; }
        public string SecondaryMobileNumber { get; set; }

        public string WorkNumber { get; set; }
        public string NationalityId { get; set; }
        public string PassportNumber { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }


        public bool IsGlobal { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }


        public static implicit operator Domain.Entities.Contact(ContactBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Contact Build()
        {
            return new Domain.Entities.Contact
            {
                Title = "Contact1",
                FullName = "test fullname",
                PreferredName = "test first name",
                Email = "test@email.com",
                MobileNumber = "4562147",
                CreatedOn = DateTime.UtcNow,
                IsGuest = false
            };
        }
    }
}
