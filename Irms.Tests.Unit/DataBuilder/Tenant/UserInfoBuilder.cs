﻿using Irms.Domain.Entities;
using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class UserInfoBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }

        public static implicit operator Domain.Entities.UserInfo(UserInfoBuilder instance)
        {
            return instance.Build();
        }

        public UserInfo Build()
        {
            var id = Guid.NewGuid();
            return new UserInfo
            {
                AvatarId = Guid.NewGuid(),
                BirthDate = DateTime.UtcNow.AddYears(-35),
                Email = "employee@mail.net",
                FullName = "employee name",
                Gender = (int)Gender.Male,
                Id = id,
                IsActive = true,
                IsDeleted = false,
                PassportNo = "DG5338531",
                MobileNo = "+380952045485",
                NationalId = "01234567890",
                Role = (int)RoleType.SuperAdmin,
                TenantId = Guid.NewGuid(),
                UserId = id
            };
        }

        public Data.EntityClasses.UserInfo BuildEntity()
        {
            var id = Guid.NewGuid();
            return new Data.EntityClasses.UserInfo
            {
                BirthDate = DateTime.UtcNow.AddYears(-35),
                Email = "employee@mail.net",
                FullName = "employee name",
                Id = id,
                IsActive = true,
                IsDeleted = false,
                PassportNo = "DG5338531",
                MobileNo = "+380952045485",
                NationalId = "01234567890",
                RoleId = (int)RoleType.TenantAdmin,
                TenantId = Guid.NewGuid(),
                UserId = id
            };
        }
    }
}
