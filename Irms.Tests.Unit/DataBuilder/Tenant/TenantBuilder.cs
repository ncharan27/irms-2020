﻿using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class TenantBuilder
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string LogoPath { get; set; }
        public Guid CountryId { get; set; }
        public string City { get; set; }
        public string ClientUrl { get; set; }
        public string EmailFrom { get; set; }
        public string SendGridApiKey { get; set; }
        public string TwilioAccountSid { get; set; }
        public string TwilioAuthToken { get; set; }
        public string TwilioNotificationServiceId { get; set; }
        public string HeaderPath { get; set; }
        public string FooterPath { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime UpdateDate { get; set; }
        public Guid? AdminId { get; set; }

        public static implicit operator Domain.Entities.Tenant.Tenant(TenantBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Tenant.Tenant Build()
        {
            return new Domain.Entities.Tenant.Tenant
            {
                Name = "tenant to create",
                Address = "address, Al khobar, saudi arabia",
                Description = "IT solutions",
                City = "Al khobar, saudi arabic",
                CountryId = Guid.Parse("4D6C60EF-3AC2-127E-206F-AA7F2C70A260"), //saudi arabia
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                IsActive = true,
                IsDeleted = false
            };
        }
    }
}
