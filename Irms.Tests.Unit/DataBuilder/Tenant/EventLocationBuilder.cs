﻿using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class EventLocationBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public Guid CountryId { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public static implicit operator Domain.Entities.EventLocation(EventLocationBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.EventLocation Build()
        {
            return new Domain.Entities.EventLocation
            {
                Id = Guid.NewGuid(),
                Name = "ithra",
                City = "alkhobar",
                Region = "Eastern province",
                ZipCode = "445215",
                Country = "SA",
            };
        }
    }
}
