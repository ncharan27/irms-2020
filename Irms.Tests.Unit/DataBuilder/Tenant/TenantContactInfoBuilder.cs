﻿using System;

namespace Irms.Tests.Unit.DataBuilder.Tenant
{
    public class TenantContactInfoBuilder
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }

        public static implicit operator Domain.Entities.Tenant.TenantContactInfo(TenantContactInfoBuilder instance)
        {
            return instance.Build();
        }

        public Domain.Entities.Tenant.TenantContactInfo Build()
        {
            return new Domain.Entities.Tenant.TenantContactInfo
            {
                Id = Guid.NewGuid(),
                FirstName = "Saif",
                LastName = "ur Rehman",
                Email = "test@email.com",
                MobileNo = "+966554154875",
                PhoneNo = "+966554154875"
            };
        }
    }
}
