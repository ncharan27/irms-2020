﻿using Irms.Domain.Entities;
using Irms.Domain.Entities.Templates;
using Irms.Data.EntityClasses;
using System;
using System.Collections.Generic;

namespace Irms.Tests.Unit.ValidData
{
    public class EntitiesValidData
    {
        //tenant data
        public Tenant TenantValidData()
        {
            return new Tenant()
            {
                Id = Guid.NewGuid(),
                Name = "Tenant",
                Address = "address, Al khobar, saudi arabia",
                Description = "IT solutions",
                City = "Al khobar, saudi arabic",
                CountryId = Guid.Parse("4D6C60EF-3AC2-127E-206F-AA7F2C70A260"), //saudi arabia
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                IsActive = true,
                IsDeleted = false
            };
        }

        //tenant contact info
        public TenantContactInfo TenantContactInfoValidData()
        {
            return new TenantContactInfo()
            {
                Id = Guid.NewGuid(),
                Email = "test@tenant.com",
                PhoneNo = "+966559985410",
                FirstName = "Saif",
                LastName = "ur Rehman"
            };
        }

        internal Data.EntityClasses.ServiceCatalog ServiceCatalogEntityValidData()
        {
            return new Data.EntityClasses.ServiceCatalog()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                CreatedById = Guid.NewGuid(),
                CreatedOn = DateTime.Now.AddDays(-1),
                Title = "Test service"
            };
        }
        internal Data.EntityClasses.ServiceCatalog ServiceCatalogWithFeatureEntityValidData()
        {
            var serviceCatalog = ServiceCatalogEntityValidData();
            serviceCatalog.ServiceCatalogFeature = ServiceCatalogFeatureEntityValidData(serviceCatalog.Id);
            return serviceCatalog;
        }
        internal Domain.Entities.ServiceCatalog DomainServiceCatalogEntityValidData(Guid? id = null)
        {
            return new Domain.Entities.ServiceCatalog("Test service", true, Guid.NewGuid(), id);
        }
        internal ICollection<Domain.Entities.ServiceCatalogFeature> DomainServiceCatalogFeatureEntityValidData()
        {
            var l = new List<Domain.Entities.ServiceCatalogFeature>();
            var scf = new Domain.Entities.ServiceCatalogFeature(Guid.NewGuid(), "Test feature", true, false);
            l.Add(scf);
            return l;
        }
        internal ICollection<Data.EntityClasses.ServiceCatalogFeature> ServiceCatalogFeatureEntityValidData(Guid serviceCatalogId)
        {
            var l = new List<Data.EntityClasses.ServiceCatalogFeature>();
            var scf = new Data.EntityClasses.ServiceCatalogFeature()
            {
                Id = Guid.NewGuid(),
                Title = "Test feature",
                IsActive = true,
                CreatedById = Guid.NewGuid(),
                CreatedOn = DateTime.Now.AddDays(-1),
                ServiceCatalogId = serviceCatalogId
            };
            l.Add(scf);
            return l;
        }

        //product 
        public Data.EntityClasses.Product ProductEntityValidData()
        {
            return new Data.EntityClasses.Product()
            {
                Id = Guid.NewGuid(),
                CurrencyId = 1,
                LicensePeriodDays = 30,
                Title = "product one"
            };
        }

        //subscription 
        public TenantSubscription SubscriptionEntityValidData()
        {
            return new TenantSubscription()
            {
                Id = Guid.NewGuid(),
                IsActive = false,
                ProductId = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                StartDateTime = DateTime.UtcNow,
                ExpiryDateTime = DateTime.UtcNow.AddDays(30),

            };
        }

        internal Data.EntityClasses.LicensePeriod LicesePeriodValidData(Data.EntityClasses.Product product = null)
        {
            var p = new List<Data.EntityClasses.Product>();
            if (product != null)
                p.Add(product);
            return new Data.EntityClasses.LicensePeriod
            {
                Id = 4,
                Title = "Test license",
                Days = 99,
                Product = p
            };
        }

        public Data.EntityClasses.Product ProductEntityValidData(Domain.Entities.Product productEntity = null, Data.EntityClasses.Currency currency = null)
        {
            if (productEntity == null)
            {
                productEntity = DomainProductEntityValidData();
            }
            var licensePeriod = new Data.EntityClasses.LicensePeriod { Days = productEntity.License.Days, Id = productEntity.License.Id, Title = productEntity.License.Title };
            return new Data.EntityClasses.Product()
            {
                Id = productEntity.Id,
                Title = productEntity.Title,
                Price = productEntity.Price,
                DiscountPercentage = (decimal)productEntity.DiscountPercentage,
                DiscountAmount = ((productEntity.Price / 100) * (decimal)productEntity.DiscountPercentage),
                FinalPrice = productEntity.Price - ((productEntity.Price / 100) * (decimal)productEntity.DiscountPercentage),
                CurrencyId = 1,
                IsDeleted = false,
                IsShowOnWeb = false,
                Currency = currency,
                LicensePeriod = licensePeriod
            };
        }

        public Domain.Entities.Product DomainProductEntityValidData()
        {
            return new Domain.Entities.Product(Guid.NewGuid(), "Test Product", 10, 5, false, 1, new Domain.Entities.LicensePeriod(1, "test licens", 9999));
        }

        //event valid data
        public Domain.Entities.Event DomainEventEntityValidData()
        {
            return new Domain.Entities.Event { Id = Guid.NewGuid(), Name = "testevent", StartDateTime = DateTime.UtcNow, EndDateTime = DateTime.UtcNow };
        }

        public Data.EntityClasses.Event EventEntityValidData()
        {
            return new Data.EntityClasses.Event
            {
                Id = Guid.NewGuid(),
                Name = "testevent",
                StartDateTime = DateTime.UtcNow,
                EndDateTime = DateTime.UtcNow,
                MaximumAttendees = 100,
                OverflowAttendees = 20,
                IsLimitedAttendees = true,
                CreatedOn = DateTime.UtcNow
            };
        }

        public Data.EntityClasses.EventLocation EventLocationEntityValidData()
        {
            return new Data.EntityClasses.EventLocation
            {
                Id = Guid.NewGuid(),
                Name = "alkhobar",
                Region = "khobar",
                City = "khobar",
                CreatedOn = DateTime.UtcNow
            };
        }

        public List<EventFeature> EventFeaturesValidData()
        {
            return new List<EventFeature>()
            {
                new EventFeature
                {
                Id = Guid.NewGuid(),
                ServiceCatalogFeatureId = Guid.NewGuid()
                },
                new EventFeature
                {
                Id = Guid.NewGuid(),
                ServiceCatalogFeatureId = Guid.NewGuid()
                },
                new EventFeature
                {
                Id = Guid.NewGuid(),
                ServiceCatalogFeatureId = Guid.NewGuid()
                }
            };
        }

        //guest reachability
        public Data.EntityClasses.ContactReachability GuestReachabilityEntityValidData()
        {
            return new Data.EntityClasses.ContactReachability
            {
                Id = Guid.NewGuid(),
                CreatedOn = DateTime.UtcNow
            };
        }

        //currency
        public Data.EntityClasses.Currency CurrencyEntityValidData()
        {
            return new Data.EntityClasses.Currency
            {
                Id = 1,
                Title = "UAH"
            };
        }

        public Data.EntityClasses.UserInfo UserInfoEntityValidData()
        {
            var id = Guid.NewGuid();
            return new Data.EntityClasses.UserInfo
            {
                BirthDate = DateTime.UtcNow.AddYears(-35),
                Email = "employee@mail.net",
                FullName = "employee name",
                GenderId = (int)Gender.Male,
                Id = id,
                IsActive = true,
                IsDeleted = false,
                PassportNo = "DG5338531",
                MobileNo = "+380952045485",
                NationalId = "01234567890",
                RoleId = (int)RoleType.SuperAdmin,
                TenantId = Guid.NewGuid(),
                UserId = id
            };
        }

        public Domain.Entities.UserInfo UserInfoValidData()
        {
            var id = Guid.NewGuid();
            return new Domain.Entities.UserInfo
            {
                BirthDate = DateTime.UtcNow.AddYears(-35),
                Email = "employee@mail.net",
                FullName = "employee name",
                Gender = Gender.Male,
                Id = id,
                IsActive = true,
                IsDeleted = false,
                PassportNo = "DG5338531",
                MobileNo = "+380952045485",
                NationalId = "01234567890",
                Role = RoleType.SuperAdmin,
                TenantId = Guid.NewGuid(),
                UserId = id
            };
        }
        public Application.UsersInfo.Commands.UpdateUserMainInfoCmd UpdateUserMainInfoRequestValidData()
        {
            return new Application.UsersInfo.Commands.UpdateUserMainInfoCmd
            (
                "first name",
                "last name",
                 Domain.Entities.Gender.Male,
                DateTime.Now.AddYears(-20),
                "+380972197945",
                "j.doe@example.com",
                "pasport",
                "national id",
                "search text"
            );
        }


        public Data.EntityClasses.Template TemplateEntityValidData()
        {
            return new Data.EntityClasses.Template
            {
                EmailBody = "email body",
                EmailCompatible = true,
                EmailSubject = "email subject",
                Id = Guid.NewGuid(),
                ModifiedById = Guid.NewGuid(),
                ModifiedOn = DateTime.Now.AddDays(3),
                Name = "Template Name",
                SmsCompatible = true,
                SmsText = "sms text",
                TenantId = Guid.NewGuid(),
                TypeId = (int)TemplateType.ActivationMessage
            };
        }

        public Data.EntityClasses.TemplateSelection TemplateSelectionEntityValidData()
        {
            return new Data.EntityClasses.TemplateSelection
            {
                EmailTemplateId = Guid.NewGuid(),
                Id = Guid.NewGuid(),
                IsEnabled = true,
                SmsTemplateId = Guid.NewGuid(),
                TenantId = Guid.NewGuid(),
                TypeId = (int)TemplateType.ActivationMessage
            };
        }
    }
}
