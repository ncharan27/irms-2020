﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Repositories.ServiceCatalogRepository
{
    [TestClass]
    public class ServiceCatalogRepositoryTest
    {
        [TestMethod]
        public async Task CanGetServiceCatalogById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();
            
            var serviceCatalogEntity = new EntitiesValidData().ServiceCatalogWithFeatureEntityValidData();
            var expected = new Domain.Entities.ServiceCatalog(serviceCatalogEntity.Title, serviceCatalogEntity.IsActive, serviceCatalogEntity.CreatedById.Value, serviceCatalogEntity.Id);
            expected.ServiceCatalogFeatures = new EntitiesValidData().DomainServiceCatalogFeatureEntityValidData().ToList();


            mapper
                .Setup(x => x.Map<Domain.Entities.ServiceCatalog>(It.IsAny<Data.EntityClasses.ServiceCatalog>()))
            .Returns(expected);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.ServiceCatalog.AddRangeAsync(
                    serviceCatalogEntity);

                await db.SaveChangesAsync();
                var test = await db.ServiceCatalog.FirstOrDefaultAsync(t => t.Id == serviceCatalogEntity.Id, CancellationToken.None);
                var product = await db.Product
                .Include(p => p.LicensePeriod)
                .Include(p => p.ProductServiceCatalogFeature)
                .FirstOrDefaultAsync(p => p.IsDeleted == false && p.Id == serviceCatalogEntity.Id, CancellationToken.None);
            }

            Irms.Domain.Entities.ServiceCatalog result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new Data.Repositories.ServiceCatalogRepository(db, currentUser.Object, mapper.Object);
                result = await sut.GetById(serviceCatalogEntity.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, serviceCatalogEntity.Id);
            Assert.AreEqual(result.Title, "Test service");
            Assert.IsNotNull(result.ServiceCatalogFeatures);
        }

        [TestMethod]
        public async Task CanCreateServiceCatalog()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var serviceCatalogEntity = new EntitiesValidData().DomainServiceCatalogEntityValidData();
            serviceCatalogEntity.ServiceCatalogFeatures = new EntitiesValidData().DomainServiceCatalogFeatureEntityValidData().ToList();
            var expectedResult = new Data.EntityClasses.ServiceCatalog
            {
                IsActive = true,
                Title = serviceCatalogEntity.Title,
                CreatedById = serviceCatalogEntity.CreatedById
            };

            mapper
                .Setup(x => x.Map<Data.EntityClasses.ServiceCatalog>(It.IsAny<Domain.Entities.ServiceCatalog>()))
            .Returns(expectedResult);

            //using (var db = new IrmsTenantDataContext(options, tenant))
            //{
            //    await db.ServiceCatalog.AddRangeAsync(
            //        serviceCatalogEntity);

            //    await db.SaveChangesAsync();
            //}



            Guid resultId;
            Data.EntityClasses.ServiceCatalog result;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new Data.Repositories.ServiceCatalogRepository(db, currentUser.Object, mapper.Object);
                resultId = await sut.Create(serviceCatalogEntity, CancellationToken.None);

                result = await db.ServiceCatalog.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Title, "Test service");
            Assert.IsTrue(result.IsActive == true);
        }

        [TestMethod]
        public async Task CanCreateServiceFeature()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var serviceCatalogEntity = new EntitiesValidData().ServiceCatalogWithFeatureEntityValidData();
            var serviceCatalogDomainEntity = new EntitiesValidData().DomainServiceCatalogEntityValidData(serviceCatalogEntity.Id);
            serviceCatalogDomainEntity.ServiceCatalogFeatures = new EntitiesValidData().DomainServiceCatalogFeatureEntityValidData().ToList();

            List<Data.EntityClasses.ServiceCatalogFeature> expected = new List<Data.EntityClasses.ServiceCatalogFeature>();
            expected.Add(serviceCatalogEntity.ServiceCatalogFeature.First());
            mapper
                .Setup(x => x.Map<List<Data.EntityClasses.ServiceCatalogFeature>>(It.IsAny<List<Domain.Entities.ServiceCatalogFeature>>()))
            .Returns(expected);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.ServiceCatalog.AddRangeAsync(
                    serviceCatalogEntity);

                await db.SaveChangesAsync();
            }

            Guid resultId;
            Data.EntityClasses.ServiceCatalogFeature result;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new Data.Repositories.ServiceCatalogRepository(db, currentUser.Object, mapper.Object);
                resultId = await sut.CreateServiceFeature(serviceCatalogDomainEntity, CancellationToken.None);

                result = await db.ServiceCatalogFeature.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Title, "Test feature");
            Assert.IsTrue(result.IsActive == true);
        }

        [TestMethod]
        public async Task CanDeleteServiceCatalog()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var serviceCatalogEntity = new EntitiesValidData().ServiceCatalogWithFeatureEntityValidData();


            mapper
                .Setup(x => x.Map<Data.EntityClasses.ServiceCatalog>(It.IsAny<Domain.Entities.ServiceCatalog>()));
            //.Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.ServiceCatalog.AddRangeAsync(
                    serviceCatalogEntity);

                await db.SaveChangesAsync();
            }


            Data.EntityClasses.ServiceCatalog result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new Data.Repositories.ServiceCatalogRepository(db, currentUser.Object, mapper.Object);
                await sut.Delete(serviceCatalogEntity.Id, CancellationToken.None);
                result = await db.ServiceCatalog
                    .Include(s => s.ServiceCatalogFeature)
                    .FirstOrDefaultAsync(p => p.IsActive == false && p.Id == serviceCatalogEntity.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, serviceCatalogEntity.Id);
            Assert.AreEqual(result.Title, "Test service");
            Assert.IsTrue(result.IsActive == false);
            Assert.IsNotNull(result.ServiceCatalogFeature);
            Assert.IsFalse(result.ServiceCatalogFeature.First().IsActive);
        }
    }
}
