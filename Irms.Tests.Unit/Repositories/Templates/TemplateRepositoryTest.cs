﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Data.Repositories.Templates;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using Irms.Data.EntityClasses;
using Irms.Tests.Unit.DataBuilder.Template;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Irms.Tests.Unit.ValidData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Z.EntityFramework.Plus;


namespace Irms.Tests.Unit.Repositories.Templates
{
    [TestClass]
    public class TemplateRepositoryTest
    {
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.Id = templateId;
            templateEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.TemplateTranslation.AddRangeAsync(
                    new Data.EntityClasses.TemplateTranslation
                    {
                        Id = Guid.NewGuid(),
                        EmailBody = "email body",
                        EmailSubject = "email subject",
                        LanguageId = Guid.NewGuid(),
                        SmsText = "sms text",
                        TenantId = tenant.Id,
                        TemplateId = templateId
                    });

                await db.SaveChangesAsync();
            }

            //act
            Domain.Entities.Templates.Template result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                result = await sut.GetById(templateId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, templateId);
            Assert.AreEqual(result.Name, "Template Name");
            Assert.AreEqual(result.EmailCompatible, true);
            Assert.AreEqual(result.SmsCompatible, true);
            Assert.AreEqual(result.EmailBody, "email body");
            Assert.AreEqual(result.EmailSubject, "email subject");
            Assert.AreEqual(result.SmsText, "sms text");
        }

        [TestMethod]
        public async Task CanGetById_ResultNull()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.Id = templateId;
            templateEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.SaveChangesAsync();
            }

            //act
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.GetById(Guid.NewGuid(), CancellationToken.None));
            }
        }

        public async Task CanGetEmailById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.Id = templateId;
            templateEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.TemplateTranslation.AddRangeAsync(
                    new Data.EntityClasses.TemplateTranslation
                    {
                        Id = Guid.NewGuid(),
                        EmailBody = "email body",
                        EmailSubject = "email subject",
                        LanguageId = Guid.NewGuid(),
                        SmsText = "sms text",
                        TenantId = tenant.Id,
                        TemplateId = templateId
                    });

                await db.SaveChangesAsync();
            }

            //act
            Domain.Entities.Templates.Template result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                result = await sut.GetEmailTemplate(templateId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, templateId);
            Assert.AreEqual(result.Name, "Template Name");
            Assert.AreEqual(result.EmailCompatible, true);
            Assert.IsFalse(result.SmsCompatible);
            Assert.AreEqual(result.EmailBody, "email body");
            Assert.AreEqual(result.EmailSubject, "email subject");
            Assert.IsNull(result.SmsText);
        }

        [TestMethod]
        public async Task CanCreate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.TenantId = tenant.Id;

            var employeeEntity = new EntitiesValidData().UserInfoEntityValidData();
            employeeEntity.UserId = userId;
            employeeEntity.TenantId = tenant.Id;

            var templateBuilder = new TemplateBuilder
            {
                Id = templateId,
                Name = "template to create"
            };

            var templateToCreate = templateBuilder.Build();

            var expectedResult = new EntitiesValidData().TemplateEntityValidData();
            expectedResult.Id = templateId;
            expectedResult.Name = "template to create";
            expectedResult.TenantId = tenant.Id;

            mapper
                .Setup(x => x.Map<Data.EntityClasses.Template>(It.IsAny<Domain.Entities.Templates.Template>()))
                .Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.UserInfo.AddRangeAsync(
                    employeeEntity);

                await db.SaveChangesAsync();
            }

            //act
            Guid resultId;
            Data.EntityClasses.Template result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                resultId = await sut.Create(templateToCreate, CancellationToken.None);
                result = await db.Template.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Name, "template to create");
            Assert.AreEqual(result.EmailCompatible, true);
            Assert.AreEqual(result.SmsCompatible, true);
            Assert.AreEqual(result.EmailBody, "email body");
            Assert.AreEqual(result.EmailSubject, "email subject");
            Assert.AreEqual(result.SmsText, "sms text");
        }

        [TestMethod]
        public async Task CanCreate_CurrentUserIdNull()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.TenantId = tenant.Id;

            var templateBuilder = new TemplateBuilder
            {
                Id = templateId,
                Name = "template to create"
            };

            var templateToCreate = templateBuilder.Build();

            var expectedResult = new EntitiesValidData().TemplateEntityValidData();
            expectedResult.Id = templateId;
            expectedResult.Name = "template to create";
            expectedResult.TenantId = tenant.Id;

            mapper
                .Setup(x => x.Map<Data.EntityClasses.Template>(It.IsAny<Domain.Entities.Templates.Template>()))
                .Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.SaveChangesAsync();
            }

            //act
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.Create(templateToCreate, CancellationToken.None));
            }
        }

        [TestMethod]
        public async Task CanCreate_CurrentUserIdIsIncorrect()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.TenantId = tenant.Id;

            var templateBuilder = new TemplateBuilder
            {
                Id = templateId,
                Name = "template to create"
            };

            var templateToCreate = templateBuilder.Build();

            var expectedResult = new EntitiesValidData().TemplateEntityValidData();
            expectedResult.Id = templateId;
            expectedResult.Name = "template to create";
            expectedResult.TenantId = tenant.Id;


            mapper
                .Setup(x => x.Map<Data.EntityClasses.Template>(It.IsAny<Domain.Entities.Templates.Template>()))
                .Returns(expectedResult);

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.SaveChangesAsync();
            }

            //act
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.Create(templateToCreate, CancellationToken.None));
            }
        }

        [TestMethod]
        public async Task CanUpdate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();
            var languageId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.Id = templateId;
            templateEntity.TenantId = tenant.Id;

            var employeeEntity = new EntitiesValidData().UserInfoEntityValidData();
            employeeEntity.UserId = userId;
            employeeEntity.TenantId = tenant.Id;

            var templateBuilder = new TemplateBuilder
            {
                Id = templateId,
                Name = "template updated",
                Translation = new List<Domain.Entities.Templates.TemplateTranslation>
                {
                    new Domain.Entities.Templates.TemplateTranslation(languageId, "email Subject U", "email body U", "sms text u"),
                    new Domain.Entities.Templates.TemplateTranslation(Guid.NewGuid(), "email Subject c", "email body c", "sms text c")
                }
            };

            var templateToUpdate = templateBuilder.Build();

            mapper
                .Setup(x => x.Map<Data.EntityClasses.Template>(It.IsAny<Domain.Entities.Templates.Template>()));
            //.Callback((Data.EntityClasses.Template i, Data.EntityClasses.Template e) =>
            //{
            //    e.Id = templateToUpdate.Id;
            //    e.Name = templateToUpdate.Name;
            //    e.EmailCompatible = templateToUpdate.EmailCompatible;
            //    e.SmsCompatible = templateToUpdate.SmsCompatible;
            //    e.EmailBody = templateToUpdate.EmailBody;
            //    e.EmailSubject = templateToUpdate.EmailSubject;
            //    e.SmsText = templateToUpdate.SmsText;
            //    e.TenantId = tenant.Id;
            //});

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.UserInfo.AddRangeAsync(
                    employeeEntity);

                await db.TemplateTranslation.AddRangeAsync(
                    new Data.EntityClasses.TemplateTranslation
                    {
                        Id = Guid.NewGuid(),
                        EmailBody = "email body",
                        EmailSubject = "email subject",
                        LanguageId = languageId,
                        SmsText = "sms text",
                        TenantId = tenant.Id,
                        TemplateId = templateId
                    });

                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.Template result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                await sut.Update(templateToUpdate, CancellationToken.None);
                result = await db.Template.FirstOrDefaultAsync(x => x.Id == templateId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, templateId);
            Assert.AreEqual(result.Name, "Template Name");
            Assert.AreEqual(result.EmailCompatible, true);
            Assert.AreEqual(result.SmsCompatible, true);
            Assert.AreEqual(result.EmailBody, "email body");
            Assert.AreEqual(result.EmailSubject, "email subject");
            Assert.AreEqual(result.SmsText, "sms text");
        }

        [TestMethod]
        public async Task CanDelete()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.Id = templateId;
            templateEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.TemplateTranslation.AddRangeAsync(
                    new Data.EntityClasses.TemplateTranslation
                    {
                        Id = Guid.NewGuid(),
                        EmailBody = "email body",
                        EmailSubject = "email subject",
                        LanguageId = Guid.NewGuid(),
                        SmsText = "sms text",
                        TenantId = tenant.Id,
                        TemplateId = templateId
                    });

                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.Template result;
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                BatchDeleteManager.InMemoryDbContextFactory = () => db;
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                await sut.Delete(templateId, CancellationToken.None);
                result = await db.Template.FirstOrDefaultAsync(x => x.Id == templateId, CancellationToken.None);
            }

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task CanDelete_IdIsIncorrect()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();

            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var templateId = Guid.NewGuid();

            var templateEntity = new EntitiesValidData().TemplateEntityValidData();
            templateEntity.Id = templateId;
            templateEntity.TenantId = tenant.Id;

            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                await db.Template.AddRangeAsync(
                    templateEntity);

                await db.SaveChangesAsync();
            }

            //act
            using (var db = new IrmsTenantDataContext(options, tenant))
            {
                var sut = new TemplateRepository(db, mapper.Object, tenant, currentUser.Object);
                await Assert.ThrowsExceptionAsync<IncorrectRequestException>(() => sut.Delete(Guid.NewGuid(), CancellationToken.None));
            }
        }
    }
}
