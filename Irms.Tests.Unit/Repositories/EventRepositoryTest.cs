﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using Irms.Tests.Unit.ValidData;
using System.Linq;
using System.Collections.Generic;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class EventRepositoryTest
    {
        /// <summary>
        /// can get event by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.Event, Event>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var @event = new EntitiesValidData().EventEntityValidData();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.Event.AddRangeAsync(@event);
                await db.SaveChangesAsync();
            }

            //act
            Event result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new EventRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetById(@event.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Id);
            Assert.AreEqual(result.Name, "testevent");
            Assert.AreEqual(result.IsLimitedAttendees, true);
            Assert.AreEqual(result.MaximumAttendees, 100);
            Assert.AreEqual(result.OverflowAttendees, 20);
        }
        /// <summary>
        /// can create event with location
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateEvent()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Event, Data.EntityClasses.Event>();
                cfg.CreateMap<EventLocation, Data.EntityClasses.EventLocation>()
                .ForMember(x => x.Country, x => x.Ignore());

            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var eventBuilder = new EventBuilder();
            var eventToCreate = eventBuilder.Build();
            eventToCreate.Location = new EventLocationBuilder().Build();
            eventToCreate.Id = id;
            eventToCreate.TenantId = tenant.Id;
            eventToCreate.Location.TenantId = tenant.Id;
            eventToCreate.Location.EventId = id;

            using (var db = new IrmsDataContext(options))
            {
                var cid = await db.Country.AddAsync(new Data.EntityClasses.Country { Id = Guid.NewGuid(), ShortIso = "SA" });
                await db.SaveChangesAsync();
            }

            //act
            Guid resultId;
            Data.EntityClasses.Event result;
            Data.EntityClasses.EventLocation eventLocation;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new EventRepository(db, mapper, currentUser.Object, tenant);
                resultId = await sut.Create(eventToCreate, CancellationToken.None);
                result = await db.Event.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
                eventLocation = await db.EventLocation.FirstOrDefaultAsync(x => x.EventId == resultId, CancellationToken.None);
            }

            //assert
            //event
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Name, "event to create");
            Assert.AreEqual(result.IsLimitedAttendees, true);
            Assert.AreEqual(result.MaximumAttendees, 200);
            Assert.AreEqual(result.OverflowAttendees, 50);

            //event location
            Assert.AreEqual(eventLocation.Name, "ithra");
            Assert.AreEqual(eventLocation.City, "alkhobar");
            Assert.AreEqual(eventLocation.Region, "Eastern province");
            Assert.AreEqual(eventLocation.ZipCode, "445215");
        }

        /// <summary>
        /// can update event basic info
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateEventInfo()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var eventEntity = new EntitiesValidData().EventEntityValidData();
            eventEntity.Id = id;
            eventEntity.TenantId = tenant.Id;

            var elEntity = new EntitiesValidData().EventLocationEntityValidData();
            elEntity.EventId = id;
            elEntity.TenantId = tenant.Id;

            var eventToUpdate = new EventBuilder().Build();
            eventToUpdate.Id = id;
            eventToUpdate.TenantId = tenant.Id;
            eventToUpdate.Location = new EventLocationBuilder().Build();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.Event>(It.IsAny<Domain.Entities.Event>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.Country.AddAsync(new Data.EntityClasses.Country { Id = Guid.NewGuid(), ShortIso = "SA" });
                await db.Event.AddRangeAsync(eventEntity);
                await db.EventLocation.AddRangeAsync(elEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.Event result;
            Data.EntityClasses.EventLocation location;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new EventRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Update(eventToUpdate, CancellationToken.None);
                result = await db.Event.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
                location = await db.EventLocation.FirstOrDefaultAsync(x => x.EventId == id, CancellationToken.None);
            }


            //assert
            //tenant
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, "testevent");
            Assert.AreEqual(result.IsLimitedAttendees, true);
            Assert.AreEqual(result.MaximumAttendees, 100);
            Assert.AreEqual(result.OverflowAttendees, 20);

            //event location
            Assert.AreEqual(location.Name, "alkhobar");
            Assert.AreEqual(location.City, "khobar");
            Assert.AreEqual(location.Region, "khobar");
            Assert.AreEqual(location.ZipCode, null);
        }

        /// <summary>
        /// event feature update test
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateEventFeatures()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var features = new EntitiesValidData().EventFeaturesValidData();
            features.First().EventId = id;
            features.Last().EventId = id;

            var elEntity = new EntitiesValidData().EventLocationEntityValidData();
            elEntity.EventId = id;

            var eventToUpdate = new EventBuilder().Build();
            eventToUpdate.Id = id;

            using (var db = new IrmsDataContext(options))
            {
                await db.EventFeature.AddRangeAsync(features);
                await db.SaveChangesAsync();
            }

            //act
            IEnumerable<Data.EntityClasses.EventFeature> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new EventRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.UpdateFeatures(eventToUpdate, CancellationToken.None);
                result = await db.EventFeature.Where(x => x.EventId == id).ToListAsync();
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 1);
            Assert.AreEqual(result.First().EventId, id);
            Assert.AreEqual(result.Last().EventId, id);
        }

        /// <summary>
        /// can delete event by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteEventById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var eventEntity = new EntitiesValidData().EventEntityValidData();

            using (var db = new IrmsDataContext(options))
            {
                await db.Event.AddRangeAsync(
                    eventEntity);
                await db.SaveChangesAsync();
            }

            Data.EntityClasses.Event result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new EventRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Delete(eventEntity.Id, CancellationToken.None);
                result = await db.Event.FirstOrDefaultAsync(p => p.IsDeleted == true && p.Id == eventEntity.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, eventEntity.Id);
            Assert.AreEqual(result.Name, "testevent");
            Assert.AreEqual(result.MaximumAttendees, 100);
            Assert.AreEqual(result.OverflowAttendees, 20);
            Assert.IsTrue(result.IsLimitedAttendees == true);
            Assert.IsTrue(result.IsDeleted == true);
        }
    }
}
