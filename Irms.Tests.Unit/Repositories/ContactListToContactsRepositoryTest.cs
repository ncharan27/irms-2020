﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Data.EntityClasses;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using Irms.Infrastructure.Services.ExcelExport;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class ContactListToContactsRepositoryTest 
    {
        [TestMethod]
        public async Task CanCreateContactListToContact()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactBuilder = new ContactListToContactsBuilder();
            contactBuilder.ContactId = Guid.NewGuid();
            contactBuilder.ContactListId = Guid.NewGuid();
            var contactToCreate = contactBuilder.Build();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactListToContact, ContactListToContacts>();
            });

            var mapper = config.CreateMapper();

            // Act

            Guid resultGuid;
            ContactListToContacts resultContact;
            using(var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                resultGuid = await sut.Create(contactToCreate, CancellationToken.None);
                resultContact = await db.ContactListToContacts.FirstOrDefaultAsync(x => x.Id == resultGuid, CancellationToken.None);
            }

            // Assert

            Assert.IsNotNull(resultContact);
            Assert.AreEqual(resultContact.Id, resultGuid);
            Assert.AreEqual(resultContact.ContactId, contactBuilder.ContactId);
            Assert.AreEqual(resultContact.ContactListId, contactBuilder.ContactListId);
        }

        [TestMethod]
        public async Task CanCreateRangeContactListsToContact()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactBuilder = new ContactListToContactsBuilder();
            contactBuilder.ContactId = Guid.NewGuid();
            contactBuilder.ContactListId = Guid.NewGuid();
            var contactToCreate = contactBuilder.Build();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactListToContact, ContactListToContacts>();
            });

            var mapper = config.CreateMapper();

            // Act

            ContactListToContacts resultContact;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                await sut.CreateRange(new List<ContactListToContact> { contactToCreate }, CancellationToken.None);
                resultContact = await db.ContactListToContacts.FirstOrDefaultAsync(x => x.Id == contactToCreate.Id, CancellationToken.None);
            }

            // Assert

            Assert.IsNotNull(resultContact);
            Assert.AreEqual(resultContact.Id, contactToCreate.Id);
            Assert.AreEqual(resultContact.ContactId, contactBuilder.ContactId);
            Assert.AreEqual(resultContact.ContactListId, contactBuilder.ContactListId);
        }

        [TestMethod]
        public async Task CanDeleteRangeByIds()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactBuilder = new ContactListToContactsBuilder();
            contactBuilder.ContactId = Guid.NewGuid();
            contactBuilder.ContactListId = Guid.NewGuid();
            var contactToCreate = contactBuilder.Build();
            contactToCreate.TenantId = tenant.Id;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactListToContact, ContactListToContacts>();
            });

            var mapper = config.CreateMapper();


            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                await sut.Create(contactToCreate, CancellationToken.None);
            }

            // Act

            ContactListToContacts resultContact;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                await sut.DeleteRangeByIds(new List<Guid> { contactToCreate.Id }, CancellationToken.None);
                resultContact = await db.ContactListToContacts
                    .FirstOrDefaultAsync(x => x.ContactId == contactToCreate.ContactId
                                       && x.ContactListId == contactToCreate.ContactListId);
            }


            // Assert

            Assert.IsNull(resultContact);
        }

        [TestMethod]
        public async Task CanDeleteRangeContactsFromContactList()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactBuilder = new ContactListToContactsBuilder();
            contactBuilder.ContactId = Guid.NewGuid();
            contactBuilder.ContactListId = Guid.NewGuid();
            var contactToCreate = contactBuilder.Build();
            contactToCreate.TenantId = tenant.Id;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactListToContact, ContactListToContacts>();
            });

            var mapper = config.CreateMapper();


            using(var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                await sut.Create(contactToCreate, CancellationToken.None);
            }

            // Act

            ContactListToContacts resultContact;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                await sut.DeleteRangeFromContactList(new List<Guid> { contactToCreate.ContactId}, contactToCreate.ContactListId, CancellationToken.None);
                resultContact = await db.ContactListToContacts
                    .FirstOrDefaultAsync(x => x.ContactId == contactToCreate.ContactId 
                                       && x.ContactListId == contactToCreate.ContactListId);
            }


            // Assert

            Assert.IsNull(resultContact);
        }

        [TestMethod]
        public async Task CanGetAllForContactList()
        {
            // Arrange

            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactBuilder = new ContactListToContactsBuilder();
            contactBuilder.ContactId = Guid.NewGuid();
            contactBuilder.ContactListId = Guid.NewGuid();
            var contactToCreate = contactBuilder.Build();
            contactToCreate.TenantId = tenant.Id;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactListToContact, ContactListToContacts>();
                cfg.CreateMap<ContactListToContacts, ContactListToContact>();
            });

            var mapper = config.CreateMapper();


            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                await sut.Create(contactToCreate, CancellationToken.None);
            }

            // Act
            IEnumerable<ContactListToContact> resultContacts;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactListToContactsRepository(db, mapper, currentUser.Object, tenant);
                resultContacts = await sut.GetAllForContactList(contactToCreate.ContactListId, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(resultContacts);
            Assert.AreEqual(contactToCreate.Id, resultContacts.First().Id);

        }
    }
}
