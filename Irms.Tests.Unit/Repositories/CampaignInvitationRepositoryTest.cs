﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using Irms.Application.Campaigns.Commands;
using System.Collections.Generic;
using System.Linq;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class CampaignInvitationRepositoryTest
    {
        /// <summary>
        /// unit test to get invitation by id...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>()
                    .ForMember(x => x.InvitationType, x => x.MapFrom(m => m.InvitationTypeId));
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var campaign = new CampaignInvitationBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            campaign.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignInvitation.AddRangeAsync(campaign);
                await db.SaveChangesAsync();
            }

            //act
            CampaignInvitation result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetById(campaign.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Id);
            Assert.AreEqual(result.IsInstant, false);
            Assert.AreEqual(result.InvitationType, InvitationType.Rsvp);
        }

        /// <summary>
        /// unit test to delete campaign invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDelete()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>()
                    .ForMember(x => x.InvitationType, x => x.MapFrom(m => m.InvitationTypeId));
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var campaign = new CampaignInvitationBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            campaign.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignInvitation.AddRangeAsync(campaign);
                await db.SaveChangesAsync();
            }

            //act
            CampaignInvitation result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                await sut.Delete(campaign.Id, CancellationToken.None);
                result = mapper.Map<Domain.Entities.CampaignInvitation>(await db.CampaignInvitation.FirstOrDefaultAsync(x => x.Id == campaign.Id));
            }

            //assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// unit test to delete sms template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteSmsTemplate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var smsTemplate = new CampaignSmsTemplateBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            smsTemplate.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignSmstemplate.AddRangeAsync(smsTemplate);
                await db.SaveChangesAsync();
            }

            //act
            CampaignSmsTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                await sut.DeleteSmsTemplate(smsTemplate.Id, CancellationToken.None);
                result = mapper.Map<Domain.Entities.CampaignSmsTemplate>(await db.CampaignSmstemplate.FirstOrDefaultAsync(x => x.Id == smsTemplate.Id));
            }

            //assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// unit test to delete email template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanDeleteEmailTemplate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var emailTemplate = new CampaignEmailTemplateBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            emailTemplate.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignEmailTemplate.AddRangeAsync(emailTemplate);
                await db.SaveChangesAsync();
            }

            //act
            CampaignEmailTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                await sut.DeleteEmailTemplate(emailTemplate.Id, CancellationToken.None);
                result = mapper.Map<Domain.Entities.CampaignEmailTemplate>(await db.CampaignEmailTemplate.FirstOrDefaultAsync(x => x.Id == emailTemplate.Id));
            }

            //assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// can create campaign invitation
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateCampaignInvitation()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CampaignInvitation, Data.EntityClasses.CampaignInvitation>()
                .ForMember(x => x.InvitationTypeId, x => x.MapFrom(m => (int)m.InvitationType));
            });

            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var invitationBuilder = new CampaignInvitationBuilder();
            var invitationToCreate = invitationBuilder.Build();
            invitationToCreate.Id = id;
            invitationToCreate.TenantId = tenant.Id;

            //act
            Guid resultId;
            Data.EntityClasses.CampaignInvitation result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                resultId = await sut.Create(invitationToCreate, CancellationToken.None);
                result = await db.CampaignInvitation.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            //campaign
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.IsInstant, false);
        }

        /// <summary>
        /// unit test to update invitation...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateInvitation()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var ciEntity = new CampaignInvitationBuilder().BuildEntity();
            ciEntity.Id = id;
            ciEntity.TenantId = tenant.Id;

            var ciToUpdate = new CampaignInvitationBuilder().Build();
            ciToUpdate.Id = id;
            ciToUpdate.TenantId = tenant.Id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.EventCampaign>(It.IsAny<Domain.Entities.Campaign>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignInvitation.AddRangeAsync(ciEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.CampaignInvitation result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Update(ciToUpdate, CancellationToken.None);
                result = await db.CampaignInvitation.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.IsInstant, false);
            Assert.AreEqual(result.InvitationTypeId, (int)InvitationType.Rsvp);
        }

        /// <summary>
        /// unit test to get email template by id...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignEmailTemplateById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var template = new CampaignEmailTemplateBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            template.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignEmailTemplate.AddRangeAsync(template);
                await db.SaveChangesAsync();
            }

            //act
            CampaignEmailTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetCampaignEmailTemplateById(template.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Subject, "TestSubject");
            Assert.AreEqual(result.Preheader, "PreHeader");
            Assert.AreEqual(result.SenderEmail, "sender@email.com");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// can create campaign email template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateCampaignEmailTemplate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CampaignEmailTemplate, Data.EntityClasses.CampaignEmailTemplate>();
            });

            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var invitationToCreate = new CampaignEmailTemplateBuilder().Build();
            invitationToCreate.Id = id;
            invitationToCreate.TenantId = tenant.Id;

            //act
            Guid resultId;
            Data.EntityClasses.CampaignEmailTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                resultId = await sut.CreateCampaignEmailTemplate(invitationToCreate, CancellationToken.None);
                result = await db.CampaignEmailTemplate.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Subject, "TestSubject");
            Assert.AreEqual(result.Preheader, "PreHeader");
            Assert.AreEqual(result.SenderEmail, "sender@email.com");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// unit test to update invitation...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateCampaignEmailTemplate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var ciEntity = new CampaignEmailTemplateBuilder().BuildEntity();
            ciEntity.Id = id;
            ciEntity.TenantId = tenant.Id;

            var ciToUpdate = new CampaignEmailTemplateBuilder().Build();
            ciToUpdate.Id = id;
            ciToUpdate.TenantId = tenant.Id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.EventCampaign>(It.IsAny<Domain.Entities.Campaign>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignEmailTemplate.AddRangeAsync(ciEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.CampaignEmailTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.UpdateCampaignEmailTemplate(ciToUpdate, CancellationToken.None);
                result = await db.CampaignEmailTemplate.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Subject, "TestSubject");
            Assert.AreEqual(result.Preheader, "PreHeader");
            Assert.AreEqual(result.SenderEmail, "sender@email.com");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// unit test to get email template by id...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignSmsTemplateById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var template = new CampaignSmsTemplateBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            template.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignSmstemplate.AddRangeAsync(template);
                await db.SaveChangesAsync();
            }

            //act
            CampaignSmsTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetCampaignSmsTemplateById(template.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, template.Id);
            Assert.AreEqual(result.CampaignInvitationId, template.CampaignInvitationId);
            Assert.AreEqual(result.AcceptButtonText, "accept_text");
            Assert.AreEqual(result.ProceedButtonText, "proceed_text");
            Assert.AreEqual(result.RejectButtonText, "reject_text");
            Assert.AreEqual(result.RSVPHtml, "rsvp_html");
            Assert.AreEqual(result.SenderName, "sender_name");
            Assert.AreEqual(result.WelcomeHtml, "welcome_html");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// unit test to get email template by id...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetCampaignSmsTemplateByInvitationId()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var template = new CampaignSmsTemplateBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            template.TenantId = tenant.Id;
            template.CampaignInvitationId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignSmstemplate.AddRangeAsync(template);
                await db.SaveChangesAsync();
            }

            //act
            CampaignSmsTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetCampaignSmsTemplateByInvitationId(template.CampaignInvitationId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, template.Id);
            Assert.AreEqual(result.CampaignInvitationId, template.CampaignInvitationId);
            Assert.AreEqual(result.AcceptButtonText, "accept_text");
            Assert.AreEqual(result.ProceedButtonText, "proceed_text");
            Assert.AreEqual(result.RejectButtonText, "reject_text");
            Assert.AreEqual(result.RSVPHtml, "rsvp_html");
            Assert.AreEqual(result.SenderName, "sender_name");
            Assert.AreEqual(result.WelcomeHtml, "welcome_html");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// can create campaign sms template
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateCampaignSmsTemplate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CampaignSmsTemplate, Data.EntityClasses.CampaignSmstemplate>();
            });

            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var smsToCreate = new CampaignSmsTemplateBuilder().Build();
            smsToCreate.Id = id;

            //act
            Guid resultId;
            Data.EntityClasses.CampaignSmstemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                resultId = await sut.CreateCampaignSmsTemplate(smsToCreate, CancellationToken.None);
                result = await db.CampaignSmstemplate.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, smsToCreate.Id);
            Assert.AreEqual(result.CampaignInvitationId, smsToCreate.CampaignInvitationId);
            Assert.AreEqual(result.AcceptButtonText, "accept_text");
            Assert.AreEqual(result.ProceedButtonText, "proceed_text");
            Assert.AreEqual(result.RejectButtonText, "reject_text");
            Assert.AreEqual(result.Rsvphtml, "rsvp_html");
            Assert.AreEqual(result.SenderName, "sender_name");
            Assert.AreEqual(result.WelcomeHtml, "welcome_html");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// unit test to update sms template...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateCampaignSmsTemplate()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var ciEntity = new CampaignSmsTemplateBuilder().BuildEntity();
            ciEntity.Id = id;
            ciEntity.TenantId = tenant.Id;

            var ciToUpdate = new CampaignSmsTemplateBuilder().Build();
            ciToUpdate.Id = id;
            ciToUpdate.TenantId = tenant.Id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.EventCampaign>(It.IsAny<Domain.Entities.Campaign>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignSmstemplate.AddRangeAsync(ciEntity);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.CampaignSmstemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.UpdateCampaignSmsTemplate(ciToUpdate, CancellationToken.None);
                result = await db.CampaignSmstemplate.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, ciEntity.Id);
            Assert.AreEqual(result.CampaignInvitationId, ciEntity.CampaignInvitationId);
            Assert.AreEqual(result.AcceptButtonText, "accept_text");
            Assert.AreEqual(result.ProceedButtonText, "proceed_text");
            Assert.AreEqual(result.RejectButtonText, "reject_text");
            Assert.AreEqual(result.Rsvphtml, "rsvp_html");
            Assert.AreEqual(result.SenderName, "sender_name");
            Assert.AreEqual(result.WelcomeHtml, "welcome_html");
            Assert.AreEqual(result.Body, "template_body");
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
            Assert.AreEqual(result.ThemeJson, "theme_json");
            Assert.AreEqual(result.BackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// unit test for update campaign email response form...
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateCampaignEmailResponseForm()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var template = new CampaignEmailTemplateBuilder().BuildEntity();
            template.Id = id;
            template.TenantId = tenant.Id;

            var ciToUpdate = new CampaignEmailTemplateBuilder().BuildUpdateEmailResponse();
            ciToUpdate.Id = id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.CampaignEmailTemplate>(It.IsAny<UpdateCampaignEmailResponseFormCmd>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.CampaignEmailTemplate.AddRangeAsync(template);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.CampaignEmailTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.UpdateCampaignEmailResponseForm(ciToUpdate, CancellationToken.None);
                result = await db.CampaignEmailTemplate.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.AcceptHtml, "accept_html");
            Assert.AreEqual(result.RejectHtml, "reject_html");
        }

        /// <summary>
        /// unit test for load template groud by campaign id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanLoadInvitationTemplateGroupByCampaignId()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid campaignId = Guid.NewGuid();
            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>();
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new TenantBasicInfoBuilder().Build();

            //build entities...
            //invitation 1
            var invitation1 = new CampaignInvitationBuilder().BuildEntity();
            var emailTemplate1 = new CampaignEmailTemplateBuilder().BuildEntity();
            var smsTemplate1 = new CampaignSmsTemplateBuilder().BuildEntity();
            emailTemplate1.CampaignInvitationId = invitation1.Id;
            smsTemplate1.CampaignInvitationId = invitation1.Id;

            //invitation 2
            var invitation2 = new CampaignInvitationBuilder().BuildEntity();
            var emailTemplate2 = new CampaignEmailTemplateBuilder().BuildEntity();
            var smsTemplate2 = new CampaignSmsTemplateBuilder().BuildEntity();
            emailTemplate2.CampaignInvitationId = invitation1.Id;
            smsTemplate2.CampaignInvitationId = invitation1.Id;

            invitation1.EventCampaignId = campaignId;
            invitation2.EventCampaignId = campaignId;

            using (var db = new IrmsDataContext(options))
            {
                //add inv 1
                await db.CampaignInvitation.AddRangeAsync(invitation1);
                await db.CampaignEmailTemplate.AddRangeAsync(emailTemplate1);
                await db.CampaignSmstemplate.AddRangeAsync(smsTemplate1);

                //add inv 2
                await db.CampaignInvitation.AddRangeAsync(invitation2);
                await db.CampaignEmailTemplate.AddRangeAsync(emailTemplate2);
                await db.CampaignSmstemplate.AddRangeAsync(smsTemplate2);

                //save
                await db.SaveChangesAsync();
            }

            //act
            IEnumerable<CampaignInvitation> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.LoadInvitationTemplateGroup(campaignId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 2);

            var first = result.First();
            Assert.AreEqual(first.EventCampaignId, campaignId);
            Assert.IsNotNull(first.EmailTemplate);
            Assert.IsNotNull(first.EmailTemplate.Body);
            Assert.IsNotNull(first.SmsTemplate);
            Assert.IsNotNull(first.SmsTemplate.Body);
            Assert.AreEqual(first.Active, true);
            Assert.AreEqual(first.IsInstant, false);
            Assert.AreEqual(result.Skip(1).Take(1).First().IsInstant, false);
        }

        /// <summary>
        /// unit test to load invitation group
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanLoadInvitationGroupByCampaignId()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid campaignId = Guid.NewGuid();
            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>();
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new TenantBasicInfoBuilder().Build();

            //build entities...
            var invitation1 = new CampaignInvitationBuilder().BuildEntity();
            var invitation2 = new CampaignInvitationBuilder().BuildEntity();

            invitation1.EventCampaignId = campaignId;
            invitation2.EventCampaignId = campaignId;

            using (var db = new IrmsDataContext(options))
            {
                //add inv 1
                await db.CampaignInvitation.AddRangeAsync(invitation1);
                await db.CampaignInvitation.AddRangeAsync(invitation2);

                //save
                await db.SaveChangesAsync();
            }

            //act
            IEnumerable<CampaignInvitation> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.LoadInvitationGroup(campaignId, InvitationType.Rsvp, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 2);

            var first = result.First();
            Assert.AreEqual(first.EventCampaignId, campaignId);
            Assert.AreEqual(first.Active, true);
            Assert.AreEqual(first.IsInstant, false);
            Assert.AreEqual(result.Skip(1).Take(1).First().IsInstant, false);
        }

        [TestMethod]
        public async Task CanLoadWhatsappApprovedTemplates()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid campaignId = Guid.NewGuid();
            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>();
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>();
                cfg.CreateMap<Data.EntityClasses.WhatsappApprovedTemplate, WhatsappApprovedTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new TenantBasicInfoBuilder().Build();

            //build entities...
            var invitation1 = new CampaignInvitationBuilder().BuildEntity();
            var invitation2 = new CampaignInvitationBuilder().BuildEntity();

            invitation1.EventCampaignId = campaignId;
            invitation2.EventCampaignId = campaignId;

            using (var db = new IrmsDataContext(options))
            {
                db.WhatsappApprovedTemplate.Add(new Data.EntityClasses.WhatsappApprovedTemplate
                {
                    TenantId =tenant.Id,
                    InvitationTypeId = 0
                });

                //save
                await db.SaveChangesAsync();
            }


            //Act
            IEnumerable<WhatsappApprovedTemplate> result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.LoadWhatsappApprovedTemplates(0, CancellationToken.None);
            }


            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 1);
        }

        [TestMethod]
        public async Task GetCampaignWhatsappTemplateById()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid campaignId = Guid.NewGuid();
            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>();
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>();
                cfg.CreateMap<Data.EntityClasses.WhatsappApprovedTemplate, WhatsappApprovedTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new TenantBasicInfoBuilder().Build();

            var id = Guid.NewGuid();

            //build entities...
            var invitation1 = new CampaignInvitationBuilder().BuildEntity();
            var invitation2 = new CampaignInvitationBuilder().BuildEntity();

            invitation1.EventCampaignId = campaignId;
            invitation2.EventCampaignId = campaignId;

            using (var db = new IrmsDataContext(options))
            {
                db.CampaignWhatsappTemplate.Add(new Data.EntityClasses.CampaignWhatsappTemplate
                {
                    TenantId = tenant.Id,
                    Id = id
                });

                //save
                await db.SaveChangesAsync();
            }


            //Act
            CampaignWhatsappTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetCampaignWhatsappTemplateById(id, CancellationToken.None);
            }


            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetCampaignWhatsappTemplateByInvitationId()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid campaignId = Guid.NewGuid();
            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.CampaignInvitation, CampaignInvitation>();
                cfg.CreateMap<Data.EntityClasses.CampaignEmailTemplate, CampaignEmailTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignWhatsappTemplate, CampaignWhatsappTemplate>();
                cfg.CreateMap<Data.EntityClasses.WhatsappApprovedTemplate, WhatsappApprovedTemplate>();
                cfg.CreateMap<Data.EntityClasses.CampaignSmstemplate, CampaignSmsTemplate>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var tenant = new TenantBasicInfoBuilder().Build();

            var id = Guid.NewGuid();

            //build entities...
            var invitation1 = new CampaignInvitationBuilder().BuildEntity();
            var invitation2 = new CampaignInvitationBuilder().BuildEntity();

            invitation1.EventCampaignId = campaignId;
            invitation2.EventCampaignId = campaignId;

            using (var db = new IrmsDataContext(options))
            {
                db.CampaignWhatsappTemplate.Add(new Data.EntityClasses.CampaignWhatsappTemplate
                {
                    TenantId = tenant.Id,
                    Id = id,
                    CampaignInvitationId = id
                });

                //save
                await db.SaveChangesAsync();
            }


            //Act
            CampaignWhatsappTemplate result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new CampaignInvitationRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetCampaignWhatsappTemplateByInvitationId(id, CancellationToken.None);
            }


            //Assert
            Assert.IsNotNull(result);
        }
    }
}
