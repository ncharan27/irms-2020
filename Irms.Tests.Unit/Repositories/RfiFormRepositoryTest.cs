﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Irms.Data.Repositories;
using Irms.Domain.Entities;
using System.Linq;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class RfiFormRepositoryTest
    {
        /// <summary>
        /// can get campaign by id
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetById()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.RfiForm, RfiForm>()
                .ForMember(x => x.RfiFormQuestions, x => x.Ignore())
                .ForMember(x => x.RfiFormResponses, x => x.Ignore());
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var rfi = new RfiFormBuilder().BuildEntity();
            var tenant = TenantBasicInfoBuilder.BuildDefault();
            rfi.TenantId = tenant.Id;

            var userId = Guid.NewGuid();
            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            using (var db = new IrmsDataContext(options))
            {
                await db.RfiForm.AddRangeAsync(rfi);
                await db.SaveChangesAsync();
            }

            //act
            RfiForm result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new RfiFormRepository(db, mapper, currentUser.Object, tenant);
                result = await sut.GetById(rfi.Id, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Id);
            Assert.AreEqual(result.WelcomeHtml, "<p>welcome...</p>");
            Assert.AreEqual(result.SubmitHtml, "<button>save</button>");
            Assert.AreEqual(result.ThanksHtml, "<p>thanks...</p>");
            Assert.AreEqual(result.FormTheme, "<p>form theme...</button>");
            Assert.AreEqual(result.FormSettings, "<p>form settings</p>");
            Assert.AreEqual(result.ThemeBackgroundImagePath, "abc.png");
        }

        /// <summary>
        /// can create campaign rfi form
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanCreateRfiForm()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<RfiForm, Data.EntityClasses.RfiForm>()
                .ForMember(x => x.RfiFormQuestions, x => x.Ignore());
            });

            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var rfi = new RfiFormBuilder().Build();
            rfi.Id = id;

            //act
            Guid resultId;
            Data.EntityClasses.RfiForm result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new RfiFormRepository(db, mapper, currentUser.Object, tenant);
                resultId = await sut.Create(rfi, CancellationToken.None);
                result = await db.RfiForm.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            //campaign
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.SubmitHtml, "<button>save</button>");
            Assert.AreEqual(result.FormTheme, "<p>form theme...</button>");
            Assert.AreEqual(result.FormSettings, "<p>form settings</p>");
            Assert.AreEqual(result.ThemeBackgroundImagePath, "abc.png");

            var question = result.RfiFormQuestions.First();
            Assert.AreEqual(result.RfiFormQuestions.Count(), 2);
            Assert.AreEqual(question.Question, "emailquestion");
        }

        /// <summary>
        /// can update campaign rfi form
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateRfiForm()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            Guid id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            var mapper = new Mock<IMapper>();
            var currentUser = new Mock<ICurrentUser>();

            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var rfi = new RfiFormBuilder().BuildEntity();
            rfi.Id = id;
            rfi.TenantId = tenant.Id;

            var rfiToUpdate = new RfiFormBuilder().Build();
            rfiToUpdate.Id = id;

            currentUser
                .SetupGet(x => x.Id)
                .Returns(userId);

            mapper
                .Setup(x => x.Map<Data.EntityClasses.EventCampaign>(It.IsAny<Domain.Entities.Campaign>()));

            using (var db = new IrmsDataContext(options))
            {
                await db.RfiForm.AddRangeAsync(rfi);
                await db.SaveChangesAsync();
            }

            //act
            Data.EntityClasses.RfiForm result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new RfiFormRepository(db, mapper.Object, currentUser.Object, tenant);
                await sut.Update(rfiToUpdate, CancellationToken.None);
                result = await db.RfiForm.FirstOrDefaultAsync(x => x.Id == id, CancellationToken.None);
            }


            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.WelcomeHtml, "<p>welcome...</p>");
            Assert.AreEqual(result.SubmitHtml, "<button>save</button>");
            Assert.AreEqual(result.ThanksHtml, "<p>thanks...</p>");
            Assert.AreEqual(result.FormTheme, "<p>form theme...</button>");
            Assert.AreEqual(result.FormSettings, "<p>form settings</p>");
            Assert.AreEqual(result.ThemeBackgroundImagePath, "abc.png");

            var question = result.RfiFormQuestions.First();
            Assert.AreEqual(result.RfiFormQuestions.Count(), 2);
            Assert.AreEqual(question.Question, "emailquestion");
        }
    }
}
