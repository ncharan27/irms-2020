﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data;
using Irms.Data.EntityClasses;
using Irms.Data.Repositories;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.Tests.Unit.DataBuilder.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Irms.Tests.Unit.Repositories
{
    [TestClass]
    public class ContactRepositoryTest
    {
        [TestMethod]
        public async Task CanCreateContact()
        {
            //arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;


            var query = new Mock<IDatabaseQueryExecutor>();

            query.Setup(x => x.ExecuteSqlRawAsync(It.IsAny<string>(), CancellationToken.None))
                .Returns((string s, CancellationToken t) => Task.FromResult(1));


            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Entities.Contact, Data.EntityClasses.Contact>();
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactToCreate = new ContactBuilder().Build();
            contactToCreate.Id = id;
            contactToCreate.TenantId = tenant.Id;


            //act
            Guid resultId;
            Data.EntityClasses.Contact result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactRepository(db, mapper, currentUser.Object, tenant, query.Object);
                resultId = await sut.Create(contactToCreate, CancellationToken.None);
                result = await db.Contact.FirstOrDefaultAsync(x => x.Id == resultId, CancellationToken.None);
            }

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, resultId);
            Assert.AreEqual(result.Title, contactToCreate.Title);
            Assert.AreEqual(result.FullName, contactToCreate.FullName);
            Assert.AreEqual(result.Email, contactToCreate.Email);
            Assert.AreEqual(result.MobileNumber, contactToCreate.MobileNumber);
        }

        [TestMethod]
        public async Task CanUpdateContactsToGuestsTest()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var query = new Mock<IDatabaseQueryExecutor>();

            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contactList = new Data.EntityClasses.ContactList
            {
                Id = Guid.NewGuid(),
            };

            var eventCampaign = new EventCampaign
            {
                Id = Guid.NewGuid(),
                GuestListId = contactList.Id
            };

            var contact = new Data.EntityClasses.Contact
            {
                Id = Guid.NewGuid(),
                FullName = "testname"
            };


            query.Setup(x => x.ExecuteSqlRawAsync(It.IsAny<string>(), CancellationToken.None))
                .Returns((string s, CancellationToken t) =>
                {
                    contact.IsGuest = true;
                    return Task.FromResult(1);
                });

            var contactListToContacts = new ContactListToContacts
            {
                Id = Guid.NewGuid(),
                ContactListId = contactList.Id,
                ContactId = contact.Id
            };

            using (var db = new IrmsDataContext(options))
            {
                db.ContactList.Add(contactList);
                db.EventCampaign.Add(eventCampaign);
                db.Contact.Add(contact);
                db.ContactListToContacts.Add(contactListToContacts);
                await db.SaveChangesAsync();
            }

            // Act
            Data.EntityClasses.Contact result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactRepository(db, mapper, currentUser.Object, tenant, query.Object);
                await sut.UpdateContactsToGuests(eventCampaign.Id, CancellationToken.None);
                result = await db.Contact.FirstOrDefaultAsync(x => x.Id == contact.Id, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, contact.Id);
            Assert.AreEqual(result.FullName, contact.FullName);
        }

        /// <summary>
        /// unit test to get contacts with custom fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanGetContactWithCustomFieldsByIds()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var query = new Mock<IDatabaseQueryExecutor>();
            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.EntityClasses.Contact, Domain.Entities.Contact>()
                .ForMember(x => x.CustomFields, y => y.Ignore());
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contact = new Data.EntityClasses.Contact
            {
                Id = id,
                FullName = "testname"
            };

            using (var db = new IrmsDataContext(options))
            {
                db.Contact.Add(contact);
                await db.SaveChangesAsync();
            }

            // Act
            Domain.Entities.Contact result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactRepository(db, mapper, currentUser.Object, tenant, query.Object);
                result = await sut.GetContactWithCustomFieldsById(id, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, contact.Id);
            Assert.AreEqual(result.FullName, contact.FullName);
        }


        /// <summary>
        /// unit test to update contacts with custom fields
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CanUpdateRangeWithCustomFields()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<IrmsDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var query = new Mock<IDatabaseQueryExecutor>();
            var currentUser = new Mock<ICurrentUser>();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Entities.Contact, Data.EntityClasses.Contact>()
                .ForMember(x => x.CustomFields, y => y.Ignore());

                cfg.CreateMap<Data.EntityClasses.Contact, Domain.Entities.Contact>()
                .ForMember(x => x.CustomFields, y => y.Ignore());
            });
            var mapper = config.CreateMapper(); // IMapper to be injected into subject under test

            var id = Guid.NewGuid();
            var tenant = TenantBasicInfoBuilder.BuildDefault();

            var contact = new Data.EntityClasses.Contact
            {
                Id = id,
                FullName = "testname",
                TenantId = tenant.Id
            };

            var contactToUpdate = new ContactBuilder().Build();
            contactToUpdate.Id = id;
            contactToUpdate.TenantId = tenant.Id;
            contactToUpdate.CustomFields = new List<Domain.Entities.ContactCustomField>();
            using (var db = new IrmsDataContext(options))
            {
                db.Contact.Add(contact);
                await db.SaveChangesAsync();
            }

            // Act
            Domain.Entities.Contact result;
            using (var db = new IrmsDataContext(options))
            {
                var sut = new ContactRepository(db, mapper, currentUser.Object, tenant, query.Object);
                await sut.UpdateContactWithCustomFields(contactToUpdate, CancellationToken.None);
                result = await sut.GetContactWithCustomFieldsById(id, CancellationToken.None);
            }

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, contact.Id);
            Assert.AreEqual(result.FullName, "test fullname");
        }
    }
}
