﻿using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CustomFields.Commands
{
    public class UpdateCustomFieldCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CustomFieldType CustomFieldType { get; set; }
    }
}
