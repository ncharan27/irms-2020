﻿using MediatR;
using System.Collections.Generic;

namespace Irms.Application.Contact.Commands
{
    public class SendgridWebhookEmailCmd : IRequest<Unit>
    {
        public IEnumerable<WebhookSendgridModel> Responses { get; set; }
    }

    public sealed class WebhookSendgridModel
    {
        public string email { get; set; }
        public string @event { get; set; }
        public string ip { get; set; }
        public string response { get; set; }
        public string sg_event_id { get; set; }
        public string sg_message_id { get; set; }
        public string smtpid { get; set; }
        public int timestamp { get; set; }
        public int tls { get; set; }
        public string type { get; set; }
    }
}
