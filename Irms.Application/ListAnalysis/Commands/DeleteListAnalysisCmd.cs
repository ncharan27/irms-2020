﻿using MediatR;
using System;

namespace Irms.Application.ListAnalysis.Commands
{
    public class DeleteListAnalysisCmd : IRequest<Guid>
    {
        public Guid GuestListId { get; set; }
        public Guid EventId { get; set; }
    }
}