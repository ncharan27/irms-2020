﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.ListAnalysis.Events
{
    public class ListAnalysisStateSet : IEvent
    {
        public ListAnalysisStateSet(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The list analysis state has been set for guest list {x(Id)} by {u}";
    }
}
