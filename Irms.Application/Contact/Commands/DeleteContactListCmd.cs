﻿using MediatR;
using System;

namespace Irms.Application.Contact.Commands
{
    public class DeleteContactListCmd : IRequest<Guid?>
    {
        public Guid Id { get; set; }
    }
}
