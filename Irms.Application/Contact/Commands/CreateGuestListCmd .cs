﻿using MediatR;
using System;

namespace Irms.Application.Contact.Commands
{
    public class CreateGuestListCmd : IRequest<Guid?>
    {
        public CreateGuestListCmd(string name, Guid? eventId)
        {
            Name = name;
            EventId = eventId;
        }
        public string Name { get; }
        public Guid? EventId { get; }
        public bool IsGuest { get; set; }
        public bool? IsGlobal { get; set; }
    }
}
