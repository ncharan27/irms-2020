﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;

namespace Irms.Application.Contact.Events
{
    public class ContactUpdated : IEvent
    {
        public ContactUpdated(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; }

        public Guid ObjectId => Guid.Empty;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact with id {Id} has been updated by {u}";
    }
}
