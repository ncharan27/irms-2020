﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Contact.Events
{
    public class GuestListRenamed : IEvent
    {
        public GuestListRenamed(Guid contactListId, string name)
        {
            ContactListId = contactListId;
            Name = name;
        }

        public Guid ContactListId { get; }
        public string Name { get; }

        public Guid ObjectId => ContactListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact list {x(ContactListId)} has been renamed to {Name} by {u}";
    }
}
