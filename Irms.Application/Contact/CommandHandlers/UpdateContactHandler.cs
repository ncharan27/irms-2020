﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class UpdateContactHandler : IRequestHandler<UpdateContactCmd, Unit>
    {
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpdateContactHandler(
            IContactRepository<Domain.Entities.Contact, Guid> repo,
            IMediator mediator,
            IMapper mapper)
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// update contact with custom fields
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateContactCmd request, CancellationToken token)
        {
            var dbContact = await _repo.GetContactWithCustomFieldsById(request.Id, token);
            if (dbContact == null)
            {
                throw new IncorrectRequestException($"Contact with {request.Id} not found");
            }

            _mapper.Map(request, dbContact);
            dbContact.CustomFields = MapperCustomFields(request.Id, request.CustomFields);

            await _repo.UpdateContactWithCustomFields(dbContact, token);

            await _mediator.Publish(new ContactUpdated(request.Id), token);
            return Unit.Value;
        }

        private IEnumerable<ContactCustomField> MapperCustomFields(Guid contactId, IEnumerable<UpdateContactCmd.CustomField> fromFields)
        {
            if (fromFields == null)
            {
                return new List<ContactCustomField>();
            }

            var fields = fromFields.Select(x =>
            {
                var cf = _mapper.Map<ContactCustomField>(x);
                cf.ContactId = contactId;
                return cf;
            }).ToList();

            return fields;
        }
    }
}
