﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class AskRfiQuestionsHandler : IRequestHandler<AskRfiQuestionsCmd, Unit>
    {
        private readonly IRfiFormRepository<RfiForm, Guid> _rfiRepository;
        private readonly IMediator _mediator;

        public AskRfiQuestionsHandler(
            IRfiFormRepository<RfiForm, Guid> rfiRepository,
            IMediator mediator)
        {
            _rfiRepository = rfiRepository;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(AskRfiQuestionsCmd request, CancellationToken token)
        {
            var rfiForm = await _rfiRepository.GetByContactListId(request.ContactListId, token);
            
            if(rfiForm == null)
            {
                throw new IncorrectRequestException($"Rfi form for contact list with id {request.ContactListId} was not found");
            }

            var id = await _rfiRepository.CreateContactListRfi(request.ContactListId, rfiForm.Id, token);

            //start background job


            await _mediator.Publish(new RfiQuestionsAsked(id), token);
            return Unit.Value;
        }
    }
}