﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class DeleteContactHandler : IRequestHandler<DeleteContactCmd, Unit>
    {
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMediator _mediator;

        public DeleteContactHandler(
            IContactRepository<Domain.Entities.Contact, Guid> contactListRepository,
            IMediator mediator)
        {
            _contactRepository = contactListRepository;
            _mediator = mediator;
        }

        /// <summary>
        /// delete contacts by ids
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteContactCmd message, CancellationToken token)
        {
            await _contactRepository.Delete(message.Ids, token);

            var contactListRemoved = new ContactsDeleted(message.Ids);
            await _mediator.Publish(contactListRemoved, token);

            return Unit.Value;
        }
    }
}
