﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class AppendListToListHandler : IRequestHandler<AppendListToListCmd, Unit>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IContactListToContactsRepository<ContactListToContact, Guid> _contactListToContactsRepository;
        private readonly IMediator _mediator;
        public AppendListToListHandler(IContactListToContactsRepository<ContactListToContact, Guid> contactListToContactsRepository,
            TenantBasicInfo tenant, IMediator mediator)
        {
            _contactListToContactsRepository = contactListToContactsRepository;
            _tenant = tenant;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(AppendListToListCmd request, CancellationToken cancellationToken)
        {

            var newcontacts = await _contactListToContactsRepository.GetAllForContactList(request.FromListId, cancellationToken);
            var allExistingContacts = await _contactListToContactsRepository.GetAllForContactList(request.ToListId, cancellationToken);

            var guestIds = newcontacts.Select(x => x.ContactId).ToList();

            var allExistingContactIds = allExistingContacts.Select(x => x.ContactId);

            if (allExistingContacts.Count() > 0)
            {
                guestIds = guestIds
                    .Where(x => !allExistingContactIds.Contains(x))
                    .ToList();
            }

            var contactListItems = new List<ContactListToContact>();

            foreach (Guid contactId in guestIds)
            {
                var contactListItem = new ContactListToContact();
                contactListItem.Create(_tenant.Id);
                contactListItem.SetContactId(contactId);
                contactListItem.SetContactListId(request.ToListId);
                contactListItems.Add(contactListItem);
            }

            await _contactListToContactsRepository.CreateRange(contactListItems, cancellationToken);

            var globalContactListCreated = new GuestsInGuestListInserted(request.ToListId);
            await _mediator.Publish(globalContactListCreated, cancellationToken);

            return Unit.Value;
        }
    }

}
