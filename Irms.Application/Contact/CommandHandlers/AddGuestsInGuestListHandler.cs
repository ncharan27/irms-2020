﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Contact.Commands;
using Irms.Application.Contact.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Contact.CommandHandlers
{
    public class AddGuestsInGuestListHandler : IRequestHandler<AddGuestsInGuestListCmd, Guid>
    {
        private readonly TenantBasicInfo _tenant;
        private readonly IContactListToContactsRepository<ContactListToContact, Guid> _contactListToContactsRepository;
        private readonly IMediator _mediator;
        public AddGuestsInGuestListHandler(IContactListToContactsRepository<ContactListToContact, Guid> contactListToContactsRepository,
            TenantBasicInfo tenant, IMediator mediator)
        {
            _contactListToContactsRepository = contactListToContactsRepository;
            _tenant = tenant;
            _mediator = mediator;
        }

        public async Task<Guid> Handle(AddGuestsInGuestListCmd request, CancellationToken cancellationToken)
        {
            var contactListItems = new List<ContactListToContact>();

            var guestIds = request.GuestIds;

            // Remove contacts from existing list
            var allExistingContacts = await _contactListToContactsRepository.GetAllForContactList(request.ListId, cancellationToken);

            var idsToDelete = allExistingContacts
                .Where(x => !guestIds.Contains(x.ContactId))
                .ToList();

            if (idsToDelete.Count > 0)
            {
                await _contactListToContactsRepository.DeleteRangeByIds(idsToDelete.Select(x => x.Id), cancellationToken);
            }

            var allExistingContactIds = allExistingContacts.Select(x => x.ContactId);

            if (allExistingContacts.Count() > 0)
            {
                guestIds = guestIds
                    .Where(x => !allExistingContactIds.Contains(x))
                    .ToList();
            }


            foreach (Guid contactId in guestIds)
            {
                var contactListItem = new ContactListToContact();
                contactListItem.Create(_tenant.Id);
                contactListItem.SetContactId(contactId);
                contactListItem.SetContactListId(request.ListId);
                contactListItems.Add(contactListItem);
            }

            await _contactListToContactsRepository.CreateRange(contactListItems, cancellationToken);

            var globalContactListCreated = new GuestsInGuestListInserted(request.ListId);
            await _mediator.Publish(globalContactListCreated, cancellationToken);

            return request.ListId;
        }
    }
}
