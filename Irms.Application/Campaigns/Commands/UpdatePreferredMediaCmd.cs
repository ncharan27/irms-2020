﻿using System;
using System.Collections.Generic;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class UpdatePreferredMediaCmd : IRequest<Unit>
    {
        public List<GuestPreferredMedia> GuestPreferredMediaList { get; set; }
        public Guid CampaignId { get; set; }
    }

    public class GuestPreferredMedia
    {
        public Guid Id { get; set; }
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public bool WhatsApp { get; set; }
    }
}
