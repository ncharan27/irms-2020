﻿using Irms.Application.Campaigns.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Campaigns.Commands
{
    public class CampaignGoLiveCmd : IRequest<CampaignGoLiveResponse>
    {
        public CampaignGoLiveCmd(Guid campaignId)
        {
            CampaignId = campaignId;
        }
        public Guid CampaignId { get; set; }
    }
}
