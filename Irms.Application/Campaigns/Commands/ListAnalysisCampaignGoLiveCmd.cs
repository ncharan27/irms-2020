﻿using Irms.Application.Campaigns.ReadModels;
using MediatR;
using System;

namespace Irms.Application.Campaigns.Commands
{
    public class ListAnalysisCampaignGoLiveCmd : IRequest<CampaignGoLiveResponse>
    {
        public ListAnalysisCampaignGoLiveCmd(Guid campaignId)
        {
            CampaignId = campaignId;
        }
        public Guid CampaignId { get; set; }
    }
}
