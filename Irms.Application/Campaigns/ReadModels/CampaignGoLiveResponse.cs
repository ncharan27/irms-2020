﻿using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Campaigns.ReadModels
{
    public class CampaignGoLiveResponse
    {
        public bool HasErrors { get; set; }
        public string Error { get; set; }
    }

}
