﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Campaigns.Events
{
    public class CampaignListAnalysisCreated : IEvent
    {
        public CampaignListAnalysisCreated(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The campaign {x(Id)} with with invitation for list analysis has been created by {u}";
    }
}
