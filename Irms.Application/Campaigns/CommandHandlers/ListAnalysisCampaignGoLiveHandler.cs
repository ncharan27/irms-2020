﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;
using Irms.Application.Campaigns.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class ListAnalysisCampaignGoLiveHandler : IRequestHandler<ListAnalysisCampaignGoLiveCmd, CampaignGoLiveResponse>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repo;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepo;
        private readonly ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> _campaignInvitationRepo;
        private readonly ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> _campaignInvitationResponseRepo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly ITransactionManager _transaction;
        private readonly IHangfireJobsWorker _hangfireJobsWorker;

        public ListAnalysisCampaignGoLiveHandler(
            ICampaignRepository<Campaign, Guid> repo,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepo,
            ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> campaignInvitationRepo,
            ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> campaignInvitationResponseRepo,
            IMediator mediator,
            IMapper mapper,
            ITransactionManager transaction,
            IHangfireJobsWorker hangfireJobsWorker)
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _transaction = transaction;
            _contactRepo = contactRepo;
            _campaignInvitationRepo = campaignInvitationRepo;
            _campaignInvitationResponseRepo = campaignInvitationResponseRepo;
            _hangfireJobsWorker = hangfireJobsWorker;
        }

        /// <summary>
        /// This method sets list analysis campaign to live state
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CampaignGoLiveResponse> Handle(ListAnalysisCampaignGoLiveCmd request, CancellationToken token)
        {
            var response = new CampaignGoLiveResponse { HasErrors = false };
            var campaign = await _repo.GetById(request.CampaignId, token);

            if (campaign.Active)
            {
                throw new IncorrectRequestException("List analysis campaign is already active!");
            }

            using (var tr = await _transaction.BeginTransactionAsync(token))
            {
                try
                {
                    var existingMedias = await _repo.GetExistingMedias(request.CampaignId, token);

                    if (!existingMedias.Any(x => x))
                    {
                        throw new CampaignGoesLiveException("Users has no active medias");
                    }

                    await _contactRepo.CheckPrefferedMedias(request.CampaignId, token);
                    var invitation = (await _campaignInvitationRepo.GetByCampaignId(request.CampaignId, token)).FirstOrDefault();
                    var contactPreferences = await _contactRepo.GetContactsByCampaignId(campaign.GuestListId.Value, campaign.Id, token);

                    var responses = new List<CampaignInvitationResponse>();

                    if (invitation.Invitation != null)
                    {
                        if (invitation.Email || invitation.Sms || invitation.Whatsapp)
                        {
                            foreach (var contact in contactPreferences)
                            {
                                if (contact.Email || contact.Sms || contact.WhatsApp)
                                    responses.Add(new CampaignInvitationResponse
                                    {
                                        CampaignInvitationId = invitation.Invitation.Id,
                                        ContactId = contact.ContactId,
                                        TenantId = invitation.Invitation.TenantId,
                                        Id = Guid.NewGuid(),
                                        CreatedOn = DateTime.UtcNow,
                                        Answer = UserAnswer.NOT_ANSWERED
                                    });
                            }

                        }

                        await _campaignInvitationResponseRepo.FillRange(responses, token);
                    }

                    //update invitations
                    //var invitationsOnly = invitation.Select(x => x.Invitation);
                    var liInvitation = UpdateStartDateForInvitation(invitation.Invitation);
                    _hangfireJobsWorker.SetupListAnalysisSending(liInvitation.Id, liInvitation.StartDate);

                    await _contactRepo.UpdateContactsToGuests(request.CampaignId, token);
                    await _campaignInvitationRepo.UpdateRange(liInvitation.Enumerate(), token);
                    campaign.Active = true;
                    campaign.CampaignStatus = CampaignStatus.Live;
                    await _repo.Update(campaign, token);
                    tr.Commit();

                    await _mediator.Publish(new ListAnalysisCampaignGoesLive(request.CampaignId), token);
                }
                catch (CampaignGoesLiveException ex)
                {
                    tr.Rollback();
                    throw new IncorrectRequestException(ex.Message);
                }
            }

            return response;
        }

        private CampaignInvitation UpdateStartDateForInvitation(CampaignInvitation invitation)
        {
            //check if start date passed.
            if (invitation.InvitationType == InvitationType.ListAnalysis && !invitation.IsInstant && invitation.StartDate <= DateTime.UtcNow)
            {
                throw new IncorrectRequestException($"Schedule datetime of Rsvp invitation is passed.");
            }

            var startDate = DateTime.UtcNow;
            if (invitation.IsInstant)
            {
                invitation.StartDate = startDate;
            }

            invitation.Active = true;
            return invitation;
        }

        private class CampaignGoesLiveException : Exception
        {
            public CampaignGoesLiveException(string message) : base(message) { }
        }
    }
}
