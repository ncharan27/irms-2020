﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Events.Events;
using Irms.Application.Campaigns.Events;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class DeleteCampaignHandler : IRequestHandler<DeleteCampaignCmd, Unit>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repository;
        private readonly IMediator _mediator;

        public DeleteCampaignHandler(
            ICampaignRepository<Campaign, Guid> repository, IMediator mediator
            )
        {
            _repository = repository;
            _mediator = mediator;
        }

        /// <summary>
        /// del campaign handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteCampaignCmd request, CancellationToken token)
        {
            await _repository.Delete(request.Id, token);

            var e = new CampaignDeleted(request.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
