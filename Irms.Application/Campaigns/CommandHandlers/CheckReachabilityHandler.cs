﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Campaigns.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Events;
using Irms.Application.Campaigns.ReadModels;
using Irms.Application.Campaigns.Queries;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using System.Collections.Generic;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class CheckReachabilityHandler : IRequestHandler<CheckReachability, (bool reachabilitySubscribed, ReachabilityStats stats)>
    {
        private readonly IContactReachabilityRepository<ContactReachability, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IEmailReachabilityManager _emailReachability;
        private readonly IWhatsappReachabilityManager _whatsappReachability;
        private readonly IPhoneNumberValidator _phoneValidator;

        public CheckReachabilityHandler(
            IContactReachabilityRepository<ContactReachability, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IEmailReachabilityManager emailReachability,
            IWhatsappReachabilityManager whatsappReachability,
            IPhoneNumberValidator phoneValidator
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _emailReachability = emailReachability;
            _whatsappReachability = whatsappReachability;
            _phoneValidator = phoneValidator;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<(bool reachabilitySubscribed, ReachabilityStats stats)> Handle(CheckReachability message, CancellationToken token)
        {
            (bool reachabilitySubscribed, IEnumerable<GuestInfoListItem> guests) = await _mediator.Send(new GetGuestsByListId(message.GuestListId), token);
            if (!reachabilitySubscribed)
            {
                return (reachabilitySubscribed, new ReachabilityStats());
            }

            var stats = new ReachabilityStats();
            foreach (var guest in guests)
            {
                var cr = new ContactReachability();
                //email
                //check if email was reachable a month before.
                var dbContactReachability = await _repo.GetByGuestId(guest.Id, token);
                bool emailReachable;
                if (dbContactReachability.EmailLastReachableOn.HasValue
                    && dbContactReachability.EmailLastReachableOn.Value.AddDays(1) > DateTime.UtcNow)
                {
                    emailReachable = dbContactReachability.Email;
                    if (emailReachable)
                    {
                        stats.EmailCount++;
                    }

                    cr.EmailLastReachableOn = dbContactReachability.EmailLastReachableOn;
                }
                else
                {
                    emailReachable = _emailReachability.CheckEmailReachability(guest.Email);
                    if (emailReachable)
                    {
                        stats.EmailCount++;
                    }

                    cr.EmailLastReachableOn = DateTime.UtcNow;
                }

                bool phoneReachable;
                if (dbContactReachability.SmsLastReachableOn.HasValue
                    && dbContactReachability.SmsLastReachableOn.Value.AddDays(1) > DateTime.UtcNow)
                {
                    phoneReachable = dbContactReachability.Sms;
                    if (phoneReachable)
                    {
                        stats.SmsCount++;
                    }

                    cr.SmsLastReachableOn = dbContactReachability.SmsLastReachableOn;
                }
                else
                {
                    phoneReachable = _phoneValidator.CheckPhoneValidation(guest.MobileNumber);
                    if (phoneReachable)
                    {
                        stats.SmsCount++;
                    }

                    cr.SmsLastReachableOn = DateTime.UtcNow;
                }

                bool whatsappReachable;
                if (dbContactReachability.WhatsAppLastReachableOn.HasValue
                    && dbContactReachability.WhatsAppLastReachableOn.Value.AddDays(1) > DateTime.UtcNow)
                {
                    whatsappReachable = dbContactReachability.WhatsApp;
                    if (whatsappReachable)
                    {
                        stats.WhatsAppCount++;
                    }

                    cr.WhatsAppLastReachableOn = dbContactReachability.WhatsAppLastReachableOn;

                }
                else
                {
                    whatsappReachable = await _whatsappReachability.CheckWhatsappReachabilityAsync(guest.MobileNumber);
                    if (whatsappReachable)
                    {
                        stats.WhatsAppCount++;
                    }

                    cr.WhatsAppLastReachableOn = DateTime.UtcNow;
                }

                cr.CreateOrUpdate(guest.Id, emailReachable, phoneReachable, whatsappReachable);
                await _repo.Update(cr, token);
            }

            await _mediator.Publish(new ReachabilityChecked(message.GuestListId), token);
            return (reachabilitySubscribed, stats);
        }
    }
}