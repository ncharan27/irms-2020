﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Events;

namespace Irms.Application.Campaigns.CommandHandlers
{
    public class UpdateCampaignHandler : IRequestHandler<UpdateCampaignCmd, Unit>
    {
        private readonly ICampaignRepository<Campaign, Guid> _repository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UpdateCampaignHandler(
            ICampaignRepository<Campaign, Guid> repository,
            IMediator mediator,
            IMapper mapper
            )
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(UpdateCampaignCmd request, CancellationToken token)
        {
            var campaign = await _repository.GetById(request.Id, token);
            _mapper.Map(request, campaign);

            campaign.Update(request.GuestListId, request.EntryCriteriaTypeId, request.ExitCriteriaType);
            await _repository.Update(campaign, token);

            var e = new CampaignUpdated(campaign.Id);
            await _mediator.Publish(e, token);
            return Unit.Value;
        }
    }
}
