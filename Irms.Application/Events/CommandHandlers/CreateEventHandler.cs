﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Events.Commands;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Files.Commands;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Events.Events;

namespace Irms.Application.Events.CommandHandlers
{
    public class CreateEventHandler : IRequestHandler<CreateEventCmd, Guid>
    {
        private readonly IEventRepository<Event, Guid> _eventRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public CreateEventHandler(
            TenantBasicInfo tenant,
            IEventRepository<Event, Guid> eventRepository,
            IMediator mediator,
            IMapper mapper
            )
        {
            _eventRepository = eventRepository;
            _mediator = mediator;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateEventCmd message, CancellationToken token)
        {
            string fileName = null;
            //upload logo to azure storage
            if (message.Image != null)
            {
                fileName = $"{FilePath.GetFileDirectory(SystemModule.Events)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(fileName, message.Image, FileType.Picture), token);
            }

            var @event = _mapper.Map<Event>(message);
            var location = _mapper.Map<EventLocation>(message);
            @event.Create(_tenant.Id, message.Features);
            @event.SetImagePath(fileName);
            @event.CreateLocation(location);

            await _eventRepository.Create(@event, token);

            var eventCreated = new EventCreated(@event.Id);
            await _mediator.Publish(eventCreated, token);

            return @event.Id;
        }
    }
}
