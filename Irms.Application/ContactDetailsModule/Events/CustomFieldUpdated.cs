﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Events
{
    class CustomFieldUpdated : IEvent
    {
        public CustomFieldUpdated(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"{u} has successfully updated custom field with Id {x(Id)}";
    }
}
