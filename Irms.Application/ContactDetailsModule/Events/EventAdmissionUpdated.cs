﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.DataModule.Events
{
    class EventAdmissionUpdated : IEvent
    {
        public EventAdmissionUpdated(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"{u} has successfully updated event admission with ID {x(Id)}";
    }
}
