﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Events
{
    class PersonalInfoUpdated : IEvent
    {
        public PersonalInfoUpdated(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"{u} has successfully updated personal info with ID {x(Id)}";
    }
}
