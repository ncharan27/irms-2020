﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class UpdateEventAdmissionHandler : IRequestHandler<UpdateEventAdmissionCmd, Unit>
    {
        private readonly ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> _repo;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        public UpdateEventAdmissionHandler(
            ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository
            )
        {
            _mapper = mapper;
            _mediator = mediator;
            _repo = repo;
            _contactRepository = contactRepository;
        }

        public async Task<Unit> Handle(UpdateEventAdmissionCmd request, CancellationToken token)
        {
            if(request.Status == "Accepted" || request.Status == "Rejected")
            {
                var response = await _repo.LoadUserStatus(request.EventId, request.ListId, request.GuestId, token);
                response.Answer = Enum.Parse<UserAnswer>(request.Status, true);
                response.ResponseDate = DateTime.UtcNow;
                await _repo.Update(response, token);

                var user = await _contactRepository.GetById(request.GuestId, token);
                await _contactRepository.Update(user, token);

                await _mediator.Publish(new EventAdmissionUpdated(request.GuestId), token); 
            }

            return Unit.Value;
        }
    }
}
