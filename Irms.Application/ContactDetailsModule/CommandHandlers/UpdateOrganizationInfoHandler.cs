﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.Events;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class UpdateOrganizationInfoHandler : IRequestHandler<UpdateOrganizationInfoCmd, Unit>
    {
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        public UpdateOrganizationInfoHandler(
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository,
            IMediator mediator,
            IMapper mapper
            )
        {
            _mapper = mapper;
            _mediator = mediator;
            _contactRepository = contactRepository;
        }

        public async Task<Unit> Handle(UpdateOrganizationInfoCmd request, CancellationToken token)
        {
            var contact = await _contactRepository.GetById(request.Id, token);
            contact.Organization = request.Organization;
            contact.Position = request.Position;

            await _contactRepository.Update(contact, token);

            await _mediator.Publish(new OrganizationInfoUpdated(request.Id), token);
            return Unit.Value;
        }
    }
}
