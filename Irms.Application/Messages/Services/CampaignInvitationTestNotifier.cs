﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations;
using Irms.Domain.Entities;

namespace Irms.Application.Messages.Services
{
    public class CampaignInvitationTestNotifier : ICampaignInvitationTestNotifier
    {
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly IWhatsappSender _whatsappSender;

        public CampaignInvitationTestNotifier(IEmailSender emailSender,
            ISmsSender smsSender,
            IWhatsappSender whatsappSender)
        {
            _smsSender = smsSender;
            _whatsappSender = whatsappSender;
            _emailSender = emailSender;
        }

        public async Task<(bool sent, EmailMessage msg)> NotifyByEmail(InvitationMessage message, CancellationToken cancellationToken)
        {
            var recipients = message.Recipients.Select(v => new EmailMessage.Recipient(
                v.Id,
                v.Email,
                message.Subject))
                .ToList();

            var emailMsg = new EmailMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _emailSender.SendEmail(emailMsg, cancellationToken);

            return (sent, emailMsg);
        }

        public async Task<(bool sent, SmsMessage msg)> NotifyBySms(InvitationMessage message, CancellationToken token)
        {
            var recipients = message.Recipients.Select(v => new SmsMessage.Recipient(
                v.Id,
                v.Phone))
                .ToList();

            var sms = new SmsMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _smsSender.SendSms(sms, token);

            return (sent, sms);
        }

        public async Task<(bool sent, WhatsappMessage msg)> NotifyByWhatsapp(InvitationMessage message, CancellationToken token)
        {
            var recipients = message.Recipients.Select(v => new WhatsappMessage.Recipient(
                v.Id,
                v.Phone))
                .ToList();

            var msg = new WhatsappMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _whatsappSender.SendWhatsappMessage(msg, token);

            return (sent, msg);
        }

    }
}
