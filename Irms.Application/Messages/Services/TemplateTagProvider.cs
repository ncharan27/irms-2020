﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Messages.Services
{
    public class TemplateTagProvider : ITemplateTagProvider
    {
        private readonly IMediator _mediator;
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepo;
        private readonly IEventRepository<Domain.Entities.Event, Guid> _eventRepository;
        private readonly ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> _campaignInvitationRepo;
        public TemplateTagProvider(IMediator mediator,
            IContactRepository<Domain.Entities.Contact, Guid> contactRepo,
            IEventRepository<Domain.Entities.Event, Guid> eventRepository,
            ICampaignInvitationRepository<Domain.Entities.CampaignInvitation, Guid> campaignInvitationRepo)
        {
            _mediator = mediator;
            _contactRepo = contactRepo;
            _eventRepository = eventRepository;
            _campaignInvitationRepo = campaignInvitationRepo;
        }

        public async Task<Dictionary<string, string>> LoadTagsForContactAndEvent(Guid contactId, Guid eventId, Guid campaignInvitationId, CancellationToken token)
        {
            var contact = await _contactRepo.GetById(contactId, token);
            var campaignEvent = await _eventRepository.GetEventWithLocation(eventId, token);
            var email = await _campaignInvitationRepo.GetCampaignEmailTemplateByInvitationId(campaignInvitationId, token);
            var dictionary = new Dictionary<string, string>()
            {
                { "event_name",campaignEvent.Name },
                { "event_datetime", campaignEvent.StartDateTime.ToString("dd-MMM-yyyy") },
                { "full_name", contact.FullName },
                { "preffered_name", contact.PreferredName },
                { "email", contact.Email },
                { "mobile_number", contact.MobileNumber },
                { "organization", contact.Organization },
                { "position", contact.Position },
                { "event_location", campaignEvent.Location.Name }
            };

            if(email != null)
            {
                dictionary.Add("sender_email", email.SenderEmail);
            }

            return dictionary;
        }

        public string ReplaceTagsForTemplate(string template, Dictionary<string, string> tags)
        {
            foreach(var tag in tags.Keys)
            {
                template = template.Replace(PlaceholderFormatHelper.GetPlaceholder(tag), tags[tag]);
            }
            return template;
        }
    }
}
