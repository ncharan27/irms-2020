﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Abstract.Services.Notifications;
using Irms.Application.CampaignInvitations;
using Irms.Domain.Entities;
using System;
using Irms.Domain;

namespace Irms.Application.Messages.Services
{
    public class CampaignInvitationNotifier : ICampaignInvitationNotifier
    {
        private readonly IEmailBGSender _emailSender;
        private readonly ISmsBGSender _smsSender;
        private readonly IWhatsappBGSender _whatsappSender;

        public CampaignInvitationNotifier(
            IEmailBGSender emailSender,
            ISmsBGSender smsSender,
            IWhatsappBGSender whatsappSender)
        {
            _emailSender = emailSender;
            _smsSender = smsSender;
            _whatsappSender = whatsappSender;
        }

        public async Task<(bool sent, EmailMessage msg)> NotifyByEmail(InvitationMessage message, CancellationToken cancellationToken)
        {
            var recipients = message.Recipients.Select(v => new EmailMessage.Recipient(
                v.Id,
                v.Email,
                message.Subject,
                MapModelToPlaceholders(v, message, MediaType.Email)
                    .Select(x => new EmailMessage.TemplateVariable(x.name, x.value))))
                .ToList();

            var emailMsg = new EmailMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _emailSender.SendEmail(message.TenantId, emailMsg, cancellationToken);

            return (sent, emailMsg);
        }

        public async Task<(bool sent, SmsMessage msg)> NotifyBySms(InvitationMessage message, CancellationToken token)
        {
            var recipients = message.Recipients.Select(v => new SmsMessage.Recipient(
                v.Id,
                v.Phone,
                MapModelToPlaceholders(v, message, MediaType.Sms)
                    .Select(x => new SmsMessage.TemplateVariable(x.name, x.value))))
                .ToList();

            var sms = new SmsMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _smsSender.SendWebhookSms(message.TenantId, sms, token);

            return (sent, sms);
        }

        public async Task<(bool sent, WhatsappMessage msg)> NotifyByWhatsapp(InvitationMessage message, CancellationToken token)
        {
            var recipients = message.Recipients.Select(v => new WhatsappMessage.Recipient(
                v.Id,
                v.Phone,
                MapModelToWhatsappPlaceholders(v, message, MediaType.WhatsApp)
                    .Select(x => new WhatsappMessage.TemplateVariable(x.name, x.value))))
                .ToList();

            var msg = new WhatsappMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _whatsappSender.SendWhatsappWebhookMessage(message.TenantId, msg, token);

            return (sent, msg);
        }

        public async Task<(bool sent, WhatsappMessage msg)> NotifyByWhatsappBOTResponse(InstantBotResponse message, CancellationToken token)
        {
            var recipients = (new WhatsappMessage.Recipient(
                Guid.Empty,
                message.To))
                .Enumerate();

            var msg = new WhatsappMessage(message.Body, recipients, message.CampaignInvitationId);
            bool sent = await _whatsappSender.SendWhatsappMessage(message.TenantId, msg, token);

            return (sent, msg);
        }

        protected IEnumerable<(string name, string value)> MapModelToPlaceholders(EventGuest guest, InvitationMessage message, MediaType mediaType)
        {
            return new (string name, string value)[]
            {
                ("invitation_accepted_link", mediaType == MediaType.Email
                                             ? guest.EmailAcceptedLink
                                             : guest.SmsAcceptedLink),
                ("invitation_rejected_link", mediaType == MediaType.Email
                                             ? guest.EmailRejectedLink
                                             : guest.SmsRejectedLink),
                ("rsvp_invitation_link", guest.SmsAcceptedLink),
                ("rfi_link", mediaType == MediaType.Email
                                             ? guest.EmailAcceptedLink
                                             : guest.SmsAcceptedLink),
                ("event_name", GetPlaceholderDefaultValue("event_name", message.EventName, message)),
                ("event_datetime",  GetPlaceholderDefaultValue("event_datetime", message.EventDateTime.ToString("dd-MMM-yyyy"),message)),
                ("event_location", GetPlaceholderDefaultValue("event_location", message.EventLocation, message)),
                ("sender_email", GetPlaceholderDefaultValue("sender_email",message.Sender,message)),
                ("full_name", GetPlaceholderDefaultValue("full_name", guest.FullName, message)),
                ("preffered_name", GetPlaceholderDefaultValue("preffered_name", guest.PreferredName, message)),
                ("email", GetPlaceholderDefaultValue("email",guest.Email, message)),
                ("mobile_number", GetPlaceholderDefaultValue("mobile_number",guest.Phone, message)),
                ("organization", GetPlaceholderDefaultValue("organization",guest.Organization, message)),
                ("position", GetPlaceholderDefaultValue("position", guest.Position, message)),
                ("sender_name", GetPlaceholderDefaultValue("sender_name", guest.SenderName, message)),
            }.Select(x => (PlaceholderFormatHelper.GetPlaceholder(x.name), x.value));
        }

        private string GetPlaceholderDefaultValue(string placeholder, string value, InvitationMessage inv)
        {
            if (string.IsNullOrEmpty(value))
            {
                inv.Body = ReplaceBodyWithDefaultValues(placeholder, inv.Body);
                return null;
            }
            else
            {
                RemoveDefaultValueFromPlaceholders(placeholder, inv);
                return value;
            }
        }

        private string ReplaceBodyWithDefaultValues(string placeholder, string body)
        {
            var total = AllIndexesOf(body, placeholder);
            for (int i = 0; i < total.Count; i++)
            {
                int index = IndexOfOccurence(body, placeholder, i + 1);
                if (index > -1)
                {
                    int pos = body.Substring(index).IndexOf(placeholder);
                    if (pos > -1)
                    {
                        int orPos = body.Substring(index).Substring(pos).IndexOf("|");
                        if (orPos > -1)
                        {
                            int endPos = body.Substring(index).Substring(pos).IndexOf("}}");
                            if (endPos - orPos - 1 > 0)
                            {
                                string value = body.Substring(index).Substring(orPos + 1, endPos - orPos - 1).Trim();
                                body = body.Remove(index + pos - 2, endPos + 4).Insert(index + pos - 2, value);// body.Replace(body.Substring(pos).Substring(body.IndexOf(placeholder) - 2, endPos + 4), body.Substring(index).Substring(orPos + 1, endPos - orPos - 1).Trim()).Trim();
                            }
                        }
                    }
                }
            }
            return body;
        }

        private void RemoveDefaultValueFromPlaceholders(string placeholder, InvitationMessage inv)
        {
            var total = AllIndexesOf(inv.Body, placeholder);
            for (int i = 0; i < total.Count; i++)
            {
                int index = IndexOfOccurence(inv.Body, placeholder, i + 1);
                if (index > -1)
                {
                    int pos = inv.Body.Substring(index).IndexOf(placeholder);
                    if (pos > -1)
                    {
                        int orPos = inv.Body.Substring(index).Substring(pos).IndexOf("|");
                        if (orPos > -1)
                        {
                            int endPos = inv.Body.Substring(index).Substring(pos).IndexOf("}}");
                            if (endPos - orPos - 1 > 0)
                            {
                                inv.Body = inv.Body.Remove(index + pos, endPos).Insert(index + pos, placeholder);// inv.Body.Remove(index + pos - 2, endPos + 4).Insert(index + pos - 2, placeholder);
                            }
                        }
                    }
                }
            }
        }

        private int IndexOfOccurence(string s, string match, int occurence)
        {
            int i = 1;
            int index = 0;

            while (i <= occurence && (index = s.IndexOf("{{" + match, index + 1)) != -1)
            {
                if (i == occurence)
                    return index + 2;

                i++;
            }

            return -1;
        }

        public List<int> AllIndexesOf(string body, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            List<int> indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = body.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
        }
        protected IEnumerable<(string name, string value)> MapModelToWhatsappPlaceholders(EventGuest guest, InvitationMessage message, MediaType mediaType)
        {
            return new (string name, string value)[]
            {
                //whatsapp
                ("guestname", guest.FullName),
                ("tenantname", guest.SenderName),
                ("eventname", message.EventName),
                ("eventdatetime", message.EventDateTime.ToString("dd-MMM-yyyy")),
                ("eventlocation", message.EventLocation),
                ("invitationurl", guest.WhatsappAcceptedLink),
                ("date", message.EventDateTime.ToString("dd-MMM-yyyy")),
                ("infourl", guest.WhatsappAcceptedLink),
                ("rfilink", guest.WhatsappAcceptedLink),

            }.Select(x => (PlaceholderFormatHelper.GetPlaceholder(x.name), x.value));
        }
    }
}
