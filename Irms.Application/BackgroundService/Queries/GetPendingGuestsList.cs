﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.BackgroundService.Queries
{
    public class GetPendingGuestsList : IRequest<IEnumerable<ReadModels.GuestsListItem>>
    {
        public GetPendingGuestsList(Guid campaignInvitationId)
        {
            CampaignInvitationId = campaignInvitationId;
        }

        public Guid CampaignInvitationId { get; }
    }
}
