﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.BackgroundService.Queries
{
    public class GetCampaignTemplates : IRequest<ReadModels.CampaignTemplates>
    {
        public GetCampaignTemplates(Guid campaignInvitationId)
        {
            CampaignInvitationId = campaignInvitationId;
        }

        public Guid CampaignInvitationId { get; }
    }
}
