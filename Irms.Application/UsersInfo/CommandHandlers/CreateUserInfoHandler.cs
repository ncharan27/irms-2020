﻿//using System;
//using System.Threading;
//using System.Threading.Tasks;
//using MediatR;
//using Irms.Application.Abstract;
//using Irms.Application.Abstract.Repositories.Base;
//using Irms.Application.Abstract.Repositories.UserManagement;
//using Irms.Application.Abstract.Services;
//using Irms.Application.UsersInfo.Commands;
//using Irms.Application.UsersInfo.Events;
//using Irms.Application.UsersInfo.Queries;
//using Irms.Domain;
//using Irms.Domain.Entities;

//namespace Irms.Application.UsersInfo.CommandHandlers
//{
//    public class CreateUserInfoHandler : IRequestHandler<CreateUserInfoCmd, Guid>
//    {
//        private readonly IRepository<UserInfo, Guid> _repository;
//        private readonly IUserManager _userManager;
//        private readonly IUserFactory _factory;
//        private readonly IMediator _mediator;
//        private readonly ITransactionManager _transactionManager;
//        private readonly ICurrentUser _currentUser;
//        private readonly IPhoneNumberValidator _phoneValidator;

//        public CreateUserInfoHandler(
//            IRepository<UserInfo, Guid> repository,
//            IMediator mediator,
//            IUserManager userManager,
//            ITransactionManager transactionManager,
//            IUserFactory factory,
//            ICurrentUser currentUser,
//            IPhoneNumberValidator phoneValidator)
//        {
//            _repository = repository;
//            _mediator = mediator;
//            _userManager = userManager;
//            _transactionManager = transactionManager;
//            _factory = factory;
//            _currentUser = currentUser;
//            _phoneValidator = phoneValidator;
//        }

//        public async Task<Guid> Handle(CreateUserInfoCmd request, CancellationToken cancellationToken)
//        {
//            var user = await _userManager.FindByEmailAsync(request.Email);
//            if (user != null)
//            {
//                throw new IncorrectRequestException("This email already exists");
//            }

//            var formattedPhone = _phoneValidator.Validate(request.Phone, out var isValid);
//            if (!isValid)
//            {
//                throw new IncorrectRequestException("The phone number format is invalid");
//            }

//            user = await _userManager.FindByPhoneNumberAsync(formattedPhone.Number);
//            if (user != null)
//            {
//                throw new IncorrectRequestException("This mobile number already exists");
//            }

//            if (!_currentUser.CanCreateNewUserOfRole(request.Role))
//            {
//                throw new IncorrectRequestException("Insufficient permission");
//            }

//            var unique = await _mediator.Send(new IsNationalIdUnique(request.NationalId), cancellationToken);
//            if (!unique)
//            {
//                throw new IncorrectRequestException("This national id is already taken");
//            }


//            //using (var tr = await _transactionManager.BeginTransactionAsync(cancellationToken))
//            //{
//            user = _factory.CreateUser(
//                id: Guid.NewGuid(),
//                userName: request.Email,
//                email: request.Email,
//                phoneNumber: formattedPhone.Number,
//                isEnabled: true,
//                emailConfirmed: true,
//                phoneNumberConfirmed: true);

//            var creationResult = await _userManager.CreateAsync(user, request.Password);
//            if (!creationResult.Succeeded)
//            {
//                throw new IncorrectRequestException(creationResult.GetErrorMessage());
//            }

//            var addToRoleResult = await _userManager.AddToRole(user, request.Role);
//            if (!addToRoleResult.Succeeded)
//            {
//                throw new IncorrectRequestException(addToRoleResult.GetErrorMessage());
//            }

//            UserInfo emp = new UserInfo(
//                request.Role,
//                request.FullName,
//                request.Gender,
//                request.BirthDate,
//                formattedPhone.Number,
//                request.Email,
//                request.NationalId,
//                request.MessagingHandle,
//                user.Id);

//            await _repository.Create(emp, cancellationToken);

//            //tr.Commit();
//            //}

//            var e = new UserInfoCreated(emp.Id);

//            await _mediator.Publish(e, cancellationToken);
//            return emp.Id;
//        }
//    }
//}
