﻿using MediatR;
using System;

namespace Irms.Application.UsersInfo.Queries
{
    public class GetUserInfoUserId : IRequest<Guid?>
    {
        public GetUserInfoUserId(Guid employeeId)
        {
            UserInfoId = employeeId;
        }

        public Guid UserInfoId { get; }
    }
}
