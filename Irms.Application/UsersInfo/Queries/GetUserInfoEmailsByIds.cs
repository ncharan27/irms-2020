﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Application.UsersInfo.Queries
{
    public class GetUserInfoEmailsByIds : IRequest<IEnumerable<string>>
    {
        public GetUserInfoEmailsByIds(IEnumerable<Guid> ids)
        {
            Ids = ids;
        }

        public IEnumerable<Guid> Ids { get; }
    }
}
