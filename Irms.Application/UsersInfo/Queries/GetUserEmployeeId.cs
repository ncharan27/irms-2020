﻿using System;
using MediatR;

namespace Irms.Application.UsersInfo.Queries
{
    public class GetUserInfoIdByAspUser : IRequest<Guid?>
    {
        public GetUserInfoIdByAspUser(Guid? userId)
        {
            UserId = userId;
        }

        public Guid? UserId { get; }
    }
}
