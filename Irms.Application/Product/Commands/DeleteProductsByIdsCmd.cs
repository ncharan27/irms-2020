﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.Product.Commands
{
    public class DeleteProductsByIdsCmd : IRequest<Unit>
    {
        public DeleteProductsByIdsCmd(IEnumerable<Guid> ids)
        {
            Ids = ids;
        }

        public IEnumerable<Guid> Ids { get; }
    }
}
