﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Product.Commands;
using Irms.Application.Product.Events;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Product.CommandHandlers
{
    public class DeleteProductByIdHandler : IRequestHandler<DeleteProductByIdCmd>
    {
        private readonly IMediator _mediator;
        private readonly IProductRepository<Irms.Domain.Entities.Product, Guid> _repository;
        public DeleteProductByIdHandler(IMediator mediator, IProductRepository<Irms.Domain.Entities.Product, Guid> repository)
        {
            _mediator = mediator;
            _repository = repository;
        }
        /// <summary>
        /// handler for marking product as deleted by id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteProductByIdCmd request, CancellationToken cancellationToken)
        {
            await _repository.Delete(request.ProductId, cancellationToken);

            await _mediator.Publish(new ProductDeleted(request.ProductId));

            return Unit.Value;
        }
    }
}
