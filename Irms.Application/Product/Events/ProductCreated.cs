﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.Product.Events
{
    class ProductCreated : IEvent
    {
        public ProductCreated(Guid productId, string productName)
        {
            ProductId = productId;
            ProductName = productName;
        }

        public Guid ProductId { get; }
        public string ProductName { get; }

        public Guid ObjectId => ProductId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"{u} has created the product \"{ProductName}\" with ID {x(ProductId)}";
    }
    
}
