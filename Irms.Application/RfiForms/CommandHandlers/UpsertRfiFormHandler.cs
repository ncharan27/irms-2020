﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Events;
using Irms.Application.Files.Commands;
using Irms.Application.RfiForms.Commands;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.RfiForms.CommandHandlers
{
    public class UpsertRfiFormHandler : IRequestHandler<UpsertRfiFormCmd, Guid>
    {
        private readonly IRfiFormRepository<RfiForm, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public UpsertRfiFormHandler(
            IRfiFormRepository<RfiForm, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IConfiguration config)
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _config = config;
        }

        /// <summary>
        /// creates or updates (if exists) campaign sms template
        /// </summary>
        /// <param name="message">new(updated) sms template</param>
        /// <param name="token">cancellation token</param>
        /// <returns>template id</returns>
        public async Task<Guid> Handle(UpsertRfiFormCmd message, CancellationToken token)
        {
            var rfi = _mapper.Map<RfiForm>(message);
            rfi.CampaignInvitationId = message.CampaignInvitationId;

            var existingRfi = await _repo.GetById(message.Id, token);
            if (existingRfi == null)
            {
                rfi.Create(message.QuestionJson);

                //upload background image
                if (message.ThemeBackgroundImage.IsNotNullOrEmpty() && !message.ThemeBackgroundImage.Contains(_config["AzureStorage:BaseUrl"]))
                {
                    rfi.ThemeBackgroundImagePath = $"{FilePath.GetFileDirectory(SystemModule.CampaignRfi)}{Guid.NewGuid()}_image.png";
                    await _mediator.Send(new UploadFileCmd(rfi.ThemeBackgroundImagePath, message.ThemeBackgroundImage, FileType.Picture), token);
                }
                //rfi.WelcomeHtml = await UploadBodyImages(rfi.WelcomeHtml, token);
                //rfi.ThanksHtml = await UploadBodyImages(rfi.ThanksHtml, token);

                //welcome
                var welcomeObj = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlContent>(rfi.WelcomeHtml);
                welcomeObj.content = await UploadBodyImages(welcomeObj.content, token);
                welcomeObj.contentArabic = await UploadBodyImages(welcomeObj.contentArabic, token);
                rfi.WelcomeHtml = Newtonsoft.Json.JsonConvert.SerializeObject(welcomeObj);

                //thanks
                //welcomeObj.content = await UploadExistingBodyImages(welcomeObj.content, existingRfi.WelcomeHtml, token);
                var thanksObj = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlContent>(rfi.ThanksHtml);
                thanksObj.content = await UploadBodyImages(thanksObj.content, token);
                thanksObj.contentArabic = await UploadBodyImages(thanksObj.contentArabic, token);
                rfi.ThanksHtml = Newtonsoft.Json.JsonConvert.SerializeObject(thanksObj);

                await _repo.Create(rfi, token);
            }
            else
            {
                rfi.Update(message.QuestionJson);

                //upload image to azure storage
                string fileName = string.Empty;
                if (message.ThemeBackgroundImage.IsNotNullOrEmpty() && !message.ThemeBackgroundImage.Contains(_config["AzureStorage:BaseUrl"]))
                {
                    //delete file from azure
                    if (existingRfi.ThemeBackgroundImagePath.IsNotNullOrEmpty())
                    {
                        await _mediator.Send(new DeleteFileCmd(existingRfi.ThemeBackgroundImagePath), token);
                    }

                    //add new logo
                    fileName = $"{FilePath.GetFileDirectory(SystemModule.CampaignRfi)}{Guid.NewGuid()}_image.png";
                    await _mediator.Send(new UploadFileCmd(fileName, message.ThemeBackgroundImage, FileType.Picture), token);

                    rfi.ThemeBackgroundImagePath = fileName;
                }
                else if (!message.ThemeBackgroundImage.IsNotNullOrEmpty() && existingRfi.ThemeBackgroundImagePath.IsNotNullOrEmpty())
                {
                    await _mediator.Send(new DeleteFileCmd(existingRfi.ThemeBackgroundImagePath), token);
                }

                //welcome
                var welcomeObj = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlContent>(rfi.WelcomeHtml);
                var existingWelcomeObj = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlContent>(existingRfi.WelcomeHtml);

                welcomeObj.content = await UploadExistingBodyImages(welcomeObj.content, existingWelcomeObj.content, token);
                welcomeObj.contentArabic = await UploadExistingBodyImages(welcomeObj.contentArabic, existingWelcomeObj.contentArabic, token);
                rfi.WelcomeHtml = Newtonsoft.Json.JsonConvert.SerializeObject(welcomeObj);

                //thanks
                //welcomeObj.content = await UploadExistingBodyImages(welcomeObj.content, existingRfi.WelcomeHtml, token);
                var thanksObj = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlContent>(rfi.ThanksHtml);
                var existingThanksObj = Newtonsoft.Json.JsonConvert.DeserializeObject<HtmlContent>(existingRfi.ThanksHtml);

                thanksObj.content = await UploadExistingBodyImages(thanksObj.content, existingThanksObj.content, token);
                thanksObj.contentArabic = await UploadExistingBodyImages(thanksObj.contentArabic, existingThanksObj.contentArabic, token);
                rfi.ThanksHtml = Newtonsoft.Json.JsonConvert.SerializeObject(thanksObj);

                await _repo.Update(rfi, token);
            }

            await _mediator.Publish(new RfiFormUpserted(rfi.Id), token);
            return rfi.Id;
        }

        #region extract image from body and save to azure
        private async Task<string> UploadBodyImages(string body, CancellationToken token)
        {
            var images = GetImagesInHTMLString(body);
            foreach (var img in images)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignRfi)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                body = body.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }
            return body;
        }

        private async Task<string> UploadExistingBodyImages(string newBody, string existingBody, CancellationToken token)
        {
            var newImages = GetImagesInHTMLString(newBody);
            var existingImages = GetImagesInHTMLString(existingBody);
            var (toAdd, toDel, toUpd) = existingImages.Compare(newImages, ((n, o) => n == o));

            //add
            foreach (var img in toAdd)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignRfi)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                newBody = newBody.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }

            //del
            foreach (var img in toDel)
            {
                string path = img.Replace($"{_config["AzureStorage:BaseUrl"]}", string.Empty);
                await _mediator.Send(new DeleteFileCmd(path), token);
            }

            return newBody;
        }

        private List<string> GetImagesInHTMLString(string htmlString)
        {
            List<string> images = new List<string>();
            string pattern = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Groups[1].Value);
            }

            return images;
        }

        private string CutImageFromString(string input)
        {
            int index = input.IndexOf(";base64,") + ";base64,".Length;
            if (index < 0)
            {
                return string.Empty;
            }
            return input.Substring(index);
        }

        #endregion

        private class HtmlContent
        {
            public string content { get; set; }
            public string contentArabic { get; set; }
            public string button { get; set; }
            public string buttonArabic { get; set; }
        }
    }
}
