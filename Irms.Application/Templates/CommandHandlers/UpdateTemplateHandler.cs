﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Templates.Commands;
using Irms.Application.Templates.Events;
using Irms.Domain.Entities.Templates;
using MediatR;

namespace Irms.Application.Templates.CommandHandlers
{
    /// <summary>
    /// Handler for updating SMS or Email template in DB based on request <see cref="UpdateTemplateCmd"/>
    /// </summary>
    public class UpdateTemplateHandler : IRequestHandler<UpdateTemplateCmd, Unit>
    {
        private readonly IRepository<Template, Guid> _repository;
        private readonly IMediator _mediator;

        public UpdateTemplateHandler(IRepository<Template, Guid> repository, IMediator mediator)
        {
            _repository = repository;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdateTemplateCmd request, CancellationToken cancellationToken)
        {
            var template = await _repository.GetById(request.Id, cancellationToken);

            template.Update(
                request.Name,
                request.EmailCompatible,
                request.SmsCompatible,
                request.EmailBody,
                request.EmailSubject,
                request.SmsText,
                request.Translations);

            await _repository.Update(template, cancellationToken);
            await _mediator.Publish(new TemplateUpdated(template.Id), cancellationToken);
            return Unit.Value;
        }
    }
}
