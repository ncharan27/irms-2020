﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.ReadModels
{
    public class TestCampaignSendInfoModel
    {
        public string Error { get; set; }
        public int EmailsSend { get; set; }
        public int SmsSend { get; set; }
        public int WhatsappSend { get; set; }
    }
}
