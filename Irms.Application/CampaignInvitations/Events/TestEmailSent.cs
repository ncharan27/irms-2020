﻿using System;
using Irms.Application.Abstract;

namespace Irms.Application.Campaigns.Events
{
    public class TestEmailSent : IEvent
    {
        public TestEmailSent(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Test email sent for campaign invitation {x(Id)} by {u}";
    }
}
