﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.CampaignInvitations.Events
{
    public class CampaignSmsTemplateCreatedOrUpdated : IEvent
    {
        public CampaignSmsTemplateCreatedOrUpdated(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The campaign sms template {x(Id)} has been created/updated by {u}";
    }
}
