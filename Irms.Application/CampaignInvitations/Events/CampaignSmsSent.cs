﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Events
{
    public class CampaignSmsSent : IEvent
    {
        public CampaignSmsSent(IEnumerable<string> numbers)
        {
            Numbers = numbers;
        }

        public IEnumerable<string> Numbers { get; }
        public Guid Id { get; }
        public Guid ObjectId => Id;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"Sms for numbers {string.Join(",", Numbers)} was sent by {u}";
    }
}
