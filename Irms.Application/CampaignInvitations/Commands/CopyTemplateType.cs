﻿namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public enum CopyTemplateType
    {
        SmsToWhatsapp = 0,
        WhatsappToSms = 1,
    }
}
