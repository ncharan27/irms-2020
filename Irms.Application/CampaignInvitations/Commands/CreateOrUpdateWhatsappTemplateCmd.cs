﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class CreateOrUpdateWhatsappTemplateCmd : IRequest<Guid>
    {
        public Guid CampaignInvitationId { get; set; }
        public string SenderName { get; set; }
        public string Body { get; set; }
        public Guid TemplateId { get; set; }
    }
}
