﻿using System;
using Irms.Domain.Entities;
using MediatR;

namespace Irms.Application.Campaigns.Commands
{
    public class UpsertCampaignInvitationAcceptedCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid EventCampaignId { get; set; }
        public bool IsInstant { get; set; }
        public int? Interval { get; set; }
        public int SortOrder { get; set; }
        public IntervalType? IntervalType { get; set; }
    }
}
