﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class SendTestWhatsappCmd : IRequest<IEnumerable<string>>
    {
        public Guid InvitationId { get; set; }
        public IEnumerable<string> WhatsappList { get; set; }
    }
}
