﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class DuplicateInvitationCmd : IRequest<Guid>
    {
        public DuplicateInvitationCmd(Guid invitationId)
        {
            InvitationId = invitationId;
        }

        public Guid InvitationId { get; set; }
    }
}
