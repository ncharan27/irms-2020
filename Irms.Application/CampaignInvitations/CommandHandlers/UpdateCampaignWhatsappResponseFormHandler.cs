﻿using AutoMapper;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.Campaigns.Events;
using Irms.Application.Files.Commands;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.Commands
{
    public class UpdateCampaignWhatsappResponseFormHandler : IRequestHandler<UpdateCampaignWhatsappResponseFormCmd, Unit>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public UpdateCampaignWhatsappResponseFormHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator,
            IMapper mapper,
            IConfiguration config
            )
        {
            _repo = repo;
            _mediator = mediator;
            _mapper = mapper;
            _config = config;
        }

        public async Task<Unit> Handle(UpdateCampaignWhatsappResponseFormCmd request, CancellationToken token)
        {
            var ci = _mapper.Map<CampaignWhatsappTemplate>(request);

            var inv = await _repo.GetCampaignWhatsappTemplateByInvitationId(request.CampaignInvitationId, token);

            ci.AcceptButtonText = request.AcceptButtonText;
            ci.ProceedButtonText = request.ProceedButtonText;
            ci.RejectButtonText = request.RejectButtonText;

            if (inv == null)
            {
                ci.Create();
                ci.Body = string.Empty;
                ci.SenderName = string.Empty;
                ci.BackgroundImagePath = await UploadExistingImage(request.BackgroundImagePath, string.Empty, token);
                ci.Rsvphtml = await UploadBodyImages(request.RsvpHtml, token);
                ci.WelcomeHtml = await UploadBodyImages(request.WelcomeHtml, token);
                ci.AcceptHtml = await UploadBodyImages(request.AcceptHtml, token);
                ci.RejectHtml = await UploadBodyImages(request.RejectHtml, token);
                ci.ThemeJson = DeleteImageIfExist(request.ThemeJson);
                await _repo.CreateCampaignWhatsappResponseForm(ci, token);
            }
            else
            {
                ci.BackgroundImagePath = await UploadExistingImage(request.BackgroundImagePath, inv.BackgroundImagePath, token);
                ci.Rsvphtml = await UploadExistingBodyImages(request.RsvpHtml, inv.Rsvphtml, token);
                ci.WelcomeHtml = await UploadExistingBodyImages(request.WelcomeHtml, inv.WelcomeHtml, token);
                ci.AcceptHtml = await UploadExistingBodyImages(request.AcceptHtml, inv.AcceptHtml, token);
                ci.RejectHtml = await UploadExistingBodyImages(request.RejectHtml, inv.RejectHtml, token);
                ci.ThemeJson = DeleteImageIfExist(ci.ThemeJson);

                await _repo.UpdateCampaignWhatsappResponseForm(ci, token);
            }

            await _mediator.Publish(new CampaignEmailResponseUpdated(request.CampaignInvitationId), token);
            return Unit.Value;
        }

        private string DeleteImageIfExist(string body)
        {
            var images = GetImagesInHTMLString(body, true);
            foreach (var img in images)
            {
                body = body.Replace(img, string.Empty);
            }

            return body;
        }

        private async Task<string> UploadExistingBodyImages(string newBody, string existingBody, CancellationToken token)
        {
            var newImages = GetImagesInHTMLString(newBody);
            var existingImages = GetImagesInHTMLString(existingBody);
            var (toAdd, toDel, toUpd) = existingImages.Compare(newImages, ((n, o) => n == o));

            //add
            foreach (var img in toAdd)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignWhatsapp)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                newBody = newBody.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }

            //del
            foreach (var img in toDel)
            {
                string path = img.Replace($"{_config["AzureStorage:BaseUrl"]}", string.Empty);
                await _mediator.Send(new DeleteFileCmd(path), token);
            }

            return newBody;
        }
        private List<string> GetImagesInHTMLString(string htmlString, bool isUri = false)
        {
            List<string> images = new List<string>();
            string pattern = isUri ? @"(data:image\/[^;]+;base64[^""]+)" : @" < img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(htmlString);

            for (int i = 0, l = matches.Count; i < l; i++)
            {
                images.Add(matches[i].Groups[1].Value);
            }

            return images;
        }

        private async Task<string> UploadBodyImages(string body, CancellationToken token)
        {
            var images = GetImagesInHTMLString(body);
            foreach (var img in images)
            {
                string path = $"{FilePath.GetFileDirectory(SystemModule.CampaignSms)}{Guid.NewGuid()}_image.png";
                await _mediator.Send(new UploadFileCmd(path, img, FileType.Picture), token);
                body = body.Replace(img, $"{_config["AzureStorage:BaseUrl"]}{path}");
            }
            return body;
        }

        private async Task<string> UploadExistingImage(string newImagePath, string existingImagePath, CancellationToken token)
        {
            if (!string.IsNullOrEmpty(newImagePath) && newImagePath != "null")
            {
                string res = CutExtension(newImagePath);
                if (res.IsNotNullOrEmpty())
                {

                    //del prev
                    if (!string.IsNullOrEmpty(existingImagePath))
                    {
                        await _mediator.Send(new DeleteFileCmd(existingImagePath), token);
                    }

                    //add
                    var path = $"{FilePath.GetFileDirectory(SystemModule.CampaignWhatsapp)}{Guid.NewGuid().ToString()}_image.{res}";
                    await _mediator.Send(new UploadFileCmd(path, newImagePath, FileType.Picture), token);
                    return path;
                }
                return existingImagePath;
            }

            return null;
        }

        private string CutExtension(string input)
        {
            int length = input.IndexOf(";base64") - "data:image/".Length;
            if (length < 0)
            {
                return string.Empty;
            }
            string extension = input.Substring("data:image/".Length, length);
            return extension;
        }

        private string CutImageFromString(string input)
        {
            int index = input.IndexOf(";base64,") + ";base64,".Length;
            if (index < 0)
            {
                return string.Empty;
            }
            return input.Substring(index);
        }
    }
}
