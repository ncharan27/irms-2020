﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class DeleteInvitationHandler : IRequestHandler<DeleteInvitationCmd, Guid>
    {
        private readonly ICampaignInvitationRepository<CampaignInvitation, Guid> _repo;
        private readonly IMediator _mediator;

        public DeleteInvitationHandler(
            ICampaignInvitationRepository<CampaignInvitation, Guid> repo,
            IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        /// <summary>
        /// deletes invitation (with templates)
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(DeleteInvitationCmd request, CancellationToken token)
        {
            var campaignInvitation = await _repo.GetById(request.Id, token);

            var items = await _repo
                .LoadInvitationGroup(campaignInvitation.EventCampaignId, campaignInvitation.InvitationType, token);

            var sortedItems = items.OrderBy(x => x.SortOrder)
                .ToList();

            var item = sortedItems.First(x => x.SortOrder == campaignInvitation.SortOrder);
            var index = sortedItems.IndexOf(item);

            if (index == sortedItems.Count - 1)
            {
                await DeleteTemplatesIfExist(request, token);

                return request.Id;
            }

            sortedItems.RemoveAt(index);

            var itemsToUpdate = sortedItems.Skip(index)
                .ToList();

            itemsToUpdate.ForEach(item =>
            {
                item.SortOrder = item.SortOrder - 1;
            });

            await DeleteTemplatesIfExist(request, token);
            await _repo.UpdateRange(itemsToUpdate, token);

            await _mediator.Publish(new InvitationDeleted(request.Id), token);

            return request.Id;
        }

        /// <summary>
        /// removes sms and email templates if any assigned to reminder
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task DeleteTemplatesIfExist(DeleteInvitationCmd request, CancellationToken token)
        {
            var smsTemplate = await _repo.GetCampaignSmsTemplateByInvitationId(request.Id, token);
            if (smsTemplate != null)
            {
                await _repo.DeleteSmsTemplate(smsTemplate.Id, token);
            }

            var emailTemplate = await _repo.GetCampaignEmailTemplateByInvitationId(request.Id, token);
            if (emailTemplate != null)
            {
                await _repo.DeleteEmailTemplate(emailTemplate.Id, token);
            }

            var whatsapptemplate = await _repo.GetCampaignWhatsappTemplateByInvitationId(request.Id, token);
            if (whatsapptemplate != null)
            {
                await _repo.DeleteWhatsappTemplate(whatsapptemplate.Id, token);
            }

            var rfiForm = await _repo.GetCampaignFormByInvitationId(request.Id, token);
            if (rfiForm != null)
            {
                await _repo.DeleteRfiForm(rfiForm.Id, token);
            }
            
            await _repo.Delete(request.Id, token);
        }
    }

}
