﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.CampaignInvitations.Events;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.CampaignInvitations.CommandHandlers
{
    public class ConvertContactsToGuestsHandler : IRequestHandler<ConvertContactsToGuestsCmd, Guid>
    {
        private readonly IContactRepository<Domain.Entities.Contact, Guid> _contactRepository;
        private readonly IMediator _mediator;
        private readonly ITransactionManager _transaction;

        public ConvertContactsToGuestsHandler(
            IContactRepository<Domain.Entities.Contact, Guid> contactRepository,
            IMediator mediator,
            ITransactionManager transaction
        )
        {
            _contactRepository = contactRepository;
            _mediator = mediator;
            _transaction = transaction;
        }

        public async Task<Guid> Handle(ConvertContactsToGuestsCmd request, CancellationToken token)
        {
            using(var tr = await _transaction.BeginTransactionAsync(token))
            {
                await _contactRepository.UpdateContactsToGuests(request.CampaignId, token);
                tr.Commit();
            }

            await _mediator.Publish(new ContactsConvertedToGuests(request.CampaignId), token);

            return request.CampaignId;
        }
    }
}
