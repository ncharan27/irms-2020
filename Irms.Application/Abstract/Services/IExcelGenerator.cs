﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Irms.Application.Abstract.Services
{
    public interface IExcelGenerator
    {
        void GenerateReport<T>(IEnumerable<T> exportItems, Stream outputStream, string sheetName, HashSet<string> fields = null);
        void GenerateContactsReport<T>(T model, Stream outputStream, string sheetName, HashSet<string> fields = null);
        void GenerateRfiExportReport<Q, A>(List<Q> questions,
            List<A> answers,
            Stream outputStream,
            string sheetName,
            bool hasGuestNameSelected = true,
            bool hasSubmissionDateSelected = true);
    }
}
