﻿using Irms.Application.CampaignInvitations;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications.Notifiers
{

    public interface ICampaignInvitationNotifier : INotifier
    {
        Task<(bool sent, EmailMessage msg)> NotifyByEmail(
            InvitationMessage message,
            CancellationToken cancellationToken);

        Task<(bool sent, SmsMessage msg)> NotifyBySms(InvitationMessage msg,
            CancellationToken token);
        Task<(bool sent, WhatsappMessage msg)> NotifyByWhatsapp(InvitationMessage msg, CancellationToken token);
        Task<(bool sent, WhatsappMessage msg)> NotifyByWhatsappBOTResponse(InstantBotResponse message, CancellationToken token);
    }
}
