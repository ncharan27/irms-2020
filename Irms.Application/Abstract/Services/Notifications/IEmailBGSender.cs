﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Services.Notifications
{
    public interface IEmailBGSender
    {
        Task<bool> SendEmail(Guid tenantId, EmailMessage emailMsg, CancellationToken cancellationToken);
    }
}
