﻿using System.Globalization;

namespace Irms.Application.Abstract.Services
{
    public interface IMessageTranslator
    {
        string Translate(string message, CultureInfo culture);
    }
}
