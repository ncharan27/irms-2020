﻿using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface IWhatsappReachabilityManager
    {
        Task<bool> CheckWhatsappReachabilityAsync(string phone);
    }
}
