﻿using System.IO;
using System.Threading.Tasks;

namespace Irms.Application.Abstract
{
    public interface IFileUploadService
    {
        Task UploadFile(string fileName, Stream content);
        Task<byte[]> LoadFile(string fileName);
        Task DeleteFile(string fileName);
        string LoadFileUrl(string filename);
    }
}
