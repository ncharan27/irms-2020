﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface IServiceRepository <TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task<TKey> CreateServiceFeature(TEntity entity, CancellationToken token);
    }
}
