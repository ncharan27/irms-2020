﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Campaigns.ReadModels;
using Irms.Domain.Abstract;

namespace Irms.Application.Abstract.Repositories
{
    public interface IContactRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task UpdateContactsToGuests(Guid campaignId, CancellationToken token);
        Task<IEnumerable<ContactPrefferedMedia>> GetContactsByCampaignId(Guid id, Guid campaignId, CancellationToken token);
        Task CheckPrefferedMedias(Guid id, CancellationToken token);
        Task<IEnumerable<string>> GetExistingEmails(IEnumerable<string> emails, CancellationToken token);
        Task<IEnumerable<string>> GetExistingPhoneNumbers(IEnumerable<string> phoneNumbers, CancellationToken token);
        Task CreateRange(IEnumerable<Domain.Entities.Contact> entities, CancellationToken token);
        Task UpdateRange(IEnumerable<Domain.Entities.Contact> entities, CancellationToken token);
        Task UpdateContactWithCustomFields(Domain.Entities.Contact entity, CancellationToken token);
        Task<IEnumerable<Domain.Entities.Contact>> GetByIds(IEnumerable<Guid> ids, CancellationToken token);
        Task<Domain.Entities.Contact> GetContactWithCustomFieldsById(Guid id, CancellationToken token);
        Task Delete(List<Guid> id, CancellationToken token);
    }
}
