﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;

namespace Irms.Application.Abstract.Repositories
{
    public interface IProviderLogsRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : IEntity<TKey>
    {
    }
}
