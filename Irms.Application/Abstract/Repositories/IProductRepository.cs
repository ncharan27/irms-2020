﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface IProductRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task DeleteList(IEnumerable<TKey> ids, CancellationToken token);
    }
}
