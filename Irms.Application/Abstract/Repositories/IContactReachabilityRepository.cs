﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface IContactReachabilityRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : IEntity<TKey>
    {
        Task<TEntity> GetByGuestId(Guid id, CancellationToken token);
    }
}
