﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Campaigns.Commands;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface IRfiFormRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task<Domain.Entities.RfiForm> GetByContactListId(Guid id, CancellationToken token);
        Task<IEnumerable<Domain.Entities.RfiFormQuestion>> GetQuestions(Guid rfiFormId, CancellationToken token);
        Task<Guid> CreateContactListRfi(Guid contactListId, Guid rfiFormId, CancellationToken token);
    }
}
