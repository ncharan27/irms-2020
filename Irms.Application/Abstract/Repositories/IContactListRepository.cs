﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;

namespace Irms.Application.Abstract.Repositories
{
    public interface IContactListRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    where TEntity : IEntity<TKey>
    {
        Task<bool> IsContactListAssignToACampaign(Guid id, CancellationToken token);
        Task<bool> IsNameAlreadyExists(string name, CancellationToken token);
        Task<bool> IsContactListEmpty(Guid id, CancellationToken token);
        Task<IEnumerable<ContactListImportantField>> GetContactListImportantFields(Guid listId, CancellationToken token);
        Task AddImportantFields(IEnumerable<ContactListImportantField> fields, CancellationToken token);
    }
}
