﻿using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain.Abstract;
using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.Abstract.Repositories
{
    public interface ICampaignInvitationNextRunRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task CreateRange(IEnumerable<CampaignInvitationNextRun> entites, CancellationToken token);
        Task UpdateRange(IEnumerable<CampaignInvitationNextRun> entities, CancellationToken token);
    }
}
