﻿using System.Threading;
using System.Threading.Tasks;
using Irms.Domain.Abstract;

namespace Irms.Application.Abstract.Repositories.Base
{
    public interface IRepository<TEntity, TKey> : IReadOnlyRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        Task<TKey> Create(TEntity entity, CancellationToken token);
        Task Update(TEntity entity, CancellationToken token);
        Task Delete(TKey id, CancellationToken token);
    }
}
