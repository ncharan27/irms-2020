﻿using System;

namespace Irms.Application
{
    public class TenantBasicInfo
    {
        public TenantBasicInfo(
            Guid id,
            string name,
            string clientUrl,
            string emailFrom,
            bool isBtoC
            )
        {
            Id = id;
            Name = name;
            ClientUrl = clientUrl;
            EmailFrom = emailFrom;
            IsBtoC = isBtoC;
        }

        public Guid Id { get; }
        public string Name { get; }
        public string ClientUrl { get; }
        public string EmailFrom { get; }
        public bool IsBtoC { get; }
    }
}
