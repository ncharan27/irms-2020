﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.Events;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class UpdateContactListImportantFieldsHandler : IRequestHandler<UpdateContactListImportantFieldsCmd, Unit>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IMediator _mediator;
        private readonly TenantBasicInfo _tenant;

        public UpdateContactListImportantFieldsHandler(TenantBasicInfo tenant,
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _tenant = tenant;
            _contactListRepository = contactListRepository;
        }

        public async Task<Unit> Handle(UpdateContactListImportantFieldsCmd request, CancellationToken token)
        {
            var contactListImportantFields = await _contactListRepository.GetContactListImportantFields(request.ListId, token);

            var newFields = new List<ContactListImportantField>();

            foreach(var importantField in request.FIeldNames)
            {
                var field = contactListImportantFields.FirstOrDefault(x => x.FieldName == importantField);
                if(field == null)
                {
                    newFields.Add(new ContactListImportantField
                    {
                        Id = Guid.NewGuid(),
                        ContactListId = request.ListId,
                        FieldName = importantField,
                        TenantId = _tenant.Id
                    });
                }
            }


            foreach (var customField in request.CustomFields)
            {
                var field = contactListImportantFields.FirstOrDefault(x => x.CustomFieldId == customField.Id);
                if (field == null)
                {
                    newFields.Add(new ContactListImportantField
                    {
                        Id = Guid.NewGuid(),
                        ContactListId = request.ListId,
                        CustomFieldId = customField.Id,
                        FieldName = customField.Name,
                        TenantId = _tenant.Id
                    });
                }
            }

            await _contactListRepository.AddImportantFields(newFields, token);

            await _mediator.Publish(new AddedImportantFields(request.ListId), token);

            return Unit.Value;
        }
    }
}
