﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.DataModule.Commands;
using Irms.Application.DataModule.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.DataModule.CommandHandlers
{
    public class DeleteContactListHandler : IRequestHandler<DeleteContactListCmd, Unit>
    {
        private readonly IContactListRepository<ContactList, Guid> _contactListRepository;
        private readonly IMediator _mediator;

        public DeleteContactListHandler(
            IContactListRepository<ContactList, Guid> contactListRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _contactListRepository = contactListRepository;
        }

        /// <summary>
        /// Deletes empty contact list
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Unit> Handle(DeleteContactListCmd request, CancellationToken token)
        {
            if (!await _contactListRepository.IsContactListEmpty(request.Id, token))
            {
                throw new IncorrectRequestException("List is not empty");
            }

            await _contactListRepository.Delete(request.Id, token);
            await _mediator.Publish(new ContactListRemoved(request.Id), token);
            return Unit.Value;
        }
    }
}
