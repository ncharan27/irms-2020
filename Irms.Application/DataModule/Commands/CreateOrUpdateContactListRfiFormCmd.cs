﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Commands
{
    public class CreateOrUpdateContactListRfiFormCmd : IRequest<Unit>
    {
        public CreateOrUpdateContactListRfiFormCmd(Guid contactListId)
        {
            ContactListId = contactListId;
        }

        public Guid ContactListId { get; set; }
    }
}
