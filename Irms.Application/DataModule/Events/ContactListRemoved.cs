﻿using Irms.Application.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.DataModule.Events
{
    public class ContactListRemoved : IEvent
    {
        public ContactListRemoved(Guid contactListId)
        {
            ContactListId = contactListId;
        }
        public Guid ContactListId { get; }

        public Guid ObjectId => ContactListId;
        public Guid? ObjectId2 => null;
        public Guid? ObjectId3 => null;
        public string Format(Func<Guid, string> x, string u) => $"The contact list  {x(ContactListId)} has been removed by {u}";
    }
}
