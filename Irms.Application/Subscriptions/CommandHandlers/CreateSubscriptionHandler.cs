﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Subscriptions.Commands;
using Irms.Application.Subscriptions.Events;
using AutoMapper;
using Irms.Domain.Entities;
using Irms.Application.Abstract.Repositories;

namespace Irms.Application.Subscriptions.CommandHandlers
{
    public class CreateSubscriptionHandler : IRequestHandler<CreateSubscriptionCmd, Guid>
    {
        private readonly ISubscriptionRepository<Subscription, Guid> _subscriptionRepository;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public CreateSubscriptionHandler(
            ISubscriptionRepository<Subscription, Guid> subscriptionRepository,
            IMediator mediator,
            IMapper mapper)
        {
            _subscriptionRepository = subscriptionRepository;
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// This method converts cmd to domain and invoke repository.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Guid> Handle(CreateSubscriptionCmd message, CancellationToken token)
        {
            var subscription = _mapper.Map<Subscription>(message);
            subscription.Create();

            await _subscriptionRepository.Create(subscription, token);

            var subscriptionCreated = new SubscriptionCreated(subscription.Id);
            await _mediator.Publish(subscriptionCreated, token);

            return subscription.Id;
        }
    }
}
