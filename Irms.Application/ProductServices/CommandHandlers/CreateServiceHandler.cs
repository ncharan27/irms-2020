﻿using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Application.ProductServices.Commands;
using Irms.Application.ProductServices.Events;
using Irms.Domain;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.ProductServices.CommandHandlers
{
    public class CreateServiceHandler : IRequestHandler<CreateServiceCmd, Guid>
    {
        private readonly ICurrentUser _user;
        private readonly IMediator _mediator;
        private readonly IServiceRepository<ServiceCatalog, Guid> _repository;
        public CreateServiceHandler(IMediator mediator, IServiceRepository<Irms.Domain.Entities.ServiceCatalog, Guid> repository, ICurrentUser user)
        {
            _mediator = mediator;
            _repository = repository;
            _user = user;
        }

        public async Task<Guid> Handle(CreateServiceCmd request, CancellationToken token)
        {
            var service = new ServiceCatalog(request.Title, request.IsActive, _user.Id);
            var id = await _repository.Create(service, token);

            await _mediator.Publish(new ServiceCreated(id), token);
            return id;
        }
    }
}
