﻿using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.ProductServices.Commands;
using Irms.Application.ProductServices.Events;
using Irms.Domain;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Application.ProductServices.CommandHandlers
{
    public class AddServiceToProductHandler : IRequestHandler<AddServiceToProductCmd, Unit>
    {
        private readonly IMediator _mediator;
        private readonly IProductRepository<Domain.Entities.Product, Guid> _productRepository;
        private readonly IServiceRepository<Domain.Entities.ServiceCatalog, Guid> _serviceRepository;
        public AddServiceToProductHandler(
            IMediator mediator,
            IProductRepository<Domain.Entities.Product, Guid> productRepository,
            IServiceRepository<Domain.Entities.ServiceCatalog, Guid> serviceRepository)
        {
            _mediator = mediator;
            _productRepository = productRepository;
            _serviceRepository = serviceRepository;
        }

        public async Task<Unit> Handle(AddServiceToProductCmd request, CancellationToken token)
        {
            var product = await _productRepository.GetById(request.ProductId, token);
            if (product == null)
            {
                throw new IncorrectRequestException($"Product not fount by id: {request.ProductId}");
            }
            var service = await _serviceRepository.GetById(request.ServiceId, token);
            if (service == null)
            {
                throw new IncorrectRequestException($"Service not fount by id: {request.ServiceId}");
            }

            product.AddServiceToProduct(service);

            await _productRepository.Update(product, token);
            await _mediator.Publish(new ServiceAddedToProduct(product.Id), token);

            return Unit.Value;
        }
    }
}
