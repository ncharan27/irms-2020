﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.ProductServices.Events
{
    public class ServiceStatusUpdated : IEvent
    {
        public ServiceStatusUpdated(Guid serviceId)
        {
            ServiceId = serviceId;
        }

        public Guid ServiceId { get; }

        public Guid ObjectId => ServiceId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"The status of service {x(ServiceId)} has been updated by {u}";
    }
}
