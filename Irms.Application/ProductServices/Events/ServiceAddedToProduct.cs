﻿using Irms.Application.Abstract;
using System;

namespace Irms.Application.ProductServices.Events
{
    public class ServiceAddedToProduct : IEvent
    {
        public ServiceAddedToProduct(Guid productId)
        {
            ProductId = productId;
        }

        public Guid ProductId { get; }

        public Guid ObjectId => ProductId;
        public Guid? ObjectId2 => default;
        public Guid? ObjectId3 => default;

        public string Format(Func<Guid, string> x, string u) => $"The product {x(ProductId)} has been updated by {u}";
    }
}
