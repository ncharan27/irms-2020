﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Services;
using Irms.Application.Files.Commands;
using Irms.Application.Files.Events;
using Irms.Domain;
using System.Text.RegularExpressions;

namespace Irms.Application.Files.CommandHandlers
{
    public class UploadFileHandler : IRequestHandler<UploadFileCmd, Unit>
    {
        private const long MaxSizeBytes = 5 * 1024 * 1024; //5mb
        private const long ResizeLimitBytes = 50 * 1024; //50kb

        private readonly IMediator _mediator;
        private readonly ICurrentUser _currentUser;
        private readonly IFileTypeValidator _fileTypeValidator;
        private readonly IImageResizer _imageResizer;
        private readonly IFileUploadService _fileUpload;


        public UploadFileHandler(
            IMediator mediator,
            ICurrentUser currentUser,
            IFileTypeValidator fileTypeValidator,
            IImageResizer imageResizer,
            IFileUploadService fileUpload)
        {
            _mediator = mediator;
            _currentUser = currentUser;
            _fileTypeValidator = fileTypeValidator;
            _imageResizer = imageResizer;
            _fileUpload = fileUpload;
        }

        /// <summary>
        /// upload file handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<Unit> Handle(UploadFileCmd request, CancellationToken cancellationToken)
        {
            var req = new Request(request);
            return HandlerInternal(req, cancellationToken);
        }


        /// <summary>
        /// handler for request internally
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task<Unit> HandlerInternal(Request request, CancellationToken cancellationToken)
        {

            byte[] byteArray = ExtractFile(request.Data);
            //byte[] byteArray = Encoding.ASCII.GetBytes(contents);

            var actualFileType = ValidateFile(request, byteArray);

            var data = actualFileType == FileType.Picture
                ? await Resize(byteArray, request.ImageMaxDim, request.ImageQuality)
                : Array.Empty<byte>();

            using (var ms = new MemoryStream(data))
            {
                await _fileUpload.UploadFile(request.Name, ms);
            }

            var e = new FileUploaded(request.Name);
            await _mediator.Publish(e, cancellationToken);

            return Unit.Value;
        }

        /// <summary>
        /// get mime type, bytes for data uri
        /// </summary>
        /// <param name="dataUri"></param>
        /// <returns></returns>
        private byte[] ExtractFile(string dataUri)
        {
            if (dataUri.Contains("data") && dataUri.Contains("image"))
            {
                Regex dataUriPattern = new Regex(@"^data\:(?<type>image\/(png|tiff|jpg|gif|jpeg));base64,(?<data>[A-Z0-9\+\/\=]+)$", RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
                Match match = dataUriPattern.Match(dataUri);

                string base64Data = match.Groups["data"].Value;
                var binData = Convert.FromBase64String(base64Data);
                return binData;
            }
            else
            {
                return Convert.FromBase64String(dataUri);
            }
        }

        /// <summary>
        /// file validation..
        /// </summary>
        /// <param name="request"></param>
        /// <param name="mimeType"></param>
        /// <param name="dataBytes"></param>
        /// <returns></returns>
        private FileType ValidateFile(Request request, byte[] dataBytes)
        {
            if (request.Name.Contains("..\\"))
            {
                throw new IncorrectRequestException("Illegal file name");
            }

            if (!_fileTypeValidator.CheckFileType(dataBytes, Path.GetExtension(request.Name).Substring(1)))
            {
                throw new IncorrectRequestException("The file extension doesn't correspond the file type");
            }

            if (dataBytes.Length > MaxSizeBytes)
            {
                throw new IncorrectRequestException("Maximum file size is 5 MB");
            }

            var isPicture = _fileTypeValidator.IsPicture(dataBytes);
            var isDocument = _fileTypeValidator.IsDocument(dataBytes);
            var isPdf = _fileTypeValidator.IsPdf(dataBytes);

            if (request.FileType.Length == 0)
            {
                if (!isPicture && !isDocument)
                {
                    throw new IncorrectRequestException("Only pictures and documents are allowed");
                }
            }
            else
            {
                if (isPdf && !request.FileType.Contains(FileType.Pdf))
                {
                    throw new IncorrectRequestException("PDF documents are not allowed");
                }

                if (isPicture && !request.FileType.Contains(FileType.Picture))
                {
                    throw new IncorrectRequestException("Images are not allowed");
                }

                if (isDocument && !request.FileType.Contains(FileType.Document))
                {
                    throw new IncorrectRequestException("Documents are not allowed");
                }
            }

            return isPdf ? FileType.Pdf
                 : isPicture ? FileType.Picture
                 : isDocument ? FileType.Document
                 : FileType.Other;
        }

        /// <summary>
        /// resize file
        /// </summary>
        /// <param name="source"></param>
        /// <param name="size"></param>
        /// <param name="quality"></param>
        /// <returns></returns>
        private async Task<byte[]> Resize(byte[] source, int size, int quality)
        {
            if (source.Length <= ResizeLimitBytes)
            {
                return source;
            }

            byte[] buf;

            using (var ms = new MemoryStream(source))
            using (var result = new MemoryStream())
            {
                await _imageResizer.ResizeImage(ms, result, size, quality);
                buf = result.ToArray();
            }

            return buf;
        }

        private class Request
        {
            public Request(UploadFileCmd x)
            {
                Name = x.Name;
                Data = x.Data;
                FileType = x.FileType;
                ImageMaxDim = 1920;
                ImageQuality = 50;
                ThumbnailMaxDim = 128;
                ThumbnailQuality = 75;
            }

            public string Name { get; }
            //public string MimeType { get; }
            public string Data { get; }
            public FileType[] FileType { get; }
            public int ImageMaxDim { get; }
            public int ImageQuality { get; }
            public int ThumbnailMaxDim { get; }
            public int ThumbnailQuality { get; }
        }
    }
}
