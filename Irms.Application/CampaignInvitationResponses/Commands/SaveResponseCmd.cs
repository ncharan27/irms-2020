﻿using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitationResponses.Commands
{
    public class SaveResponseCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public UserAnswer Answer { get; set; }
        public MediaType MediaType { get; set; }
    }
}
