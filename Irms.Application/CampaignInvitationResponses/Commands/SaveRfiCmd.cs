﻿using Irms.Application.Templates.Queries;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Application.CampaignInvitationResponses.Commands
{
    public class SaveRfiCmd : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public MediaType MediaType { get; set; }

        public ICollection<SaveRfiAnswer> Answers { get; set; }

        public class SaveRfiAnswer
        {
            public Guid Id { get; set; }
            public string Answer { get; set; }
        }
    }
}
