﻿//using Irms.Application;
//using Irms.Application.Abstract.Repositories;
//using Irms.Application.Abstract.Repositories.Base;
//using Irms.Domain;
//using Irms.Domain.Entities.Tenant;
//using Irms.Infrastructure.Services;
//using Irms.Infrastructure.Services.Unifonic;
//using System;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Irms.Data
//{
//    /// <summary>
//    /// Configuration class for unifonic API
//    /// </summary>
//    public class UnifonicConfigurationProvider : IUnifonicConfigurationProvider
//    {
//        private readonly ITenantRepository<Tenant, Guid> _tenantRepository;
//        private readonly TenantBasicInfo _tenant;

//        public UnifonicConfigurationProvider(ITenantRepository<Tenant, Guid> tenantRepository, TenantBasicInfo tenant)
//        {
//            _tenantRepository = tenantRepository;
//            _tenant = tenant;
//        }

//        public async Task<UnifonicConfiguration> GetConfiguration(CancellationToken token)
//        {
//            var tenant = await _tenantRepository.GetById(_tenant.Id, token);
//            var result = new UnifonicConfiguration(tenant.UnifonicSid, tenant.UnifonicSender);

//            if (!result.IsValid)
//            {
//                throw new IncorrectRequestException("Unifonic configuration is invalid!");
//            }

//            return result;
//        }

//        public async Task<UnifonicConfiguration> GetConfiguration(Guid tenantId, CancellationToken token)
//        {
//            var tenant = await _tenantRepository.GetById(tenantId, token);
//            var result = new UnifonicConfiguration(tenant.UnifonicSid, tenant.UnifonicSender);

//            if (!result.IsValid)
//            {
//                throw new IncorrectRequestException("Unifonic configuration is invalid!");
//            }

//            return result;
//        }
//    }
//}
