﻿using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities.Tenant;
using Irms.Infrastructure.Services.Twilio;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    public class TwilioBGConfigurationProvider : ITwilioBGConfigurationProvider
    {
        private readonly ITenantRepository<Tenant, Guid> _tenantRepository;

        public TwilioBGConfigurationProvider(ITenantRepository<Tenant, Guid> tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        public async Task<TwilioConfiguration> GetConfiguration(Guid tenantId, CancellationToken token)
        {
            var tenant = await _tenantRepository.GetById(tenantId, token);
            var result = new TwilioConfiguration(tenant.TwilioAccountSid, tenant.TwilioAuthToken, tenant.TwilioNotificationServiceId);

            if (!result.IsValid)
            {
                throw new IncorrectRequestException("Twilio configuration is invalid!");
            }

            return result;
        }
    }
}