﻿using Irms.Application.Abstract.Repositories.UserManagement;
using Microsoft.AspNetCore.Identity;
using System;

namespace Irms.Data.IdentityClasses
{
    public class ApplicationUser : IdentityUser<Guid>, IUser
    {
        public bool IsSuperAdmin { get; set; }
        public Guid TenantId { get; set; }
        public bool IsEnabled { get; set; }
    }
}
