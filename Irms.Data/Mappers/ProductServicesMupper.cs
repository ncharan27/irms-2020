﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Mappers
{
    public class ProductServicesMupper : AutoMapper.Profile
    {
        public ProductServicesMupper()
        {
            CreateMap<Irms.Application.Product.Commands.ServiceFeature, Irms.Domain.Entities.ServiceCatalogFeature>()
                .ForMember(x => x.Id, x => x.MapFrom(m => m.FeatureId))
                .ForMember(x => x.FeatureTitle, x => x.MapFrom(m => m.FeatureTitle))
                .ForMember(x => x.IsCustom, x => x.MapFrom(m => m.IsCustom))
                .ForMember(x => x.IsFeatureEnabled, x => x.MapFrom(m => m.IsFeatureEnabled));

            CreateMap<Irms.Application.Product.Commands.ProductService, Domain.Entities.ServiceCatalog>()
                .ForMember(x => x.Id, x => x.MapFrom(m => m.Id))
                .ForMember(x => x.Title, x => x.MapFrom(m => m.Title))
                .ForMember(x => x.ServiceCatalogFeatures, x => x.MapFrom(m => m.ServiceFeatures));


        }

        
    }
}
