﻿using Irms.Application.Subscriptions.Commands;
using Irms.Data.EntityClasses;

namespace Irms.Data.Mappers
{
    public class SubscriptionMapper : AutoMapper.Profile
    {
        public SubscriptionMapper()
        {
            CreateMap<Domain.Entities.Subscription, TenantSubscription>();
            CreateMap<TenantSubscription, Domain.Entities.Subscription>();

            //commands mappers
            CreateMap<CreateSubscriptionCmd, Domain.Entities.Subscription>();

        }
    }


}
