﻿using System;

namespace Irms.Data.Abstract
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class HasSqlQueryAttribute : Attribute
    {
        public HasSqlQueryAttribute(string queryField, string paramsField = null, params string[] queryComplements)
        {
            ParamsField = paramsField;
            QueryField = queryField;
            QueryComplements = queryComplements;
        }

        public string QueryField { get; }
        public string[] QueryComplements { get; }
        public string ParamsField { get; }
    }
}
