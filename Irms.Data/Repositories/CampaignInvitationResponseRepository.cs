﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories;
using Irms.Domain;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class CampaignInvitationResponseRepository : ICampaignInvitationResponseRepository<Domain.Entities.CampaignInvitationResponse, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public CampaignInvitationResponseRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        public async Task<IEnumerable<RfiFormQuestion>> LoadQuestions(IEnumerable<Guid> ids, CancellationToken token)
        {
            var questions = await _dataContext.RfiFormQuestion
                .Where(x => ids.Contains(x.Id))
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<RfiFormQuestion>>(questions);
        }

        public async Task<bool> HasAnswersOnRfi(Guid rfiFormId, Guid contactId, CancellationToken token)
        {
            var hasAnswers = await _dataContext.RfiFormResponse
                .Where(x => x.RfiFormId == rfiFormId && x.ContactId == contactId)
                .AnyAsync(token);

            return hasAnswers;
        }

        public async Task SaveRfiAnswers(IEnumerable<Domain.Entities.RfiFormResponse> answers, CancellationToken token)
        {
            var mapped = _mapper.Map<IEnumerable<EntityClasses.RfiFormResponse>>(answers).ToList();
            mapped.ForEach(answer =>
            {
                answer.Id = Guid.NewGuid();
                answer.ResponseDate = DateTime.UtcNow;

                _dataContext.RfiFormResponse.Add(answer);
            });

            await _dataContext.SaveChangesAsync(token);
        }


        public Task<Guid> Create(CampaignInvitationResponse entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public async Task CreateRange(IEnumerable<CampaignInvitationResponse> entities, CancellationToken token)
        {
            entities.ToList().ForEach(x =>
            {
                x.Create();
            });

            var mapped = _mapper.Map<IEnumerable<EntityClasses.CampaignInvitationResponse>>(entities).ToList();
            CreateMediaResponses(mapped);

            _dataContext.CampaignInvitationResponse.AddRange(mapped);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task CreateRange(IEnumerable<CampaignInvitationResponse> entities, MediaType type, bool wasCreated, CancellationToken token)
        {
            List<EntityClasses.CampaignInvitationResponse> mapped = _mapper.Map<IEnumerable<EntityClasses.CampaignInvitationResponse>>(entities).ToList();
            await CreateMediaResponses(mapped, type, wasCreated);

            if (wasCreated)
                _dataContext.CampaignInvitationResponse.AddRange(mapped);

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task FillRange(IEnumerable<CampaignInvitationResponse> entities, CancellationToken token)
        {
            List<EntityClasses.CampaignInvitationResponse> mapped = _mapper.Map<IEnumerable<EntityClasses.CampaignInvitationResponse>>(entities).ToList();

            _dataContext.CampaignInvitationResponse.AddRange(mapped);
            await _dataContext.SaveChangesAsync(token);

            var ents = new List<EntityClasses.CampaignInvitationResponseMediaType>();
            foreach (var entity in mapped)
            {
                var emailMediaAccept = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.Email,
                    TenantId = entity.TenantId,
                    AcceptOrReject = ResponseMediaType.ACCEPTED,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    CampaignInvitationResponseId = entity.Id
                };

                var emailMediaReject = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.Email,
                    TenantId = entity.TenantId,
                    AcceptOrReject = ResponseMediaType.REJECTED,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    CampaignInvitationResponseId = entity.Id
                };

                var smsMediaAccept = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.Sms,
                    TenantId = entity.TenantId,
                    AcceptOrReject = ResponseMediaType.ACCEPTED,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    CampaignInvitationResponseId = entity.Id
                };

                var smsMediaReject = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.Sms,
                    TenantId = entity.TenantId,
                    AcceptOrReject = ResponseMediaType.REJECTED,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    CampaignInvitationResponseId = entity.Id
                };

                var whatsappMediaAccept = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.WhatsApp,
                    TenantId = entity.TenantId,
                    AcceptOrReject = ResponseMediaType.ACCEPTED,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    CampaignInvitationResponseId = entity.Id
                };

                var whatsappMediaReject = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.WhatsApp,
                    TenantId = entity.TenantId,
                    AcceptOrReject = ResponseMediaType.REJECTED,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    CampaignInvitationResponseId = entity.Id
                };

                ents.Add(emailMediaAccept);
                ents.Add(emailMediaReject);
                ents.Add(smsMediaAccept);
                ents.Add(smsMediaReject);
                ents.Add(whatsappMediaAccept);
                ents.Add(whatsappMediaReject);
                //entity.CampaignInvitationResponseMediaTypes = new List<EntityClasses.CampaignInvitationResponseMediaType>
                //{
                //    emailMediaAccept,
                //    emailMediaReject,
                //    smsMediaAccept,
                //    smsMediaReject
                //};
            }

            _dataContext.CampaignInvitationResponseMediaType.AddRange(ents);
            await _dataContext.SaveChangesAsync(token);
        }


        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get CampaignInvitationResponse by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Domain.Entities.CampaignInvitationResponse> GetById(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationResponse
                .FirstOrDefaultAsync(x => x.Id == id, token);

            var result = _mapper.Map<EntityClasses.CampaignInvitationResponse, Domain.Entities.CampaignInvitationResponse>(data);
            return result;
        }

        public async Task<Guid> LoadRfiFormIdByResponseId(Guid id, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationResponse
                .Include(x => x.CampaignInvitation)
                .ThenInclude(x => x.RfiForm)
                .FirstOrDefaultAsync(x => x.Id == id, token);

            return data.CampaignInvitation.RfiForm.First().Id;
        }


        public async Task<IEnumerable<ContactListToContact>> GetContactListsByInvitationId(Guid invitationId, CancellationToken token)
        {
            var invitation = await _dataContext.CampaignInvitation
                .Include(x => x.EventCampaign)
                .FirstOrDefaultAsync(x => x.Id == invitationId, token);

            var contactLists = await _dataContext.ContactList
                .Where(x => x.Id == invitation.EventCampaign.GuestListId)
                .Include(x => x.ContactListToContacts)
                .SelectMany(x => x.ContactListToContacts)
                .ToListAsync(token);

            return _mapper.Map<IEnumerable<ContactListToContact>>(contactLists);
        }

        public async Task<IEnumerable<Contact>> GetContactsByCampaignId(Guid campaignId, CancellationToken token)
        {
            var campaign = await _dataContext.EventCampaign
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == campaignId);

            var result = await _dataContext.ContactListToContacts
                .AsNoTracking()
                .Include(x => x.Contact)
                .Where(x => x.ContactListId == campaign.GuestListId)
                .Select(x => x.Contact)
                .FirstOrDefaultAsync(token);

            return _mapper.Map<IEnumerable<Contact>>(result);
        }

        public async Task Update(CampaignInvitationResponse entity, CancellationToken token)
        {
            var data = await _dataContext.CampaignInvitationResponse
                .FirstOrDefaultAsync(x => x.Id == entity.Id);

            if (data == null)
            {
                throw new IncorrectRequestException("Campaign invitation response id is incorrect");
            }

            data.Answer = (int?)entity.Answer;
            data.ResponseMediaType = (int?)entity.ResponseMediaType;
            data.ResponseDate = DateTime.UtcNow;
            await _dataContext.SaveChangesAsync(token);
        }

        private static void CreateMediaResponses(List<EntityClasses.CampaignInvitationResponse> mapped, ResponseMediaType acceptedOrRejected = ResponseMediaType.UNSET)
        {
            mapped.ForEach(entity =>
            {
                var smsMedia = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.Sms,
                    TenantId = entity.TenantId,
                    AcceptOrReject = acceptedOrRejected,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid()
                };

                var emailMedia = new EntityClasses.CampaignInvitationResponseMediaType
                {
                    MediaType = MediaType.Email,
                    TenantId = entity.TenantId,
                    AcceptOrReject = acceptedOrRejected,
                    CreatedOn = DateTime.UtcNow,
                    Id = Guid.NewGuid()
                };

                entity.CampaignInvitationResponseMediaTypes = new List<EntityClasses.CampaignInvitationResponseMediaType>
                {
                    smsMedia,
                    emailMedia
                };
            });
        }

        private async Task CreateMediaResponses(List<EntityClasses.CampaignInvitationResponse> mapped, MediaType type, bool isNew)
        {
            var ents = new List<EntityClasses.CampaignInvitationResponseMediaType>();

            if(type == MediaType.Email)
            {
                mapped.ForEach(entity =>
                {
                    var emailMediaAccept = new EntityClasses.CampaignInvitationResponseMediaType
                    {
                        MediaType = MediaType.Email,
                        TenantId = entity.TenantId,
                        AcceptOrReject = ResponseMediaType.ACCEPTED,
                        CreatedOn = DateTime.UtcNow,
                        Id = Guid.NewGuid(),
                        CampaignInvitationResponseId = entity.Id
                    };

                    var emailMediaReject = new EntityClasses.CampaignInvitationResponseMediaType
                    {
                        MediaType = MediaType.Email,
                        TenantId = entity.TenantId,
                        AcceptOrReject = ResponseMediaType.REJECTED,
                        CreatedOn = DateTime.UtcNow,
                        Id = Guid.NewGuid(),
                        CampaignInvitationResponseId = entity.Id
                    };

                    if (isNew)
                    {
                        entity.CampaignInvitationResponseMediaTypes = new List<EntityClasses.CampaignInvitationResponseMediaType>
                        {
                            emailMediaAccept,
                            emailMediaReject
                        };
                    }
                    else
                    {
                        emailMediaAccept.CampaignInvitationResponseId = entity.Id;
                        emailMediaReject.CampaignInvitationResponseId = entity.Id;
                        ents.Add(emailMediaAccept);
                        ents.Add(emailMediaReject);
                    }
                });
            }

            if(type == MediaType.Sms)
            {
                mapped.ForEach(entity =>
                {
                    var smsMediaAccept = new EntityClasses.CampaignInvitationResponseMediaType
                    {
                        MediaType = MediaType.Sms,
                        TenantId = entity.TenantId,
                        AcceptOrReject = ResponseMediaType.ACCEPTED,
                        CreatedOn = DateTime.UtcNow,
                        Id = Guid.NewGuid()
                    };
                    var smsMediaReject = new EntityClasses.CampaignInvitationResponseMediaType
                    {
                        MediaType = MediaType.Sms,
                        TenantId = entity.TenantId,
                        AcceptOrReject = ResponseMediaType.REJECTED,
                        CreatedOn = DateTime.UtcNow,
                        Id = Guid.NewGuid()
                    };

                    if (isNew)
                    {
                        entity.CampaignInvitationResponseMediaTypes = new List<EntityClasses.CampaignInvitationResponseMediaType>
                        {
                            smsMediaAccept,
                            smsMediaReject
                        };
                    }
                    else
                    {
                        smsMediaAccept.CampaignInvitationResponseId = entity.Id;
                        smsMediaReject.CampaignInvitationResponseId = entity.Id;
                        ents.Add(smsMediaAccept);
                        ents.Add(smsMediaReject);
                    }
                });
            }

            if (!isNew)
            {
                //var responses = mapped.SelectMany(x => x.CampaignInvitationResponseMediaTypes);
                //_dataContext.CampaignInvitationResponseMediaType.AddRange(responses);
                _dataContext.CampaignInvitationResponseMediaType.AddRange(ents);
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task<Domain.Entities.CampaignInvitationResponse> LoadUserStatus(Guid eventId, Guid listId, Guid contactId, CancellationToken token)
        {
            var availableInvitations = await _dataContext.CampaignInvitation
                .AsNoTracking()
                .Include(x => x.EventCampaign)
                .Where(x => x.EventCampaign.EventId == eventId && x.EventCampaign.GuestListId == listId)
                .ToListAsync(token);

            var availableInvitationIds = availableInvitations.Select(x => x.Id);

            var responses = await _dataContext.CampaignInvitationResponse
                .AsNoTracking()
                .Include(x => x.CampaignInvitationResponseMediaTypes)
                .Where(x => x.ContactId == contactId && availableInvitationIds.Contains(x.CampaignInvitationId))
                .ToListAsync(token);

            var responseWithAnswer = responses
                .FirstOrDefault(x => x.Answer.HasValue && (x.Answer == (int)UserAnswer.ACCEPTED || x.Answer == (int)UserAnswer.REJECTED))
                ?? responses.FirstOrDefault();

            return _mapper.Map<Domain.Entities.CampaignInvitationResponse>(responseWithAnswer);
        }
    }
}
