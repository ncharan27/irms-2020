﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ListAnalysis = Irms.Domain.Entities.ListAnalysis;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract;
using Irms.Application;
using Microsoft.EntityFrameworkCore;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.Application.Campaigns.Commands;
using System.Linq;
using System.Collections.Generic;
using Irms.Application.Campaigns.ReadModels;
using Irms.Application.ListAnalysis.Commands;

namespace Irms.Data.Repositories
{
    public class ListAnalysisRepository : IListAnalysisRepository<ListAnalysis, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public ListAnalysisRepository(IrmsDataContext dataContext, IMapper mapper,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _user = user;
            _tenant = tenant;
        }

        public Task<Guid> Create(ListAnalysis entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public async Task Delete(Guid id, CancellationToken token)
        {
            var fields = await _dataContext.ListAnalysisImportantFields
                .Where(x => x.GuestListId == id)
                .ToListAsync(token);

            //del
            _dataContext.ListAnalysisImportantFields.RemoveRange(fields);

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task DeleteCampaign(Guid id, CancellationToken token)
        {
            var data = await _dataContext.EventCampaign
                .Include(x => x.CampaignPrefferedMedia)
                .FirstOrDefaultAsync(x => x.Id == id, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Campaign Id is incorrect");
            }

            //delete medias
            _dataContext.CampaignPrefferedMedia.RemoveRange(data.CampaignPrefferedMedia);

            //delete campaign
            _dataContext.EventCampaign.Remove(data);

            await _dataContext.SaveChangesAsync(token);
        }

        public Task<ListAnalysis> GetById(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task Update(ListAnalysis entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// update campaign email response form
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task AddImportantFieldsToGuestList(CreateListAnalysisImportantFieldsCmd entity, CancellationToken token)
        {
            var fields = await _dataContext.ListAnalysisImportantFields
                .Where(x => x.GuestListId == entity.ListId)
                .ToListAsync(token);

            //del
            _dataContext.ListAnalysisImportantFields.RemoveRange(fields);

            //add custom fields
            entity.CustomFields.ToList()
                .ForEach(x =>
                {
                    var d = new EntityClasses.ListAnalysisImportantFields();
                    d.Id = Guid.NewGuid();
                    d.GuestListId = entity.ListId;
                    d.FieldType = (int)FieldType.Custom;
                    d.Field = x.Value;
                    d.IsRequired = x.IsRequired;
                    d.TenantId = _tenant.Id;
                    d.CreatedById = _user.Id;
                    d.CreatedOn = DateTime.UtcNow;

                    _dataContext.ListAnalysisImportantFields.Add(d);
                });

            //add reserve fields
            entity.ReservedFields.ToList()
                .ForEach(x =>
                {
                    var d = new EntityClasses.ListAnalysisImportantFields();
                    d.Id = Guid.NewGuid();
                    d.GuestListId = entity.ListId;
                    d.FieldType = (int)FieldType.Reserved;
                    d.Field = x.Value;
                    d.IsRequired = x.IsRequired;
                    d.TenantId = _tenant.Id;
                    d.CreatedById = _user.Id;
                    d.CreatedOn = DateTime.UtcNow;

                    _dataContext.ListAnalysisImportantFields.Add(d);
                });

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task SetListAnalysisState(SetListAnalysisStateCmd request, CancellationToken token)
        {
            var data = await _dataContext.ContactList
                .FirstOrDefaultAsync(x => x.Id == request.ListId && x.EventId == request.EventId, token);
            if (data == null)
            {
                throw new IncorrectRequestException("Guest list Id is incorrect");
            }

            data.ListAnalysisStep = (int)request.ListAnalysisStep;
            data.ListAnalysisPageNo = request.ListAnalysisPageNo;
            data.ListAnalysisPageSize = request.ListAnalysisPageSize;

            await _dataContext.SaveChangesAsync(token);
        }

        public async Task ResetListAnalysisState(Guid listId, Guid eventId, CancellationToken token)
        {
            var data = await _dataContext.ContactList
                .FirstOrDefaultAsync(x => x.Id == listId && x.EventId == eventId, token);

            if (data == null)
            {
                throw new IncorrectRequestException("Guest list Id is incorrect");
            }

            data.ListAnalysisStep = data.ListAnalysisPageNo = data.ListAnalysisPageSize = null;
            await _dataContext.SaveChangesAsync(token);
        }
    }
}
