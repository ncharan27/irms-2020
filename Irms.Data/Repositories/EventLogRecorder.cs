﻿using Irms.Application;
using Irms.Application.Abstract;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Application.Abstract.Services;
using Irms.Data.EntityClasses;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class EventLogRecorder : IEventLogRecorder
    {
        private readonly ISerializer _serializer;
        private readonly IrmsTenantDataContext _dataContext;
        private readonly ICurrentUser _user;
        private readonly TenantBasicInfo _tenant;

        public EventLogRecorder(
            ISerializer serializer,
            IrmsTenantDataContext dataContext,
            ICurrentUser user,
            TenantBasicInfo tenant)
        {
            _serializer = serializer;
            _dataContext = dataContext;
            _user = user;
            _tenant = tenant;
        }

        public async Task AppendEventToLog(IEvent @event, CancellationToken token)
        {
            var record = new EventLog
            {
                Id = Guid.NewGuid(),
                OccurredOn = DateTime.UtcNow,
                EventType = @event.GetType().AssemblyQualifiedName,
                Action = @event.GetType().Name,
                Description = @event.Format(d => d.ToString(), _user.Id.ToString()),
                TenantId = _tenant.Id,
                UserId = _user.Id,
                Data = _serializer.Serialize(@event),
                ObjectId = @event.ObjectId,
                ObjectId2 = @event.ObjectId2,
                ObjectId3 = @event.ObjectId3,
            };

            await _dataContext.EventLog.AddAsync(record, token);
            await _dataContext.SaveChangesAsync(token);
        }

        public async Task AppendEventToTenantLog(ITenantEvent @event, CancellationToken token)
        {
            var record = new EventLog
            {
                Id = Guid.NewGuid(),
                OccurredOn = DateTime.UtcNow,
                EventType = @event.GetType().AssemblyQualifiedName,
                Action = @event.GetType().Name,
                Description = @event.Format(d => d.ToString()),
                TenantId = @event.TenantId,
                Data = _serializer.Serialize(@event),
                ObjectId = @event.ObjectId,
                ObjectId2 = @event.ObjectId2,
                ObjectId3 = @event.ObjectId3,
            };

            await _dataContext.EventLog.AddAsync(record, token);
            await _dataContext.SaveChangesAsync(token);
        }
    }
}
