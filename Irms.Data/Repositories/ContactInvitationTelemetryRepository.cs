﻿using AutoMapper;
using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Repositories
{
    public class ContactInvitationTelemetryRepository : IContactInvitationTelemetryRepository<ContactInvitationTelemetry, Guid>
    {
        private readonly IrmsDataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly TenantBasicInfo _tenant;

        public ContactInvitationTelemetryRepository(
            IrmsDataContext context, 
            IMapper mapper,
            TenantBasicInfo tenant
            )
        {
            _dataContext = context;
            _mapper = mapper;
            _tenant = tenant;
        }

        public async Task<Guid> Create(ContactInvitationTelemetry entity, CancellationToken token)
        {
            var data = _mapper.Map<ContactInvitationTelemetry, EntityClasses.ContactInvitationTelemetry>(entity);
            data.TenantId = _tenant.Id;
            _dataContext.ContactInvitationTelemetry.Add(data);
            await _dataContext.SaveChangesAsync(token);
            return data.Id;
        }

        public Task Delete(Guid id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public async Task<ContactInvitationTelemetry> GetById(Guid id, CancellationToken token)
        {
            var entity = await _dataContext.ContactInvitationTelemetry
                .FirstOrDefaultAsync(x => x.Id == id, token);

            return _mapper.Map<ContactInvitationTelemetry>(entity);
        }

        public Task Update(ContactInvitationTelemetry entity, CancellationToken token)
        {
            throw new NotImplementedException();
        }
    }
}
