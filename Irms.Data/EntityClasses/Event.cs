﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class Event
    {
        public Event()
        {
            EventCampaign = new HashSet<EventCampaign>();
            EventFeature = new HashSet<EventFeature>();
            EventLocation = new HashSet<EventLocation>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public short? EventTypeId { get; set; }
        public bool IsLimitedAttendees { get; set; }
        public int? MaximumAttendees { get; set; }
        public int? OverflowAttendees { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string TimeZoneUtcOffset { get; set; }
        public string TimeZoneName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual EventType EventType { get; set; }
        public virtual ICollection<EventCampaign> EventCampaign { get; set; }
        public virtual ICollection<EventFeature> EventFeature { get; set; }
        public virtual ICollection<EventLocation> EventLocation { get; set; }
    }
}
