﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class Country
    {
        public Country()
        {
            CountryTranslation = new HashSet<CountryTranslation>();
            EventLocation = new HashSet<EventLocation>();
            Tenant = new HashSet<Tenant>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ShortIso { get; set; }
        public string LongIso { get; set; }
        public string Latitude { get; set; }
        public string Longigude { get; set; }
        public string Nationality { get; set; }

        public virtual ICollection<CountryTranslation> CountryTranslation { get; set; }
        public virtual ICollection<EventLocation> EventLocation { get; set; }
        public virtual ICollection<Tenant> Tenant { get; set; }
    }
}
