﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class Tenant
    {
        public Tenant()
        {
            ContactList = new HashSet<ContactList>();
            EventLog = new HashSet<EventLog>();
            Language = new HashSet<Language>();
            Template = new HashSet<Template>();
            TemplateSelection = new HashSet<TemplateSelection>();
            TemplateTranslation = new HashSet<TemplateTranslation>();
            TenantContactInfo = new HashSet<TenantContactInfo>();
            TenantSubscription = new HashSet<TenantSubscription>();
            UserInfoNavigation = new HashSet<UserInfo>();
            CampaignInvitationResponses = new HashSet<CampaignInvitationResponse>();
            CampaignInvitationResponseMediaTypes = new HashSet<CampaignInvitationResponseMediaType>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string LogoPath { get; set; }
        public Guid CountryId { get; set; }
        public string City { get; set; }
        public string ClientUrl { get; set; }
        public string EmailFrom { get; set; }
        public string SendGridApiKey { get; set; }
        public string MalathUsername { get; set; }
        public string MalathPassword { get; set; }
        public string MalathSender { get; set; }
        public string UnifonicSid { get; set; }
        public string UnifonicSender { get; set; }
        public string TwilioAccountSid { get; set; }
        public string TwilioAuthToken { get; set; }
        public string TwilioNotificationServiceId { get; set; }
        public string HeaderPath { get; set; }
        public string FooterPath { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool? HasOwnDomain { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Guid? AdminId { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }

        public virtual Country Country { get; set; }
        public virtual UserInfo UserInfo { get; set; }
        public virtual ICollection<ContactList> ContactList { get; set; }
        public virtual ICollection<EventLog> EventLog { get; set; }
        public virtual ICollection<Language> Language { get; set; }
        public virtual ICollection<Template> Template { get; set; }
        public virtual ICollection<TemplateSelection> TemplateSelection { get; set; }
        public virtual ICollection<TemplateTranslation> TemplateTranslation { get; set; }
        public virtual ICollection<TenantContactInfo> TenantContactInfo { get; set; }
        public virtual ICollection<TenantSubscription> TenantSubscription { get; set; }
        public virtual ICollection<UserInfo> UserInfoNavigation { get; set; }
        public virtual ICollection<CampaignInvitationResponse> CampaignInvitationResponses { get; set; }
        public virtual ICollection<CampaignInvitationResponseMediaType> CampaignInvitationResponseMediaTypes { get; set; }
    }
}
