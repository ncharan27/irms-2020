﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class CampaignEntryCriteriaType
    {
        public CampaignEntryCriteriaType()
        {
            EventCampaign = new HashSet<EventCampaign>();
        }

        public byte Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<EventCampaign> EventCampaign { get; set; }
    }
}
