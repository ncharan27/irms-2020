﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class EventLog
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string EventType { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public DateTime OccurredOn { get; set; }
        public Guid? UserId { get; set; }
        public string Data { get; set; }
        public Guid? ObjectId { get; set; }
        public Guid? ObjectId2 { get; set; }
        public Guid? ObjectId3 { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
