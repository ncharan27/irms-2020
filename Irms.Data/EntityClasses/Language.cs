﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class Language
    {
        public Language()
        {
            TemplateTranslation = new HashSet<TemplateTranslation>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Culture { get; set; }
        public string Name { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<TemplateTranslation> TemplateTranslation { get; set; }
    }
}
