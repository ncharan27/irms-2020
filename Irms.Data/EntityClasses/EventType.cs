﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class EventType
    {
        public EventType()
        {
            Event = new HashSet<Event>();
        }

        public short Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<Event> Event { get; set; }
    }
}
