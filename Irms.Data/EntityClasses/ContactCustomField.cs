﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.EntityClasses
{
    public class ContactCustomField
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CustomFieldId { get; set; }
        public Guid ContactId { get; set; }
        public string Value { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedById { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual CustomField CustomField { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
