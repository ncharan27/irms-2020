﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class ContactList
    {
        public ContactList()
        {
            ContactListToContacts = new HashSet<ContactListToContacts>();
            ContactListImportantField = new HashSet<ContactListImportantField>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public bool IsGlobal { get; set; }
        public bool IsGuest { get; set; }
        public Guid? EventId { get; set; }
        public int? ListAnalysisStep { get; set; }
        public int? ListAnalysisPageNo { get; set; }
        public int? ListAnalysisPageSize { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<ContactListToContacts> ContactListToContacts { get; set; }
        public virtual ICollection<ContactListImportantField> ContactListImportantField { get; set; }
    }
}