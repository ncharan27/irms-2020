﻿using System;
using System.Collections.Generic;

namespace Irms.Data.EntityClasses
{
    public partial class UserInfo
    {
        public UserInfo()
        {
            Template = new HashSet<Template>();
            Tenant = new HashSet<Tenant>();
        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public int RoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public int? GenderId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string NationalId { get; set; }
        public string PassportNo { get; set; }
        public string SearchText { get; set; }
        public bool IsActive { get; set; }
        public Guid? UserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }

        public virtual Tenant TenantNavigation { get; set; }
        public virtual ICollection<Template> Template { get; set; }
        public virtual ICollection<Tenant> Tenant { get; set; }
    }
}
