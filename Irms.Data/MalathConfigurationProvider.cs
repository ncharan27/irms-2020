﻿using Irms.Application;
using Irms.Application.Abstract.Repositories;
using Irms.Application.Abstract.Repositories.Base;
using Irms.Domain;
using Irms.Domain.Entities.Tenant;
using Irms.Infrastructure.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data
{
    /// <summary>
    /// Configuration class for Twilio API
    /// </summary>
    public class MalathConfigurationProvider : IMalathConfigurationProvider
    {
        private readonly ITenantRepository<Tenant, Guid> _tenantReposiotry;
        private readonly TenantBasicInfo _tenant;

        public MalathConfigurationProvider(ITenantRepository<Tenant, Guid> tenantRepository, TenantBasicInfo tenant)
        {
            _tenantReposiotry = tenantRepository;
            _tenant = tenant;
        }

        public async Task<MalathConfiguration> GetConfiguration(CancellationToken token)
        {
            var tenant = await _tenantReposiotry.GetById(_tenant.Id, token);
            var result = new MalathConfiguration(
                username: tenant.MalathUsername,
                password: tenant.MalathPassword,
                senderName: tenant.MalathSender);

            if (!result.IsValid)
            {
                throw new IncorrectRequestException("Malath configuration is invalid!");
            }

            return result;
        }
    }
}
