﻿using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading.Tasks;
using Dapper;
using Serilog;

namespace Irms.Data
{
    public static class DapperExtensions
    {
        public static async Task<IEnumerable<T>> QueryAndLog<T>(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {
            var sw = Stopwatch.StartNew();
            var result = await cnn.QueryAsync<T>(sql, param, transaction, commandTimeout, commandType);
            Log.Information("Dapper has executed a query: {0}, elapsed ms: {1}", sql, sw.ElapsedMilliseconds);
            sw.Stop();
            return result;
        }

        public static async Task<T> QueryFirstOrDefaultAndLog<T>(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {
            var sw = Stopwatch.StartNew();
            var result = await cnn.QueryFirstOrDefaultAsync<T>(sql, param, transaction, commandTimeout, commandType);
            Log.Information("Dapper has executed a query: {0}, elapsed ms: {1}", sql, sw.ElapsedMilliseconds);
            sw.Stop();
            return result;
        }

        public static async Task<T> ExecuteScalarAndLog<T>(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {
            var sw = Stopwatch.StartNew();
            var result = await cnn.ExecuteScalarAsync<T>(sql, param, transaction, commandTimeout, commandType);
            Log.Information("Dapper has executed a query: {0}, elapsed ms: {1}", sql, sw.ElapsedMilliseconds);
            sw.Stop();
            return result;
        }

        public static async Task<object> ExecuteScalarAndLog(
            this IDbConnection cnn,
            string sql,
            object param = null,
            IDbTransaction transaction = null,
            int? commandTimeout = null,
            CommandType? commandType = null)
        {
            var sw = Stopwatch.StartNew();
            var result = await cnn.ExecuteScalarAsync(sql, param, transaction, commandTimeout, commandType);
            Log.Information("Dapper has executed a query: {0}, elapsed ms: {1}", sql, sw.ElapsedMilliseconds);
            sw.Stop();
            return result;
        }
    }
}
