﻿CREATE TABLE [dbo].[LicensePeriod] (
    [Id]    TINYINT       NOT NULL,
    [Title] NVARCHAR (50) NOT NULL,
    [Days]  INT           NULL,
    CONSTRAINT [PK_LicensePeriodId] PRIMARY KEY CLUSTERED ([Id] ASC)
);

