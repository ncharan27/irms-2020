﻿CREATE TABLE [dbo].[ContactReachability] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [TenantId]                UNIQUEIDENTIFIER NOT NULL,
    [ContactId]               UNIQUEIDENTIFIER NOT NULL,
    [WhatsApp]                BIT              NOT NULL,
    [Sms]                     BIT              NOT NULL,
    [Email]                   BIT              NOT NULL,
    [WhatsAppLastReachableOn] DATETIME         NULL,
    [OptStatus]               BIT              NULL,
    [OptDate]                 DATETIME         NULL,
    [SmsLastReachableOn]      DATETIME         NULL,
    [EmailLastReachableOn]    DATETIME         NULL,
    [CreatedOn]               DATETIME         NOT NULL,
    [CreatedById]             UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]              DATETIME         NULL,
    [ModifiedById]            UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ContactReachability] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_ContactReachability_Contacts] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId])
);



