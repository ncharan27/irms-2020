﻿CREATE TABLE [dbo].[TenantContactInfo] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [TenantId]  UNIQUEIDENTIFIER NOT NULL,
    [FirstName] NVARCHAR (100)   NOT NULL,
    [LastName]  NVARCHAR (100)   NOT NULL,
    [Email]     NVARCHAR (50)    NOT NULL,
    [MobileNo]  NVARCHAR (20)    NOT NULL,
    [PhoneNo]   NVARCHAR (20)    NULL,
    [UpdatedOn] DATETIME         NULL,
    [UpdateBy]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TenantContactInfo_1] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_TenantContactInfo_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);

