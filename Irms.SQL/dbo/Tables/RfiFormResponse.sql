﻿CREATE TABLE [dbo].[RfiFormResponse] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NOT NULL,
    [RfiFormId]         UNIQUEIDENTIFIER NOT NULL,
    [RfiFormQuestionId] UNIQUEIDENTIFIER NOT NULL,
    [ContactId]         UNIQUEIDENTIFIER NOT NULL,
    [Answer]            NVARCHAR (MAX)   NOT NULL,
    [ResponseDate]      DATETIME         NOT NULL,
    CONSTRAINT [PK_CampaignFormResponse] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignFormResponse_CampaignForm] FOREIGN KEY ([RfiFormId], [TenantId]) REFERENCES [dbo].[RfiForm] ([Id], [TenantId]),
    CONSTRAINT [FK_CampaignFormResponse_CampaignFormQuestion] FOREIGN KEY ([RfiFormQuestionId], [TenantId]) REFERENCES [dbo].[RfiFormQuestion] ([Id], [TenantId]),
    CONSTRAINT [FK_CampaignFormResponse_Contact] FOREIGN KEY ([ContactId], [TenantId]) REFERENCES [dbo].[Contact] ([Id], [TenantId])
);

