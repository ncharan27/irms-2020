﻿CREATE TABLE [dbo].[Template] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (250)   NOT NULL,
    [TypeId]          INT              NOT NULL,
    [SmsCompatible]   BIT              NOT NULL,
    [EmailCompatible] BIT              NOT NULL,
    [SmsText]         NVARCHAR (1000)  NULL,
    [EmailSubject]    NVARCHAR (500)   NULL,
    [EmailBody]       NVARCHAR (MAX)   NULL,
    [ModifiedOn]      DATETIME         NOT NULL,
    [ModifiedById]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_Template_Employee] FOREIGN KEY ([ModifiedById], [TenantId]) REFERENCES [dbo].[UserInfo] ([Id], [TenantId]),
    CONSTRAINT [FK_Template_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);

