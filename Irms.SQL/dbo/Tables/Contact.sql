﻿CREATE TABLE [dbo].[Contact] (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [TenantId]              UNIQUEIDENTIFIER NOT NULL,
    [Title]                 NVARCHAR (100)   NULL,
    [FullName]              NVARCHAR (100)   NULL,
    [PreferredName]         NVARCHAR (100)   NULL,
    [GenderId]              INT              NULL,
    [Email]                 NVARCHAR (100)   NOT NULL,
    [AlternativeEmail]      NVARCHAR (50)    NULL,
    [MobileNumber]          NVARCHAR (50)    NOT NULL,
    [SecondaryMobileNumber] NVARCHAR (50)    NULL,
    [WorkNumber]            NVARCHAR (50)    NULL,
    [NationalityId]         UNIQUEIDENTIFIER NULL,
    [DocumentTypeId]        INT              NULL,
    [DocumentNumber]        NVARCHAR (50)    NULL,
    [IssuingCountryId]      UNIQUEIDENTIFIER NULL,
    [ExpirationDate]        DATETIME         NULL,
    [Organization]          NVARCHAR (100)   NULL,
    [Position]              NVARCHAR (100)   NULL,
    [Active]                BIT              CONSTRAINT [DF_Contact_Active] DEFAULT ((1)) NOT NULL,
    [Deleted]               BIT              CONSTRAINT [DF_Contact_Deleted] DEFAULT ((0)) NOT NULL,
    [IsGuest]               BIT              NULL,
    [CreatedById]           UNIQUEIDENTIFIER NULL,
    [CreatedOn]             DATETIME         NULL,
    [ModifiedById]          UNIQUEIDENTIFIER NULL,
    [ModifiedOn]            DATETIME         NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_Contacts_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);











