﻿CREATE TABLE [dbo].[RfiForm] (
    [Id]                       UNIQUEIDENTIFIER NOT NULL,
    [TenantId]                 UNIQUEIDENTIFIER NOT NULL,
    [CampaignInvitationId]     UNIQUEIDENTIFIER NOT NULL,
    [WelcomeHtml]              NVARCHAR (MAX)   NOT NULL,
    [SubmitHtml]               NVARCHAR (MAX)   NOT NULL,
    [ThanksHtml]               NVARCHAR (MAX)   NOT NULL,
    [FormTheme]                NVARCHAR (MAX)   NOT NULL,
    [ThemeBackgroundImagePath] NVARCHAR (500)   NULL,
    [FormSettings]             NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]                DATETIME         NOT NULL,
    [CreatedById]              UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]               DATETIME         NULL,
    [ModifiedById]             UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CampaignForm] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignForm_CampaignInvitation] FOREIGN KEY ([CampaignInvitationId], [TenantId]) REFERENCES [dbo].[CampaignInvitation] ([Id], [TenantId])
);

