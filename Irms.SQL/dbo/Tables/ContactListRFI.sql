﻿CREATE TABLE [dbo].[ContactListRFI]
(
    Id UNIQUEIDENTIFIER NOT NULL,
    TenantId UNIQUEIDENTIFIER NOT NULL,
    RfiFormId UNIQUEIDENTIFIER NOT NULL,
    GuestListId UNIQUEIDENTIFIER NOT NULL,
    Status INT NOT NULL,
    SentDate DATETIME NULL,
    [CreatedById] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn] DATETIME NOT NULL,
    [ModifiedById] UNIQUEIDENTIFIER NULL,
    [ModifiedOn] DATETIME NULL,
    CONSTRAINT PK_ContactListRFI PRIMARY KEY CLUSTERED (Id ASC, TenantId ASC),
    CONSTRAINT FK_ContactListRFI_Tenant FOREIGN KEY (TenantId) REFERENCES [dbo].[Tenant] (Id),
    CONSTRAINT [FK_ContactListRFI_RfiForm] FOREIGN KEY (RfiFormId, [TenantId]) REFERENCES [dbo].RfiForm ([Id], [Tenantid]),
    CONSTRAINT [FK_ContactListRFI_ContactList] FOREIGN KEY (GuestListId, [TenantId]) REFERENCES [dbo].ContactList ([Id], [Tenantid]),
)
