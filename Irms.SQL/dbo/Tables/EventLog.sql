﻿CREATE TABLE [dbo].[EventLog] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [EventType]   NVARCHAR (256)   NOT NULL,
    [Action]      NVARCHAR (256)   CONSTRAINT [DF_EventLog_Action] DEFAULT (N'Test') NOT NULL,
    [Description] NVARCHAR (MAX)   CONSTRAINT [DF_EventLog_Description] DEFAULT (N'test') NOT NULL,
    [OccurredOn]  DATETIME         NOT NULL,
    [UserId]      UNIQUEIDENTIFIER NULL,
    [Data]        NVARCHAR (MAX)   NOT NULL,
    [ObjectId]    UNIQUEIDENTIFIER NULL,
    [ObjectId2]   UNIQUEIDENTIFIER NULL,
    [ObjectId3]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC)
);







