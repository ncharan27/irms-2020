﻿
CREATE TABLE [dbo].[CampaignWhatsappTemplate] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [TenantId]             UNIQUEIDENTIFIER NOT NULL,
    [CampaignInvitationId] UNIQUEIDENTIFIER NOT NULL,
    [SenderName]           NVARCHAR (50)    NOT NULL,
    [Body]                 NVARCHAR (1000)  NOT NULL,
    [WelcomeHtml]          NVARCHAR (MAX)   NOT NULL,
    [ProceedButtonText]    NVARCHAR (100)   NOT NULL,
    [RSVPHtml]             NVARCHAR (MAX)   NOT NULL,
    [AcceptButtonText]     NVARCHAR (100)   NOT NULL,
    [RejectButtonText]     NVARCHAR (100)   NOT NULL,
    [AcceptHtml]           NVARCHAR (MAX)   NOT NULL,
    [RejectHtml]           NVARCHAR (MAX)   NOT NULL,
    [ThemeJson]            NVARCHAR (MAX)   NULL,
    [BackgroundImagePath]  NVARCHAR (300)   NULL,
    [CreatedOn]            DATETIME         NOT NULL,
    [CreatedById]          UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]           DATETIME         NULL,
    [ModifiedById]         UNIQUEIDENTIFIER NULL,
    [TemplateId]           UNIQUEIDENTIFIER NULL,
    [IsBot]                BIT              NULL,
    CONSTRAINT [PK_CampaignWhatsappTemplate] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_CampaignWhatsappTemplate_CampaignInvitation] FOREIGN KEY ([CampaignInvitationId], [TenantId]) REFERENCES [dbo].[CampaignInvitation] ([Id], [TenantId]),
    CONSTRAINT [FK_CampaignWhatsappTemplate_WhatsappApprovedTemplate] FOREIGN KEY ([TemplateId], [TenantId]) REFERENCES [dbo].[WhatsappApprovedTemplate] ([Id], [TenantId])
);


