﻿CREATE TABLE [dbo].[EventLocation] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [EventId]      UNIQUEIDENTIFIER NOT NULL,
    [Name]         NVARCHAR (500)   NOT NULL,
    [City]         NVARCHAR (200)   NULL,
    [Region]       NVARCHAR (200)   NULL,
    [ZipCode]      NVARCHAR (20)    NULL,
    [CountryId]    UNIQUEIDENTIFIER NOT NULL,
    [Latitude]     DECIMAL (12, 9)  NOT NULL,
    [Longitude]    DECIMAL (12, 9)  NOT NULL,
    [CreatedById]  UNIQUEIDENTIFIER NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_EventLocation_CreatedOn] DEFAULT (getdate()) NULL,
    [ModifiedById] UNIQUEIDENTIFIER NULL,
    [ModifiedOn]   DATETIME         NULL,
    CONSTRAINT [PK_EventLocation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_EventLocation_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([ID]),
    CONSTRAINT [FK_EventLocation_Event] FOREIGN KEY ([EventId], [TenantId]) REFERENCES [dbo].[Event] ([Id], [TenantId])
);



