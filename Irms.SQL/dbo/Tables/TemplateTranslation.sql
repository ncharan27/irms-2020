﻿CREATE TABLE [dbo].[TemplateTranslation] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [TemplateId]   UNIQUEIDENTIFIER NOT NULL,
    [LanguageId]   UNIQUEIDENTIFIER NOT NULL,
    [SmsText]      NVARCHAR (1000)  NULL,
    [EmailSubject] NVARCHAR (500)   NULL,
    [EmailBody]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_TemplateTranslation] PRIMARY KEY CLUSTERED ([Id] ASC, [TenantId] ASC),
    CONSTRAINT [FK_TemplateTranslation_Language] FOREIGN KEY ([LanguageId], [TenantId]) REFERENCES [dbo].[Language] ([Id], [TenantId]),
    CONSTRAINT [FK_TemplateTranslation_Template] FOREIGN KEY ([TemplateId], [TenantId]) REFERENCES [dbo].[Template] ([Id], [TenantId]),
    CONSTRAINT [FK_TemplateTranslation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Template_Translation]
    ON [dbo].[TemplateTranslation]([TemplateId] ASC, [LanguageId] ASC);

