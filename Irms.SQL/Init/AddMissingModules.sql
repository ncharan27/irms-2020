DECLARE @T TABLE (Id INT)
SET NOCOUNT ON
INSERT INTO @T VALUES (0)
INSERT INTO @T VALUES (1)
INSERT INTO @T VALUES (2)
INSERT INTO @T VALUES (3)
INSERT INTO @T VALUES (4)
INSERT INTO @T VALUES (5)
INSERT INTO @T VALUES (6)
INSERT INTO @T VALUES (7)
INSERT INTO @T VALUES (8)
INSERT INTO @T VALUES (9)
INSERT INTO @T VALUES (10)
SET NOCOUNT OFF

MERGE INTO [TenantToSystemModule] as target USING(
	SELECT NEWID(), t1.ID, T.Id
	FROM Tenant t1, @T T

) as source([Id],[TenantId], [SystemModuleId])
on target.[TenantId] = source.[TenantId] and target.[SystemModuleId] = source.[SystemModuleId]
WHEN NOT MATCHED BY TARGET THEN
	INSERT([Id],[TenantId], [SystemModuleId])
	VALUES([Id],[TenantId], [SystemModuleId]);


select * from TenantToSystemModule