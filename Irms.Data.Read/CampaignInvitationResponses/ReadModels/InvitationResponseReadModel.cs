﻿using Irms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitationResponses.ReadModels
{
    public class InvitationResponseReadModel
    {
        public Guid CampaignInvitationId { get; set; }
        public Guid CampaignInvitationResponseId { get; set; }
        public Guid? PendingRfiId { get; set; }
        public string WelcomeHtml { get; set; }
        public string RsvpHtml { get; set; }
        public string AcceptedHtml { get; set; }
        public string RejectedHtml { get; set; }
        public MediaType MediaType { get; set; }
        public ResponseMediaType? AcceptedOrRejected { get; set; }
        public string BackgroundImageUrl { get; set; }

        // only for sms
        public string ProceedButtonText { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
        public string ThemeObj { get; set; }

        // error message
        public string Error { get; set; }
    }
}
