﻿using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitationResponses.QueryHandlers
{
    public class GetCampaignInvitationRfiQuery : IRequest<InvitationResponseRfiModel>
    {
        public GetCampaignInvitationRfiQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
