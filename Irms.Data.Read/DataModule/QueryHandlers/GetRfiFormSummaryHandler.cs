﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Data.Read.DataModule.ReadModels.RfiFormSummaryReadModel;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    public class GetRfiFormSummaryHandler : IRequestHandler<GetRfiFormSummaryQuery, RfiFormSummaryReadModel>
    {
        public readonly string GetRfiFormTotalCountQuery = @"
            SELECT
            rf.Id,
            COUNT (DISTINCT rfr.ContactId) as ResponseCount
			FROM RfiForm rf
            INNER JOIN RfiFormResponse rfr on rfr.RfiFormId = rf.Id
            WHERE rf.id = @formId
			AND rfr.TenantId=@tenantId
            GROUP BY rf.Id";

        public readonly string GetRfiFormQuestionsQuery = @"SELECT
	            rfq.Id,
	            rfq.Mapped AS QuestionMappingType,
	            rfq.MappedField AS MappedField,
	            rfq.SortOrder,
	            rfq.Question,
	            COUNT(DISTINCT rfr.ContactId) AS AnsweredCount,
				JSON_VALUE(rfq.question, '$.type') AS TP,
				CASE
					WHEN JSON_VALUE(rfq.question, '$.type') = 'rating' THEN 
						(SELECT
							rfr1.Answer AS [option],
							CAST((
								CAST(
									COUNT(rfr1.Id) AS FLOAT) / CAST((SELECT COUNT(*) FROM RfiFormResponse rfr2 
										WHERE rfr2.RfiFormId = rf.Id AND rfr2.RfiFormQuestionId = rfq.Id) AS FLOAT) 
									* 100)
								AS INT) 
							AS [percent]
					 	FROM RfiFormResponse rfr1
					 	WHERE rfr1.RfiFormId = rf.Id AND rfr1.RfiFormQuestionId = rfq.Id
						GROUP BY rfr1.Answer
					 	FOR JSON PATH)


					WHEN JSON_VALUE(rfq.Question, '$.type') IN ('select', 'selectSearch') THEN
						
						(SELECT 
							JSON_VALUE(Choices.value, '$.label') AS [option],
							JSON_VALUE(Choices.value, '$.value') AS value,
							(
								SELECT 
									CAST((CAST(COUNT(*) AS FLOAT) / CAST(
										(SELECT COUNT(*) FROM RfiFormResponse rfr3 where rfr3.RfiFormId = rf.Id AND rfr3.RfiFormQuestionId = rfq.Id) AS FLOAT
									) * 100) AS INT)
								FROM RfiFormResponse rfr2
								WHERE rfr2.RfiFormQuestionId = rfq1.Id
								AND (rfr2.Answer = JSON_VALUE(Choices.value, '$.value') OR rfr2.Answer = JSON_VALUE(Choices.value, '$.label'))
							) AS [percent]
					 	FROM RfiFormQuestion rfq1
						OUTER APPLY OPENJSON(JSON_QUERY(rfq1.Question, '$.choices')) Choices
						WHERE rfq1.RfiFormId = rf.Id AND rfq1.Id = rfq.id
					 	FOR JSON PATH)

						
					ELSE (SELECT TOP 5
							rfr1.Id as id,
							rfr1.Answer AS response
					 	FROM RfiFormResponse rfr1
					 	WHERE rfr1.RfiFormId = rf.Id AND rfr1.RfiFormQuestionId = rfq.Id
						ORDER BY rfr1.ResponseDate DESC
					 	FOR JSON PATH)
				END AS Responses
            FROM RfiForm rf
            INNER JOIN RfiFormQuestion rfq on rfq.RfiFormId = rf.Id
            INNER JOIN RfiFormResponse rfr on rfr.RfiFormQuestionId = rfq.Id
            WHERE rf.id = @formId
			AND rf.TenantId=@tenantId
            GROUP BY rfq.Id, rfq.Mapped, rfq.MappedField, rfq.SortOrder, rfq.Question, rf.Id
            ORDER BY rfq.SortOrder";

        private readonly IConnectionString _connectionString;
		private readonly TenantBasicInfo _tenant;
		public GetRfiFormSummaryHandler(IConnectionString connectionString,
			TenantBasicInfo tenant)
		{
			_connectionString = connectionString;
			_tenant = tenant;
		}

		/// <summary>
		/// Retrieves information for RFI page with responses according to rfi form Id
		/// </summary>
		/// <param name="request">Command that contains rfi form id</param>
		/// <param name="cancellationToken">Cancellation token</param>
		/// <returns></returns>
		public async Task<RfiFormSummaryReadModel> Handle(GetRfiFormSummaryQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                formId = request.Id,
				tenantId = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var result = await connection.QueryFirstOrDefaultAndLog<RfiFormSummaryReadModel>(GetRfiFormTotalCountQuery, parameters);

                if (result != null)
                {
                    result.Questions = await connection.QueryAndLog<RfiFormSummaryQuestionReadModel>(GetRfiFormQuestionsQuery, parameters);
                }
                return result;
            }
        }
    }
}
