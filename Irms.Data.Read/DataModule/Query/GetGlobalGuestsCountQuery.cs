﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to fetch total count of global guests
    /// </summary>
    public class GetGlobalGuestsCountQuery : IRequest<int> { }
}
