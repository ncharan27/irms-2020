﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    public class GetGuestEventAdmissionQuery : IPageQuery<GuestEventAdmissionReadModel>
    {
        public GetGuestEventAdmissionQuery(
            Guid id, 
            int pageNo, 
            int pageSize,
            string searchText)
        {
            GuestId = id;
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
        }

        public Guid GuestId { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public string SearchText { get; set; }
    }
}
