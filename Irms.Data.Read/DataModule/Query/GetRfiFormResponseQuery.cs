﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Query
{
    /// <summary>
    /// Query to fetch rfi form response page. Contains rfi form id and default page attributes
    /// </summary>
    public class GetRfiFormResponseQuery : IPageQuery<RfiFormResponseReadModel>
    {
        public GetRfiFormResponseQuery(
            Guid formId,
            int pageNo,
            int pageSize,
            string searchText)
        {
            FormId = formId;
            PageNo = pageNo;
            PageSize= pageSize;
            SearchText = searchText;
        }
        public Guid FormId { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string SearchText { get; set; }
    }
}
