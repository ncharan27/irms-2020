﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class RfiFormNameReadModel
    {
        public RfiFormNameReadModel(string name, int responsesCount)
        {
            Name = name;
            ResponsesCount = responsesCount;
        }

        public string Name { get; set; }
        public int ResponsesCount { get; set; }
    }
}
