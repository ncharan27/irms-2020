﻿namespace Irms.Data.Read
{
    public enum TimeFilterRange
    {
        LastDay = 0,
        LastWeek = 1,
        LastMonth = 2,
        AllTime = 3
    }
}
