﻿using Irms.Data.Read.CustomField.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CustomField.Queries
{
    public class GetCustomFieldQuery : IRequest<CustomFieldReadModel>
    {
        public Guid Id { get; set; }
    }
}
