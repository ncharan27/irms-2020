﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Read.RfiForm.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.RfiForm.QueryHandlers
{
    public class GetRfiFormHandler : IRequestHandler<GetRfiFormQry, ReadModels.RfiForm>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public GetRfiFormHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
        }

        /// <summary>
        /// get campaign rfi form
        /// </summary>
        /// <param name="request">mostly invitation id</param>
        /// <param name="cancellationToken">cancellation token</param>
        /// <returns>sms template</returns>
        public async Task<ReadModels.RfiForm> Handle(GetRfiFormQry request, CancellationToken token)
        {
            var entity = await _context.RfiForm
                .Include(x => x.RfiFormQuestions)
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == request.CampaignInvitationId, token);

            if (entity == null)
            {
                return null;
            }

            var rfi = _mapper.Map<ReadModels.RfiForm>(entity);
            rfi.RfiFormQuestions = _mapper.Map<List<ReadModels.RfiFormQuestion>>(entity.RfiFormQuestions);

            if (rfi.ThemeBackgroundImagePath.IsNotNullOrEmpty())
            {
                rfi.ThemeBackgroundImagePath = $"{_config["AzureStorage:BaseUrl"]}{rfi.ThemeBackgroundImagePath}";
            }

            return rfi;
        }
    }
}
