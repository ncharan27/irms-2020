﻿using System;
using System.Collections.Generic;

namespace Irms.Data.Read.ListAnalysis.ReadModels
{
    public class ValidationsCount
    {
        public int Errors { get; set; }
        public int FieldWarnings { get; set; }
        public int GuestWarnings { get; set; }
        public List<Guid> MissingGuests { get; set; }
    }
}