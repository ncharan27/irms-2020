﻿using Dapper;
using Irms.Application.Product.Commands;
using Irms.Data.Abstract;
using Irms.Data.Read.Service.Queries;
using Irms.Data.Read.Service.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Service.QueryHandlers
{
    public class GetProductServicesHandler : IRequestHandler<GetProductServicesCmd, IEnumerable<ServicesList>>
    {
        private const string Query = @"SELECT DISTINCT 
                 [sc].[Id]
		        ,[sc].[IsActive]
		        ,[sc].[Title]
          FROM [dbo].[ProductServiceCatalogFeature] AS psc
          LEFT JOIN [dbo].[ServiceCatalog] AS sc ON sc.Id = psc.ServiceCatalogId
          WHERE [psc].[ProductId] = @ProductId
          SELECT [psc].[ServiceCatalogId] AS ServiceId
		        ,[scf].[Id] AS FeatureId
		        ,[scf].[Title] AS FeatureTitle
		        ,[scf].[IsActive] AS IsFeatureEnabled
		        ,[scf].[IsCustom]
          FROM [dbo].[ProductServiceCatalogFeature] AS psc
          LEFT JOIN [dbo].[ServiceCatalogFeature] AS scf ON scf.Id = psc.ServiceCatalogFeatureId
          WHERE [psc].[ProductId] = @ProductId";


        private readonly IConnectionString _connectionString;

        public GetProductServicesHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// This method returns the list of <see cref="ServicesList"/> releted to concreet <see cref="Data.EntityClasses.Product"/>
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ServicesList>> Handle(GetProductServicesCmd request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                ProductId = request.Id
            };

            IEnumerable<ServicesList> serviceList;

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                using (var multi = await connection.QueryMultipleAsync(Query, parameters))
                {
                    serviceList = (await multi.ReadAsync<ServicesList>()).ToList();
                    if (serviceList != null)
                    {
                        var features = (await multi.ReadAsync<ServiceFeature>()).ToList();
                        foreach (var s in serviceList)
                        {
                            s.ServiceFeatures = features.Where(x => x.ServiceId == s.Id).ToList();
                        }
                    }
                }
            }
            return serviceList;
                       
        }
    }
}
