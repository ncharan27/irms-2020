﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Event.Queries;
using Irms.Data.Read.Event.ReadModels;
using Irms.Data.Read.Service.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Event.QueryHandlers
{
    public class GetEventFeaturesHandler : IRequestHandler<GetEventFeatures, IEnumerable<EventFeatureListItem>>
    {
        private const string Query = @"
                    SELECT scf.Id,
                           scf.Title,
                           scf.Description,
                           isnull(IsPurchased, 0) IsPurchased,
                           (CASE
                                WHEN ef.Id IS NOT NULL THEN cast(1 AS bit)
                                ELSE cast(0 AS bit)
                            END) IsChecked
                    FROM ServiceCatalogFeature scf
                    LEFT JOIN EventFeature ef ON scf.Id = ef.ServiceCatalogFeatureId
                    AND ef.EventId = @eventId
                    AND ef.TenantId = @tenant
                    LEFT JOIN
                      (SELECT cf.Id,
                              Title,
                              Description,
                              cast(1 AS bit) IsPurchased
                       FROM TenantSubscription ts
                       INNER JOIN ProductServiceCatalogFeature pc ON ts.ProductId=pc.ProductId
                       INNER JOIN ServiceCatalogFeature cf ON pc.ServiceCatalogFeatureId=cf.Id
                       WHERE TenantId = @tenant) pr ON scf.Id=pr.Id";


        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetEventFeaturesHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }
        /// <summary>
        /// This method returns list of features for an event.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<EventFeatureListItem>> Handle(GetEventFeatures request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                tenant = _tenant.Id,
                eventId = request.Id
            };


            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var features = await connection.QueryAndLog<EventFeatureListItem>(Query, parameters);
                return features;
            }
        }
    }
}
