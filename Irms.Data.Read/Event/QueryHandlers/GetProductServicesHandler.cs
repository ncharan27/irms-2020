﻿using Dapper;
using Irms.Application;
using Irms.Application.Product.Commands;
using Irms.Data.Abstract;
using Irms.Data.Read.Event.Queries;
using Irms.Data.Read.Event.ReadModels;
using Irms.Data.Read.Service.Queries;
using Irms.Data.Read.Service.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Event.QueryHandlers
{
    public class GetProductFeaturesHandler : IRequestHandler<GetProductFeatures, IEnumerable<ProductFeatureListItem>>
    {
        private const string Query = @"
                SELECT scf.Id,
                       scf.Title,
                       scf.Description,
                       isnull(IsPurchased, 0) IsPurchased
                FROM ServiceCatalogFeature scf
                LEFT JOIN
                  (SELECT cf.Id,
                          Title,
                          Description,
                          cast(1 AS bit) IsPurchased
                   FROM TenantSubscription ts
                   INNER JOIN ProductServiceCatalogFeature pc ON ts.ProductId=pc.ProductId
                   INNER JOIN ServiceCatalogFeature cf ON pc.ServiceCatalogFeatureId=cf.Id
                   WHERE TenantId = @tenant) pr ON scf.Id=pr.Id";


        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetProductFeaturesHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }
        /// <summary>
        /// This method returns the list of <see cref="ServicesList"/> releted to concreet <see cref="Data.EntityClasses.Product"/>
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ProductFeatureListItem>> Handle(GetProductFeatures request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                tenant = _tenant.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var features = await connection.QueryAndLog<ProductFeatureListItem>(Query, parameters);
                return features;
            }
        }
    }
}
