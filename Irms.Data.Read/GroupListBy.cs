﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read
{
    public enum GroupListBy
    {
        Id = 0,
        Name = 1,
        AscendingDate = 2,
        DecreasingDate = 3
    }
}
