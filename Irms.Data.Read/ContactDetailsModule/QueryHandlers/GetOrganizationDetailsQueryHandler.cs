﻿using Dapper;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(OrganizationDetailsQuery)]
    public class GetOrganizationDetailsQueryHandler : IRequestHandler<GetOrganizationDetailsQuery, OrganizationReadModel>
    {
        private const string OrganizationDetailsQuery = @"
            SELECT
            c.Id,
            c.Organization,
            c.Position
            FROM Contact c
            WHERE c.Id = @id";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetOrganizationDetailsQueryHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        public async Task<OrganizationReadModel> Handle(GetOrganizationDetailsQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                id = request.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var details = await connection.QueryFirstOrDefaultAsync<OrganizationReadModel>(OrganizationDetailsQuery, parameters);
                return details;
            }
        }
    }
}
