﻿using Dapper;
using Irms.Data.Abstract;
using Irms.Data.Read.DataModule.Queries;
using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.DataModule.QueryHandlers
{
    [HasSqlQuery(PersonalInfoQuery)]
    public class GetPersonalInfoHandler : IRequestHandler<GetPersonalInfoQuery, PersonalInfoReadModel>
    {
        private const string PersonalInfoQuery = @"
            SELECT
            c.Id,
            c.Title,
            c.FullName,
            c.PreferredName,
            CASE 
	            WHEN c.GenderId = 0 THEN 'male'
	            WHEN c.GenderId = 1 THEN 'female'
            END AS Gender,
            c.DocumentNumber,
            c.ExpirationDate,
            c.Email,
            c.AlternativeEmail,
            c.MobileNumber,
            c.SecondaryMobileNumber,
            c.WorkNumber,
            c.NationalityId,
            c.DocumentTypeId,
            c.IssuingCountryId,
            c.Position,
            c.Organization
            FROM Contact c
            WHERE c.Id = @id";

        private readonly IConnectionString _connectionString;

        public GetPersonalInfoHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<PersonalInfoReadModel> Handle(GetPersonalInfoQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                id = request.Id
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var details = await connection.QueryFirstOrDefaultAsync<PersonalInfoReadModel>(PersonalInfoQuery, parameters);
                return details;
            }
        }
    }
}
