﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.ReadModels
{
    public class EventAdmissionReadModel
    {
        public string Status { get; set; }
        public string Seat { get; set; }
        public string Parking { get; set; }
        public string BadgeUrl { get; set; }
    }
}
