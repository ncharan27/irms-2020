﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.DataModule.Queries
{
    public class GetPersonalDetailsQuery : IRequest<PersonalDetailsReadModel>
    {
        public GetPersonalDetailsQuery(
            string guestListId,
            Guid contactId
            )
        {
            if (Guid.TryParse(guestListId, out var lId))
            {
                GuestListId = lId;
            }

            ContactId = contactId;
        }
        public Guid? GuestListId { get; set; }
        public Guid ContactId { get; set; }
    }
}
