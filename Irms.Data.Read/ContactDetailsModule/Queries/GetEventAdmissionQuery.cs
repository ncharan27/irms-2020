﻿using Irms.Data.Read.DataModule.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.DataModule.Queries
{
    public class GetEventAdmissionQuery : IRequest<EventAdmissionReadModel>
    {
        public GetEventAdmissionQuery(Guid listId, Guid contactId)
        {
            ListId = listId;
            ContactId = contactId;
        }

        public Guid ListId { get; set; }
        public Guid ContactId { get; set; }
    }
}
