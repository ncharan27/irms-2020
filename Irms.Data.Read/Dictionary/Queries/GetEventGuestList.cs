﻿using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.Dictionary.Queries
{
    public class GetEventGuestList : IRequest<IEnumerable<DictionaryItem>>
    {
        public GetEventGuestList(Guid eventId)
        {
            EventId = eventId;
        }

        public Guid EventId { get; }
    }
}
