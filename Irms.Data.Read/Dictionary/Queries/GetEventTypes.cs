﻿using Irms.Data.Read.Dictionary.ReadModels;
using MediatR;
using System.Collections.Generic;

namespace Irms.Data.Read.Dictionary.Queries
{
    public class GetEventTypes : IRequest<IEnumerable<EventTypeItem>>
    {
    }
}
