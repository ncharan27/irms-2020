﻿using System.Collections.Generic;
using MediatR;
using Irms.Data.Read.Dictionary.ReadModels;

namespace Irms.Data.Read.Dictionary.Queries
{
    public class GetAllLanguages : IRequest<IEnumerable<Language>>
    {
    }
}
