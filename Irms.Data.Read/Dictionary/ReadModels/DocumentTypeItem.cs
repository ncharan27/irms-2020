﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class DocumentTypeItem
    {
        public DocumentTypeItem(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; }
        public string Name { get; }
    }
}
