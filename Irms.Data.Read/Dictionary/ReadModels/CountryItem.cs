﻿using System;

namespace Irms.Data.Read.Dictionary.ReadModels
{
    public class CountryItem
    {
        public CountryItem(Guid id, string value, string shortIso)
        {
            Id = id;
            Value = value;
            ShortIso = shortIso;
        }

        public Guid Id { get; }
        public string Value { get; }
        public string ShortIso { get; }
    }
}
