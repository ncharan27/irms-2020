﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class EmailsUniqueListReadModel
    {
        public IEnumerable<EmailUniqueReadModel> Data { get; set; }

        public class EmailUniqueReadModel
        {
            public string Email { get; set; }
            public bool IsExists { get; set; }
        }
    }
}
