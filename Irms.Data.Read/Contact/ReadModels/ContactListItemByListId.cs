﻿using System;

namespace Irms.Data.Read.Contact.ReadModels
{
    public class ContactListItemByListId
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
