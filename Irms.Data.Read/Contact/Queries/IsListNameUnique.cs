﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.Queries
{
    public class IsListNameUnique : IRequest<bool>
    {
        public IsListNameUnique(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
