﻿using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Contact.Queries
{
    public class GetPhoneNumberUniqueCheck: IRequest<PhoneNumbersUniqueListReadModel>
    {
        public IEnumerable<string> PhoneNumbers { get; set; }
    }
}
