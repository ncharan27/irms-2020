﻿using Irms.Application;
using Irms.Data.Read.Contact.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class IsListNameUniqueHandler : IRequestHandler<IsListNameUnique, bool>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public IsListNameUniqueHandler(IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<bool> Handle(IsListNameUnique request, CancellationToken cancellationToken)
        {
            var list = await _context.ContactList.FirstOrDefaultAsync(x => x.Name == request.Name);
            return list != null;
        }
    }
}
