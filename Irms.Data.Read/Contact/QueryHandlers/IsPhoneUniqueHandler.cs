﻿using Irms.Data.Read.Contact.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class IsPhoneUniqueHandler : IRequestHandler<IsPhoneUnique, bool>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public IsPhoneUniqueHandler(IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        /// <summary>
        /// Is phone number unique
        /// </summary>
        /// <param name="request">phone number</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> Handle(IsPhoneUnique request, CancellationToken token)
        {
            var isExists = await _context.Contact
                .Where(t => t.TenantId == _tenant.Id && t.MobileNumber == request.MobileNumber)
                .AnyAsync(token);

            return isExists;
        }
    }
}
