﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using Irms.Data.Read.Registration;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetContactListFilteredHandler : IPageHandler<GetContactListFiltered, ContactListItemFiltered>
    {
        private const string CountPlaceholder = "_countClause";
        private const string CountQuery = @"
                SELECT
                  COUNT(cl.Id)
                FROM ContactList cl
                Where cl.TenantId = @tenant
                  AND cl.IsGuest = @guest
                {0}";

        private const string PageQuery = @"
                SELECT
                  cl.Id,
                  cl.Name,
				   _countClause as Count,
				   (select Name from [Event] e where e.Id = cl.EventId) as [Event],
				   cl.ModifiedById as LastUpdatedBy,
				   cl.ModifiedOn as UpdatedDate,
                   (SELECT DISTINCT COALESCE(active, 0) 
                     FROM eventCampaign ec 
                     WHERE ec.GuestListId = cl.Id 
                     GROUP BY active having active > 0) AS IsReadonly,
				(CASE WHEN EXISTS (SELECT Id FROM EventCampaign ec WHERE ec.GuestListId = cl.Id AND CampaignType=1 AND Active=1)
				    THEN 1 
				    ELSE 0
			    END) IsLive
                FROM ContactList cl 
                Where cl.TenantId = @tenant                  
                  AND cl.IsGuest = @guest
                {0}
                ORDER BY cl.CreatedOn DESC
                OFFSET @skip ROWS
                FETCH NEXT @take ROWS ONLY";


        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;


        private const string IsGlobalClause = "AND cl.IsGlobal = @global";
        private const string SearchClause = "AND cl.Name like @searchText";
        private const string EventClause = "AND cl.EventId = @eventId";
        private const string ContactCountClause = "(select count(1) from ContactListToContacts clc INNER JOIN Contact c ON clc.ContactId = c.Id where clc.ContactListId = cl.Id AND c.Deleted = 0 )";
        private const string GuestCountClause = "(select count(1) from ContactListToContacts where ContactListId = cl.Id)";

        public GetContactListFilteredHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        /// <summary>
        /// this method will return list of events with pagination
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IPageResult<ContactListItemFiltered>> Handle(GetContactListFiltered request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            if (request.EventId != null)
            {
                clauses.Add(EventClause);
            }
            else if (request.IsGlobal != null)
            {
                clauses.Add(IsGlobalClause);
            }

            string pageQuery = string.Empty;
            if (request.IsGuest)
            {
                pageQuery = PageQuery.Replace(CountPlaceholder, GuestCountClause);
            }
            else
            {
                pageQuery = PageQuery.Replace(CountPlaceholder, ContactCountClause);
            }
            var parameters = new
            {
                tenant = _tenant.Id,
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                global = request.IsGlobal,
                guest = request.IsGuest,
                eventId = request.EventId,
                skip = request.Skip(),
                take = request.Take()
            };

            var conditionStr = string.Join("\r\n", clauses);
            var countQuery = string.Format(CountQuery, conditionStr);
            pageQuery = string.Format(pageQuery, conditionStr, request.EventId);

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var total = await connection.ExecuteScalarAndLog<int>(countQuery, parameters);
                var contactList = await connection.QueryAndLog<ContactListItemFiltered>(pageQuery, parameters);
                return new PageResult<ContactListItemFiltered>(contactList, total);
            }
        }
    }
}
