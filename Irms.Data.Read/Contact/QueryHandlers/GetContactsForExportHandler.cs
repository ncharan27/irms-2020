﻿using Dapper;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using Irms.Infrastructure.Services.ExcelExport;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Irms.Infrastructure.Services.ExcelExport.ContactsExportModel;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    [HasSqlQuery(nameof(ExportAllQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(ExportCustomFieldsQuery), nameof(SearchClause))]
    public class GetContactsForExportHandler : IRequestHandler<GetContactsForExport, ContactsExportModel>
    {
        private const string ExportAllQuery = @"
        SELECT DISTINCT
			c.Id,
			Title,
			FullName,
			PreferredName,
			CASE
				WHEN GenderId = 0 THEN 'Male'
				WHEN GenderId = 1 THEN 'Female'
				ELSE ''
			END AS Gender,
			c.Email,
			AlternativeEmail,
			MobileNumber,
			(SELECT TOP 1 e.Name FROM Event e WHERE e.Id = '{2}') 
			AS EventName,
			SecondaryMobileNumber,
			WorkNumber,
			country.Nationality AS Nationality,
			doctype.Name As DocumentType,
			DocumentNumber,
			issuer.Name AS IssuingCountry,
			ExpirationDate,
			Organization,
			Position,
			CASE
				WHEN IsGuest IS NULL OR IsGuest = 0 THEN 'Contact'
				WHEN IsGuest IS NOT NULL THEN 'Guest'
			END AS IsGuest,
			creator.UserName as CreatedBy,
			c.CreatedOn,
			modifier.UserName AS ModifiedBy,
			c.ModifiedOn
		FROM contact c
		LEFT JOIN contactlisttocontacts cltc ON cltc.ContactId = c.Id
		LEFT JOIN Country country ON country.ID = c.NationalityId
		LEFT JOIN DocumentType doctype ON doctype.Id = c.DocumentTypeId
		LEFT JOIN Country issuer ON issuer.ID = c.IssuingCountryId
		LEFT JOIN aspnetusers creator ON creator.id = c.CreatedById
		LEFT JOIN aspnetusers modifier ON modifier.Id = c.ModifiedById
		WHERE cltc.ContactListId = '{0}'";

        private const string ExportCustomFieldsQuery = @"
			SELECT
			c.Id as ContactId,
			ccf.CustomFieldId as CustomFieldId,
			cf.FieldName,
			ccf.Value
			FROM ContactList cl
			INNER JOIN ContactListtoContacts cltc on cltc.ContactListId=cl.Id
			INNER JOIN Contact c on c.Id=cltc.ContactId
			LEFT JOIN ContactCustomField ccf on ccf.ContactId=c.id
			INNER JOIN CustomField cf on cf.Id= ccf.CustomFieldId
			WHERE cl.Id=@contactListId
			AND cl.TenantId=@tenantId";

        private const string CustomFieldsQuery = @"
			SELECT 
			cf.Id,
			cf.FieldName AS Name
			FROM CustomField cf
			WHERE cf.TenantId=@tenantId
			{0}";

        private const string QueryWithIds = ExportAllQuery + SearchClause;


        private const string SearchClause = " AND c.Id IN ({1})";

        private const string CustomFieldsSearchClause = " AND cf.Id IN ({0})";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;

        public GetContactsForExportHandler(IConnectionString connectionString, TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        public async Task<ContactsExportModel> Handle(GetContactsForExport request, CancellationToken cancellationToken)
        {
            var query = ExportAllQuery
                .Replace("{0}", request.ContactListId.ToString())
                .Replace("{2}", request.EventId.ToString());

            var customFieldsQuery = CustomFieldsQuery;
            var contactCustomFieldsQuery = ExportCustomFieldsQuery;

            if (request.ContactIds != null && request.ContactIds.Count() > 0)
            {
                var parameters = request.ContactIds
                    .Select(x => $"'{x}'")
                    .ToArray();

                query = query + SearchClause.Replace("{1}", string.Join(",", parameters));
                contactCustomFieldsQuery = contactCustomFieldsQuery + SearchClause.Replace("{1}", string.Join(",", parameters));
            }

            if (request.CustomFieldIds == null)
            {
                request.CustomFieldIds = new List<Guid>();
            }

            if (request.CustomFieldIds.Count() > 0)
            {
                var customParams = request.CustomFieldIds
                    .Select(x => $"'{x}'")
                    .ToArray();

                customFieldsQuery = customFieldsQuery.Replace("{0}", CustomFieldsSearchClause.Replace("{0}", string.Join(",", customParams)));
            }
            else
            {
                customFieldsQuery = customFieldsQuery.Replace("{0}", string.Empty);
            }

            var queryParameters = new
            {
                tenantId = _tenant.Id,
                contactListId = request.ContactListId
            };

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var data = new ContactsExportModel();
                data.CustomFieldsCount = request.CustomFieldIds.LongCount();
                data.Contacts = await connection.QueryAndLog<GuestExportModel>(query);

                if (request.CustomFieldIds.Count() > 0)
                {
                    data.CustomFields = await connection.QueryAndLog<ContactFieldModel>(customFieldsQuery, queryParameters);
                    data.ContactCustomFields = await connection.QueryAndLog<ContactCustomFieldModel>(contactCustomFieldsQuery, queryParameters);
                }
                else if (request.CustomFieldIds.Count() == 0 && request.Columns.Count() == 0)
                {
                    data.CustomFields = await connection.QueryAndLog<ContactFieldModel>(customFieldsQuery, queryParameters);
                    data.ContactCustomFields = await connection.QueryAndLog<ContactCustomFieldModel>(contactCustomFieldsQuery, queryParameters);
                }
                else
                {
                    data.CustomFields = new List<ContactFieldModel>();
                    data.ContactCustomFields = new List<ContactCustomFieldModel>();
                }

                return data;
            }
        }
    }
}