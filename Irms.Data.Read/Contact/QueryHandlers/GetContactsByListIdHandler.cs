﻿using Irms.Application;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetContactsByListIdHandler : IRequestHandler<GetContactsByListId, IReadOnlyCollection<ContactItemAndListId>>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetContactsByListIdHandler(IrmsDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        /// <summary>
        /// Get Contacts By Lists Id
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IReadOnlyCollection<ContactItemAndListId>> Handle(GetContactsByListId request, CancellationToken token)
        {
            var contacts = await _context.ContactListToContacts
                .Include(x => x.Contact)
                .Where(t => request.ListId.Contains(t.ContactListId) && t.TenantId==_tenant.Id)
                .Select(x => new ContactItemAndListId(){ Item = new ContactItem() 
                    {
                        Id = x.Contact.Id,
                        FullName = x.Contact.FullName, 
                        TenantId = x.Contact.TenantId, 
                        Title = x.Contact.Title,
                        Email = x.Contact.Email,
                        Phone = x.Contact.MobileNumber
                    }, ContactListId = x.ContactListId })
                .ToListAsync(token);

            if (request.LoadUnassigned)
            {
                var unassignedContacts = await _context.Contact
                    .Include(x => x.ContactListToContacts)
                    .Where(x => x.ContactListToContacts.Count() == 0 && x.TenantId == _tenant.Id)
                    .Select(x => new ContactItemAndListId()
                    {
                        Item = new ContactItem()
                        {
                            Id = x.Id,
                            FullName = x.FullName,
                            TenantId = x.TenantId,
                            Title = x.Title,
                            Email = x.Email,
                            Phone = x.MobileNumber
                        }
                    })
                    .ToListAsync(token);

                contacts.AddRange(unassignedContacts);
            }

            return contacts;
        }
    }
}
