﻿using AutoMapper;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class GetContactListHandler : IRequestHandler<GetContactList, IReadOnlyCollection<ContactListItem>>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;
        private readonly IMapper _mapper;

        public GetContactListHandler(IrmsDataContext context,
            TenantBasicInfo tenant,
            IMapper mapper)
        {
            _context = context;
            _tenant = tenant;
            _mapper = mapper;
        }

        /// <summary>
        /// get event basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IReadOnlyCollection<ContactListItem>> Handle(GetContactList request, CancellationToken token)
        {
            var contactsList = _context.ContactList
                .Where(t => t.TenantId == _tenant.Id && t.IsGuest == request.IsGuest);

            if (request.IsGuest)
            {
                contactsList = contactsList.Where(t => t.EventId != null);
            }

            if (request.IsGlobal != null)
            {
                contactsList = contactsList.Where(t => t.IsGlobal == request.IsGlobal);
            }

            var result = await contactsList.ToListAsync(token);
            return _mapper.Map<IReadOnlyCollection<ContactListItem>>(result);
        }
    }
}
