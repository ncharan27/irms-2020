﻿using Irms.Data.Read.Contact.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;

namespace Irms.Data.Read.Contact.QueryHandlers
{
    public class IsEmailUniqueHandler : IRequestHandler<IsEmailUnique, bool>
    {
        private readonly IrmsDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public IsEmailUniqueHandler(IrmsDataContext context,
            TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        /// <summary>
        /// Is phone number unique
        /// </summary>
        /// <param name="request">email</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> Handle(IsEmailUnique request, CancellationToken token)
        {
            var isExists = await _context.Contact
                .Where(t => t.TenantId == _tenant.Id && t.Email == request.Email)
                .AnyAsync(token);

            return isExists;
        }
    }
}
