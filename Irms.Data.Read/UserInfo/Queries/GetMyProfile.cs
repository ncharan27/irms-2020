﻿using MediatR;
using Irms.Data.Read.UserInfo.ReadModels;
using System;

namespace Irms.Data.Read.UserInfo.Queries
{
    public class GetMyProfile : IRequest<MyProfile>
    {
        public GetMyProfile(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
