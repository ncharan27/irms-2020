﻿using System;
using Irms.Domain.Entities;

namespace Irms.Data.Read.UserInfo.ReadModels
{
    public class UserInfoReadModel
    {
        public UserInfoReadModel(
            Guid id,
            RoleType role,
            string fullName,
            Gender gender,
            DateTime? birthDate,
            string phoneNo,
            string email,
            string messagingHandle,
            string nationalId,
            bool isActive)
        {
            Id = id;
            Role = role;
            FullName = fullName;
            Gender = gender;
            BirthDate = birthDate;
            PhoneNo = phoneNo;
            Email = email;
            MessagingHandle = messagingHandle;
            NationalId = nationalId;
            IsActive = isActive;
        }

        public Guid Id { get; }
        public RoleType Role { get; }
        public string FullName { get; }
        public Gender Gender { get; }
        public DateTime? BirthDate { get; }
        public string PhoneNo { get; }
        public string Email { get; }
        public string MessagingHandle { get; }
        public string NationalId { get; }
        public bool IsActive { get; }
    }
}
