﻿using System;
using Irms.Domain.Entities;

namespace Irms.Data.Read.UserInfo.ReadModels
{
    public class MyProfile
    {

        public MyProfile(
            string firstName,
            string phoneNo,
            string email,
            int role)
        {
            FirstName = firstName;
            PhoneNo = phoneNo;
            Email = email;
            Role = (RoleType)role;
        }

        public string FirstName { get; }
        public string PhoneNo { get; }
        public string Email { get; }
        public RoleType Role { get; }
    }
}