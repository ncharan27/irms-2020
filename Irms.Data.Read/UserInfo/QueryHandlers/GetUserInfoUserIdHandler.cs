﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Application.UsersInfo.Queries;
using Irms.Application;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetUserInfoUserIdHandler : IRequestHandler<GetUserInfoUserId, Guid?>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetUserInfoUserIdHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<Guid?> Handle(GetUserInfoUserId request, CancellationToken cancellationToken)
        {
            var employee = await _context.UserInfo
                .FirstOrDefaultAsync(
                    x => x.Id == request.UserInfoId
                         && !x.IsDeleted
                         && x.TenantId == _tenant.Id,
                    cancellationToken);

            return employee?.UserId;
        }
    }
}
