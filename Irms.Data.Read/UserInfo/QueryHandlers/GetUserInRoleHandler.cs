﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Irms.Data.Read.Dictionary.ReadModels;
using Irms.Data.Read.UserInfo.Queries;
using Irms.Application;

namespace Irms.Data.Read.UserInfo.QueryHandlers
{
    public class GetUserInRoleHandler : IRequestHandler<GetUserInRole, IEnumerable<DictionaryItem>>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetUserInRoleHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<IEnumerable<DictionaryItem>> Handle(GetUserInRole request, CancellationToken cancellationToken)
        {
            return (await _context.UserInfo
                .Where(x => x.RoleId == (int)request.RoleType
                            && !x.IsDeleted
                            && x.TenantId == _tenant.Id)
                .Select(x => new { x.Id, x.FullName })
                .ToListAsync(cancellationToken))
                .Select(x => new DictionaryItem(x.Id, x.FullName));
        }
    }
}
