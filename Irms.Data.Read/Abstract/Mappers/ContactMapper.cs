﻿using Irms.Data.Read.Contact.ReadModels;
using Irms.Domain.Entities;

namespace Irms.Data.Mappers
{
    public class ContactMapper : AutoMapper.Profile
    {
        public ContactMapper()
        {
            CreateMap<ContactListItem, EntityClasses.ContactList>();
            CreateMap<EntityClasses.ContactList, ContactListItem>();

            CreateMap<EntityClasses.Contact, ContactWithCustomFields>()
                .ForMember(x => x.Gender, y => y.MapFrom(z => (Gender?)z.GenderId));

            CreateMap<EntityClasses.ContactCustomField, ContactWithCustomFields.ContactCustomField>()
                .ForMember(x => x.Name, y => y.MapFrom(z => z.CustomField.FieldName));
            //update tenant
            //CreateMap<UpdateEventCmd, Domain.Entities.Event>();
            //CreateMap<UpdateLocationCmd, Domain.Entities.Event.EventLocation>();

            ////update config, create/update users
            //CreateMap<UpdateConfigCmd, Domain.Entities.Event.Event>();
            //CreateMap<UpdateConfigCmd, Domain.Entities.UserInfo>();
        }
    }
}
