﻿using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Abstract.Mappers
{
    public class CampaignInvitationRfiMapper : AutoMapper.Profile
    {
        public CampaignInvitationRfiMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.RfiForm, InvitationResponseRfiModel>()
                .ForMember(f => f.BackgroundImageUrl, m => m.MapFrom(o => o.ThemeBackgroundImagePath))
                .ForMember(f => f.Welcome, m => m.MapFrom(o => o.WelcomeHtml))
                .ForMember(f => f.Submit, m => m.MapFrom(o => o.SubmitHtml))
                .ForMember(f => f.Thanks, m => m.MapFrom(o => o.ThanksHtml))
                .ForMember(f => f.Welcome, m => m.MapFrom(o => o.WelcomeHtml))
                .ForMember(f => f.FormSettings, m => m.MapFrom(o => o.FormSettings))
                .ForMember(f => f.FormTheme, m => m.MapFrom(o => o.FormTheme))
                .ForMember(f => f.Questions, m => m.MapFrom(o => o.RfiFormQuestions));

            CreateMap<EntityClasses.CampaignInvitationResponseMediaType, InvitationResponseRfiModel>()
                .ForMember(f => f.Id, m => m.MapFrom(o => o.Id));

            CreateMap<EntityClasses.RfiFormQuestion, InvitationResponseRfiModel.InvitationResponseRfiQuestionModel>()
                .ForMember(f => f.Question, m => m.MapFrom(o => o.Question))
                .ForMember(f => f.QuestionId, m => m.MapFrom(o => o.Id));

            CreateMap<EntityClasses.RfiFormQuestion, Domain.Entities.RfiFormQuestion>();
            CreateMap<Domain.Entities.RfiFormQuestion, EntityClasses.RfiFormQuestion>();

            CreateMap<EntityClasses.RfiFormResponse, Domain.Entities.RfiFormResponse>()
                .ForMember(f => f.QuestionId, m => m.MapFrom(o => o.RfiFormQuestionId));
            CreateMap<Domain.Entities.RfiFormResponse, EntityClasses.RfiFormResponse>()
                .ForMember(f => f.RfiFormQuestionId, m => m.MapFrom(o => o.QuestionId));
        }
    }
}
