﻿namespace Irms.Data.Read.Abstract.Mappers
{
    public class RfiFormReadMapper : AutoMapper.Profile
    {
        public RfiFormReadMapper()
        {
            //for data read layer
            CreateMap<EntityClasses.RfiForm, RfiForm.ReadModels.RfiForm>();
            CreateMap<EntityClasses.RfiFormQuestion, RfiForm.ReadModels.RfiFormQuestion>();
        }
    }
}
