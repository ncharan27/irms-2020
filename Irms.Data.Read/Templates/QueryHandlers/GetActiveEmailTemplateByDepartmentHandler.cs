﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Templates.Queries;
using Irms.Domain.Entities.Templates;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Irms.Data.Read.Templates.QueryHandlers
{
    /// <summary>
    /// Handler for getting active Email template based ong DepartmentId
    /// </summary>
    public class GetActiveEmailTemplateByDepartmentHandler : IRequestHandler<GetActiveEmailTemplateByDepartment, Template>
    {
        private readonly IrmsDataContext _context;
        private readonly ILogger<GetActiveEmailTemplateHandler> _logger;

        public GetActiveEmailTemplateByDepartmentHandler(IrmsDataContext context, ILogger<GetActiveEmailTemplateHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Template> Handle(GetActiveEmailTemplateByDepartment request, CancellationToken cancellationToken)
        {
            var templateSel = await _context.TemplateSelection
                .FirstOrDefaultAsync(x => x.TypeId == (int)request.TemplateType && x.TenantId == request.TenantId, cancellationToken);

            if (templateSel == null)
            {
                _logger.LogError("The template selection for the template type {0} is absent.", request.TemplateType.ToString());
                return null;
            }

            if (!templateSel.IsEnabled)
            {
                _logger.LogInformation("The template for {0} is disabled. The Email will NOT be sent", request.TemplateType.ToString());
                return null;
            }

            if (!templateSel.EmailTemplateId.HasValue)
            {
                _logger.LogError("The notifications for {0} are enabled, but the email template is not specified", request.TemplateType.ToString());
                return null;
            }

            return await _context.Template
                .Where(x => x.Id == templateSel.EmailTemplateId.Value)
                .Include(x => x.TemplateTranslation)
                .Select(x => new Template(
                    x.Id,
                    x.Name,
                    x.EmailCompatible,
                    x.SmsCompatible,
                    x.EmailBody,
                    x.EmailSubject,
                    x.SmsText,
                    (TemplateType)x.TypeId,
                    x.TemplateTranslation
                        .Select(t => new TemplateTranslation(
                            t.LanguageId,
                            t.EmailSubject,
                            t.EmailBody,
                            t.SmsText))))
                .FirstOrDefaultAsync(cancellationToken);
        }
    }
}
