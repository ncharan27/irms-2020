﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Read.Templates.Queries;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Irms.Data.Read.Templates.QueryHandlers
{
    /// <summary>
    /// Handler for getting an extra information about SMS or Email template based on template Id
    /// </summary>
    public class GetTemplateSelectionDetailsHandler : IRequestHandler<GetTemplateSelectionDetails, TemplateSelectionDetails>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetTemplateSelectionDetailsHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<TemplateSelectionDetails> Handle(
            GetTemplateSelectionDetails request,
            CancellationToken cancellationToken)
        {
            var templateSelection = await _context.TemplateSelection
                .Include(x => x.Template)
                .Where(x => x.Id == request.Id && x.TenantId == _tenant.Id)
                .Select(c => new TemplateSelectionDetails
                {
                    Id = c.Id,
                    TemplateType = (TemplateType)c.TypeId,
                    Email = new TemplateSelectionLookup { Id = c.EmailTemplateId, Name = c.Template.Name }, 
                    Sms = new TemplateSelectionLookup { Id = c.SmsTemplateId, Name = c.Template.Name }, 
                    IsEnabled = c.IsEnabled
                }).FirstOrDefaultAsync(cancellationToken);

            if (templateSelection == null)
            {
                throw new IncorrectRequestException("Template selection id is incorrect");
            }

            return templateSelection;
        }
    }
}
