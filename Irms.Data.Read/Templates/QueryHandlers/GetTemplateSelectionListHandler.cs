﻿using Irms.Application;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Templates.Queries;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Templates.QueryHandlers
{
    /// <summary>
    /// Handler returns list of templates based on searched text
    /// </summary>
    public class GetTemplateSelectionListHandler : IPageHandler<GetTemplateSelectionList, TemplateSelectionListItem>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetTemplateSelectionListHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<IPageResult<TemplateSelectionListItem>> Handle(GetTemplateSelectionList request, CancellationToken cancellationToken)
        {
            var total = await _context.TemplateSelection
                .Where(x => x.TenantId == _tenant.Id)
                .CountAsync(cancellationToken);

            if (total == 0)
            {
                throw new IncorrectRequestException("Template Selections are empty");
            }

            var page = await _context.TemplateSelection
                .Where(x => x.TenantId == _tenant.Id)
                .Select(c => new TemplateSelectionListItem
                {
                    Id = c.Id,
                    TemplateType = (TemplateType)c.TypeId,
                    HasEmailTemplate = c.EmailTemplateId != null,
                    HasSmsTemplate = c.SmsTemplateId != null,
                    IsEnabled = c.IsEnabled
                })
                .OrderBy(x => x.TemplateType.ToString())
                .Skip(request.Skip())
                .Take(request.Take())
                .ToListAsync(cancellationToken);

            return new PageResult<TemplateSelectionListItem>(page, total);
        }
    }
}
