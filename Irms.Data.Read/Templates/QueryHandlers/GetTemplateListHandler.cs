﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Templates.Queries;
using Irms.Data.Read.Templates.ReadModels;
using Microsoft.EntityFrameworkCore;

namespace Irms.Data.Read.Templates.QueryHandlers
{
    /// <summary>
    /// Handler which return list of templates from DB based on filters from request  <see cref="GetTemplateList"/>
    /// </summary>
    public class GetTemplateListHandler : IPageHandler<GetTemplateList, TemplateListItem>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetTemplateListHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<IPageResult<TemplateListItem>> Handle(GetTemplateList request, CancellationToken cancellationToken)
        {
            var query = _context.Template
                .Where(x => x.TypeId == (int)request.TemplateType && x.TenantId == _tenant.Id);

            if (request.TemplateCompatibility == TemplateCompatibility.Email)
            {
                query = query.Where(x => x.EmailCompatible);
            }

            if (request.TemplateCompatibility == TemplateCompatibility.Sms)
            {
                query = query.Where(x => x.SmsCompatible);
            }

            var total = await query.CountAsync(cancellationToken);

            var page = await query
                .Select(c => new TemplateListItem
                {
                    Id = c.Id,
                    Name = c.Name,
                    SmsCompatible = c.SmsCompatible,
                    EmailCompatible = c.EmailCompatible
                })
                .Skip(request.Skip())
                .Take(request.Take())
                .ToListAsync(cancellationToken);

            return new PageResult<TemplateListItem>(page, total);
        }
    }
}
