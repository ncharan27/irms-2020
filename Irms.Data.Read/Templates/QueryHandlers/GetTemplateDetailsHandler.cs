﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Read.Templates.Queries;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities.Templates;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Irms.Data.Read.Templates.QueryHandlers
{
    /// <summary>
    /// Handler returns template deatials from DB by template Id
    /// </summary>
    public class GetTemplateDetailsHandler : IRequestHandler<GetTemplateDetails, TemplateDetails>
    {
        private readonly IrmsTenantDataContext _context;
        private readonly TenantBasicInfo _tenant;

        public GetTemplateDetailsHandler(IrmsTenantDataContext context, TenantBasicInfo tenant)
        {
            _context = context;
            _tenant = tenant;
        }

        public async Task<TemplateDetails> Handle(GetTemplateDetails request, CancellationToken cancellationToken)
        {
            var template = await _context.Template
                .Where(x => x.Id == request.Id && x.TenantId == _tenant.Id)
                .Include(x => x.TemplateTranslation)
                .Select(c => new TemplateDetails
                {
                    Id = c.Id,
                    Name = c.Name,
                    Type = (TemplateType)c.TypeId,
                    EmailSubject = c.EmailSubject,
                    EmailBody = c.EmailBody,
                    EmailCompatible = c.EmailCompatible,
                    SmsText = c.SmsText,
                    SmsCompatible = c.SmsCompatible,
                    //TODO FIX THIS
                    //ModifiedBy = c.ModifiedBy.FullName,
                    ModifiedOn = c.ModifiedOn,
                    Translations = c.TemplateTranslation
                        .Select(t => new TemplateTranslation(t.LanguageId, t.EmailSubject, t.EmailBody, t.SmsText))
                })
                .FirstOrDefaultAsync(cancellationToken);

            if (template == null)
            {
                throw new IncorrectRequestException("Template id is incorrect");
            }

            return template;
        }
    }
}
