﻿using Irms.Application;
using Irms.Domain.Entities.Templates;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Templates.ReadModels
{
    public class TemplateSelectionDetails
    {
        public Guid Id { get; set; }
        public TemplateType TemplateType { get; set; }
        public bool IsEnabled { get; set; }

        public TemplateSelectionLookup Email { get; set; }
        public TemplateSelectionLookup Sms { get; set; }

        public string Title => TemplateType.ToString().SplitCamelCase();
    }
}
