﻿using Irms.Domain.Entities;
using System;

namespace Irms.Data.Read.Campaign.ReadModels
{
    public class CampaignInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? GuestListId { get; set; }
        public byte? EntryCriteriaTypeId { get; set; }
        public byte? ExitCriteriaType { get; set; }
        public CampaignStatus CampaignStatus { get; set; }
    }
}
