﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Domain;
using MediatR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Campaign.QueryHandlers
{
    [HasSqlQuery(nameof(LoadCampaignSummary))]
    [HasSqlQuery(nameof(LoadCampaignInvitations))]
    public class GetCampaignSummaryHandler : IRequestHandler<GetCampaignSummaryQuery, CampaignSummary>
    {
        private const int EmailSendTime = 15;   // in seconds
        private const int SmsSendTime = 10;     // in seconds
        private const int WhatsAppSendTime = 4; // in seconds

        private const string LoadCampaignSummary = @"
        SELECT *,
	        (EmailCount * {emailSendTime} + SmsCount * {smsSendTime} + WhatsAppCount * {whatsAppSendTime}) / 60 AS SendTime
	        FROM (
	        SELECT 
		        COUNT(*) AS GuestCount,
		        (SELECT COUNT(Email) FROM CampaignPrefferedMedia icpm WHERE Email = 1 AND icpm.EventCampaignId = cpm.EventCampaignId) AS EmailCount,
		        (SELECT COUNT(Sms) FROM CampaignPrefferedMedia icpm WHERE Sms = 1 AND icpm.EventCampaignId = cpm.EventCampaignId) AS SmsCount,
		        (SELECT COUNT(WhatsApp) FROM CampaignPrefferedMedia icpm WHERE WhatsApp = 1 AND icpm.EventCampaignId = cpm.EventCampaignId) AS WhatsAppCount
	        FROM CampaignPrefferedMedia cpm where EventCampaignId = '{0}'
	        GROUP BY EventCampaignId
        ) AS subquery";

        private const string LoadCampaignInvitations = @"
        SELECT ci.Id,
	        Interval,
	        IntervalType,
	        StartDate,
	        Title,
	        (SELECT COUNT(Id) FROM CampaignEmailTemplate icet WHERE icet.CampaignInvitationId = ci.Id) AS IsEmail,
	        (SELECT COUNT(Id) FROM CampaignSmsTemplate icst WHERE icst.CampaignInvitationId = ci.Id) AS IsSms,
	        0 As IsWhatsApp,
	        (SELECT COUNT(Id) FROM RfiForm irfi WHERE irfi.CampaignInvitationId = ci.Id) AS RfiForm,
	        'Rfi' As RfiFormTitle,
	        ci.InvitationTypeId As InvitationType,
	    ci.IsInstant
        FROM CampaignInvitation ci
        LEFT JOIN CampaignEmailTemplate cet ON cet.CampaignInvitationId = ci.Id
        LEFT JOIN CampaignSmsTemplate cst ON cst.CampaignInvitationId = ci.Id
        LEFT JOIN RfiForm rfi ON rfi.CampaignInvitationId = ci.Id
        WHERE EventCampaignId = '{0}'
		GROUP BY ci.Id, Interval, IntervalType, StartDate, Title, ci.InvitationTypeId, ci.IsInstant";

        private readonly IConnectionString _connectionString;

        public GetCampaignSummaryHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<CampaignSummary> Handle(GetCampaignSummaryQuery request, CancellationToken cancellationToken)
        {
            var campaignSummary = new CampaignSummary();

            var campaignStatsQuery = LoadCampaignSummary.Replace("{0}", request.Id.ToString())
                .Replace("{emailSendTime}", EmailSendTime.ToString())
                .Replace("{smsSendTime}", SmsSendTime.ToString())
                .Replace("{whatsAppSendTime}", WhatsAppSendTime.ToString());

            var campaignInvitationsQuery = LoadCampaignInvitations.Replace("{0}", request.Id.ToString());

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var stats = await connection.QueryAndLog<CampaignSummary.CampaignSummaryStats>(campaignStatsQuery);
                if (stats.Any())
                {
                    campaignSummary.Stats = stats.First();
                }

                var invitations = await connection.QueryAndLog<CampaignSummary.CampaignSummaryInvitation>(campaignInvitationsQuery);
                campaignSummary.Invitation = invitations.FirstOrDefault(x => x.InvitationType == 0);
                campaignSummary.Pending = invitations.Where(x => x.InvitationType == 1);
                campaignSummary.Accepted = invitations.Where(x => x.InvitationType == 2);
                campaignSummary.Rejected = invitations.Where(x => x.InvitationType == 3);
            }

            return campaignSummary;
        }
    }
}
