﻿using AutoMapper;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.Campaign.QueryHandlers
{
    public class GetPrefferedMediaStatsHandler : IRequestHandler<GetPrefferedMediaStats, PrefferedMediaStats>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        public GetPrefferedMediaStatsHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
        }

        /// <summary>
        /// get event basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<PrefferedMediaStats> Handle(GetPrefferedMediaStats request, CancellationToken token)
        {
            var stats = new PrefferedMediaStats();
            stats.EmailCount = await _context.CampaignPrefferedMedia
                .LongCountAsync(x => x.EventCampaignId == request.Id
                && x.Email
                && x.Contact.ContactListToContacts.Any(y => y.ContactListId == request.ListId),
                token);

            stats.SmsCount = await _context.CampaignPrefferedMedia
                .LongCountAsync(x => x.EventCampaignId == request.Id && x.Sms
                && x.Contact.ContactListToContacts.Any(y => y.ContactListId == request.ListId),
                token);

            stats.WhatsAppCount = await _context.CampaignPrefferedMedia
                .LongCountAsync(x => x.EventCampaignId == request.Id && x.WhatsApp
                && x.Contact.ContactListToContacts.Any(y => y.ContactListId == request.ListId),
                token);

            return stats;
        }
    }
}
