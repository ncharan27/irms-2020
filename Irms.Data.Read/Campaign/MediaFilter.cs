﻿namespace Irms.Data.Read.Campaign
{
    public enum MediaFilter
    {
        Email = 0,
        Sms = 1,
        WhatsApp = 2
    }
}
