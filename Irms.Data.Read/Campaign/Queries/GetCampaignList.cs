﻿using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign.ReadModels;
using System;

namespace Irms.Data.Read.Campaign.Queries
{
    public class GetCampaignList : IPageQuery<CampaignListItem>
    {
        public GetCampaignList(
         int pageNo,
         int pageSize,
         string searchText,
         Guid eventId)
        {
            PageNo = pageNo;
            PageSize = pageSize;
            SearchText = searchText;
            EventId = eventId;
        }

        public int PageNo { get; }
        public int PageSize { get; }
        public string SearchText { get; }
        public Guid EventId { get; }
    }
}
