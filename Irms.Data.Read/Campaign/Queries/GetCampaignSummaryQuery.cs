﻿using Irms.Data.Read.Campaign.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.Campaign.Queries
{
    public class GetCampaignSummaryQuery : IRequest<CampaignSummary>
    {
        public GetCampaignSummaryQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
