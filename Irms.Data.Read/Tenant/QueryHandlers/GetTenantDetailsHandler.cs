﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Tenant.ReadModels;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Irms.Data.Read.Dictionary.ReadModels;
using Microsoft.Extensions.Configuration;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    public class GetTenantDetailsHandler : IRequestHandler<GetTenantDetails, TenantDetails>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        public GetTenantDetailsHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
        }

        /// <summary>
        /// get tenant basic info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TenantDetails> Handle(GetTenantDetails request, CancellationToken token)
        {
            var t = await _context.Tenant
                .Include(_ => _.Country)
                .FirstOrDefaultAsync(x => x.Id == request.Id, token);

            var tenant = _mapper.Map<TenantDetails>(t);
            tenant.Country = new CountryItem(t.Country.Id, t.Country.Name, t.Country.ShortIso);
            tenant.LogoPath = t.LogoPath == null ? t.LogoPath : $"{_config["AzureStorage:BaseUrl"]}{t.LogoPath}";
            return tenant;
        }
    }
}
