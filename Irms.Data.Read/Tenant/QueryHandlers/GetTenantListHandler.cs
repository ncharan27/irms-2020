﻿using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Registration;
using Irms.Data.Read.Tenant.Queries;
using Irms.Data.Read.Tenant.ReadModels;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Irms.Data.Read.Tenant.QueryHandlers
{
    [HasSqlQuery(nameof(CountQuery), nameof(SearchClause))]
    [HasSqlQuery(nameof(PageQuery), nameof(SearchClause))]
    public class GetTenantListHandler : IPageTimeHandler<TenantListQuery, TenantListItem>
    {
        private const string CountQuery = @"
            SELECT count(*)
            FROM [Tenant] t
            INNER JOIN [TenantContactInfo] c ON c.TenantId = t.Id
            WHERE t.IsDeleted = 0
                AND [t].Id <> @tenant 
                AND [t].CreatedOn >= @date 
                {0}";

        private const string PageQuery = @"
            SELECT t.Id,
                   t.Name,
                   t.Address,
                   c.Email,
                   @path + t.LogoPath as LogoPath
            FROM [Tenant] t
            INNER JOIN [TenantContactInfo] c ON c.TenantId = t.Id
            WHERE t.IsDeleted = 0
              AND [t].Id <> @tenant 
              AND [t].CreatedOn >= @date 
              {0}
            ORDER BY t.CreatedOn DESC
              OFFSET @skip ROWS
              FETCH NEXT @take ROWS ONLY";

        private const string SearchClause = "AND t.Name like @searchText";

        private readonly IConnectionString _connectionString;
        private readonly IConfiguration _config;
        private readonly TenantBasicInfo _tenant;

        public GetTenantListHandler(IConnectionString connectionString,
            IConfiguration config,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _config = config;
            _tenant = tenant;
        }

        public async Task<IPageResult<TenantListItem>> Handle(TenantListQuery request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            var parameters = new
            {
                tenant = _tenant.Id,
                date = request.MinDate(),
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                path = _config["AzureStorage:BaseUrl"],
                skip = request.Skip(),
                take = request.Take(),
            };


            var conditionStr = string.Join("\r\n", clauses);
            var pageQuery = string.Format(PageQuery, conditionStr);
            var countQuery = string.Format(CountQuery, conditionStr);

            using (IDbConnection db = new SqlConnection(_connectionString.Value))
            {
                var total = await db.ExecuteScalarAndLog<int>(countQuery, parameters);
                var page = await db.QueryAndLog<TenantListItem>(pageQuery, parameters);

                return new PageResult<TenantListItem>(page, total);
            }
        }
    }
}
