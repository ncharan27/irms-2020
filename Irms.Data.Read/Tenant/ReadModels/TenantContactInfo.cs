﻿using System;

namespace Irms.Data.Read.Tenant.ReadModels
{
    public class TenantContactInfo
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
    }
}
