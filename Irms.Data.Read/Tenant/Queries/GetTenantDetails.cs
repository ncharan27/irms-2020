﻿using System;
using MediatR;
using Irms.Data.Read.Tenant.ReadModels;

namespace Irms.Data.Read.Tenant.Queries
{
    public class GetTenantDetails : IRequest<TenantDetails>
    {
        public GetTenantDetails(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
