﻿using System;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignWhatsappBOTTemplate
    {
        public Guid Id { get; set; }
        public string DefaultResponse { get; set; }
        public string AcceptCode { get; set; }
        public string RejectCode { get; set; }
        public string AcceptedResponse { get; set; }
        public string RejectedResponse { get; set; }
        public string ApprovedTemplate { get; set; }
    }
}
