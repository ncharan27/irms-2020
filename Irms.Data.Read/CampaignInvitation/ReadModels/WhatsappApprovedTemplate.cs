﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class WhatsappApprovedTemplate
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public bool? IsBot { get; set; }
        public string AcceptCode { get; set; }
        public string RejectCode { get; set; }
    }
}
