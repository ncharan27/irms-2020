﻿using Irms.Domain.Entities;
using System;

namespace Irms.Data.Read.CampaignInvitation.ReadModels
{
    public class CampaignInvitationListItem
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsInstant { get; set; }
        public int SortOrder { get; set; }
        public int Interval { get; set; }
        public IntervalType IntervalType { get; set; }
    }
}