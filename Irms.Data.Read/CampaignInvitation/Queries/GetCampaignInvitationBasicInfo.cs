﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignInvitationBasicInfo : IRequest<CampaignInvitationBasicInfo>
    {
        public GetCampaignInvitationBasicInfo(Guid id)
        {
            Id = id;
        }
        public Guid Id { get; set; }
    }
}
