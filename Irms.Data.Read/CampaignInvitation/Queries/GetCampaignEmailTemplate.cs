﻿using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using System;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignEmailTemplate : IRequest<CampaignEmailTemplate>
    {

        public GetCampaignEmailTemplate(Guid campaignInvitationId)
        {
            CampaignInvitationId = campaignInvitationId;
        }

        public Guid CampaignInvitationId { get; }
    }
}
