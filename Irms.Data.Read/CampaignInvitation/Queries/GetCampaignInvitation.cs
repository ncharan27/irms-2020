﻿using Irms.Domain.Entities;
using MediatR;
using System;

namespace Irms.Data.Read.CampaignInvitation.Queries
{
    public class GetCampaignInvitation : IRequest<ReadModels.CampaignInvitation>
    {

        public GetCampaignInvitation(Guid id, InvitationType invitationType)
        {
            Id = id;
            InvitationType = invitationType;
        }

        public Guid Id { get; }
        public InvitationType InvitationType { get; }
    }
}