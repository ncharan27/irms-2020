﻿using Irms.Application;
using Irms.Data.Abstract;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetWhatsappApprovedTemplateHandler : IRequestHandler<GetWhatsappApprovedTemplateQuery, IEnumerable<WhatsappApprovedTemplate>>
    {
        public const string TemplateQuery = @"SELECT
            Id,
            Name,
            TemplateBody AS Content,
            IsBot
            FROM WhatsappApprovedTemplate
            WHERE TenantId=@tenantId";

        public const string InvitationTemplateQuery = @"WITH Invitation AS (
	            SELECT TOP 1 
	            InvitationTypeId,
				AcceptCode,
				RejectCode
	            FROM CampaignInvitation
				INNER JOIN EventCampaign ON CampaignInvitation.EventCampaignId = EventCampaign.Id
	            WHERE CampaignInvitation.Id=@type
                AND CampaignInvitation.TenantId=@tenantId
            )
            SELECT
                Id,
                Name,
                TemplateBody AS Content,
                IsBot,
                (SELECT TOP 1 AcceptCode FROM Invitation) AcceptCode,
                (SELECT TOP 1 RejectCode FROM Invitation) RejectCode
                FROM WhatsappApprovedTemplate
	            WHERE InvitationTypeId = (SELECT TOP 1 InvitationTypeId FROM Invitation)
                AND TenantId=@tenantId
                ORDER BY HasRfi DESC";

        public const string TemplateClause = @" AND InvitationTypeId=@type";

        private readonly IConnectionString _connectionString;
        private readonly TenantBasicInfo _tenant;
        public GetWhatsappApprovedTemplateHandler(IConnectionString connectionString,
            TenantBasicInfo tenant)
        {
            _connectionString = connectionString;
            _tenant = tenant;
        }

        public async Task<IEnumerable<WhatsappApprovedTemplate>> Handle(GetWhatsappApprovedTemplateQuery request, CancellationToken cancellationToken)
        {
            var parameters = new
            {
                tenantId = _tenant.Id,
                type = request.InvitationId
            };

            string query = request.InvitationId.HasValue
                ? InvitationTemplateQuery
                : TemplateQuery;

            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var data = await connection.QueryAndLog<WhatsappApprovedTemplate>(query, parameters);
                foreach (var template in data)
                {
                    if (template.IsBot == true)
                    {
                        template.Content = template.Content.Replace("{{acceptcode}}", template.AcceptCode).Replace("{{rejectcode}}", template.RejectCode);
                    }
                }
                return data;
            }
        }
    }
}
