﻿using Irms.Application;
using Irms.Application.Campaigns.Queries;
using Irms.Application.Campaigns.ReadModels;
using Irms.Data.Abstract;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Registration;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    [HasSqlQuery(nameof(PageQuery))]
    public class GetCampaignPlaceholderListHandler : IRequestHandler<GetCampaignPlaceholderList, IEnumerable<CampaignPlaceholderListItem>>
    {
        private const string PageQuery = @"
                SELECT Id,
	                   Title,
	                   Placeholder 
                FROM CampaignPlaceholder cp
                WHERE (cp.InvitationTypeId IS NULL OR cp.InvitationTypeId = 
		                (SELECT TOP 1 InvitationTypeId 
		                 FROM CampaignInvitation ci
		                 WHERE ci.Id = '{invitationId}'))
	                AND (cp.TemplateTypeId = @templateId OR cp.TemplateTypeId IS NULL) ";

        private const string SearchClause = @"
            AND cp.Title like @searchText";

        private const string ResponseClause = @"
            AND cp.Response = 1";

        private readonly IConnectionString _connectionString;

        public GetCampaignPlaceholderListHandler(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// this method will return list of placeholders
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CampaignPlaceholderListItem>> Handle(GetCampaignPlaceholderList request, CancellationToken cancellationToken)
        {
            var clauses = new List<string>();
            if (request.SearchText.IsNotNullOrEmpty())
            {
                clauses.Add(SearchClause);
            }

            if (request.Response)
            {
                clauses.Add(ResponseClause);
            }

            var parameters = new
            {
                searchText = FullTextHelpers.MakeLike(request.SearchText),
                templateId = request.TemplateTypeId
            };

            string pageQuery = PageQuery;

            if (clauses.Count > 0)
            {
                var conditionStr = string.Join("\r\n", clauses);
                pageQuery = pageQuery + conditionStr;
            }

            var guid = request.InvitationId.ToString();
            pageQuery = pageQuery.Replace("{invitationId}", guid);


            using (IDbConnection connection = new SqlConnection(_connectionString.Value))
            {
                var placeholders = await connection.QueryAndLog<CampaignPlaceholderListItem>(pageQuery, parameters);
                return placeholders;
            }
        }
    }
}
