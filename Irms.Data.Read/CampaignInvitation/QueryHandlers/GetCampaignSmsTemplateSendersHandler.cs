﻿using AutoMapper;
using Irms.Application;
using Irms.Data.Read.CampaignInvitation.Queries;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignSmsTemplateSendersHandler : IRequestHandler<GetCampaignSmsTemplateSenders, IReadOnlyCollection<string>>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly TenantBasicInfo _tenant;
        private readonly IMapper _mapper;

        public GetCampaignSmsTemplateSendersHandler(IrmsDataContext context,
            IConfiguration config,
            IMapper mapper,
            TenantBasicInfo tenant)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
            _tenant = tenant;
        }

        /// <summary>
        /// loads all available sms template names
        /// currently, it would return a collection with only one item - tenant name
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IReadOnlyCollection<string>> Handle(GetCampaignSmsTemplateSenders request, CancellationToken token)
        {
            var result = new List<string>() { _tenant.Name };

            return await Task.FromResult(result);
        }
    }
}
