﻿using AutoMapper;
using Irms.Application.Abstract;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using Irms.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignInvitationRsvpHandler : IRequestHandler<GetCampaignInvitationRsvp, CampaignInvitationRsvp>
    {
        private readonly IrmsDataContext _context;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly IFileUploadService _fileUploadService;
        public GetCampaignInvitationRsvpHandler(IrmsDataContext context,
            IConfiguration config,
            IFileUploadService fileUploadService,
            IMapper mapper)
        {
            _context = context;
            _config = config;
            _mapper = mapper;
            _fileUploadService = fileUploadService;
        }

        /// <summary>
        /// get campaign invitation info
        /// </summary>
        /// <param name="request">contains id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<CampaignInvitationRsvp> Handle(GetCampaignInvitationRsvp request, CancellationToken token)
        {
            var inv = await _context.CampaignInvitation
                .Include(x => x.CampaignEmailTemplate)
                .Include(x => x.CampaignSmsTemplate)
                .FirstOrDefaultAsync(x => x.EventCampaignId == request.CampaignId
                && x.InvitationTypeId == (int)InvitationType.Rsvp, token);

            var invitation = _mapper.Map<CampaignInvitationRsvp>(inv);

            if (inv != null)
            {
                var et = inv.CampaignEmailTemplate.FirstOrDefault();
                var st = inv.CampaignSmsTemplate.FirstOrDefault();

                invitation.EmailSubject = et?.Subject;
                invitation.EmailSender = et?.SenderEmail;
                invitation.SmsSender = st?.SenderName;

                if (et != null)
                {
                    if (!string.IsNullOrEmpty(et.BackgroundImagePath))
                    {
                        var emailBinary = await _fileUploadService.LoadFile(et.BackgroundImagePath);
                        invitation.EmailImage = Encoding.UTF8.GetString(emailBinary);
                    }
                    else
                    {
                        invitation.EmailImage = CutImageFromBody(et.Body);
                    }
                }

                if (st != null && !string.IsNullOrEmpty(st.BackgroundImagePath))
                {
                    var smsBinary = await _fileUploadService.LoadFile(st.BackgroundImagePath);
                    invitation.SmsImage = Encoding.UTF8.GetString(smsBinary);
                }
            }
            return invitation;
        }

        /// <summary>
        /// Cuts first image from email body, uses as a thumbnail, if needed
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        private string CutImageFromBody(string body)
        {
            string pattern = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(body);
            if (matches.Count > 0)
            {
                var image = matches[0].Groups[1].Value;
                return image;
            }

            return default;
        }
    }
}
