﻿using AutoMapper;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Data.Read.CampaignInvitation.QueryHandlers
{
    public class GetCampaignWhatsappBOTTemplateHandler : IRequestHandler<GetCampaignWhatsappBOTTemplateQuery, CampaignWhatsappBOTTemplate>
    {
        private readonly IrmsDataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public GetCampaignWhatsappBOTTemplateHandler(IrmsDataContext context,
            IMapper mapper,
            IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        public async Task<CampaignWhatsappBOTTemplate> Handle(GetCampaignWhatsappBOTTemplateQuery request, CancellationToken token)
        {
            var dbTemplate = await _context.CampaignWhatsappTemplate
                //.Include(x => x.EventCampaign)
                .Include(x => x.CampaignInvitation.EventCampaign)
                .FirstOrDefaultAsync(x => x.CampaignInvitationId == request.Id, token);

            if (dbTemplate == null)
            {
                var temp = await _context.EventCampaign
                    .Where(x => x.CampaignInvitations.Any(y => y.Id == request.Id))
                    .Select(x => new CampaignWhatsappBOTTemplate
                    {
                        DefaultResponse = x.CampaignInvitations.FirstOrDefault(x => x.Id == request.Id).InvitationTypeId.ToString(),
                        AcceptCode = x.AcceptCode,
                        RejectCode = x.RejectCode
                    }).FirstOrDefaultAsync(token);

                var approvedTemplate = await _context.WhatsappApprovedTemplate
                    .FirstOrDefaultAsync(x => x.InvitationTypeId == Convert.ToInt32(temp.DefaultResponse) && x.IsBot == true, token);

                temp.DefaultResponse = null;
                temp.ApprovedTemplate = approvedTemplate.TemplateBody.Replace("{{acceptcode}}", temp.AcceptCode).Replace("{{rejectcode}}", temp.RejectCode);
                return temp;
            }

            var template = new CampaignWhatsappBOTTemplate
            {
                ApprovedTemplate = dbTemplate.Body,
                DefaultResponse = dbTemplate.WelcomeHtml,
                AcceptedResponse = dbTemplate.AcceptHtml,
                RejectedResponse = dbTemplate.RejectHtml,
                AcceptCode = dbTemplate.CampaignInvitation.EventCampaign.AcceptCode,
                RejectCode = dbTemplate.CampaignInvitation.EventCampaign.RejectCode
            };

            return template;
        }
    }
}
