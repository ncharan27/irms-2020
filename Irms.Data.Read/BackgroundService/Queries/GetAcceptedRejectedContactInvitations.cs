﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Irms.Data.Read.BackgroundService.Queries
{
    public class GetAcceptedRejectedContactInvitations : IRequest<IEnumerable<ReadModels.AcceptedRejectedContactInvitationListItem>>
    {
        public GetAcceptedRejectedContactInvitations(Guid invitationId)
        {
            InvitationId = invitationId;
        }
        public Guid InvitationId { get; set; }
    }
}
