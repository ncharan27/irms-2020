﻿using AutoMapper;
using Irms.Application;
using Irms.Application.BackgroundService.Queries;
using Irms.Application.CampaignInvitations;
using Irms.Background.Abstract;
using Irms.Data;
using Irms.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.Background.Jobs
{
    public class PendingJob
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IBackgroundJobsHelperService _backgroundJobsHelper;
        public PendingJob(IMediator mediator,
            IMapper mapper,
            IBackgroundJobsHelperService backgroundJobsHelper)
        {
            _mediator = mediator;
            _mapper = mapper;
            _backgroundJobsHelper = backgroundJobsHelper;
        }

        public async Task DoActions(Guid invitationId, CancellationToken token)
        {
            var type = InvitationType.Pending;
            var guests = await _mediator.Send(new GetPendingGuestsList(invitationId), token);

            //if campaign has guests
            if (guests.Any())
            {
                var templates = await _backgroundJobsHelper.GetCampaignTemplates(invitationId, token);

                //change invitation status and selected guests status to inprogress
                await _backgroundJobsHelper.ChangeInvitationStatus(invitationId, InvitationStatus.InProgress, token);
                await _backgroundJobsHelper.ChangeRsvpPendingGuestsStatus(_backgroundJobsHelper.RsvpPendingGuestsToUpdate(guests, templates.TenantId, invitationId), token);

                var ege = _mapper.Map<List<EventGuest>>(guests.Where(x => x.EmailPreferred && templates.EmailBody.IsNotNullOrEmpty()));
                bool emailsSent = false;

                //atleast one recipent
                if (ege.Any())
                {
                    emailsSent = await _backgroundJobsHelper.SendEmail(ege, templates, invitationId, type, token);
                }
                else
                {
                    emailsSent = true;
                }

                //sms
                var egs = _mapper.Map<List<EventGuest>>(guests.Where(x => x.SmsPreferred && templates.SmsBody.IsNotNullOrEmpty()));

                bool msgsSent = true;
                //atleast one recipent
                if (egs.Any())
                {
                    msgsSent = await _backgroundJobsHelper.SendSms(egs, templates, invitationId, type, token);
                }
                else
                {
                    msgsSent = true;
                }

                //whatsapp
                var egw = _mapper.Map<List<EventGuest>>(guests.Where(x => x.WhatsappPreferred && templates.WhatsappBody.IsNotNullOrEmpty()));

                bool whatsAppMsgsSent = true;
                //atleast one recipent
                if (egw.Any())
                {
                    whatsAppMsgsSent = await _backgroundJobsHelper.SendWhatsappMessage(egw, templates, invitationId, type, token);
                }
                else
                {
                    whatsAppMsgsSent = true;
                }

                if (emailsSent && msgsSent && whatsAppMsgsSent)
                {
                    await _backgroundJobsHelper.ChangeInvitationStatus(invitationId, InvitationStatus.Sent, token);
                }
                else
                {
                    await _backgroundJobsHelper.ChangeInvitationStatus(invitationId, InvitationStatus.Draft, token);
                }
            }
        }
    }
}
