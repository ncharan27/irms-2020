﻿//using AutoMapper;
//using Irms.Application.Webhooks.Events;
//using Irms.Data;
//using Irms.Domain.Entities;
//using Irms.Domain.Entities.Tenant;
//using MediatR;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using Newtonsoft.Json;
//using System;
//using System.Globalization;
//using System.Linq;
//using System.Net.Http;
//using System.Threading;
//using System.Threading.Tasks;

//namespace Irms.Background
//{
//    public class UnifonicWebhookService //: IUnifonicWebhookService
//    {
//        //private readonly IrmsDataBackgroundJobContext _context;
//        private readonly IServiceProvider _serviceProvider;
//        private readonly IMediator _mediator;
//        private readonly IHttpClientFactory _clientFactory;

//        private readonly Uri BaseAddress = new Uri("https://api.unifonic.com/rest/");

//        public UnifonicWebhookService(//IrmsDataBackgroundJobContext context,
//            IMediator mediator,
//            //IMapper mapper,
//            IServiceProvider serviceProvider,
//            IHttpClientFactory clientFactory)
//        {
//            //_context = context;
//            _serviceProvider = serviceProvider;
//            _mediator = mediator;
//            _clientFactory = clientFactory;
//        }

//        public async Task StartUnifonicWebhook(CancellationToken token)
//        {
//            using (IServiceScope scope = _serviceProvider.CreateScope())
//            using (IrmsDataBackgroundJobContext _context = scope.ServiceProvider.GetRequiredService<IrmsDataBackgroundJobContext>())
//            {
//                var logs = await _context.CampaignInvitationMessageLog
//                .AsNoTracking()
//                .Where(x => x.ProviderTypeId == (int)ProviderType.Unifonic
//                && (!x.ProviderResponses.Any() ||
//                x.ProviderResponses.Any(y =>
//                y.MessageStatus != (int)InvitationMessageStatus.Delivered
//                && y.MessageStatus != (int)InvitationMessageStatus.Undeliverable
//                && y.MessageStatus != (int)InvitationMessageStatus.Failed
//                && y.MessageStatus != (int)InvitationMessageStatus.Rejected
//                && y.MessageStatus != (int)InvitationMessageStatus.NoResponse)))
//                .ToListAsync(token);

//                var keys = await _context.Tenant
//                    .AsNoTracking()
//                    .Where(x => logs.Select(y => y.TenantId).Contains(x.Id))
//                    .Select(x => new Tenant
//                    {
//                        Id = x.Id,
//                        UnifonicSid = x.UnifonicSid
//                    })
//                    .ToListAsync(token);

//                foreach (var log in logs)
//                {
//                    var tenant = keys.FirstOrDefault(x => x.Id == log.TenantId);
//                    if (tenant != null)
//                    {
//                        //var apiKey = "cNss7qUYGXs7o4SyblIq_T5Tw_3NnK";

//                        var httpClient = _clientFactory.CreateClient();
//                        httpClient.BaseAddress = BaseAddress;

//                        using (var content = new StringContent($"AppSid={tenant.UnifonicSid}&MessageID={log.MessageId}", System.Text.Encoding.Default, "application/x-www-form-urlencoded"))
//                        using (var resp = await httpClient.PostAsync("Messages/GetMessageIDStatus", content))
//                        {
//                            string responseData = await resp.Content.ReadAsStringAsync();
//                            dynamic result = JsonConvert.DeserializeObject(responseData);
//                            bool success = result.success;
//                            if (success)
//                            {
//                                var res = JsonConvert.DeserializeObject<UnifonicWebhookResponse>(responseData);
//                                string status = string.Empty;
//                                //if (string.IsNullOrEmpty(res.Data.DLR) && res.Data.Status == null)
//                                //    continue; //discuss with Tasawar

//                                if (res.Data.DLR == "Delivered")
//                                {
//                                    status = "Delivered";
//                                }
//                                else if (res.Data.DLR == "Undeliverable")
//                                {
//                                    status = "Undeliverable";
//                                }
//                                else
//                                {
//                                    status = Convert.ToString(res.Data.Status);
//                                }

//                                var st = (InvitationMessageStatus)Enum.Parse(typeof(InvitationMessageStatus), CultureInfo.CurrentCulture.TextInfo.ToTitleCase(status.ToLower()), true);

//                                var eLog = await _context.ProviderResponse
//                                    .FirstOrDefaultAsync(x => x.TenantId == log.TenantId
//                                    && x.MessageStatus == (int)st
//                                    && x.CampaignInvitationMessageLogId == log.Id
//                                    && x.ProviderType == (int)ProviderType.Unifonic, token);

//                                if (eLog == null)
//                                {
//                                    Guid id = Guid.NewGuid();
//                                    await _context.ProviderResponse.AddAsync(new Data.EntityClasses.ProviderResponse
//                                    {
//                                        Id = id,
//                                        TenantId = log.TenantId,
//                                        MessageStatus = (int)st,
//                                        ContactId = log.ContactId,
//                                        ResponseDate = DateTime.UtcNow,
//                                        ProviderType = (int)ProviderType.Unifonic,
//                                        ResponseJson = responseData,
//                                        CampaignInvitationMessageLogId = log.Id
//                                    });

//                                    //await _mediator.Publish(new UnifonicWebhookSmsStatusLogged(id, log.TenantId), token);
//                                }
//                                else
//                                {
//                                    if (log.TimeStamp.AddHours(1) <= DateTime.UtcNow)
//                                    {
//                                        eLog.MessageStatus = (int)InvitationMessageStatus.NoResponse;
//                                    }

//                                    eLog.ResponseDate = DateTime.UtcNow;
//                                }

//                                await _context.SaveChangesAsync(token);
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        private class UnifonicWebhookResponse
//        {
//            public bool Success { get; set; }
//            public string Message { get; set; }
//            public string ErrorCode { get; set; }
//            public UnifonicWebhookDataResponse Data { get; set; }
//            public class UnifonicWebhookDataResponse
//            {
//                public string Status { get; set; }
//                public string DLR { get; set; }
//            }
//        }
//    }
//}
