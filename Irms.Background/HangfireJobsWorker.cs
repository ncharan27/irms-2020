﻿using Hangfire;
using Irms.Application.Abstract;
using Irms.Background.Jobs;
using System;
using System.Threading;

namespace Irms.Background
{
    public class HangfireJobsWorker : IHangfireJobsWorker
    {
        private readonly IBackgroundJobClient _client;
        public HangfireJobsWorker(IBackgroundJobClient client)
        {
            _client = client;
        }

        public void SetupRsvpSending(Guid campaignInvitationId, DateTime when)
        {
            string id = _client.Schedule<RsvpJob>(x => x.DoActions(campaignInvitationId, CancellationToken.None), when);
            //save id somewhere!
        }

        public void SetupPendingSending(Guid campaignInvitationId, DateTime when)
        {
            string id = _client.Schedule<PendingJob>(x => x.DoActions(campaignInvitationId, CancellationToken.None), when);
            //save id somewhere!
        }

        //list analysis
        public void SetupListAnalysisSending(Guid campaignInvitationId, DateTime when)
        {
            string id = _client.Schedule<ListAnalysisJob>(x => x.DoActions(campaignInvitationId, CancellationToken.None), when);
            //save id somewhere!
        }

        //public async Task StartUnifonicWebhookAsync(CancellationToken token)
        //{
        //    await _unifonicWebhook.StartUnifonicWebhook(token);
        //}

        public void SetupAcceptedSending(Guid campaignInvitationId, Guid contactId, DateTime when)
        {
            string id = _client.Schedule<AcceptedJob>(x => x.SendInvitation(campaignInvitationId, contactId, CancellationToken.None), when);
        }

        public void SetupRejectedSending(Guid campaignInvitationId, Guid contactId, DateTime when)
        {
            string id = _client.Schedule<RejectedJob>(x => x.SendInvitation(campaignInvitationId, contactId, CancellationToken.None), when);
        }
    }
}
