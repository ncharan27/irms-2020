﻿using System;

namespace Irms.Domain
{
    public class IncorrectRequestException : Exception
    {
        public IncorrectRequestException()
        {
        }

        /// <summary>
        /// incorrect exception constructor with one argument
        /// </summary>
        /// <param name="message"></param>
        public IncorrectRequestException(string message)
            : base(message)
        {
        }

        public IncorrectRequestException(int code, string message)
            : base(message)
        {
            Code = code;
        }

        public int? Code { get; }
    }
}
