﻿namespace Irms.Domain.Abstract
{
    public interface IEntity<out T>
    {
        T Id { get; }
    }
}
