﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain
{
    public static class Extensions
    {
        public static IEnumerable<T> Enumerate<T>(this T item)
        {
            yield return item;
        }

        public static bool EqualsIgnoreCase(this string src, string other)
        {
            return src.Equals(other, StringComparison.OrdinalIgnoreCase);
        }
    }
}
