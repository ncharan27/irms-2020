﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class ContactList : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public bool IsGlobal { get; set; }
        public bool IsGuest { get; set; }
        public Guid? EventId { get; set; }
        public Guid? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// intializing ids, dates
        /// </summary>
        public void Create(Guid tenantId)
        {
            Id = Guid.NewGuid();
            TenantId = tenantId;
            CreatedOn = DateTime.UtcNow;
        }

        public void SetName(string name)
        {
            Name = name;
        }

        public void Update()
        {
            ModifiedOn = DateTime.UtcNow;
        }

        public void SetGlobal()
        {
            IsGlobal = true;
        }

        public void SetGuest(bool isGuest)
        {
            IsGuest = isGuest;
        }
    }
}
