﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public enum SystemModule
    {
        Customers = 0,
        Events = 1,
        CampaignEmail = 2,
        CampaignSms = 3,
        CampaignRfi = 4,
        CampaignWhatsapp = 5
    }

    public static class FilePath
    {
        public static string GetFileDirectory(SystemModule module)
        {
            switch (module)
            {
                case SystemModule.Customers: return "customers/";
                case SystemModule.Events: return "events/";
                case SystemModule.CampaignEmail: return "campaignEmail/";
                case SystemModule.CampaignSms: return "campaignSms/";
                case SystemModule.CampaignRfi: return "campaignRfi/";
                case SystemModule.CampaignWhatsapp: return "campaignWhatsapp/";
                default: throw new ArgumentException("File type is invalid");
            }
        }
    }
}
