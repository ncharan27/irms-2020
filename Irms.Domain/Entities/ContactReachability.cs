﻿using System;
using Irms.Domain.Abstract;

namespace Irms.Domain.Entities
{
    public class ContactReachability : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid ContactId { get; set; }
        public bool WhatsApp { get; set; }
        public bool Sms { get; set; }
        public bool Email { get; set; }
        public DateTime? WhatsAppLastReachableOn { get; set; }
        public DateTime? SmsLastReachableOn { get; set; }
        public DateTime? EmailLastReachableOn { get; set; }

        public void CreateOrUpdate(Guid id, bool emailReachable, bool phoneReachable, bool whatsappReachable)
        {
            ContactId = id;
            Email = emailReachable;
            Sms = phoneReachable;
            WhatsApp = whatsappReachable;
        }
    }
}
