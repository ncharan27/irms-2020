﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class CampaignInvitationResponseMediaType : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationResponseId { get; set; }
        public MediaType MediaType { get; set; }
        public ResponseMediaType AcceptOrReject { get; set; }
        public DateTime CreatedOn { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.UtcNow;
        }
    }
}
