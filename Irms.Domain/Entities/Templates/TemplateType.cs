﻿namespace Irms.Domain.Entities.Templates
{
    //TODO FIX THIS ENUM!!!! 
    public enum TemplateType
    {
        ActivationMessage = 0,
        SuccessfulActivation = 1,

        EventInvitation = 2,
        EventEnrollment = 3,
        EventAssignment = 4,
        EventAssignmentChange = 5,
        ForgotPassword = 6,
        SuccessfulRegistration = 7,

        GuestIsActivated = 8,
        GuestIsDeactivated = 9,

        OTPCode = 10,
        WithdrawGuest = 11,
        NotificationsExceeded = 12,
        UpdateTaskStatus = 13
    }
}
