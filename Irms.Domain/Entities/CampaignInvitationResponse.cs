﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class CampaignInvitationResponse : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public Guid ContactId { get; set; }
        public MediaType? ResponseMediaType { get; set; }
        public UserAnswer? Answer { get; set; }
        public DateTime? ResponseDate { get; set; }
        public DateTime CreatedOn { get; set; }

        public void Create()
        {
            Id = Guid.NewGuid();
            Answer = UserAnswer.NOT_ANSWERED;
            CreatedOn = DateTime.UtcNow;
        }

        public void Expire()
        {
            Answer = Answer == UserAnswer.NOT_ANSWERED 
                ? UserAnswer.EXPIRED 
                : Answer;

            ResponseDate = DateTime.UtcNow;
        }
    }
}
