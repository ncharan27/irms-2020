﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class CampaignSmsTemplate
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid CampaignInvitationId { get; set; }
        public string SenderName { get; set; }
        public string Body { get; set; }
        public string WelcomeHtml { get; set; }
        public string ProceedButtonText { get; set; }
        public string RSVPHtml { get; set; }
        public string AcceptButtonText { get; set; }
        public string RejectButtonText { get; set; }
        public string AcceptHtml { get; set; }
        public string RejectHtml { get; set; }
        public string ThemeJson { get; set; }
        public string BackgroundImagePath { get; set; }

        /// <summary>
        /// intializing data
        /// </summary>
        public void Create()
        {
            Id = Guid.NewGuid();
        }
    }
}
