﻿using Irms.Domain.Abstract;
using System;

namespace Irms.Domain.Entities
{
    public class Language : IEntity<Guid>
    {
        public Language(Guid id, string culture, string name)
        {
            Id = id;
            Culture = culture;
            Name = name;
        }

        public Guid Id { get; }
        public string Culture { get; }
        public string Name { get; }
    }
}
