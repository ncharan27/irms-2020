﻿namespace Irms.Domain.Entities
{
    public enum FieldType
    {
        Reserved = 0,
        Custom = 1,
    }
}
