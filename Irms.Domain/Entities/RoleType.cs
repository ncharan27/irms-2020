﻿namespace Irms.Domain.Entities
{
    public enum RoleType
    {
        SuperAdmin = 0,
        TenantAdmin = 1
        //Guest = 1,
        //Interviewer = 2,
        //Physician = 3,
        //ReadOnlyAdmin = 4,
        //ContentAdmin = 5,
        //AreaAdmin = 6,
        //EventAdmin = 7,
        //TenantAdmin = 8,
        //Trainer = 10,
        //DepartmentAdmin = 11,
        //SettingsAdmin = 12,
        //DashboardAdmin = 13
    }
}
