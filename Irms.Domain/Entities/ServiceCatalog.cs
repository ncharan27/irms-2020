﻿using Irms.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Irms.Domain.Entities
{
    public class ServiceCatalog : IEntity<Guid>
    {
        public ServiceCatalog()
        {
            Id = Guid.NewGuid();
        }

        public ServiceCatalog(string title, bool? isActive, Guid createdByUserId, Guid? id = null)
        {
            Id = id ?? Guid.NewGuid();
            Title = title;
            IsActive = isActive;
            CreatedById = createdByUserId;
        }

        public Guid Id { get; private set; }
        public string Title { get; private set; }
        public bool? IsActive { get; private set; }
        public Guid? CreatedById { get; private set; }
        public DateTime? CreatedOn { get; private set; }
        public Guid? ModifiedById { get; private set; }
        public DateTime? ModifiedOn { get; private set; }
        public List<ServiceCatalogFeature> ServiceCatalogFeatures { get; set; }

        public void Deactivate()
        {
            IsActive = false;
        }
        public void Activate()
        {
            IsActive = true;
        }
    }
}
