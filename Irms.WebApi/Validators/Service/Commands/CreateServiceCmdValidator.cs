﻿using FluentValidation;
using Irms.Application.ProductServices.Commands;

namespace Irms.WebApi.Validators.Service.Commands
{
    public class CreateServiceCmdValidator : AbstractValidator<CreateServiceCmd>
    {
        public CreateServiceCmdValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(x => x.IsActive)
                .NotEmpty();
        }
    }
}
