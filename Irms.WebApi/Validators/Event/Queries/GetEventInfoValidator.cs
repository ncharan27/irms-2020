﻿using FluentValidation;
using Irms.Data.Read.Event.Queries;

namespace Irms.WebApi.Validators.Event.Queries
{
    public class GetEventInfoValidator : AbstractValidator<GetEventInfo>
    {
        public GetEventInfoValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
