﻿using FluentValidation;
using Irms.Data.Read.Event.Queries;

namespace Irms.WebApi.Validators.Event.Queries
{
    public class GetEventListValidator : AbstractValidator<GetEventList>
    {
        public GetEventListValidator()
        {
            RuleFor(x => x.PageNo)
                .GreaterThan(0);

            RuleFor(x => x.PageSize)
                .InclusiveBetween(1, 100);

            RuleForEach(x => x.Filters)
                .IsInEnum();
        }
    }
}
