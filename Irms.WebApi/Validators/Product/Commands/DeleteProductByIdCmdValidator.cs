﻿using FluentValidation;
using Irms.Application.Product.Commands;

namespace Irms.WebApi.Validators.Product.Commands
{
    public class DeleteProductByIdCmdValidator : AbstractValidator<DeleteProductByIdCmd>
    {
        public DeleteProductByIdCmdValidator()
        {
            RuleFor(x => x.ProductId)
                .NotEmpty();
        }
    }
}
