﻿using FluentValidation;
using Irms.Application.Campaigns.Commands;

namespace Irms.WebApi.Validators.Campaign.Commands
{
    public class UpdateCampaignCmdValidator : AbstractValidator<UpdateCampaignCmd>
    {
        public UpdateCampaignCmdValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(e => e.Id)
                .NotEmpty();

            RuleFor(e => e.GuestListId)
                .NotEmpty();

            RuleFor(e => e.ExitCriteriaType)
                .NotEmpty();

            RuleFor(e => e.EntryCriteriaTypeId)
                .NotEmpty();
        }
    }
}
