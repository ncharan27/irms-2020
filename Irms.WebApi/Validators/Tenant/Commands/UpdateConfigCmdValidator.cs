﻿using FluentValidation;
using Irms.Application.Tenants.Commands;

namespace Irms.WebApi.Validators.Tenant.Commands
{
    public class UpdateConfigCmdValidator : AbstractValidator<UpdateConfigCmd>
    {
        public UpdateConfigCmdValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x.ClientUrl)
                .NotEmpty()
                .MaximumLength(255);

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .MaximumLength(50);


            RuleFor(x => x.LastName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Email)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(e => e.MobileNo)
             .NotEmpty()
             .MaximumLength(50);

            RuleFor(e => e.EmailFrom)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}