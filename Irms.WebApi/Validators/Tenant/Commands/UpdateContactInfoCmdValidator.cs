﻿using FluentValidation;
using Irms.Application.Tenants.Commands;

namespace Irms.WebApi.Validators.Tenant.Commands
{
    public class UpdateContactInfoCmdValidator : AbstractValidator<UpdateContactInfoCmd>
    {
        public UpdateContactInfoCmdValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.LastName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Email)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(e => e.MobileNo)
             .NotEmpty()
             .MaximumLength(50);

        }
    }
}