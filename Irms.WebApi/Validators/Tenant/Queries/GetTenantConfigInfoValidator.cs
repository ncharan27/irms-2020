﻿using FluentValidation;
using Irms.Data.Read.Tenant.Queries;

namespace Irms.WebApi.Validators.Tenant.Queries
{
    public class GetTenantConfigInfoValidator : AbstractValidator<GetTenantConfigInfo>
    {
        public GetTenantConfigInfoValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty();
        }
    }
}
