﻿using FluentValidation;
using Irms.Data.Read.Tenant.Queries;

namespace Irms.WebApi.Validators.Tenant.Queries
{
    public class GetTenantByUrlValidator : AbstractValidator<GetTenantByUrl>
    {
        public GetTenantByUrlValidator()
        {
            RuleFor(x => x.TenantUrl)
                .NotEmpty();
        }
    }
}
