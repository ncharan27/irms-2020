﻿using FluentValidation;
using Irms.Application.Contact.Commands;

namespace Irms.WebApi.Validators.Contact.Commands
{
    public class CreateGlobalContactListCmdValidator : AbstractValidator<CreateGlobalContactListCmd>
    {
        public CreateGlobalContactListCmdValidator()
        {
            //basic info
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(100);
        }
    }
}
