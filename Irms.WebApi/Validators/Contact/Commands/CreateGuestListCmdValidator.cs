﻿using FluentValidation;
using Irms.Application.Contact.Commands;

namespace Irms.WebApi.Validators.Contact.Commands
{
    public class CreateGuestListCmdValidator : AbstractValidator<CreateGuestListCmd>
    {
        public CreateGuestListCmdValidator()
        {

            //basic info
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(x => x.EventId)
               .NotEmpty();
        }
    }
}
