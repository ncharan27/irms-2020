﻿using FluentValidation;
using Irms.Data.Read.Contact.Queries;

namespace Irms.WebApi.Validators.Contact.Queries
{
    public class GetContactListFilteredValidator : AbstractValidator<GetContactListFiltered>
    {
        public GetContactListFilteredValidator()
        {
            RuleFor(x => x.PageNo)
                .GreaterThan(0);

            RuleFor(x => x.PageSize)
                .InclusiveBetween(1, 100);
        }
    }
}
