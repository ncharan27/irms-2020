﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Irms.WebApi.LocalizationTransform.Template.Models
{
    public class TemplateDetailsUpdate
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public bool EmailCompatible { get; set; }
        public bool SmsCompatible { get; set; }

        public string EnglishEmailSubject { get; set; } // <--
        public string ArabicEmailSubject { get; set; } // <--

        public string EnglishEmailBody { get; set; } // <--
        public string ArabicEmailBody { get; set; } // <--

        public string EnglishSmsText { get; set; } // <--
        public string ArabicSmsText { get; set; } // <--

        public bool ArabicEnabled { get; set; } // <--
    }
}
