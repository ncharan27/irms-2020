﻿using FluentValidation;
using Irms.WebApi.LocalizationTransform.Template.Models;

namespace Irms.WebApi.LocalizationTransform.Template.Validators
{
    public class TemplateDetailsCreateValidator : AbstractValidator<TemplateDetailsCreate>
    {
        public TemplateDetailsCreateValidator()
        {
            //en
            RuleFor(x => x.EnglishEmailSubject)
                .NotEmpty()
                .When(x => x.EmailCompatible);

            RuleFor(x => x.EnglishEmailBody)
                .NotEmpty()
                .When(x => x.EmailCompatible);

            RuleFor(x => x.EnglishSmsText)
                .NotEmpty()
                .When(x => x.SmsCompatible);

            //ar
            RuleFor(x => x.ArabicEmailSubject)
                .NotEmpty()
                .When(x => x.ArabicEnabled && x.EmailCompatible);

            RuleFor(x => x.ArabicEmailBody)
                .NotEmpty()
                .When(x => x.ArabicEnabled && x.EmailCompatible);

            RuleFor(x => x.ArabicSmsText)
                .NotEmpty()
                .When(x => x.ArabicEnabled && x.SmsCompatible);

            //generic
            RuleFor(x => x.Name)
                .NotEmpty();

            RuleFor(x => x.Type)
                .IsInEnum();
        }
    }
}
