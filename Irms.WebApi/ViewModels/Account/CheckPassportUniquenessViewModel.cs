﻿namespace Irms.WebApi.ViewModels.Account
{
    public class CheckPassportUniquenessViewModel
    {
        public CheckPassportUniquenessViewModel(string passportNo)
        {
            PassportNo = passportNo;
        }

        public string PassportNo { get; }
    }
}
