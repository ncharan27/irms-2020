﻿namespace Irms.WebApi.ViewModels.Account
{
    public class RecoverPasswordBySmsViewModel
    {
        public string PhoneNumber { get; set; }
    }
}
