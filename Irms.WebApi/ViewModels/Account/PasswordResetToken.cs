﻿namespace Irms.WebApi.ViewModels.Account
{
    public class PasswordResetToken
    {
        public string Token { get; set; }
        public string UserId { get; set; }
    }
}
