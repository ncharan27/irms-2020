﻿namespace Irms.WebApi.ViewModels.Account
{
    public class RecoverPasswordBySmsWithCaptchaViewModel
    {
        public string PhoneNumber { get; set; }
        public string RecaptchaResponse { get; set; }
    }
}
