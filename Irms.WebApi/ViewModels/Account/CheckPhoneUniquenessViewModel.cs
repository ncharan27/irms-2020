﻿namespace Irms.WebApi.ViewModels.Account
{
    public class CheckPhoneUniquenessViewModel
    {
        public CheckPhoneUniquenessViewModel(string phoneNo)
        {
            PhoneNo = phoneNo;
        }

        public string PhoneNo { get; }
    }
}
