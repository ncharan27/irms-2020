﻿namespace Irms.WebApi.ViewModels.Account
{
    public class OTPViewModel
    {
        public OTPViewModel(string code)
        {
            Code = code;
        }

        public string Code { get; }
    }
}
