﻿using Irms.Data.Abstract;
using Microsoft.Extensions.Configuration;

namespace Irms.WebApi.Helpers
{
    public class DefaultConnectionString : IConnectionString
    {
        private readonly IConfiguration _configuration;

        public DefaultConnectionString(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Value => _configuration.GetConnectionString("DefaultConnection");
    }
}
