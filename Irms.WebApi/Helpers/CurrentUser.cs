﻿using System;
using Irms.Application.Abstract;
using Irms.Domain;
using Irms.Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace Irms.WebApi.Helpers
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IHttpContextAccessor _accessor;

        public CurrentUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public Guid Id => GetUserId();

        private Guid GetUserId()
        {
            var id = _accessor?.HttpContext?.User?.UserId();
            if (id == null)
            {
                throw new IncorrectRequestException("User/Actor is unauthorized");
            }

            return id.Value;
        }

        public bool IsInRole(RoleType role)
        {
            return _accessor?.HttpContext?.User?.IsInRole(role.ToString()) ?? false;
        }
    }
}
