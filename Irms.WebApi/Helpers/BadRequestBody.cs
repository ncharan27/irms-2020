﻿namespace Irms.WebApi.Helpers
{
    public class BadRequestBody
    {
        public BadRequestBody(int? code, string message, object details = null)
        {
            Code = code;
            Message = message;
            Details = details;
        }

        public int? Code { get; }
        public string Message { get; }
        public object Details { get; }
    }
}
