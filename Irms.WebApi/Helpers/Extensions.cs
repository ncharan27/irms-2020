﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Irms.WebApi.Helpers
{
    public static class Extensions
    {
        public static Guid? UserId(this ClaimsPrincipal user)
        {
            var claim = user.Claims.FirstOrDefault(t => t.Type == ClaimTypes.NameIdentifier)?.Value;
            return Guid.TryParse(claim, out var result) ? result : default;
        }
    }
}
