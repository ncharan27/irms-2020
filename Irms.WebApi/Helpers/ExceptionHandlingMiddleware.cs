﻿using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using IdentityServer4.Extensions;
using Irms.Application.Abstract.Services;
using Irms.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Irms.WebApi.Helpers
{
    public static class ExceptionHandlingMiddleware
    {
        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public static void CorrectStatusCode(
            this IApplicationBuilder app,
            ILoggerFactory log,
            ILanguageDetector detector,
            IMessageTranslator translator)
        {
            var logger = log.CreateLogger<Startup>();

            CultureInfo GetHttpCulture(HttpContext ctx)
            {
                var cultString = detector.DetectRequestLanguage(ctx);
                return string.IsNullOrEmpty(cultString)
                    ? new CultureInfo("en-US")
                    : new CultureInfo(cultString);
            }

            async Task BadRequest(HttpContext ctx, string message, Exception ex, int? code, object[] parameters = null)
            {
                logger.LogError(ex, message);

                var culture = GetHttpCulture(ctx);
                var msg = translator.Translate(message, culture);
                if (parameters != null)
                {
                    msg = string.Format(msg, parameters);
                }

                ctx.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                var body = JsonConvert.SerializeObject(new BadRequestBody(code, msg), SerializerSettings);
                await ctx.Response.WriteJsonAsync(body);
            }

            app.Use(async (ctx, next) =>
            {
                try
                {
                    await next();
                }
                catch (IncorrectRequestException e)
                {
                    await BadRequest(ctx, e.Message, e, e.Code, e.Data["parameters"] as object[]);
                }
                catch (DbUpdateException ex)
                {
#if DEBUG
                    await BadRequest(ctx, GetMessage(ex), ex, null);
#else
                    await BadRequest(ctx, "Can not delete the record because it is being used", ex, null);
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    await BadRequest(ctx, GetMessage(ex), ex, null);
#else
                    await BadRequest(ctx, "Unable to process the request. Please check your internet connection and try again.", ex, null);
#endif
                }
            });
        }

        private static string GetMessage(Exception ex)
        {
            while (ex.InnerException != null)
                ex = ex.InnerException;
            return ex.Message;
        }
    }
}
