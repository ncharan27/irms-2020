﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Irms.WebApi.Helpers
{
    public class SwaggerSampleModelProvider : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (model?.Properties?.Keys == null)
            {
                return;
            }

            foreach (var key in model.Properties.Keys)
            {
                //We use immutable requests.
                //Model binder can bind the request to an immutable object.
                //When swagger sees an immutable field it excludes the fields from the sample request
                model.Properties[key].ReadOnly = false; //TODO chack is this correct. It was null
            }
        }
    }
}
