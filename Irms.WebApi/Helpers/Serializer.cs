﻿using System;
using Irms.Application.Abstract.Services;
using Newtonsoft.Json;

namespace Irms.WebApi.Helpers
{
    public class Serializer : ISerializer
    {
        public string Serialize(object src) => JsonConvert.SerializeObject(src);

        public T Deserialize<T>(string src) => JsonConvert.DeserializeObject<T>(src);
        public object Deserialize(string src, Type type) => JsonConvert.DeserializeObject(src, type);
    }
}
