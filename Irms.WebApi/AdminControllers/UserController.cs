﻿using Irms.Application.Abstract.Repositories.UserManagement;
using Irms.Application.UsersInfo.Commands;
using Irms.Data.Read.UserInfo.Queries;
using Irms.Data.Read.UserInfo.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.AdminControllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserManager _userManager;
        public UserController(IMediator mediator, IUserManager userManager)
        {
            _mediator = mediator;
            _userManager = userManager;
        }

        [Authorize]
        /// <summary>
        /// Use this method for getting user profile
        /// </summary>
        /// <returns><see cref="MyProfile"/></returns>
        [HttpGet("myProfile")]
        public async Task<MyProfile> Get(CancellationToken token)
        {
            var user = await _userManager.FindByIdAsync(User.UserId() ?? Guid.Empty);
            if (user == null)
            {
                throw new IncorrectRequestException("Something went wrong. User can not be found by the claim");
            }

            var e = await _mediator.Send(new GetMyProfile(user.Id), token);
            return e;
        }

        [AuthorizeRoles(RoleType.SuperAdmin)]
        /// <summary>
        /// Use this method for updating general user info
        /// </summary>
        /// <param name="cmd"><see cref="UpdateUserMainInfoCmd"/></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<bool> UpdateUserMainInfo([FromBody]UpdateUserMainInfoCmd cmd, CancellationToken token)
        {
            var user = await _userManager.FindByIdAsync(User.UserId() ?? Guid.Empty);
            if (user == null)
            {
                throw new IncorrectRequestException("Something went wrong. User can not be found by the claim");
            }

            var result = await _mediator.Send(cmd, token);
            return result;
        }
    }
}
