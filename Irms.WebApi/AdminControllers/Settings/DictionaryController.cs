﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Irms.Data.Read.Dictionary.Queries;
using Irms.Data.Read.Dictionary.ReadModels;
using System.Collections.Generic;
using Irms.WebApi.Helpers;
using Irms.Domain.Entities;
using System;
using System.Linq;

namespace Irms.WebApi.AdminControllers.Settings
{
    [Route("api/[controller]")]
    public class DictionaryController : Controller
    {
        private readonly IMediator _mediator;
        public DictionaryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// get country list by current language
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
       // [AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("country")]
        public async Task<IEnumerable<CountryItem>> GetCountries(CancellationToken token)
        {
            var result = await _mediator.Send(new GetCountries(), token);
            return result;
        }

        /// <summary>
        /// get nationality list
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //[AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("nationality")]
        public async Task<IEnumerable<NationalityItem>> GetNationalities(CancellationToken token)
        {
            var result = await _mediator.Send(new GetNationalities(), token);
            return result;
        }

        /// <summary>
        /// get document type
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //[AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("document-type")]
        public async Task<IEnumerable<DocumentTypeItem>> GetDocumentTypes(CancellationToken token)
        {
            var result = await _mediator.Send(new GetDocumentType(), token);
            return result;
        }

        /// <summary>
        /// get list of avalible products
        /// </summary>
        /// <param name="token"></param>
        /// <returns>list of <see cref="DictionaryItem"/></returns>
        [AuthorizeRoles(RoleType.SuperAdmin)]
        [HttpGet("product")]
        public async Task<IEnumerable<DictionaryItem>> GetProducts(CancellationToken token)
        {
            var result = await _mediator.Send(new GetProducts(), token);
            return result;
        }

        /// <summary>
        /// get list of avalible tenants
        /// </summary>
        /// <param name="token"></param>
        /// <returns>list of <see cref="DictionaryItem"/></returns>
        [AuthorizeRoles(RoleType.SuperAdmin)]
        [HttpGet("tenant")]
        public async Task<IEnumerable<DictionaryItem>> GetTenants(CancellationToken token)
        {
            var result = await _mediator.Send(new GetTenants(), token);
            return result;
        }

        /// <summary>
        /// get list of avalible currencys
        /// </summary>
        /// <param name="token"></param>
        /// <returns>list of <see cref="CurrencyItem"/></returns>
        [AuthorizeRoles(RoleType.SuperAdmin)]
        [HttpGet("currency")]
        public async Task<IEnumerable<CurrencyItem>> GetCurrency(CancellationToken token)
        {
            var result = await _mediator.Send(new GetCurrency(), token);
            return result;
        }

        /// <summary>
        /// get event types
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("event-type")]
        public async Task<IEnumerable<EventTypeItem>> GetEventTypes(CancellationToken token)
        {
            var result = await _mediator.Send(new GetEventTypes(), token);
            return result;
        }

        /// <summary>
        /// get time zones
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("time-zone")]
        public IEnumerable<TimeZoneItem> GetTimeZones(CancellationToken token)
        {
            var timeZones = TimeZoneInfo.GetSystemTimeZones();
            return timeZones.Select(x => new TimeZoneItem(x.BaseUtcOffset.ToString(), x.DisplayName));
        }

        /// <summary>
        /// get campaign crieteria type
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("campaign-criteria-type")]
        public async Task<IEnumerable<CampaignCriteriaTypeListItem>> GetCampaignCriteriaType(CancellationToken token)
       {
            var result = await _mediator.Send(new GetCampaignCriteriaType(), token);
            return result;
        }

        /// <summary>
        /// get event guest lists
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AuthorizeRoles(RoleType.TenantAdmin)]
        [HttpGet("{id}/event-guest-list")]
        public async Task<IEnumerable<DictionaryItem>> GetEventGuestList(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetEventGuestList(id), token);
            return result;
        }
    }
}
