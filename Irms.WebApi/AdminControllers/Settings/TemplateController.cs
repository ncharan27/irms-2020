﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Irms.Application.Abstract.Services.Notifications.Notifiers;
using Irms.Application.Templates.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Templates.Queries;
using Irms.Data.Read.Templates.ReadModels;
using Irms.Domain.Entities;
using Irms.Domain.Entities.Templates;
using Irms.WebApi.Helpers;
using Irms.WebApi.LocalizationTransform.Template;
using Irms.WebApi.LocalizationTransform.Template.Models;

namespace Irms.WebApi.AdminControllers.Settings
{
    [AuthorizeRoles(RoleType.SuperAdmin)]
    [Route("api/[controller]")]
    public class TemplateController : Controller
    {
        private readonly IMediator _mediator;
        private readonly INotifierProvider _notifierProvider;
        private readonly ITemplateTranslator _translator;

        public TemplateController(IMediator mediator, INotifierProvider notifierProvider, ITemplateTranslator translator)
        {
            _mediator = mediator;
            _notifierProvider = notifierProvider;
            _translator = translator;
        }

        //=====[Template Selection]=====

        //Queries
        [HttpPost("selection/list")]
        public async Task<IPageResult<TemplateSelectionListItem>> GetTemplateSelectionList([FromBody]GetTemplateSelectionList query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return result;
        }

        [HttpGet("selection/{id}")]
        public async Task<TemplateSelectionDetails> GetTemplateSelectionDetails(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetTemplateSelectionDetails(id), token);
            return result;
        }

        //Commands
        [HttpPut("selection")]
        public async Task UpdateTemplateSelection([FromBody]UpdateTemplateSelectionCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }


        //======[Templates]===========

        //Queries
        [HttpPost("list")]
        public async Task<IPageResult<TemplateListItem>> GetTemplateList([FromBody]GetTemplateList query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return result;
        }

        [HttpGet("{id}")]
        public async Task<TemplateDetailsRead> GetTemplateDetails(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetTemplateDetails(id), token);
            var translated = await _translator.TranslateReadModel(result);
            return translated;
        }

        //[HttpGet("placeholders/{templateType}")]
        //public IEnumerable<string> GetTemplatePlaceholders(Domain.Entities.Templates.TemplateType templateType, CancellationToken token)
        //{
        //    var result = _notifierProvider.GetNotifier(templateType);
        //    return result.Placeholders;
        //}

        //Commands
        [HttpPost]
        public async Task<Guid> CreateTemplate([FromBody]TemplateDetailsCreate vm, CancellationToken token)
        {
            var cmd = await _translator.TranslateCreateModel(vm);
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPut]
        public async Task UpdateTemplate([FromBody]TemplateDetailsUpdate vm, CancellationToken token)
        {
            var cmd = await _translator.TranslateUpdateModel(vm);
            await _mediator.Send(cmd, token);
        }

        [HttpDelete]
        public async Task DeleteTemplates([FromBody]DeleteTemplatesCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }
    }
}
