﻿using Irms.Application.BackgroundService.Commands;
using Irms.Application.CampaignInvitations.CommandHandlers;
using Irms.Application.CampaignInvitations.Commands;
using Irms.Application.Campaigns.Commands;
using Irms.Application.Campaigns.Queries;
using Irms.Application.Campaigns.ReadModels;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Campaign.QueryHandlers;
using Irms.Data.Read.Campaign.ReadModels;
using Irms.Data.Read.CampaignInvitation.Queries;
using Irms.Data.Read.CampaignInvitation.ReadModels;
using Irms.Domain.Entities;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class CampaignInvitationController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CampaignInvitationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Loads all available sms sender names. This is needed for combobox on frontend part. 
        /// Currently, it would return a collection with
        /// </summary>
        /// <param name="token">cancellation token</param>
        /// <returns>available names for sms senders' list</returns>
        [HttpGet("load-sms-senders")]
        public async Task<IReadOnlyCollection<string>> LoadAvailableSmsTemplateNames(CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignSmsTemplateSenders(), token);
            return result;
        }

        [HttpPost("placeholder")]
        public async Task<IEnumerable<CampaignPlaceholderListItem>> GetCampaignPlaceholders([FromBody]GetCampaignPlaceholderList query, CancellationToken token)
        {
            var result = await _mediator.Send(query, token);
            return result;
        }

        [HttpGet("{id}")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignInvitation> Get(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitation(id, InvitationType.Rsvp), token);
            return result;
        }

        /// <summary>
        /// create invitation
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> CreateOrUpdate([FromBody]CreateOrUpdateCampaignInvitationCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpGet("{id}/email")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignEmailTemplate> GetCampaignEmailTemplate(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignEmailTemplate(id), token);
            return result;
        }

        [HttpPost("email-template")]
        public async Task<Guid> CreateOrUpdateCampaignEmailTemplate([FromForm]CreateOrUpdateCampaignEmailTemplateCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }


        /// <summary>
        /// updates email response form
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut("email-response-form")]
        public async Task<Unit> UpdateCampaignEmailResponseForm([FromForm]UpdateCampaignEmailResponseFormCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPost("test-email")]
        public async Task<bool> SendTestEmail([FromBody]SendTestEmailCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// load sms template by its own id
        /// </summary>
        /// <param name="id">sms template id</param>
        /// <param name="token">cancellation token</param>
        /// <returns>sms template</returns>
        [HttpGet("{id}/sms")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignSmsTemplate> GetCampaignSmsTemplate(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignSmsTemplate(id), token);
            return result;
        }

        /// <summary>
        /// create new or update existing sms template
        /// </summary>
        /// <param name="cmd">new(updated) template</param>
        /// <param name="token">cancellation token</param>
        /// <returns>sms template id</returns>
        [HttpPost("sms-template")]
        public async Task<Guid> CreateOrUpdateCampaignSmsTemplate([FromForm]CreateOrUpdateCampaignSmsTemplateCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// sends test sms
        /// </summary>
        /// <param name="cmd">send test sms command</param>
        /// <param name="token">cancellation token</param>
        /// <returns>numbers after sending sms</returns>
        [HttpPost("send-test-sms")]
        public async Task<IEnumerable<string>> SendTestSms([FromBody]SendTestSmsCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// Endpoint to send whatsapp test message from editor view
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("send-test-whatsapp")]
        public async Task<IEnumerable<string>> SendTestWhatsapp([FromBody]SendTestWhatsappCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// Saves template page for whatsapp
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("whatsapp-template")]
        public async Task<Guid> CreateOrUpdateCampaignWhatsappTemplate([FromBody]CreateOrUpdateWhatsappTemplateCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// save bot template for whatsapp
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("whatsapp-bot-template")]
        public async Task<Guid> CreateOrUpdateWhatsappBOTTemplate([FromBody]CreateOrUpdateWhatsappBOTTemplateCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Saves response for whatsapp form
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("whatsapp-response-form")]
        public async Task CreateOrUpdateWhatsappResponseForm([FromForm]UpdateCampaignWhatsappResponseFormCmd cmd, CancellationToken token)
        {
            _ = await _mediator.Send(cmd, token);
        }


        [HttpPost("copy-invitation-template")]
        public async Task<Guid> CopyInvitationTemplate([FromBody]CopyInvitationTemplateCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }


        /// <summary>
        /// Fetches whatsapp entity from database by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/whatsapp")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignWhatsappTemplate> LoadWhatsappTemplate([FromRoute] Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignWhatsappTemplateQuery(id), token);
            return result;
        }

        /// <summary>
        /// fetch whatsapp bot templates
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id}/whatsapp/bot")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignWhatsappBOTTemplate> LoadWhatsappBOTTemplate([FromRoute] Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignWhatsappBOTTemplateQuery(id), token);
            return result;
        }


        //invitation pending...

        /// <summary>
        /// create/update invitation pending...
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/pending")]
        public async Task<IEnumerable<CampaignInvitationListItem>> GetCampaignInvitationPendingList(Guid campaignId, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitationList(campaignId, InvitationType.Pending), token);
            return result;
        }

        /// <summary>
        /// get campaign pending detail
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("pending/{id}")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignInvitation> GetCampaignInvitationPending(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitation(id, InvitationType.Pending), token);
            return result;
        }

        /// <summary>
        /// create/update invitation pending...
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("pending")]
        public async Task<Guid> UpsertCampaignInvitationPending([FromBody]UpsertCampaignInvitationPendingCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        //campaign invitation accepted

        /// <summary>
        /// get invitation accepted list
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/accepted")]
        public async Task<IEnumerable<CampaignInvitationListItem>> GetCampaignInvitationAcceptedList(Guid campaignId, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitationList(campaignId, InvitationType.Accepted), token);
            return result;
        }

        /// <summary>
        /// get campaign invitation accepted detail
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("accepted/{id}")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignInvitation> GetCampaignInvitationAccepted(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitation(id, InvitationType.Accepted), token);
            return result;
        }

        /// <summary>
        /// create/update invitation accepted...
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("accepted")]
        public async Task<Guid> UpsertCampaignInvitationAccepted([FromBody]UpsertCampaignInvitationAcceptedCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }


        //campaign invitation rejected

        /// <summary>
        /// get campaign invitation rejected list
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{campaignId}/rejected")]
        public async Task<IEnumerable<CampaignInvitationListItem>> GetCampaignInvitationRejectedList(Guid campaignId, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitationList(campaignId, InvitationType.Rejected), token);
            return result;
        }

        /// <summary>
        /// get campaign invitation rejected detail
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("rejected/{id}")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignInvitation> GetCampaignInvitationRejected(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitation(id, InvitationType.Rejected), token);
            return result;
        }

        [HttpGet("list-analysis/{id}")]
        public async Task<Data.Read.CampaignInvitation.ReadModels.CampaignInvitation> GetCampaignInvitationListAnalysis(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitation(id, InvitationType.ListAnalysis), token);
            return result;
        }

        /// <summary>
        /// create/update invitation rejected...
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("rejected")]
        public async Task<Guid> UpsertCampaignInvitationRejected([FromBody]UpsertCampaignInvitationRejectedCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// duplicate invitation
        /// </summary>
        /// <param name=""></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("duplicate")]
        public async Task<Guid> DuplicateInvitation([FromBody]DuplicateInvitationCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// delete invitation
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task<Guid> DeleteInvitation([FromBody]DeleteInvitationCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        /// <summary>
        /// delete invitation
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("delete-whatsapp-template")]
        public async Task<Unit> DeleteWhatsappTemplate([FromBody]DeleteWhatsappTemplateCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// get basic invitation info
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("invitation-info/{id}")]
        public async Task<CampaignInvitationBasicInfo> GetBasicInvitationInfo(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitationBasicInfo(id), token);
            return result;
        }

        /// <summary>
        /// Creates responses for invitation
        /// WARNING: ONLY FOR TESTING PURPOSES
        /// </summary>
        /// <param name="invitationId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("responses-for-invitation/{id}")]
        public async Task CreateResponsesForInvitation(Guid id, CancellationToken token)
        {
            await _mediator.Send(new CreateResponsesForInvitationCmd(id), token);
        }

        [HttpGet("campaign-summary/{id}")]
        public async Task<CampaignSummary> GetCampaignSummary(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignSummaryQuery(id), token);
            return result;
        }

        /// <summary>
        /// Fetch all whatsapp templates endpoint
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        //[HttpGet("whatsapp-templates")]
        //public async Task<IEnumerable<Data.Read.CampaignInvitation.ReadModels.WhatsappApprovedTemplate>> GetWhatsappApprovedTemplates(CancellationToken token)
        //{
        //    return await _mediator.Send(new GetWhatsappApprovedTemplateQuery(), token);
        //}

        /// <summary>
        /// Fetch whatsapp template by invitation id
        /// </summary>
        /// <param name="id">invitation id</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("whatsapp-templates/{id}")]
        public async Task<IEnumerable<Data.Read.CampaignInvitation.ReadModels.WhatsappApprovedTemplate>> GetWhatsappApprovedTemplates([FromRoute]Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetWhatsappApprovedTemplateQuery(id), token);
        }
    }
}
