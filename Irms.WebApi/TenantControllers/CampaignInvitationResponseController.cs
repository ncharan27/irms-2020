﻿using Irms.Application.CampaignInvitationResponses.Commands;
using Irms.Data.Read.CampaignInvitationResponses.Queries;
using Irms.Data.Read.CampaignInvitationResponses.QueryHandlers;
using Irms.Data.Read.CampaignInvitationResponses.ReadModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [Route("api/[controller]")]
    public class CampaignInvitationResponseController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CampaignInvitationResponseController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("response/{id}")]
        public async Task<InvitationResponseReadModel> Load(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitationResponseQuery(id), token);
            return result;
        }

        [HttpPost("save-response")]
        public async Task<Guid> SaveResults([FromBody]SaveResponseCmd cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpGet("rfi/{id}")]
        public async Task<InvitationResponseRfiModel> LoadForRfi(Guid id, CancellationToken token)
        {
            var result = await _mediator.Send(new GetCampaignInvitationRfiQuery(id), token);
            return result;
        }

        [HttpPost("save-rfi")]
        public async Task<Guid> SaveRfi([FromBody]SaveRfiCmd cmd,  CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }
    }
}
