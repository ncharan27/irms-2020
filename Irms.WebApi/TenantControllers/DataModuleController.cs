﻿using IdentityServer4.Models;
using Irms.Application.Abstract.Services;
using Irms.Application.DataModule.Commands;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.DataModule.Query;
using Irms.Data.Read.DataModule.ReadModels;
using Irms.Domain.Entities;
using Irms.Infrastructure.Services.ExcelExport;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class DataModuleController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IExcelGenerator _excelGenerator;

        public DataModuleController(IMediator mediator,
            IExcelGenerator excelGenerator)
        {
            _mediator = mediator;
            _excelGenerator = excelGenerator;
        }

        [HttpGet("campaigns-list/{id}")]
        public async Task<IPageResult<CampaignDataReadModel>> GetCampaignList([FromRoute] Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetCampaignDataQuery(id), token);
        }

        [HttpPost("rfiform-info")]
        public async Task<IPageResult<RfiFormInfoReadModel>> GetRfiFormInfo([FromBody]GetRfiFormInfoQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        [HttpPost("list-analysis-rfiform-info")]
        public async Task<IPageResult<ListAnalysisRfiFormInfoReadModel>> GetListAnalysisRfiFormInfo([FromBody]GetListAnalysisRfiFormInfoQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        [HttpPost("rfi-name")]
        public async Task<RfiFormNameReadModel> GetRfiFormName([FromBody]GetRfiFormNameQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Getting rfi form summary endpoint
        /// </summary>
        /// <param name="id">RFI form id</param>
        /// <param name="token">Cancellation token</param>
        /// <returns></returns>
        [HttpGet("rfiform-summary/{id}")]
        public async Task<RfiFormSummaryReadModel> GetRfiFormSummary([FromRoute]Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetRfiFormSummaryQuery(id), token);
        }

        /// <summary>
        /// Get rfi form additional info endpoint
        /// </summary>
        /// <param name="cmd">Page query for additional info for rfi form</param>
        /// <param name="token">Cancellation token</param>
        /// <returns></returns>
        [HttpPost("rfiform-additional")]
        public async Task<IPageResult<RfiFormSummaryAdditionalInfo>> GetRfiFormAdditionalInfo([FromBody]GetRfiFormSummaryAdditionalInfoQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Get rfi form responses page endpoint
        /// </summary>
        /// <param name="cmd">Rfi form information</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("rfiform-responses")]
        public async Task<IPageResult<RfiFormResponseReadModel>> GetRfiFormResponse([FromBody]GetRfiFormResponseQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Get rfi form questions for export modal endpoint
        /// </summary>
        /// <param name="id">Id of RFI form</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("rfiform-questions/{id}")]
        public async Task<IEnumerable<RfiFormQuestionsModel>> GetRfiFormQuestions([FromRoute]Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetRfiFormQuestionQuery(id), token);
        }

        /// <summary>
        /// Exports rfi form by form id, returns excel file endpoint
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("rfiform-export")]
        public async Task<FileContentResult> ExportRfiFormQuestions([FromBody]GetRfiFormExportDataQuery cmd, CancellationToken token)
        {
            var questions = await _mediator.Send(new GetRfiFormQuestionQuery(cmd.FormId), token);
            var answers = await _mediator.Send(cmd, token);

            using (var memoryStream = new MemoryStream())
            {
                _excelGenerator.GenerateRfiExportReport(questions.ToList(), answers.ToList(), memoryStream, "RfiResponse", cmd.HasGuestNameSelected, cmd.HasSubmissionDateSelected);
                var bytes = memoryStream.ToArray();
                return File(bytes, "application/excel", "RfiReport.xlsx");
            }
        }

        /// <summary>
        /// Load only one guest rfi responses from database endpoint
        /// </summary>
        /// <param name="cmd">Contains information about form and contact</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("rfiform-guest-response")]
        public async Task<GuestResponseReadModel> GetGuestResponse([FromBody] GetRfiFormResponseDetailPageQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Remove empty contact list endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("remove-contact-list/{id}")]
        public async Task<Unit> RemoveContactList([FromRoute]Guid id, CancellationToken token)
        {
            return await _mediator.Send(new DeleteContactListCmd(id), token);
        }

        /// <summary>
        /// Load total global guests count
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("global-guests-count")]
        public async Task<int> GetGlobalGuestsCount(CancellationToken token)
        {
            return await _mediator.Send(new GetGlobalGuestsCountQuery(), token);
        }

        /// <summary>
        /// Load guest lists with pagination and search
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("load-global-guests")]
        public async Task<IPageResult<GlobalGuestListReadModel>> GetGlobalGuests([FromBody] GetGlobalGuestListQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Load global contacts in guest list
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("global-guest-list")]
        public async Task<IPageResult<GlobalGuestContactReadModel>> GetGlobalGuestList([FromBody] GetGlobalGuestContactQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Loads event admission information by user for all participated events
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("guest-event-admission")]
        public async Task<IPageResult<GuestEventAdmissionReadModel>> GetGuestEventAdmission([FromBody] GetGuestEventAdmissionQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        [HttpGet("selected-important-fields/{id}")]
        public async Task<IEnumerable<SelectedImportantField>> GetSelectedImportantField(Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetSelectedImportantFieldsQuery(id), token);
        }

        [HttpPost("selected-important-fields")]
        public async Task SaveImportantFields([FromBody] UpdateContactListImportantFieldsCmd cmd, CancellationToken token)
        {
            await _mediator.Send(cmd, token);
        }
    }
}