﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Irms.Application.Abstract.Services;
using Irms.Application.Contact.Commands;
using Irms.Application.UsersInfo.Queries;
using Irms.Data.Read.Abstract;
using Irms.Data.Read.Campaign.Queries;
using Irms.Data.Read.Contact.Queries;
using Irms.Data.Read.Contact.ReadModels;
using Irms.Domain;
using Irms.Domain.Entities;
using Irms.Infrastructure.Services.ExcelExport;
using Irms.WebApi.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static System.Net.WebRequestMethods;

namespace Irms.WebApi.TenantControllers
{
    [AuthorizeRoles(RoleType.TenantAdmin)]
    [Route("api/[controller]")]
    public class ContactController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IPhoneNumberValidator _phoneValidator;
        private readonly IExcelGenerator _excelGenerator;

        public ContactController(IMediator mediator, IPhoneNumberValidator phoneValidator, IExcelGenerator excelGenerator)
        {
            _mediator = mediator;
            _phoneValidator = phoneValidator;
            _excelGenerator = excelGenerator;
        }

        /// <summary>
        /// get list of contact lists
        /// </summary>
        /// <param name="query">Send only PageNo and PageSize and SearchText</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("contact-lists")]
        public async Task<IPageResult<ContactListItemFiltered>> GetContactListsFiltered([FromBody]GetContactListFiltered query, CancellationToken token)
        {
            query.IsGlobal = null;
            query.IsGuest = false;
            var result = await _mediator.Send(query, token);
            return result;
        }

        /// <summary>
        /// get list of global guest lists
        /// </summary>
        /// <param name="query">Send only PageNo and PageSize and SearchText</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("global-guest-lists")]
        public async Task<IPageResult<ContactListItemFiltered>> GetGlobalGuestListsFiltered([FromBody]GetContactListFiltered query, CancellationToken token)
        {
            query.IsGlobal = true;
            query.IsGuest = true;
            var result = await _mediator.Send(query, token);
            return result;
        }

        /// <summary>
        /// get list of contact lists
        /// </summary>
        /// <param name="query">Send only EventId, PageNo and PageSize and SearchText</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("event-guest-lists")]
        public async Task<IPageResult<ContactListItemFiltered>> GetEventGuestListsFiltered([FromBody]GetContactListFiltered query, CancellationToken token)
        {
            query.IsGlobal = false;
            query.IsGuest = true;
            var result = await _mediator.Send(query, token);
            return result;
        }


        /// <summary>
        /// get guest contacts by list id with pagination and search 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("local-guest-contact-by-list-id")]
        public async Task<IPageResult<GuestContactItemFiltered>> GetContactByListIdFiltered([FromBody]GetGuestContactByListFiltered query, CancellationToken token)
        {
            query.IsGuest = true;
            var result = await _mediator.Send(query, token);
            return result;
        }

        /// <summary>
        /// create global contact list
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("contact-list")]
        public async Task<Guid?> CreateContactList([FromBody]CreateGlobalContactListCmd cmd, CancellationToken token)
        {
            var id = await _mediator.Send(cmd, token);
            return id;
        }

        /// <summary>
        /// insert guests in guest list
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("add-guests-in-guest-list")]
        public async Task<Guid> AddGuestsInGuestList([FromBody]AddGuestsInGuestListCmd cmd, CancellationToken token)
        {
            var id = await _mediator.Send(cmd, token);
            return id;
        }

        /// <summary>
        /// create local(for event only) guest list with eventId and name, name and eventId are required
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("create-event-guest-list")]
        public async Task<Guid?> CreateEventGuestList([FromBody] CreateGuestListCmd model, CancellationToken token)
        {
            model.IsGuest = true;
            model.IsGlobal = false;
            return await _mediator.Send(model, token);
        }

        /// <summary>
        /// create guest contact
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("create-guest-contact")]
        public async Task<Guid?> CreateGuestContact([FromBody]CreateContactCmd cmd, CancellationToken token)
        {
            cmd.IsGuest = true;
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// create contact
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("create-contact")]
        public async Task<Guid?> CreateContact([FromBody]CreateContactCmd cmd, CancellationToken token)
        {
            cmd.IsGuest = false;
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// get contact list
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("contact-list")]
        public async Task<IReadOnlyCollection<ContactListItem>> GetContactList(CancellationToken token)
        {
            var result = await _mediator.Send(new GetContactList(false, null), token);
            return result;
        }


        /// <summary>
        /// get contacts count
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("contacts-count")]
        public async Task<long> GetContactsCount(CancellationToken token)
        {
            var result = await _mediator.Send(new GetContactsCount(false), token);
            return result;
        }

        /// <summary>
        /// get guests count
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("guests-count")]
        public async Task<long> GetGuestsCount(CancellationToken token)
        {
            var result = await _mediator.Send(new GetContactsCount(true), token);
            return result;
        }

        /// <summary>
        /// get local contact list
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("global-guest-list")]
        public async Task<IReadOnlyCollection<ContactListItem>> GetGlobalGuestList(CancellationToken token)
        {
            var result = await _mediator.Send(new GetContactList(true, true), token);
            return result;
        }

        /// <summary>
        /// check if email is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("CheckEmailUniqueness")]
        public async Task<bool> CheckEmailUniqueness([FromBody]IsEmailUnique model, CancellationToken token)
        {
            return await _mediator.Send(model, token);
        }

        /// <summary>
        /// check if phone is unique
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("CheckPhoneUniqueness")]
        public async Task<bool> CheckPhoneUniqueness([FromBody]IsPhoneUnique model, CancellationToken token)
        {
            _phoneValidator.Validate(model.MobileNumber, out var isValid);
            if (!isValid)
            {
                throw new IncorrectRequestException(400, "Phone number is not correct");
            }

            return await _mediator.Send(model, token);
        }

        [HttpPost("list-name-uniqueness")]
        public async Task<bool> CheckListNameUniqueness([FromBody]IsListNameUnique model, CancellationToken token)
        {
            return await _mediator.Send(model, token);
        }
        //--------------------------------------------------------------------//

        /// <summary>
        /// get contacts by listIds
        /// </summary>
        /// <param name="contactListId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("contacts-by-list-id")]
        public async Task<IReadOnlyCollection<ContactItemAndListId>> GetContactsByContactListId([FromBody] List<Guid> contactListId, CancellationToken token)
        {
            bool loadUnassigned = false;
            if (contactListId.Contains(Guid.Empty))
            {
                contactListId.Remove(Guid.Empty);
                loadUnassigned = true;
            }

            IReadOnlyCollection<ContactItemAndListId> result = await _mediator.Send(new GetContactsByListId(contactListId, loadUnassigned), token);
            return result;
        }

        /// <summary>
        /// get contact list by EventId
        /// </summary> 
        /// <param name="eventId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("contactlist-event")]
        public async Task<IReadOnlyCollection<ContactListItem>> GetGlobalContactListByEvent(Guid eventId, CancellationToken token)
        {
            var result = await _mediator.Send(new GetContactListByEventId(eventId), token);
            return result;
        }

        /// <summary>
        /// rename list
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("rename-list")]
        public async Task<Guid> RenameContactList([FromBody]RenameGuestListCmd request, CancellationToken token)
        {
            var result = await _mediator.Send(request, token);
            return result;
        }

        /// <summary>
        /// remove users from contact list
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("remove-users")]
        public async Task RemoveUsers([FromBody]RemoveContactsFromContactListCmd request, CancellationToken token)
        {
            await _mediator.Send(request, token);
        }

        /// <summary>
        /// remove contact list by id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("delete-contact-list")]
        public async Task<Guid?> DeleteContactList([FromBody]DeleteContactListCmd model, CancellationToken token)
        {
            var result = await _mediator.Send(model, token);
            return result;
        }

        /// <summary>
        /// delete contacts by ids
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete("delete-contacts")]
        public async Task<Unit> DeleteContact([FromBody]DeleteContactCmd model, CancellationToken token)
        {
            var result = await _mediator.Send(model, token);
            return result;
        }

        /// <summary>
        /// loads list name by id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("list-name")]
        public async Task<ContactListName> GetListName([FromBody]GetContactListName model, CancellationToken token)
        {
            var result = await _mediator.Send(model, token);
            return result;
        }

        #region global contact
        /// <summary>
        /// load contact list by list id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("contact-list-by-list-id")]
        public async Task<IPageResult<ContactListItemByListId>> GetContactListItemByListId([FromBody]GetContactListByListId model, CancellationToken token)
        {
            var result = await _mediator.Send(model, token);
            return result;
        }

        [HttpPost("all-contacts")]
        public async Task<IPageResult<AllContactItem>> GetAllContacts([FromBody]GetAllContacts model, CancellationToken token)
        {
            var result = await _mediator.Send(model, token);
            return result;
        }

        /// <summary>
        /// delete contact from list.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("remove-contact-from-list")]
        public async Task<Unit> RemoveContactFromList([FromBody]RemoveContactsFromContactListCmd model, CancellationToken token)
        {
            return await _mediator.Send(model, token);
        }
        #endregion

        /// <summary>
        /// export users to excel sheet
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("export-users")]
        public async Task<FileContentResult> ExportContacts([FromBody]GetContactsForExport model, CancellationToken token)
        {
            var result = await _mediator.Send(model, token);
            using (var memoryStream = new MemoryStream())
            {
                _excelGenerator.GenerateContactsReport(result, memoryStream, "Sheet", model.Columns.ToHashSet());
                var bytes = memoryStream.ToArray();
                return File(bytes, "application/excel", $"{model.Filename}.xlsx");
            }
        }

        [HttpPost("emails-unique")]
        public async Task<EmailsUniqueListReadModel> AreEmailsUnique([FromBody] GetEmailsUniqueCheck cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPost("phonenumbers-unique")]
        public async Task<PhoneNumbersUniqueListReadModel> ArePhoneNumbersUnique([FromBody] GetPhoneNumberUniqueCheck cmd, CancellationToken token)
        {
            var result = await _mediator.Send(cmd, token);
            return result;
        }

        [HttpPost("import-csv")]
        public async Task<int> ImportUsersFromCsv([FromBody]JObject jObject)
        {
            var data = jObject.ToObject<Dictionary<string, object>>();
            var listInfo = (data["list"] as JObject).ToObject<Dictionary<string, string>>();
            var contacts = (data["contacts"] as JArray).ToObject<List<Dictionary<string, string>>>();
            var result = await _mediator.Send(new ImportUsersFromCsvCmd(listInfo, contacts), CancellationToken.None);
            return await Task.FromResult(1);
        }

        [HttpGet("template-csv-file")]
        public async Task<FileContentResult> LoadTemplateCsvFile(CancellationToken token)
        {
            var data = await System.IO.File.ReadAllBytesAsync("Templates/user_import_template.xlsx", token);
            return File(data, "application/vnd.ms-excel", "default.xlsx");
        }

        /// <summary>
        /// Fetch unlocked lists endpoint
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("unlocked-lists/{id}")]
        public async Task<IEnumerable<EventGuestReadModel>> LoadUnlockedLists([FromRoute]Guid id, CancellationToken token)
        {
            return await _mediator.Send(new GetEventGuestListsQuery(id), token);
        }

        /// <summary>
        /// Append contacts that were not already on the list endpoint
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("append-contacts-to-list")]
        public async Task AppendContactsToList([FromBody]AppendContactsToListCmd cmd, CancellationToken token)
        {
            _ = await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// Get contacts with custom fields endpoint
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("contacts-custom-fields")]
        public async Task<IPageResult<ContactWithCustomFields>> FetchContactsWithCustomFields([FromBody]GetContactsWithCustomFieldsQuery cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// append contact from one list to another
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("append-list-to-list")]
        public async Task AppendListToList([FromBody]AppendListToListCmd cmd, CancellationToken token)
        {
            _ = await _mediator.Send(cmd, token);
        }

        /// <summary>
        /// update contacts bulk
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Unit> UpdateContacts([FromBody]UpdateContactCmd cmd, CancellationToken token)
        {
            return await _mediator.Send(cmd, token);
        }
    }
}