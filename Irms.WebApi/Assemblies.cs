﻿using System.Reflection;

namespace Irms.WebApi
{
    public class Assemblies
    {
        public static Assembly[] Get()
        {
            return new[]
            {
                typeof(IMarker).Assembly,
                typeof(Application.IMarker).Assembly,
                typeof(Domain.IMarker).Assembly,
                typeof(Data.IMarker).Assembly,
                typeof(Data.Read.IMarker).Assembly,
                typeof(Infrastructure.IMarker).Assembly,
                typeof(Irms.Background.IMarker).Assembly,
            };
        }
    }
}
